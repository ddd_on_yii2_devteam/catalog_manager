<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/address/23 expect 204 No content');
$I->sendDELETE('billing/address/23');
$I->canSeeResponseCodeIs(204);