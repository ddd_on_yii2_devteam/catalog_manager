<?php 
$I = new ApiTester($scenario);
$I->wantTo('test PUT billing/client/2 expect updated entity');
$I->sendPUT('billing/client/2', ['name' => 'codeception updated name', 'isActive' => false]);
$I->seeResponseEquals('{"id":2,"name":"codeception updated name","isActive":"0","elementClasses":null,"elementTypes":null,"properties":null,"parents":null,"children":null,"root":null,"relationClasses":null,"relationGroups":null,"relations":null,"entity":"commonprj\\\\components\\\\core\\\\entities\\\\billing\\\\client\\\\Client"}');