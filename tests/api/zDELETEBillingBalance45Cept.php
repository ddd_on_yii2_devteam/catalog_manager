<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/balance/45 expect 204 No content');
$I->sendDELETE('billing/balance/45');
$I->canSeeResponseCodeIs(204);