<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/service/91 expect 204 No content');
$I->sendDELETE('billing/service/91');
$I->canSeeResponseCodeIs(204);