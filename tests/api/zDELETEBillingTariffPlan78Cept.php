<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/tariff-plan/78 expect 204 No content');
$I->sendDELETE('billing/tariff-plan/78');
$I->canSeeResponseCodeIs(204);