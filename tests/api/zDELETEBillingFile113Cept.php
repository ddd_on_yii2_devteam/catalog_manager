<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/file/113 expect 204 No content');
$I->sendDELETE('billing/file/113');
$I->canSeeResponseCodeIs(204);