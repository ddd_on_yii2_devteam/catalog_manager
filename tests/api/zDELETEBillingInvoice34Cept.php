<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/invoice/34 expect 204 No content');
$I->sendDELETE('billing/invoice/34');
$I->canSeeResponseCodeIs(204);