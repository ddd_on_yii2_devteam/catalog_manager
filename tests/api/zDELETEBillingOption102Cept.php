<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/option/102 expect 204 No content');
$I->sendDELETE('billing/option/102');
$I->canSeeResponseCodeIs(204);