<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE common/property/4 expect 204 No content');
$I->sendDELETE('common/property/4');
$I->seeResponseEquals('');