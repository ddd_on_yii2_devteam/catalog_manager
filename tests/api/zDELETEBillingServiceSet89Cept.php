<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/service-set/89 expect 204 No content');
$I->sendDELETE('billing/service-set/89');
$I->canSeeResponseCodeIs(204);