<?php 
$I = new ApiTester($scenario);
$I->wantTo('test GET common/element?name=client7 expect 1 element filtered by name');
$I->sendGET('common/element?name=client7');
$I->seeResponseEquals('{"7":{"id":7,"name":"client7","isActive":true,"elementClasses":null,"elementTypes":null,"properties":null,"parents":null,"children":null,"root":null,"relationClasses":null,"relationGroups":null,"relations":null,"entity":"commonprj\\\\components\\\\core\\\\entities\\\\billing\\\\client\\\\Client"}}');
