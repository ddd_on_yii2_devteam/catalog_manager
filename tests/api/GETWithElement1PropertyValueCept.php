<?php 
$I = new ApiTester($scenario);
$I->wantTo('test GET common/element/1?with=properties.propertyValue expect properties with all property values');
$I->sendGET('common/element/1?with=properties.propertyValue');
$I->seeResponseEquals('{"id":1,"name":"client1","isActive":true,"elementClasses":null,"elementTypes":null,"properties":{"1":{"id":1,"propertyTypeId":10,"name":"Агрегатное состояние (при нормальный условиях)","sysname":null,"isSpecific":false,"propertyUnitId":null,"description":null,"propertyValues":null,"elements":null,"elementClasses":null,"elementTypes":null,"propertyUnit":null,"propertyType":null,"entity":"commonprj\\\\components\\\\core\\\\entities\\\\common\\\\property\\\\Property"}},"parents":null,"children":null,"root":null,"relationClasses":null,"relationGroups":null,"relations":null,"entity":"commonprj\\\\components\\\\core\\\\entities\\\\billing\\\\client\\\\Client"}');
