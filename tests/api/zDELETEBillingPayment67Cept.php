<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/payment/67 expect 204 No content');
$I->sendDELETE('billing/payment/67');
$I->canSeeResponseCodeIs(204);