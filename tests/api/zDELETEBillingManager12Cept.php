<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/manager/12 expect 204 No content');
$I->sendDELETE('billing/manager/12');
$I->canSeeResponseCodeIs(204);