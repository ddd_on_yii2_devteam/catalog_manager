<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/client/1 expect 204 No content');
$I->sendDELETE('billing/client/1');
$I->canSeeResponseCodeIs(204);