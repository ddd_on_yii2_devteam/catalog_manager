<?php 
$I = new ApiTester($scenario);
$I->wantTo('test DELETE billing/contract/56 expect 204 No content');
$I->sendDELETE('billing/contract/56');
$I->canSeeResponseCodeIs(204);