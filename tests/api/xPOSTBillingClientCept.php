<?php 
$I = new ApiTester($scenario);
$I->wantTo('test POST billing/client expect created entity');
$I->sendPOST('billing/client', ['name' => 'codeception created name', 'isActive' => true]);
$I->seeResponseEquals('{"id":141,"name":"codeception created name","isActive":"1","elementClasses":null,"elementTypes":null,"properties":null,"parents":null,"children":null,"root":null,"relationClasses":null,"relationGroups":null,"relations":null,"entity":null}');