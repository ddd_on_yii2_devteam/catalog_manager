<?php

namespace console\controllers;

use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\ElementClassRecord;
use commonprj\components\core\models\PropertyRecord;
use console\models\Material\ColorationImportMapping;
use console\models\material\MaterialCatalog;
use console\models\material\MaterialCategory;
use console\models\Material\MaterialCategoryImportMapping;
use console\models\material\MaterialCollectionImportMapping;
use console\models\material\MaterialDepiction;
use console\models\Product\ProductImportMapping;
use Yii;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;

class ProductController extends BaseImportController
{

    private $properties = [];

    const MATERIALCOLLECTION = 'materialCollection';
    const PRICECATEGORY = 'priceCategory';
    const PRODUCTMATERIAL = 'productMaterial';
    const PRODUCTMODEL = 'productModel';

    public $limit = 1000;

    public $continue;
    private $domainModels = [
        self::MATERIALCOLLECTION => [
            'context' => 'catalog',
            'name'    => 'MaterialCollection',
        ],
        self::PRICECATEGORY      => [
            'context' => 'catalog',
            'name'    => 'PriceCategory',
        ],
        self::PRODUCTMATERIAL    => [
            'context' => 'catalog',
            'name'    => 'ProductMaterial',
        ],
        self::PRODUCTMODEL       => [
            'context' => 'catalog',
            'name'    => 'ProductModel',
        ],
    ];
    private $baseProperties = [
        self::MATERIALCOLLECTION =>
            [
                'name',
                'MaterialCollection2Manufacturer',
            ],
        self::PRICECATEGORY      => [
            'name',
            'PriceCategory2Manufacturer',
        ],
        self::PRODUCTMATERIAL    => [
            'name',
            'vendorCode',
            'image',
            'texture',
            'colors',
            'pattern',
            'colorResistance',
            'wearResistance',
            'fireResistance',
            'fabricDensity',
            'fabricStructure',
            'pvcFilmFinishing',
            'pvcFilmType',
            'pvcFilmThickness',
            'glassBrand',
            'glassProperty',
            'glassDecoration',
            'glassKind',
            'facadeDesign',
            'additionalFinishingOptions',
            'hidingPower',
            'glossDegree',
            'ProductMaterial2Manufacturer',
            'ProductMaterial2PriceCategory',
            'ProductMaterial2MaterialCollection',
            'ProductMaterial2Material',
        ],
        self::PRODUCTMODEL       => [
            'name',
            'vendorCode',
            'vendorCodeMpn', // артикул производителя
            'country',
            'description',
            'image',
            'price',
            'furnitureType',
            //'razmer',
            'nalichieOtdeleniyaDlyaObuvi',
            'nalichieVeshalki',
            'nalichieZerkala',
            'sRamkoyBagetom',
            'spalnoeMestoVtorogoYarusaShirinaMm',
            'spalnoeMestoVtorogoYarusaDlinaMm',
            'mehanizmVrashcheniya',
            'skladnoyStul',
            'barnyyStul',
            'podlokotnikiStula',
            'glubinaMm',
            'dlinaVRazlozhennomSostoyaniiMm',
            'nalichiePodgolovnika',
            'nalichiePodemnogoMehanizma',
            'nalichieOrtopedicheskogoOsnovaniya',
            'nalichieVydvizhnogoYashchika',
            'nalichieTumby',
            'myagkayaSpinka',
            'nalichieKoles',
            'nalichieYashchikaDlyaBelya',
            'vysotaSidenyaMm',
            'spalnoeMestoShirinaMm',
            'spalnoeMestoDlinaMm',
            'vysotaMm',
            'shirinaMm',
            'dlinaMm',
            'materialVazy',
            'konstruktsiyaKnizhnogoShkafa',
            'nalichieVKabinete',
            'vidPodstavki',
            'nalichieVDetskoy',
            'konfiguratsiya',
            'vidOsveshcheniya',
            'mehanizmReklayner',
            'sposobUstanovki',
            'kolichestvoDverey',
            'krovatOptsii',
            'matrasOptsii',
            'uglovoeRaspolozhenie',
            'poverhnost',
            'functionalArea',
            'materialIzgotovleniyaKarkasa',
            'teksturaDrevesiny',
            'podlokotnikMaterial',
            'podlokotnikNalichie',
            'napolnitelMatrasaKrovati',
            'osnovaFasadovKorpusnoyMebeli',
            'tsvet',
            'stil',
            'napolnitelDlyaMyagkoyMebeli',
            'konstruktsiyaMatrasa',
            'kolichestvoNozhekStola',
            'formaStola',
            'raskladnoyMehanizmStola',
            'tipMyagkoyMebeli',
            'raskladnoyMehanizm',
            'vidyMaterialovNozhekStola',
            'kolichestvoMest',
            'segment',
            'grade',
            'popularity',
            //'upholsteryMaterial',
            //'fabricPattern',
            //'woodMaterial',
            //'woodPattern',
        ],
    ];
    private $assocIds = [
        'getFabricStructure'                    => 57, //Состав тканей
        'getFabricDensity'                      => 59, //Плотность ткани
        'getFireResistance'                     => 61, //Огнеустойчивость ткани
        'getWearResistance'                     => 62, //Износостойкость (тест Мартиндейла)
        'getColorResistance'                    => 63, //Цветоустойчивость ткани
        'getGlossDegree'                        => 70, //Степень глянца
        'getHidingPower'                        => 77, //Укрывистость
        'getAdditionalFinishingOptions'         => 78, //Дополнительные параметры отделки
        'getFacadeDesign'                       => 87, //Конструкция фасада
        'getGlassKind'                          => 112, //Вид стекла
        'getGlassDecoration'                    => 113, //Отделка стекла
        'getGlassProperty'                      => 114, //Свойства стекла
        'getGlassBrand'                         => 115, //Бренды стекла
        'getPvcFilmThickness'                   => 118, //Толщина пленки ПВХ (мм)
        'getPvcFilmType'                        => 119, //Тип пленки ПВХ
        'getPvcFilmFinishing'                   => 120, //Отделка пленки ПВХ
        //'getRazmer',                                          => 111,
        'getNalichieOtdeleniyaDlyaObuvi'        => 104,
        'getNalichieVeshalki'                   => 102,
        'getNalichieZerkala'                    => 98,
        'getSRamkoyBagetom'                     => 97,
        'getSpalnoeMestoVtorogoYarusaShirinaMm' => 94,
        'getSpalnoeMestoVtorogoYarusaDlinaMm'   => 93,
        'getMehanizmVrashcheniya'               => 80,
        //'getSkladnoyStul',                                    => 111,
        //'getBarnyyStul',                                      => 111,
        'getPodlokotnikiStula'                  => 72,
        'getGlubinaMm'                          => 71,
        'getDlinaVRazlozhennomSostoyaniiMm'     => 54,
        'getNalichiePodgolovnika'               => 16,
        'getNalichiePodemnogoMehanizma'         => 15,
        'getNalichieOrtopedicheskogoOsnovaniya' => 14,
        'getNalichieVydvizhnogoYashchika'       => 13,
        'getNalichieTumby'                      => 12,
        'getMyagkayaSpinka'                     => 11,
        'getNalichieKoles'                      => 10,
        'getNalichieYashchikaDlyaBelya'         => 9,
        'getVysotaSidenyaMm'                    => 8,
        'getSpalnoeMestoShirinaMm'              => 6,
        'getSpalnoeMestoDlinaMm'                => 5,
        'getVysotaMm'                           => 3,
        'getShirinaMm'                          => 2,
        'getDlinaMm'                            => 1,
        //'getMaterialVazy',                                    => 111,
        'getKonstruktsiyaKnizhnogoShkafa'       => 111,
        'getNalichieVKabinete'                  => 109,
        'getVidPodstavki'                       => 108,
        'getNalichieVDetskoy'                   => 107,
        'getKonfiguratsiya'                     => 105,
        'getVidOsveshcheniya'                   => 101,
        'getMehanizmReklayner'                  => 100,
        'getSposobUstanovki'                    => 99,
        'getKolichestvoDverey'                  => 96,
        'getKrovatOptsii'                       => 92,
        'getMatrasOptsii'                       => 90,
        //'getUglovoeRaspolozhenie',                            => 111,
        //'getPoverhnost',                                      => 111,
        'getFunktsionalnyeZony'                 => 79,
        'getMaterialIzgotovleniyaKarkasa'       => 56,
        'getTeksturaDrevesiny'                  => 53,
        'getPodlokotnikMaterial'                => 48,
        'getPodlokotnikNalichie'                => 44,
        'getNapolnitelMatrasaKrovati'           => 43,
        //'getOsnovaFasadovKorpusnoyMebeli',                    => 111,
        'getTsvet'                              => 38,
        'getStil'                               => 36,
        'getNapolnitelDlyaMyagkoyMebeli'        => 34,
        'getKonstruktsiyaMatrasa'               => 32,
        'getKolichestvoNozhekStola'             => 30,
        'getFormaStola'                         => 29,
        'getRaskladnoyMehanizmStola'            => 28,
        'getTipMyagkoyMebeli'                   => 25,
        'getRaskladnoyMehanizm'                 => 24,
        'getVidyMaterialovNozhekStola'          => 23,
        'getKolichestvoMest'                    => 22,
        //'getUpholsteryMaterial'                 => 35,
        //'getPatternMaterial'                    => 58,
        //'getTexture'                        => 67,
    ];
    private $propsIds = [
    ];
    private $fieldsGroup = [
        self::MATERIALCOLLECTION => [
            'property' => [
                'name' => 'name',
            ],
        ],
        self::PRICECATEGORY      => [
            'property' => [
                'name' => "name",
            ],
        ],
        self::PRODUCTMATERIAL    => [
            'property' => [
                'name'                       => 'name',
                'vendorCode'                 => 'code',
                'image'                      => 'getImageName',
                'texture'                    => 'getTextureName',
                'colors'                     => 'getColorFromRal',
                'pattern'                    => 'getPattern',
                'colorResistance'            => 'getColorResistance',
                'wearResistance'             => 'getWearResistance',
                'fireResistance'             => 'getFireResistance', // * bool
                'fabricDensity'              => 'getFabricDensity', // * int ???
                'fabricStructure'            => 'getFabricStructure',
                'pvcFilmFinishing'           => 'getPvcFilmFinishing',
                'pvcFilmType'                => 'getPvcFilmType',
                'pvcFilmThickness'           => 'getPvcFilmThickness', // * float
                'glassBrand'                 => 'getGlassBrand',
                'glassProperty'              => 'getGlassProperty',
                'glassDecoration'            => 'getGlassDecoration',
                'glassKind'                  => 'getGlassKind',
                'facadeDesign'               => 'getFacadeDesign',
                'additionalFinishingOptions' => 'getAdditionalFinishingOptions',
                'hidingPower'                => 'getHidingPower',
                'glossDegree'                => 'getGlossDegree',
            ],
        ],
        self::PRODUCTMODEL       => [
            'property' => [
                'name'                               => 'name',
                'description'                        => 'description',
                'vendorCode'                         => 'sku',
                'vendorCodeMpn'                      => 'mpn', // артикул производителя
                'country'                            => 'country',
                'image'                              => 'getImage',
                'price'                              => 'getPrice',
                'furnitureType'                      => 'getProductCatalog',
//                'razmer'                             => 'getRazmer',
                'nalichieOtdeleniyaDlyaObuvi'        => 'getNalichieOtdeleniyaDlyaObuvi',
                'nalichieVeshalki'                   => 'getNalichieVeshalki',
                'nalichieZerkala'                    => 'getNalichieZerkala',
                'sRamkoyBagetom'                     => 'getSRamkoyBagetom',
                'spalnoeMestoVtorogoYarusaShirinaMm' => 'getSpalnoeMestoVtorogoYarusaShirinaMm',
                'spalnoeMestoVtorogoYarusaDlinaMm'   => 'getSpalnoeMestoVtorogoYarusaDlinaMm',
                'mehanizmVrashcheniya'               => 'getMehanizmVrashcheniya',
//                'skladnoyStul'                       => 'getSkladnoyStul',
//                'barnyyStul'                         => 'getBarnyyStul',
                'podlokotnikiStula'                  => 'getPodlokotnikiStula',
                'glubinaMm'                          => 'getGlubinaMm',
                'dlinaVRazlozhennomSostoyaniiMm'     => 'getDlinaVRazlozhennomSostoyaniiMm',
                'nalichiePodgolovnika'               => 'getNalichiePodgolovnika',
                'nalichiePodemnogoMehanizma'         => 'getNalichiePodemnogoMehanizma',
                'nalichieOrtopedicheskogoOsnovaniya' => 'getNalichieOrtopedicheskogoOsnovaniya',
                'nalichieVydvizhnogoYashchika'       => 'getNalichieVydvizhnogoYashchika',
                'nalichieTumby'                      => 'getNalichieTumby',
                'myagkayaSpinka'                     => 'getMyagkayaSpinka',
                'nalichieKoles'                      => 'getNalichieKoles',
                'nalichieYashchikaDlyaBelya'         => 'getNalichieYashchikaDlyaBelya',
                'vysotaSidenyaMm'                    => 'getVysotaSidenyaMm',
                'spalnoeMestoShirinaMm'              => 'getSpalnoeMestoShirinaMm',
                'spalnoeMestoDlinaMm'                => 'getSpalnoeMestoDlinaMm',
                'vysotaMm'                           => 'getVysotaMm',
                'shirinaMm'                          => 'getShirinaMm',
                'dlinaMm'                            => 'getDlinaMm',
//                'materialVazy'                       => 'getMaterialVazy',
                'konstruktsiyaKnizhnogoShkafa'       => 'getKonstruktsiyaKnizhnogoShkafa',
                'nalichieVKabinete'                  => 'getNalichieVKabinete',
                'vidPodstavki'                       => 'getVidPodstavki',
                'nalichieVDetskoy'                   => 'getNalichieVDetskoy',
                'konfiguratsiya'                     => 'getKonfiguratsiya',
                'vidOsveshcheniya'                   => 'getVidOsveshcheniya',
                'mehanizmReklayner'                  => 'getMehanizmReklayner',
                'sposobUstanovki'                    => 'getSposobUstanovki',
                'kolichestvoDverey'                  => 'getKolichestvoDverey',
                'krovatOptsii'                       => 'getKrovatOptsii',
                'matrasOptsii'                       => 'getMatrasOptsii',
//                'uglovoeRaspolozhenie'               => 'getUglovoeRaspolozhenie',
//                'poverhnost'                         => 'getPoverhnost',
                'functionalArea'                     => 'getFunktsionalnyeZony',
                'materialIzgotovleniyaKarkasa'       => 'getMaterialIzgotovleniyaKarkasa',
                'teksturaDrevesiny'                  => 'getTeksturaDrevesiny',
                'podlokotnikMaterial'                => 'getPodlokotnikMaterial',
                'podlokotnikNalichie'                => 'getPodlokotnikNalichie',
                'napolnitelMatrasaKrovati'           => 'getNapolnitelMatrasaKrovati',
//                'osnovaFasadovKorpusnoyMebeli'       => 'getOsnovaFasadovKorpusnoyMebeli',
                'tsvet'                              => 'getTsvet',
                'stil'                               => 'getStil',
                'napolnitelDlyaMyagkoyMebeli'        => 'getNapolnitelDlyaMyagkoyMebeli',
                'konstruktsiyaMatrasa'               => 'getKonstruktsiyaMatrasa',
                'kolichestvoNozhekStola'             => 'getKolichestvoNozhekStola',
                'formaStola'                         => 'getFormaStola',
                'raskladnoyMehanizmStola'            => 'getRaskladnoyMehanizmStola',
                'tipMyagkoyMebeli'                   => 'getTipMyagkoyMebeli',
                'raskladnoyMehanizm'                 => 'getRaskladnoyMehanizm',
                'vidyMaterialovNozhekStola'          => 'getVidyMaterialovNozhekStola',
                'kolichestvoMest'                    => 'getKolichestvoMest',
                'segment'                            => 'getSegment',
                'grade'                              => 'getGrade',
                'popularity'                         => 'getPopularity',
                //'patternMaterial'                    => 'getPatternMaterial',
                //'texture'                            => 'getTexture',
            ],
        ],
    ];
    private $fieldsToPoperties = null;

    public function __construct(string $id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function options($actionID)
    {
        return ['continue'];
    }

    public function actionIndex()
    {
        $help = "\nДля добавления продуктовых моделей наберите в консоли:\n" .
            "php yii product/create\n\n" .
            "\nДля привязки продуктовых моделей:\n" .
            "php yii product/bind\n\n";
        $this->stdout($help, Console::FG_GREEN);


        $data = [
            44  => "КОРСА",
            45  => "
    Диван-кровать «Корса» предназначен для ежедневного, комфортного отдыха, легко трансформируется в комфортную кровать. Уникальный дизайн подлокотников, четкие, прямые линии придают модели современный вид. Красивая прострочка чехла подушек переходящая на сидушку.\r\n
    \r\n
    Преимущества:\r\n
    \r\n
    Современный дизайн. В отличие от моделей конкурентов стёжка выполнена на ППУ, а не на холлофайбере, что сохранит первоначальный вид изделия в ходе эксплуатации.\r\n
    Каркас изделия выполнен из массива бруса хвойных пород собственной обработки. Мягким элементом спального места является пружинная змейка, высокоэластичный ППУ толщиной 80 мм с дополнительным слоем ППУ толщиной 20 мм в чехле.\r\n
    Механизм трансформации «еврокнижка» один из самых надежных. Принцип трансформации основан на движении системы роликов по березовым направляющим, что увеличивает срок службы изделия.\r\n
    Бельевой ящик выполнен из ламинированного ДСП толщиной 16 мм.\r\n
    Наполнением приспинных подушек является синтепух. Стёжка приспинных подушек выполнена на основе материала Hollcon на итальянских стегальных машинах RESTA.
    ",
            58  => "RU.10008.SL.43",
            47  => "ru",
            60  => [
                0 => "94273f3c3ae121e119ffc156b802f3ad.jpg",
            ],
            139 => "3590000",
            140 => [
                0 => "2",
            ],
            132 => true,
            134 => "1450",
            135 => "2000",
            136 => "830",
            137 => "1020",
            138 => "2400",
            94  => [
                0 => "4",
                1 => "6",
                2 => "8",
            ],
            95  => [
                0 => "1",
                1 => "2",
                2 => "3",
            ],
            97  => [
                0 => "2",
            ],
            98  => [
                0 => "2",
                1 => "3",
            ],
            102 => [
                0 => "4",
                1 => "17",
                2 => "19",
            ],
            103 => [
                0 => "3",
            ],
            108 => [
                0 => "1",
            ],
            109 => [
                0 => "12",
            ],
        ];
        $model = new ProductModel(['isActive' => true]);
        $model->save();
        dump($model);
        $result = $model->saveProperties($data);
        die;
    }

    public function actionCreate()
    {
        $this->createSupportImportTables();

        if ($this->confirm("Are you sure you want to clear ALL previosly imported data?")) {
            $this->clearSupportImportTables();
        }

        $this->initDomainData();
        $this->initEntityData();



        // materialCollection
        $materialCatalogs = MaterialCatalog::find()
            ->all();

        foreach ((array)$materialCatalogs as $materialCatalog) {
            $this->addMaterialCatalog($materialCatalog);
        }

        // priceCategory
        $categories = MaterialCategory::find()
            ->all();

        foreach ((array)$categories as $category) {
            $this->addPriceCategory($category);
        }

        // productMaterial
        $pics = MaterialDepiction::find()
            ->limit($this->limit)
            ->all();

        foreach ((array)$pics as $pic) {
            $this->addProductMaterial($pic);
        }

        // productModel
        $sql = "SELECT
            p.id,
            p.manufacturer_id,
            p.sku,
            p.mpn,
            p.price_amount,
            p.price_currency, 
            pt.name,
            pt.description,
            pq.*,
            GROUP_CONCAT(DISTINCT (i.name) SEPARATOR ';') image,
            GROUP_CONCAT(DISTINCT (zt.name) SEPARATOR ';') zone_name,
            GROUP_CONCAT(DISTINCT (ct.name) SEPARATOR ';') catalog,
            LOWER(o.country) country
        FROM
            product p
                INNER JOIN
            organization o ON p.manufacturer_id = o.id
                INNER JOIN
            product_translation pt ON p.id = pt.translatable_id
                INNER JOIN
            product_image i ON p.id = i.product_id
                INNER JOIN
            product_zone pz ON p.id = pz.product_id
                INNER JOIN
            zone z ON pz.zone_id = z.id
                INNER JOIN
            zone_translation zt ON z.id = zt.translatable_id
                INNER JOIN
            catalog_translation ct ON ct.translatable_id = p.primary_category_id          
                INNER JOIN
            product_quality pq ON pq.product_id = p.id
            WHERE
                 (ct.locale = 'ru' OR ct.locale IS NULL)
                 AND zt.locale = 'ru'
                 /*and p.id=966*/
            /*WHERE
            zt.locale = 'ru'
                AND p.price_currency = 'RUB'
                AND pt.locale = 'ru'
                AND (ct.locale = 'ru' OR ct.locale IS NULL)*/
                GROUP BY i.product_id 
                limit {$this->limit}
                ";
        // TODO: усеченный импорт товаров
        // удалить лимит и фильтры по локали и валюте при полном импорте
        $products = Yii::$app->db_mcore->createCommand($sql)->queryAll();
        foreach ($products as $product) {
            /*dump(method_exists($this,'link'));
            dump($product);
            die;*/
            $this->addProductModel($product);
        }

        // bind Data
        if ($this->confirm("Are you sure you want to bind data?")) {

            $this->actionBindMaterialCollection();
            $this->actionBindPriceCategory();
            $this->actionBindProductMaterial();
            $this->actionBindProductModel($products);
        } else {
            $this->stdout("Action was cancelled by user. Nothing has been performed.\n");
        }

        // update Data
//        if ($this->confirm("Are you sure you want to update productMaterial properties?")) {
//            foreach ((array) $pics as $pic) {
//                $this->updateProductMaterial($pic);
//            }
//        } else {
//            $this->stdout("Action was cancelled by user. Nothing has been performed.\n");
//        }


        $this->stdout("TOTAL: " . count($materialCatalogs) . " materialCollection\n", Console::FG_GREEN);
        $this->stdout("TOTAL: " . count($categories) . " priceCategory\n", Console::FG_GREEN);
        $this->stdout("TOTAL: " . count($pics) . " productMaterial\n", Console::FG_GREEN);
        $this->stdout("TOTAL: " . count($products) . " productModel\n", Console::FG_GREEN);
        $this->stdout("------------\n", Console::FG_BLACK);
    }

    public function actionBindMaterialCollection()
    {
        // materialCollection
        $materialCatalogs = MaterialCatalog::find()
            ->all();

        foreach ((array)$materialCatalogs as $materialCatalog) {
            $this->bindMaterialCollection2Manufacturer($materialCatalog);
        }
    }

    public function actionBindPriceCategory()
    {
        // priceCategory
        $categories = MaterialCategory::find()
            ->all();

        foreach ((array)$categories as $category) {
            $this->bindPriceCategory2Manufacturer($category);
        }
    }

    public function actionBindProductMaterial()
    {
        // coloration
        $pics = MaterialDepiction::find()
            ->all();

        foreach ((array)$pics as $pic) {
            $this->bindProductMaterial2Manufacturer($pic);
        }
        foreach ((array)$pics as $pic) {
            $this->bindProductMaterial2MaterialCollection($pic);
        }

        foreach ((array)$pics as $pic) {
            $this->bindProductMaterial2PriceCategory($pic);
        }

        foreach ((array)$pics as $pic) {
            $this->bindProductMaterial2Material($pic);
        }
    }

    public function actionBindProductModel($products)
    {
        // products
        foreach ($products as $product) {
            $this->bindProductModel2Manufacturer($product);
        }
        foreach ($products as $product) {
            $this->bindProductModel2Material($product);
        }
        foreach ($products as $product) {
            $this->bindProductModel2ProductMaterial($product);
        }
    }

    public function addMaterialCatalog(MaterialCatalog $element)
    {
        $element_id = $this->findMaterialCollection($element->id);
        $entityData = $this->getDataForEntity($element->toArray(), self::MATERIALCOLLECTION);
        // если элемент не создан
        if (!$element_id) {
            $model = new MaterialCollection(['isActive' => $element->active]);
            $propertyValues = $this->renderPropertyValues($entityData['property'], $element->toArray());
            $model->save();
            dump($propertyValues);

            $result = $model->saveProperties($propertyValues);

            if ($result === true) {
                $element_id = $model->id;
                (new MaterialCollectionImportMapping([
                    'element_id' => $element_id,
                    'origin_id'  => $element->id,
                ]))->save();
            }
            $this->stdoutText('  -> Add MaterialCollection with id ', $element->id, $element_id);
        } else {
            $this->stdoutTextExist('  -> Exist MaterialCollection with id ', $element->id);
        }

        return $element_id ?? null;
    }

    public function addPriceCategory(MaterialCategory $element)
    {
        $element_id = $this->findPriceCategory($element->id);
        $entityData = $this->getDataForEntity($element->toArray(), self::PRICECATEGORY);
        // если элемент не создан
        if (!$element_id) {
            $model = new PriceCategory(['isActive' => true]);
            $propertyValues = $this->renderPropertyValues($entityData['property'], $element->toArray());
            $model->save();
            dump($propertyValues);
            $result = $model->saveProperties($propertyValues);

            if ($result === true) {
                $element_id = $model->id;
                (new MaterialCategoryImportMapping([
                    'element_id' => $element_id,
                    'origin_id'  => $element->id,
                ]))->save();
            }
            $this->stdoutText('  -> Add PriceCategory with id ', $element->id, $element_id);
        } else {
            $this->stdoutTextExist('  -> Exist PriceCategory with id ', $element->id);
        }

        return $element_id ?? null;
    }

    public function addProductMaterial(MaterialDepiction $element)
    {
        $element_id = $this->findColoration($element->id);
        $entityData = $this->getDataForEntity($element->toArray(), self::PRODUCTMATERIAL);
        // если элемент не создан
        if (!$element_id) {
            $model = new ProductMaterial(['isActive' => true]);
            $propertyValues = $this->renderPropertyValues($entityData['property'], $element->toArray());

            $model->save();
            dump($propertyValues);
            $result = $model->saveProperties($propertyValues);

            if ($result === true) {
                $element_id = $model->id;
                (new ColorationImportMapping([
                    'element_id' => $element_id,
                    'origin_id'  => $element->id,
                ]))->save();
            }
            $this->stdoutText('  -> Add ProductMaterial with id ', $element->id, $element_id);
        } else {
            $this->stdoutTextExist('  -> Exist ProductMaterial with id ', $element->id);
        }

        return $element_id ?? null;
    }

    public function addProductModel($element)
    {
        $element_id = $this->findProduct($element['id']);
        $entityData = $this->getDataForEntity($element, self::PRODUCTMODEL);
        // если элемент не создан
        if (!$element_id) {
            $model = new ProductModel(['isActive' => true]);
            $propertyValues = $this->renderPropertyValues($entityData['property'], $element);
            $model->save();
            dump($propertyValues);
            $result = $model->saveProperties($propertyValues);

            if ($result === true) {
                $element_id = $model->id;
                (new ProductImportMapping([
                    'element_id' => $element_id,
                    'origin_id'  => $element['id'],
                ]))->save();
            }
            $this->stdoutText('  -> Add ProductModel with id ', $element['id'], $element_id);
        } else {
            $this->stdoutTextExist('  -> Exist ProductModel with id ', $element['id']);
        }

        return $element_id ?? null;
    }

    public function bindMaterialCollection2Manufacturer(MaterialCatalog $element)
    {
        $manufacturer_id = $this->findCompany($element->category->directory->manufacturer->id);
        $element_id = $this->findMaterialCollection($element->id);
        if (!$manufacturer_id || !$element_id) {
            return false;
        }
        $model = new MaterialCollection();
        $materialCollection = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $materialCollection->bindManufacturer($manufacturer_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $materialCollection->id, $manufacturer_id);

        return $result ?? null;
    }

    public function bindPriceCategory2Manufacturer(MaterialCategory $element)
    {
        $manufacturer_id = $this->findCompany($element->directory->manufacturer->id);
        $element_id = $this->findPriceCategory($element->id);
        if (!$manufacturer_id || !$element_id) {
            return false;
        }
        $model = new PriceCategory();
        $priceCategory = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $priceCategory->bindManufacturer($manufacturer_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $priceCategory->id, $manufacturer_id);

        return $result ?? null;
    }

    public function bindProductMaterial2Manufacturer(MaterialDepiction $element)
    {

        $manufacturer_id = $this->findCompany($element->catalog->category->directory->manufacturer->id);
        $element_id = $this->findColoration($element->id);
        if (!$manufacturer_id || !$element_id) {
            return false;
        }
        $model = new ProductMaterial();
        $productMaterial = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $productMaterial->bindManufacturer($manufacturer_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $productMaterial->id, $manufacturer_id);

        return $result ?? null;
    }

    public function bindProductMaterial2MaterialCollection(MaterialDepiction $element)
    {
        $materialCollection_id = $this->findMaterialCollection($element->catalog_id);
        $element_id = $this->findColoration($element->id);
        if (!$materialCollection_id || !$element_id) {
            return false;
        }
        $model = new ProductMaterial();
        $productMaterial = $model->findOne(['id' => $element_id]);

        $result = $materialCollection_id ? $productMaterial->bindMaterialCollection($materialCollection_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $productMaterial->id, $materialCollection_id);

        return $result ?? null;
    }

    public function bindProductMaterial2PriceCategory(MaterialDepiction $element)
    {
        $element_id = $this->findColoration($element->id);
        $category_id = $this->findPriceCategory($element->catalog->category->id);
        if (!$category_id || !$element_id) {
            return false;
        }
        $model = new ProductMaterial();
        $productMaterial = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $productMaterial->bindPriceCategory($category_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $productMaterial->id, $category_id);

        return $result ?? null;
    }

    public function bindProductModel2Manufacturer($element)
    {
        $manufacturer_id = $this->findCompany($element['manufacturer_id']);
        $element_id = $this->findProduct($element['id']);
        if (!$manufacturer_id || !$element_id) {
            return false;
        }
        $model = new ProductModel();
        $productModel = $model->findOne(['id' => $element_id]);

        $result = $element_id && $manufacturer_id ? $productModel->bindManufacturer($manufacturer_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $element_id, $manufacturer_id ?? '---');

        return $result ?? null;
    }

    public function bindProductModel2Material($element)
    {
        $sql = "SELECT
                value
            FROM
                mcore.product_property pp
                    INNER JOIN
                product_property_set_value ppsv ON pp.id = ppsv.product_property_set_id
                    INNER JOIN
                property_value_translation pvt ON ppsv.property_value_id = pvt.translatable_id
            WHERE
                pp.product_id ={$element['id']}";
        $rows = Yii::$app->db_mcore->createCommand($sql)->queryAll();
        $materials = ArrayHelper::getColumn($rows, 'value');

        $element_id = $this->findProduct($element['id']);
        if (!$element_id) {
            return null;
        }
        $model = new ProductModel();
        $productModel = $model->findOne(['id' => $element_id]);

        foreach ($materials as $material) {
            $material_id = $this->findMaterial($material);
            if (!$material_id) {
                return null;
            }
            $result = $material_id ? $productModel->bindMaterial($material_id) : false;
            $this->stdoutBindAssociatedElements(__FUNCTION__, $productModel->id, $material . ':' . $material_id ?? '---');
        }

        return $result ?? null;
    }

    public function bindProductModel2ProductMaterial($element)
    {
        $sql = "SELECT
            ppd.*,
            pp.*,
            ppdc.*,
            ppdv.*,
            GROUP_CONCAT(DISTINCT ppdv.price_category_id) pcids,
            mpc.*,
            GROUP_CONCAT(DISTINCT md.id) mdids
        FROM
            mcore.product_property pp
                INNER JOIN
            product p ON pp.product_id = p.id
                INNER JOIN
            product_property_directory ppd ON pp.id = ppd.id
                INNER JOIN
            product_property_directory_category ppdc ON ppd.id = ppdc.product_property_directory_id
                INNER JOIN
            product_property_directory_value ppdv ON ppd.id = ppdv.product_property_directory_id
                INNER JOIN
            material_price_category mpc ON ppdv.price_category_id = mpc.id
                INNER JOIN
            material_catalog mc ON mpc.id = mc.price_category_id
                INNER JOIN
            material_depiction md ON mc.id = md.catalog_id
        WHERE
            ppd.directory_id IS NOT NULL
            AND
            p.id={$element['id']}
        GROUP BY ppdc.product_property_directory_id , pp.property_id";
        $rows = Yii::$app->db_mcore->createCommand($sql)->queryAll();
        $depictions = ArrayHelper::getColumn($rows, 'mdids');

        $element_id = $this->findProduct($element['id']);
        if (!$element_id) {
            return null;
        }

        $model = new ProductModel();
        $productModel = $model->findOne(['id' => $element_id]);

        foreach ($depictions as $depiction) {
            $ids = explode(',', $depiction);
            foreach ($ids as $depiction_id) {
                $productMaterial_id = $this->findColoration($depiction_id);
                if (!$productMaterial_id) {
                    continue;
                }
                $result = $productModel->bindProductMaterial($productMaterial_id);
                $this->stdoutBindAssociatedElements(__FUNCTION__, $productModel->id, $productMaterial_id);
            }
        }
    }

    public function bindProductMaterial2Material(MaterialDepiction $element)
    {
        $sql = "SELECT
                pic.id, pvt.value
            FROM
                material_depiction pic
                    INNER JOIN
                material_depiction_property_value mdpv ON pic.id = mdpv.material_depiction_id
                    INNER JOIN
                property_value pv ON mdpv.property_value_id = pv.id
                    INNER JOIN
                property p ON pv.property_id = p.id
                    INNER JOIN
                property_value_translation pvt ON pv.id = pvt.translatable_id
            WHERE
                locale = 'ru'
                    AND p.id IN (35, 40, 65, 81, 82) AND pic.id={$element->id}";
        $rows = Yii::$app->db_mcore->createCommand($sql)->queryAll();
        $materials = ArrayHelper::getColumn($rows, 'value');
        $element_id = $this->findColoration($element->id);
        if (!$element_id) {
            return false;
        }
        $model = new ProductMaterial();
        $productMaterial = $model->findOne(['id' => $element_id]);
        foreach ($materials as $material) {
            $material_id = $this->findMaterial($material);

            $result = $material_id ? $productMaterial->bindMaterial($material_id) : false;
            $this->stdoutBindAssociatedElements(__FUNCTION__, $productMaterial->id, $material . ':' . $material_id ?? '---');
        }

        return $result ?? null;
    }

    private function getDataForEntity($data, $objectType)
    {
        foreach ($this->fieldsToPoperties[$objectType] as $fieldGroup => $propMap) {

            foreach ($propMap as $propName => $propId) {
                if (is_array($propId)) {
                    $result[$fieldGroup][key($propId)][reset($propId)] = $data[$propName];
                } else {
                    $result[$fieldGroup][$propId] = $this->getPropertyValue($data, $propName);
                }
            }
        }

        return $result ?? null;
    }

    protected function getPattern(...$args)
    {
        $model = $args[0];
        if ($model['pattern_type'] == 99) {
            return null;
        }
        return [$model['pattern_type']];
    }

    protected function getColorResistance(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getWearResistance(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getFireResistance(...$args)
    {
        $model = $args[0];
        return (bool)$this->getAdditionalProperties($model, __FUNCTION__, true);
    }

    protected function getFabricDensity(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getFabricStructure(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getPvcFilmFinishing(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getPvcFilmType(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getPvcFilmThickness(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__, true);
    }

    protected function getGlassBrand(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getGlassProperty(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getGlassDecoration(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getGlassKind(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getFacadeDesign(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getAdditionalFinishingOptions(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getHidingPower(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getGlossDegree(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProperties($model, __FUNCTION__);
    }

    protected function getCountry(...$args)
    {
        $model = $args[0];
        return [$model['country']] ?? [];
    }

    protected function getPopularity(...$args)
    {
        $model = $args[0];
        return [$model['popularity']] ?? [];
    }

    protected function getGrade(...$args)
    {
        $model = $args[0];
        return [$model['grade']] ?? [];
    }

    protected function getSegment(...$args)
    {
        $model = $args[0];
        return [$model['segment']] ?? [];
    }

    protected function getNalichieOtdeleniyaDlyaObuvi(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichieVeshalki(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichieZerkala(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getSRamkoyBagetom(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getSpalnoeMestoVtorogoYarusaShirinaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getSpalnoeMestoVtorogoYarusaDlinaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getMehanizmVrashcheniya(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getSkladnoyStul(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getBarnyyStul(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getPodlokotnikiStula(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getGlubinaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getDlinaVRazlozhennomSostoyaniiMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getNalichiePodgolovnika(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichiePodemnogoMehanizma(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichieOrtopedicheskogoOsnovaniya(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichieVydvizhnogoYashchika(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichieTumby(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getMyagkayaSpinka(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichieKoles(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getNalichieYashchikaDlyaBelya(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'boolean');
    }

    protected function getVysotaSidenyaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getSpalnoeMestoShirinaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getSpalnoeMestoDlinaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getVysotaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getShirinaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getDlinaMm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'integer');
    }

    protected function getMaterialVazy(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getKonstruktsiyaKnizhnogoShkafa(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getNalichieVKabinete(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getVidPodstavki(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getNalichieVDetskoy(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getKonfiguratsiya(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getVidOsveshcheniya(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getMehanizmReklayner(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getSposobUstanovki(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getKolichestvoDverey(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getKrovatOptsii(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getMatrasOptsii(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getUglovoeRaspolozhenie(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getPoverhnost(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getFunktsionalnyeZony(...$args)
    {
        $model = $args[0];
        $propSysname = lcfirst(str_replace('get', '', __FUNCTION__));
        if (empty($this->properties[$propSysname])) {

            foreach ($this->getFunctionalZoneMapping() as $label => $itemid) {
                $this->properties[$propSysname][$label] = $itemid;
            }
        }
        $zoneNames = explode(';', $model['zone_name']);
        $zoneNames = array_unique($zoneNames);
        $listIds = [];
        foreach ($zoneNames as $zoneName) {
            $listIds[] = $this->properties[$propSysname][$zoneName];
        }
        return $listIds;
    }

    protected function getMaterialIzgotovleniyaKarkasa(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getTeksturaDrevesiny(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getPodlokotnikMaterial(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getPodlokotnikNalichie(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getNapolnitelMatrasaKrovati(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getOsnovaFasadovKorpusnoyMebeli(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getTsvet(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getStil(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getNapolnitelDlyaMyagkoyMebeli(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'set');
    }

    protected function getKonstruktsiyaMatrasa(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getKolichestvoNozhekStola(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getFormaStola(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getRaskladnoyMehanizmStola(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getTipMyagkoyMebeli(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getRaskladnoyMehanizm(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getVidyMaterialovNozhekStola(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getKolichestvoMest(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getPatternMaterial(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getTexture(...$args)
    {
        $model = $args[0];
        return $this->getAdditionalProductProperties($model, __FUNCTION__, 'enum');
    }

    protected function getProductCatalog(...$args)
    {
        $model = $args[0];
        $propSysname = lcfirst(str_replace('get', '', __FUNCTION__));
        if (empty($this->properties[$propSysname])) {

            foreach ($this->getProductCatalogMapping() as $label => $itemid) {
                $this->properties[$propSysname][$label] = $itemid;
            }
        }
        $zoneNames = explode(';', $model['catalog']);
        $zoneNames = array_unique($zoneNames);
        $listIds = [];
        foreach ($zoneNames as $zoneName) {
            $listIds[] = $this->properties[$propSysname][$zoneName];
        }
        return $listIds;
    }

    protected function getFunctionalZoneMapping()
    {
        return [
            'Жилая мебель'                          => 1,
            'Кухня'                                 => 4,
            'Прихожая'                              => 6,
            'Гостиная'                              => 2,
            'Столовая'                              => 4,
            'Рабочий кабинет'                       => 7,
            'Кладовая'                              => 12,
            'Спальня'                               => 3,
            'Ванная комната'                        => 5,
            'Для новорожденных'                     => 9,
            'Для детей'                             => 10,
            'Игровая, кинотеатр'                    => 15,
            'Мебель для бизнеса'                    => 101,
            'Детские учреждения'                    => 102,
            'Образовательные учреждения'            => 15,
            'Культурно-развлекательные учреждения'  => 16,
            'Физкультурные и спортивные учреждения' => 14,
            'Организации общественного питания'     => 18,
            'Учреждения общественного транспорта'   => 19,
            'Медицинские учреждения'                => 20,
            'Административно-офисные помещения'     => 103,
            'Торговые учреждения'                   => 22,
            'Коммерческие учреждения'               => 23,
            'Прочие'                                => 24,
            'Мебель для улицы'                      => 201,
            'Для дачи и сада'                       => 202,
            'Для открытых кафе и ресторанов'        => 27,
            'Для парков и скверов'                  => 28,
            'Кейтеринг'                             => 29,
            'Для пляжа'                             => 30,
            'Для открытых торговых площадок'        => 31,
            'Детская уличная мебель'                => 203,
            'Гардеробная'                           => 8,
            'Прочее'                                => 34,
            'Для подростков'                        => 11,
        ];
    }

    protected function getProductCatalogMapping()
    {
        return [
            'Хранение'                            => 75,
            'Диваны прямые'                       => 151,
            'Диваны угловые'                      => 152,
            'Кресла'                              => 176,
            'Банкетки'                            => 226,
            'Комплекты мягкой мебели'             => 128,
            'Ванные комнаты'                      => 10,
            'Шкафы детские'                       => 12,
            'Зеркала'                             => 350,
            'Манежи'                              => 401,
            'Тумбы'                               => 26,
            'Комоды детские'                      => 57,
            'Кресла-реклайнеры'                   => 177,
            'Шкафы для ванн'                      => 10,
            'Зеркала для ванн'                    => 352,
            'Кровати и матрасы'                   => 250,
            'Односпальные кровати'                => 271,
            'Двуспальные кровати'                 => 252,
            'Матрасы'                             => 263,
            'Вешалки'                             => 325,
            'Туалетные столики'                   => 302,
            'Стулья'                              => 276,
            'Складные стулья'                     => 277,
            'Табуретки'                           => 279,
            'Перегородки'                         => 355,
            'Шкафы под мойку'                     => 34,
            'Барные стулья'                       => 278,
            'Тумбочки детские'                    => 35,
            'Мягкая мебель'                       => 150,
            'Детская мебель'                      => 425,
            'Кровати детские'                     => 261,
            'Матрасы детские'                     => 269,
            'Детские столы'                       => 311,
            'Комоды'                              => 51,
            'Колыбели'                            => 258,
            'Шкафы-купе'                          => 5,
            'Игровая мебель'                      => 320,
            'Диваны детские'                      => 159,
            'Шкафы распашные'                     => 2,
            'Модульные системы для детской'       => 105,
            'Пеленальные столики'                 => 403,
            'Комплекты детской мебели'            => 132,
            'Предметы интерьера'                  => 350,
            'Пуфы'                                => 202,
            'Детские стулья'                      => 282,
            'Стеллажи'                            => 77,
            'Столы'                               => 300,
            'Компьютерные столы'                  => 303,
            'Кухонные уголки'                     => 129,
            'Обеденные столы'                     => 301,
            'Письменные столы'                    => 304,
            'Кофейные столики'                    => 305,
            'Журнальные столы'                    => 306,
            'Консольные столики'                  => 309,
            'Диваны модульные'                    => 153,
            'Диваны с оттоманкой'                 => 154,
            'Диваны-реклайнеры'                   => 155,
            'Кресла-качалки'                      => 178,
            'Кресла-мешки'                        => 179,
            'Скамейки мягкие'                     => 227,
            'Мини-диваны'                         => 156,
            'Кушетки'                             => 157,
            'Корзины'                             => 375,
            'Настенные полки'                     => 76,
            'Секретеры'                           => 55,
            'Буфеты'                              => 8,
            'Шкафы навесные'                      => 4,
            'Стенки'                              => 101,
            'Тумбочки прикроватные'               => 28,
            'Кровати двухъярусные детские'        => 254,
            'Сервировочные столики'               => 308,
            'Тумбы под ТВ'                        => 29,
            'Прихожие'                            => 102,
            'Витрины'                             => 9,
            'Каминные порталы'                    => 356,
            'Полуторные кровати'                  => 251,
            'Часы'                                => 365,
            'Освещение'                           => 475,
            'Тумбы для обуви'                     => 27,
            'Открытые полки'                      => 76,
            'Комплекты мебели для ванной комнаты' => 127,
            'Подставки'                           => 357,
            'Кровати-чердаки детские'             => 255,
            'Приставные столики'                  => 307,
            'Скамейки'                            => 227,
            'Кабинеты'                            => 126,
            'Шкафы книжные'                       => 3,
            'Стеллажи детские'                    => 81,
            'Стульчики для кормления'             => 283,
            'Кресла-качалки для кормления'        => 181,
            'Столы письменные для подростков'     => 312,
            'Парты'                               => 313,
            'Компьютерные столы для подростков'   => 314,
            'Детские рабочие кресла'              => 285,
            'Кресла детские'                      => 182,
            'Кресла-мешки детские'                => 183,
            'Кушетки детские'                     => 160,
            'Детские пуфы'                        => 203,
            'Кроватки для новорожденных'          => 257,
            'Вазы'                                => 362,
        ];
    }

    protected function getAdditionalProperties($model, $callback, $rawValue = false)
    {
        $propId = $this->assocIds[$callback];
        $sql = "SELECT
                pvt.value
            FROM
                material_depiction pic
                    INNER JOIN
                material_depiction_property_value mdpv ON pic.id = mdpv.material_depiction_id
                    INNER JOIN
                property_value pv ON mdpv.property_value_id = pv.id
                    INNER JOIN
                property p ON pv.property_id = p.id
                    INNER JOIN
                property_value_translation pvt ON pv.id = pvt.translatable_id
            WHERE
                locale = 'ru'
                    AND p.id={$propId} AND pic.id={$model['id']}";

        $row = Yii::$app->db_mcore->createCommand($sql)->queryOne();
        if ($rawValue) {
            return $row['value'] ?? null;
        }
        if ($propId == 59) {
            return !empty($row['value']) ? 150 : null;
        }
        if ($row) {
            $propSysname = lcfirst(str_replace('get', '', $callback));
            if (empty($this->properties[$propSysname])) {
                $property = (new Property())->findOne(['sysname' => $propSysname]);
                foreach ($property->getListItems() as $listItem) {
                    $this->properties[$propSysname][$listItem->label] = $listItem->item;
                }
            }
            $listId = $this->properties[$propSysname][$row['value']];
            return [$listId];
        }

        return [];
    }

    protected function getAdditionalProductProperties($model, $callback, $type, $rawValue = false)
    {
        $propId = $this->assocIds[$callback];
        switch ($type) {
            case 'enum':
                $sql = "SELECT
                    pp.product_id, pp.property_id, pvt.value
                FROM
                    product_property pp
                        INNER JOIN
                    product_property_enum ppe ON ppe.id = pp.id
                        INNER JOIN
                    property_value pv ON pv.id = ppe.value_id
                        INNER JOIN
                    property_value_translation pvt ON pv.id = pvt.translatable_id
                WHERE
                    pvt.locale = 'ru' AND pp.property_id={$propId} AND pp.product_id={$model['id']}";
                break;
            case 'integer':
                $sql = "SELECT
                    pp.product_id, pp.property_id, ppi.value
                FROM
                    product_property pp
                        INNER JOIN
                    product_property_integer ppi ON ppi.id = pp.id
                WHERE
                    pp.property_id={$propId} AND pp.product_id={$model['id']}";
                break;
            case 'boolean':
                $sql = "SELECT
                    pp.product_id, pp.property_id, ppb.value
                FROM
                    product_property pp
                        INNER JOIN
                    product_property_boolean ppb ON ppb.id = pp.id
                WHERE
                    pp.property_id={$propId} AND pp.product_id={$model['id']}";
                break;
            case 'set':
                $sql = "SELECT
                    pp.product_id, pp.property_id, pvt.value
                FROM
                    product_property pp
                        INNER JOIN
                    product_property_set_value pps ON pps.product_property_set_id = pp.id
                        INNER JOIN
                    property_value pv ON pv.id = pps.property_value_id
                        INNER JOIN
                    property_value_translation pvt ON pv.id = pvt.translatable_id
                 WHERE
                    pvt.locale = 'ru' AND pp.property_id={$propId} AND pp.product_id={$model['id']}";
                break;
        }

        $rows = Yii::$app->db_mcore->createCommand($sql)->queryAll();

        switch ($type) {
            case 'enum':
                $listIds = [];
                if ($rows) {
                    foreach ($rows as $row) {
                        $propSysname = lcfirst(str_replace('get', '', $callback));
                        if (empty($this->properties[$propSysname])) {
                            $property = (new Property())->findOne(['sysname' => $propSysname]);
                            $list = $property->getListItems();
                            foreach ($property->getListItems() as $listItem) {
                                $this->properties[$propSysname][$listItem->label] = $listItem->item;
                            }
                        }
                        $listIds[] = $this->properties[$propSysname][$row['value']];
                    }
                }
                return $listIds;
                break;
            case 'integer':
                $listIds = null;
                if ($rows) {
                    foreach ($rows as $row) {
                        $listIds = $row['value'];
                    }
                }
                return $listIds;
                break;
            case 'boolean':
                $listIds = null;
                if ($rows) {
                    foreach ($rows as $row) {
                        $listIds = (bool)$row['value'];
                    }
                }
                return $listIds;
                break;
            case 'set':
                $listIds = [];
                if ($rows) {
                    foreach ($rows as $row) {
                        $propSysname = lcfirst(str_replace('get', '', $callback));
                        if (empty($this->properties[$propSysname])) {
                            $property = (new Property())->findOne(['sysname' => $propSysname]);
                            $list = $property->getListItems();
                            foreach ($property->getListItems() as $listItem) {
                                $this->properties[$propSysname][$listItem->label] = $listItem->item;
                            }
                        }
                        $listIds[] = $this->properties[$propSysname][$row['value']];
                    }
                }
                return $listIds;
                break;
        }
    }

    protected function getImage(...$args)
    {
        $product = $args[0];
        $img = [];
        if (!empty($product['image'])) {
            $images = explode(';', $product['image']);
            $images = array_unique($images);
            foreach ($images as $image) {
                if (!($data = $this->check('/tmp/' . $image))) {
                    $content = @file_get_contents('http://content.furniprice.com/product/' . $image);
                    if (!$content) {
                        continue;
                    }
                    file_put_contents('/tmp/' . $image, $content);
                    $data = $this->upload('/tmp/' . $image);
                }
                if (!empty($data->id)) {
                    $img[] = $data->id . '.' . $data->format;
                }
            }
        }
        return $img;
    }

    protected function getImageName(...$args)
    {
        $depiction = $args[0];
        if ($depiction['image_name']) {
            $p1 = substr($depiction['image_name'], 20, 2);
            $p2 = substr($depiction['image_name'], 17, 3);
            if (!($data = $this->check('/tmp/' . $depiction['image_name']))) {
                $content = @file_get_contents('http://content.furniprice.ru/catalog/materials/' . $p1 . '/' . $p2 . '/' . $depiction['image_name']);
                if (!$content) {
                    return '';
                }
                file_put_contents('/tmp/' . $depiction['image_name'], $content);
                $data = $this->upload('/tmp/' . $depiction['image_name']);
            }
            if (!empty($data->id)) {
                return $data ? ($data->id . '.' . $data->format) : '';
            }
        }
        return '';
    }


    protected function getPrice(...$args)
    {
        $product = $args[0];
        $currency = [
            'value'    => ((double)$product['price_amount'] / 100) ?? 0,
            'currency' => $product['price_currency'] ?? 'USD',
        ];

        return $currency;
    }

    protected function getTextureName(...$args)
    {
        $depiction = $args[0];
        if ($depiction['texture_name']) {
            $p1 = substr($depiction['texture_name'], 20, 2);
            $p2 = substr($depiction['texture_name'], 17, 3);
            if (!($data = $this->check('/tmp/' . $depiction['texture_name']))) {
                $content = @file_get_contents('http://content.furniprice.ru/catalog/materials/' . $p1 . '/' . $p2 . '/' . $depiction['texture_name']);
                if (!$content) {
                    return '';
                }
                file_put_contents('/tmp/' . $depiction['texture_name'], $content);
                $data = $this->upload('/tmp/' . $depiction['texture_name']);
            }
            if (!empty($data->id)) {
                return $data ? ($data->id . '.' . $data->format) : '';
            }
        }
        return '';
    }

    protected function getColorFromRal(...$args)
    {
        $depiction = $args[0];
        if ($depiction['colors']) {
            $colors = explode(',', trim($depiction['colors'], ','));
            $sql = 'select * from color where code in (\'' . implode('\',\'', $colors) . '\')';
            $rows = Yii::$app->dbMedia->createCommand($sql)->queryAll();
            $rals = ArrayHelper::getColumn($rows, 'id');
            $this->stdout(implode(',', $rals) . PHP_EOL, Console::FG_GREEN);
            return $rals ?? [];
        }
        return [];
    }

    private function check($file)
    {
        $args['id'] = md5($file);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://media-service.furniprice.local/image/' . $args['id']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $jsondata = json_decode($data);
        curl_close($ch);
        if (isset($jsondata->id) && !is_null($jsondata->id)) {
            $this->stdout('    -> Image exists: ' . $jsondata->id . '.' . $jsondata->format . PHP_EOL, Console::FG_GREEN);
            return $jsondata;
        }
        return false;
    }

    private function upload($file)
    {
        $args['id'] = md5($file);
        $args['image'] = new \CurlFile($file);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://media-service.furniprice.local/image');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $jsondata = json_decode(curl_exec($ch));
        curl_close($ch);
        if (!empty($jsondata->id)) {
            $this->stdout('    done: ' . $jsondata->id . '.' . $jsondata->format . PHP_EOL, Console::FG_YELLOW);
        } else {
            $this->stdout('    something wrong! ' . $file . PHP_EOL, Console::FG_RED);
        }
        return $jsondata ?? null;
    }

    private function getPropertyValue($data, $propName)
    {
        if (method_exists($this, $propName)) {
            $value = [$this, $propName];
        } elseif (isset($data[$propName])) {
            $value = $data[$propName] == '\N' ? null : $data[$propName];
        }

        return $value ?? null;
    }

    private function initEntityData()
    {
        foreach ($this->domainModels as $index => $elementClass) {
            $elementClassId = $this->getElementClassId($elementClass['context'], $elementClass['name']);
            $this->initProperties($elementClassId, $index);
        }
    }

    private function initProperties($elementClassId, $modelName)
    {
        $existingProps = PropertyRecord::find()->indexBy('sysname')->all();
        $properties = [];
        $propertiesIds = [];

        if (!isset($this->baseProperties[$modelName])) {
            throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
        }

        foreach ($this->baseProperties[$modelName] as $needProperty) {
            $property = $this->getProperty($existingProps[$needProperty]);
            $properties[$property->sysname] = $property;
            $propertiesIds[] = $property->id;
        }
dump($properties);
        foreach ($this->fieldsGroup[$modelName] as $fieldGroup => $fieldsMapping) {

            foreach ($fieldsMapping as $propName => $fieldName) {

                if (is_array($fieldName)) {
                    foreach ($fieldName as $key => $value) {
                        $this->fieldsToPoperties[$modelName][$fieldGroup][$value] = [$properties[$propName]->id => $key];
                    }
                } else {
                    $this->fieldsToPoperties[$modelName][$fieldGroup][$fieldName] = $properties[$propName]->id;
                }
            }
        }
    }

    /**
     * Получаем ID Element_class, если не существует - добавялем
     * @param $context
     * @param $name
     * @return int
     */
    private function getElementClassId($context, $name)
    {
        $elementClass = ElementClassRecord::findOne([
            'name' => $className = $context . '\\' . $name,
        ]);

        if (!$elementClass) {
            throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
        }

        return $elementClass->id ?? null;
    }

    protected function createSupportImportTables()
    {
        $query = 'CREATE TABLE IF NOT EXISTS materialcollection_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'CREATE TABLE IF NOT EXISTS materialcategory_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'CREATE TABLE IF NOT EXISTS coloration_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'CREATE TABLE IF NOT EXISTS product_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();
    }

    protected function clearSupportImportTables()
    {
        $query = 'TRUNCATE TABLE materialcollection_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'TRUNCATE TABLE materialcategory_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'TRUNCATE TABLE coloration_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'TRUNCATE TABLE product_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();
    }

    private function stdoutText($text, $id, $elementId)
    {
        $this->stdout($text);
        $this->stdout($id, Console::FG_CYAN);
        $this->stdout(' as Element ');
        $this->stdout($elementId, Console::FG_GREEN);
        $this->stdout("\n");
    }

    private function stdoutTextExist($text, $id)
    {
        $this->stdout($text);
        $this->stdout($id, Console::FG_YELLOW);
        $this->stdout("\n");
    }

    private function stdoutBindElementToParentElement($childId, $parentId, $childName = 'child', $parentName = 'parent')
    {
        $this->stdout("Bind to $parentName ");
        $this->stdout($parentId, Console::FG_CYAN);
        $this->stdout(" $childName with id = ");
        $this->stdout($childId, Console::FG_CYAN);
        $this->stdout("\n");
    }

    private function stdoutBindAssociatedElements($function, $childId, $parentId)
    {
        $this->stdout("{$function}: ");
        $this->stdout($childId, Console::FG_CYAN);
        $this->stdout(" <==> ");
        $this->stdout($parentId, Console::FG_CYAN);
        $this->stdout("\n");
    }

}
