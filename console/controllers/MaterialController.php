<?php

namespace console\controllers;

use commonprj\components\catalog\entities\material\Material;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplateDBRepository;
use commonprj\components\core\entities\associationRelationValue\AssociationRelationValue;
use commonprj\components\core\entities\element\Element;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyArrayValue\PropertyArrayValue;
use commonprj\components\core\entities\propertyValueBoolean\PropertyValueBoolean;
use commonprj\components\core\entities\propertyValueFloat\PropertyValueFloat;
use commonprj\components\core\entities\propertyValueInt\PropertyValueInt;
use commonprj\components\core\entities\propertyValueListItem\PropertyValueListItem;
use commonprj\components\core\entities\propertyValueString\PropertyValueString;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\components\core\models\ElementClassRecord;
use commonprj\components\core\models\ElementRecord;
use commonprj\components\core\models\PropertyArrayRecord;
use commonprj\components\core\models\PropertyRecord;
use commonprj\components\core\models\PropertyTypeRecord;
use commonprj\components\core\models\PropertyValueBigintRecord;
use Yii;
use yii\db\Exception;
use yii\helpers\Console;

class MaterialController extends BaseImportController
{

    const MULTIPLICITY_ARRAY = 3;
    const ATTR_TYPE_STRING = 'string';
    const ATTR_TYPE_LIST = 'list';
    const ATTR_TYPE_FLOAT = 'float';
    const ATTR_TYPE_INT = 'int';
    const ATTR_TYPE_ARRAY_INT = 'int';
    const ATTR_TYPE_BOOLEAN = 'boolean';
    const ATTR_TYPES = [
        self::ATTR_TYPE_STRING    => PropertyValueString::class,
        self::ATTR_TYPE_LIST      => PropertyValueListItem::class,
        self::ATTR_TYPE_FLOAT     => PropertyValueFloat::class,
        self::ATTR_TYPE_INT       => PropertyValueInt::class,
        self::ATTR_TYPE_ARRAY_INT => PropertyValueInt::class,
        self::ATTR_TYPE_BOOLEAN   => PropertyValueBoolean::class,
    ];
    const ATTR_TYPES_TRANSFORMED = [
        self::ATTR_TYPE_ARRAY_INT => self::ATTR_TYPE_INT,
    ];
    const ATTR_TEMPLATE = 'templateParams';
    const ATTR_ORIGIN_INDEX = 'old_name';
    var $attributesSchema = [
        'materialGroup' => [
            [
                'elements'   => ['furniture_facing', 'furniture_skeleton', 'upholstery_materials', 'element_filler'],
                'attributes' => ['title'],
            ],
        ],
        'material' => [
            ['elements' =>
                 [
                     'wood',
//                     'wood_grain',
//                     'wood_grain_cedar',
//                     'wood_grain_fir',
//                     'wood_grain_pine',
//                     'wood_grain_redwood',
//                     'wood_grain_ash',
//                     'wood_grain_birch',
//                     'wood_cherry',
//                     'wood_grain_mahagon',
//                     'wood_grain_maple',
//                     'wood_grain_oak',
//                     'wood_grain_poplar',
//                     'wood_grain_teak',
//                     'wood_grain_nut',
//                     'wood_grain_linden',
//                     'wood_grain_beech',
//                     'wood_mdf',
//                     'wood_chipboard',
                 ],
             'template' => 'wood',
             'attributes' => ['title',],
            ],
            ['elements' =>
                 [
                     'textile',
                     'leather',
                     'suede',
//                     'textile_cotton',
//                     'textile_cotton_velveteen',
//                     'textile_wool_velour',
//                     'leather_natural',
//                     'leather_synthetic',
//                     'leather_synthetic_eco',
//                     'suede_synthetic',
//                     'textile_jacquard',
//                     'textile_jacquard_micro',
//                     'textile_jacquard_thermo',
//                     'textile_microfiber',
//                     'textile_rogozhka',
//                     'textile_scotch_gard',
//                     'textile_chenille',
//                     'textile_flock',
//                     'textile_gobelin',
//                     'textile_gabardine',
//                     'textile_canvas',
//                     'textile_wool',
//                     'textile_wool_velour',
//                     'felt',
//                     'polyester',
//                     'latex',
//                     'leather',
                 ],
             'template' => 'fabric',
             'attributes' => ['title',],
            ],
//            ['elements' =>
//                 [
//                     'metal',
////                     'metal_aluminum',
////                     'metal_bronze',
////                     'metal_steel',
////                     'metal_cast_iron',
////                     'metal_iron',
//                 ],
//             'template' => 'metal',
//            ],
            ['elements' =>
                 [
                     'glass',
                 ],
             'template' => 'glass',
             'attributes' => ['title',],
            ],
        ],
    ];
    var $attributeParams = [
        'title'                      => [
            'type'        => self::ATTR_TYPE_STRING,
            'name'        => 'Title',
            'description' => 'Title / Наименование',
        ],
//        'colorResistance'            => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Color resistance',
//            'description' => 'Цветоустойчивость',
//            'variants'    => [
//                '1' => 'Неустойчивые <3',
//                '2' => 'Невысокая устойчивость 3-4',
//                '3' => 'Устойчивые 4-5',
//                '4' => 'Высокая устойчивость >5',
//            ],
//        ],
//        'wearResistance'             => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Wear resistance',
//            'description' => 'Износоустойчивость',
//            'variants'    => [
//                '1' => 'Декоративные. Менее 10 000 циклов',
//                '2' => 'Деликатные. От 10 000 до 15 000 циклов',
//                '3' => 'Повседневной эксплуатации. От 15 000 до 25 000 циклов',
//                '4' => 'Интенсивной эксплуатации. От 25 000 до 30 000 циклов',
//                '5' => 'Коммерческое использование. Выше 30 000 циклов',
//            ],
//        ],
        'fireResistance'             => [
            'type'        => self::ATTR_TYPE_BOOLEAN,
            'name'        => 'Fire resistance',
            'description' => 'Огнеустойчивость',
        ],
        'fabricDensity'              => [
            'type'        => self::ATTR_TYPE_INT,
            'name'        => 'Fabric density of a material in grams per square meter',
            'description' => 'Плотность ткани Грамм на квадратный метр',
        ],
//        'fabricStructure'            => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => '`Natural or synthetic` type of a material',
//            'description' => 'Состав материала (по сути тип материала' .
//                ' с точки зрения натуральный он или синтетический)',
//            'variants'    => [
//                '1' => 'Натуральные',
//                '2' => 'Синтетические',
//                '3' => 'Искусственные',
//                '4' => 'Смешанные',
//            ],
//        ],
//        'pvcFilmFinishing'           => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'PVC film finishing',
//            'description' => 'Отделка пленки ПВХ',
//            'variants'    => [
//                1 => 'Однотонная',
//                2 => 'С рисунком',
//                3 => 'Софт-тач',
//                4 => '3Д',
//                5 => 'Голограмма',
//                6 => 'Патина',
//                7 => 'Тиснение',
//            ],
//        ],
//        'pvcFilmType'                => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Type of PVC film',
//            'description' => 'Тип пленки ПВХ',
//            'variants'    => [
//                1 => 'Матовая',
//                2 => 'Глянцевая',
//                3 => 'Текстурная',
//                4 => 'Зеркальная',
//            ],
//        ],
        'pvcFilmThickness'           => [
            'type'        => self::ATTR_TYPE_FLOAT,
            'name'        => 'Thickness of PVC film',
            'description' => 'Толщина пленки ПВХ (мм)',
        ],
//        'glassBrand'                 => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Glass Brands',
//            'description' => 'Бренды стекла',
//            'variants'    => [
//                1 => 'Лакобель',
//                2 => 'Стемалит',
//                3 => 'Оракал',
//            ],
//        ],
//        'glassProperty'              => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Properties of glass',
//            'description' => 'Свойства стекла',
//            'variants'    => [
//                1 => 'Закаленное',
//                2 => 'Ударопрочное',
//                3 => 'Термоустйчивое',
//            ],
//        ],
//        'glassDecoration'            => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Glass decoration',
//            'description' => 'Отделка стекла',
//            'variants'    => [
//                1 => 'Деколь',
//                2 => 'Фьюзинг',
//                3 => 'Шелкография',
//                4 => 'Витраж',
//                5 => 'Покраска',
//                6 => 'Триплекс',
//                7 => 'Термопечать',
//                8 => 'Лакобель',
//            ],
//        ],
//        'glassKind'                  => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Kind of glass',
//            'description' => 'Вид стекла',
//            'variants'    => [
//                1 => 'Прозрачное',
//                2 => 'Цветное',
//                3 => 'Тонированное',
//                4 => 'Зеркальное',
//            ],
//        ],
//        'facadeDesign'               => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Facade design',
//            'description' => 'Конструкция фасада',
//            'variants'    => [
//                1 => 'Рамочный',
//                2 => 'Сплошной',
//            ],
//        ],
//        'additionalFinishingOptions' => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Additional finishing options',
//            'description' => 'Дополнительные параметры отделки',
//            'variants'    => [
//                1 => 'Антикатура (искусственное старение)',
//                2 => 'Софт-тач',
//                3 => 'Паспарту',
//                4 => 'Аэрография',
//                5 => 'Фотопечать',
//                6 => 'Инкрустация',
//            ],
//        ],
//        'hidingPower'                => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Hiding power',
//            'description' => 'Укрывистость',
//            'variants'    => [
//                1 => 'Прозрачная',
//                2 => 'Непрозрачная',
//                3 => 'Открытые поры',
//                4 => 'Закрытые поры',
//            ],
//        ],
//        'glossDegree'                => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Degree of gloss',
//            'description' => 'Степень глянца',
//            'variants'    => [
//                1 => 'Высокий глянец',
//                2 => 'Обычный глянец',
//                3 => 'Полуглянец',
//                4 => 'Полуматовый',
//                5 => 'Матовый',
//                6 => 'Глубокоматовый',
//            ],
//        ],
//        'texture'                    => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Types of textures',
//            'description' => 'Вид текстуры',
//            'variants'    => [
//                1 => 'Слабовыраженная',
//                2 => 'Кольца',
//                3 => 'Полосы',
//                4 => 'Пламя',
//                5 => 'Птичий глаз',
//                6 => 'Скрипичная спинка',
//                7 => 'Муар',
//            ],
//        ],
//        'patternMaterial'            => [
//            'type'        => self::ATTR_TYPE_LIST,
//            'name'        => 'Types of patterns',
//            'description' => 'Вид рисунка',
//            'variants'    => [
//                1  => 'Однотонные',
//                2  => 'Многотонные',
//                3  => 'Клетка',
//                4  => 'Полоска',
//                5  => 'Цветочный рисунок',
//                6  => 'Рисунок с геометрическими фигурами',
//                7  => 'Пейзажи, животные',
//                8  => 'Детский рисунок',
//                9  => 'Сложный рисунок',
//                10 => 'Камень',
//                11 => 'Вензеля',
//                12 => 'Рогожка',
//            ],
//        ],
    ];
    private $tmpTableName = 'material_mapping_named';
    private $tmpTableFieldOriginName = 'origin_name';
    private $tmpTableFieldOriginType = 'text';
    private $elementClassMapping = [];
    private $elementClassMappingReverted = [];
    private $elementMapping = [];
    private $elementMappingReverted = [];
    private $elementClassNameSchema = [];
    private $elementClassNameSchemaReverted = [];
    private $propertyTypeMapping = [];
    private $propertyTypeMappingReverted = [];
    private $propertyMapping = [];
    private $propertyMappingReverted = [];
    private $propertyMappingList = [];
    private $dbConnections = [
        'db',
        'dbI18n',
        // 'dbMedia',
        'dbSearch',
        'import',
    ];
    private $data = [
        'materialGroup'    => [
            'context'  => 'catalog',
            'name'     => 'MaterialGroup',
            'elements' => [
                'furniture_facing'     => [
                    'title' => ['rus' => 'Материал отделки', 'eng' => 'Furniture facing'],
                ],
                'furniture_skeleton'   => [
                    'title' => ['rus' => 'Материал каркаса', 'eng' => 'Furniture skeleton'],
                ],
                'upholstery_materials' => [
                    'title' => ['rus' => 'Материал обивки', 'eng' => 'Upholstery materials'],
                ],
                'element_filler'       => [
                    'title' => ['rus' => 'Наполнитель', 'eng' => 'Filler materials'],
                ],
            ],
        ],
        'material'         => [
            'context'  => 'catalog',
            'name'     => 'Material',
            'elements' => [
                'wood'              => [
                    'title'                     => ['rus' => 'Дерево', 'eng' => 'Wood'],
                    '_relations_materialGroup_' => [
                        'furniture_facing', 'furniture_skeleton',
                    ],
                ],
                'wood_grain'        => [
                    'title'                 => ['rus' => 'Древесный массив', 'eng' => 'Wood grain'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Массив',
                ],
                'wood_grain_ash'    => [
                    'title'                 => ['rus' => 'Ясень', 'eng' => 'Ash-tree'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Ясень',
                ],
                'wood_grain_beech'  => [
                    'title'                 => ['rus' => 'Бук', 'eng' => 'Beech'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Бук',
                ],
                'wood_grain_birch'  => [
                    'title'                 => ['rus' => 'Берёза', 'eng' => 'Birch'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Берёза',
                ],
                'wood_cherry'       => [
                    'title'                 => ['rus' => 'Вишня', 'eng' => 'Cherry'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Вишня',
                ],
                'wood_grain_poplar' => [
                    'title'                 => ['rus' => 'Тополь', 'eng' => 'Poplar'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Тополь',
                ],
                'wood_grain_pine'   => [
                    'title'                 => ['rus' => 'Сосна', 'eng' => 'Pine'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Сосна',
                ],
                'wood_grain_nut'    => [
                    'title'                 => ['rus' => 'Орех', 'eng' => 'Nut'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Орех',
                ],
                'wood_grain_linden' => [
                    'title'                 => ['rus' => 'Липа', 'eng' => 'Linden'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Липа',
                ],

                'wood_grain_fir'           => [
                    'title'                 => ['rus' => 'Пихта', 'eng' => 'Fir'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Пихта',
                ],
                'wood_grain_teak'          => [
                    'title'                 => ['rus' => 'Тик', 'eng' => 'Teak'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Тик',
                ],
                'wood_grain_mahagon'       => [
                    'title'                 => ['rus' => 'Махагон', 'eng' => 'Mahagon'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Махагон',
                ],
                'wood_grain_oak'           => [
                    'title'                 => ['rus' => 'Дуб', 'eng' => 'Oak'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Дуб',
                ],
                'wood_grain_cedar'         => [
                    'title'                 => ['rus' => 'Кедр', 'eng' => 'Cedar'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Кедр',
                ],
                'wood_grain_maple'         => [
                    'title'                 => ['rus' => 'Клён', 'eng' => 'Maple'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Клён',
                ],
                'wood_grain_redwood'       => [
                    'title'                 => ['rus' => 'Красное дерево', 'eng' => 'Redwood'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_grain',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Красное дерево',
                ],
                'wood_plywood'             => [
                    'title'                 => ['rus' => 'Фанера', 'eng' => 'Plywood'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Фанера',
                ],
                'wood_veneer'              => [
                    'title'                 => ['rus' => 'Шпон', 'eng' => 'Veneer'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Шпон',
                ],
                'wood_mdf'                 => [
                    'title'                     => ['rus' => 'Древесноволокнистая плита (МДФ)', 'eng' => 'Medium Density Fibreboard (MDF)'],
                    '_relations_materialGroup_' => [
                        'furniture_facing', 'furniture_skeleton',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'МДФ',
                ],
                'wood_chipboard'           => [
                    'title'                     => ['rus' => 'Древесно-стружечная плита (ДСП)', 'eng' => 'Chipboard'],
                    '_relations_materialGroup_' => [
                        'furniture_facing', 'furniture_skeleton',
                    ],
                ],
                'wood_chipboard_laminated' => [
                    'title'                 => ['rus' => 'Ламинированная древесно-стружечная плита (ЛДСП)', 'eng' => 'Laminated chipboard'],
                    '_relations_hierarchy_' => [
                        'parent' => 'wood_chipboard',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'ЛДСП',
                ],
                'stone'                    => [
                    'title'                     => ['rus' => 'Камень', 'eng' => 'Stone'],
                    '_relations_materialGroup_' => [
                        'furniture_facing',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Камень',
                ],
                'stone_synthetic'          => [
                    'title'                 => ['rus' => 'Искусственный камень', 'eng' => 'Synthetic stone'],
                    '_relations_hierarchy_' => [
                        'parent' => 'stone',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Искусственный камень',
                ],
                'stone_natural'            => [
                    'title'                 => ['rus' => 'Натуральный камень', 'eng' => 'Natural stone'],
                    '_relations_hierarchy_' => [
                        'parent' => 'stone',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Натуральный камень',
                ],
                'stone_natural_lazurite'   => [
                    'title'                 => ['rus' => 'Лазурит', 'eng' => 'Lazurite'],
                    '_relations_hierarchy_' => [
                        'parent' => 'stone_natural',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Лазурит',
                ],
                'glass'                    => [
                    'title'                     => ['rus' => 'Стекло', 'eng' => 'Glass'],
                    '_relations_materialGroup_' => [
                        'furniture_facing',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Стекло',
                ],
                'glass_fiberglass'         => [
                    'title'                 => ['rus' => 'Стекловолокно', 'eng' => 'Fiberglass'],
                    '_relations_hierarchy_' => [
                        'parent' => 'stone',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Стекловолокно',
                ],
                'marble_and_granite'       => [
                    'title'                     => ['rus' => 'Мрамор / Гранит', 'eng' => 'Marble / Granite'],
                    '_relations_materialGroup_' => [
                        'furniture_facing',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Мрамор/Гранит',
                ],
                'ceramic_tile'             => [
                    'title'                     => ['rus' => 'Керамическая плитка', 'eng' => 'Ceramic tile'],
                    '_relations_materialGroup_' => [
                        'furniture_facing',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Керамическая плитка',
                ],
                'plastic'                  => [
                    'title'                     => ['rus' => 'Пластик', 'eng' => 'Plastic'],
                    '_relations_materialGroup_' => [
                        'furniture_facing', 'furniture_skeleton',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Пластик',
                ],
                'metal'                    => [
                    'title'                     => ['rus' => 'Металл', 'eng' => 'Metal'],
                    '_relations_materialGroup_' => [
                        'furniture_facing', 'furniture_skeleton',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Металл',
                ],
                'metal_aluminum'           => [
                    'title'                 => ['rus' => 'Алюминий', 'eng' => 'Aluminum'],
                    '_relations_hierarchy_' => [
                        'parent' => 'metal',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Алюминий',
                ],
                'metal_bronze'             => [
                    'title'                 => ['rus' => 'Бронза', 'eng' => 'Bronze'],
                    '_relations_hierarchy_' => [
                        'parent' => 'metal',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Бронза',
                ],
                'metal_steel'              => [
                    'title'                 => ['rus' => 'Сталь', 'eng' => 'Steel'],
                    '_relations_hierarchy_' => [
                        'parent' => 'metal',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Сталь',
                ],
                'metal_cast_iron'          => [
                    'title'                 => ['rus' => 'Чугун', 'eng' => 'Cast iron'],
                    '_relations_hierarchy_' => [
                        'parent' => 'metal',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Чугун',
                ],
                'metal_iron'               => [
                    'title'                 => ['rus' => 'Железо', 'eng' => 'Cast iron'],
                    '_relations_hierarchy_' => [
                        'parent' => 'metal',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Железо',
                ],
                'textile'                  => [
                    'title'                     => ['rus' => 'Текстиль', 'eng' => 'Textile'],
                    '_relations_materialGroup_' => [
                        'upholstery_materials',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Текстиль',
                ],
                'textile_cotton'           => [
                    'title'                 => ['rus' => 'Хлопок', 'eng' => 'Cotton'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Хлопок',
                ],
                'textile_cotton_velveteen' => [
                    'title'                 => ['rus' => 'Вельвет', 'eng' => 'Velveteen'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile_cotton',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Вельвет',
                ],
                'textile_scotch_gard'      => [
                    'title'                 => ['rus' => 'Скотчгард', 'eng' => 'Scotch-gard'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Скотчгард',
                ],
                'textile_chenille'         => [
                    'title'                 => ['rus' => 'Шенилл', 'eng' => 'Chenille'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Шенилл',
                ],
                'textile_chenille_easy'    => [
                    'title'                 => ['rus' => 'Легкий шенилл', 'eng' => 'Easy chenille'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile_chenille',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Легкий шенилл',
                ],
                'textile_microfiber'       => [
                    'title'                 => ['rus' => 'Микрофибра', 'eng' => 'microfiber'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Микрофибра',
                ],
                'textile_flock'            => [
                    'title'                 => ['rus' => 'Флок', 'eng' => 'Flock'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Флок',
                ],
                'textile_gobelin'            => [
                    'title'                 => ['rus' => 'Гобелен', 'eng' => 'Gobelin'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Флок',
                ],
                'textile_gabardine'            => [
                    'title'                 => ['rus' => 'Габардин', 'eng' => 'Gabardine'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Флок',
                ],
                'textile_canvas'            => [
                    'title'                 => ['rus' => 'Канвас', 'eng' => 'Canvas'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Флок',
                ],
                'textile_jacquard'         => [
                    'title'                 => ['rus' => 'Жаккард', 'eng' => 'Jacquard'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Жаккард',
                ],
                'textile_jacquard_micro'   => [
                    'title'                 => ['rus' => 'Микрожаккард', 'eng' => 'Micro-Jacquard'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile_jacquard',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Микрожаккард',
                ],
                'textile_jacquard_thermo'  => [
                    'title'                 => ['rus' => 'Терможаккард', 'eng' => 'Thermo-jacquard'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile_jacquard',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Терможаккард',
                ],
                'textile_rogozhka'         => [
                    'title'                 => ['rus' => 'Рогожка', 'eng' => 'Rogozhka'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Рогожка',
                ],
                'textile_wool'             => [
                    'title'                 => ['rus' => 'Шерстяные ткани', 'eng' => 'Wool'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Шерстяные ткани',
                ],
                'textile_wool_velour'      => [
                    'title'                 => ['rus' => 'Велюр', 'eng' => 'Velour'],
                    '_relations_hierarchy_' => [
                        'parent' => 'textile_wool',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Велюр',
                ],
                'leather'                  => [
                    'title'                     => ['rus' => 'Кожа', 'eng' => 'Leather'],
                    '_relations_materialGroup_' => [
                        'upholstery_materials',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Кожа',
                ],
                'pvc_film'                  => [
                    'title'                     => ['rus' => 'Пленка ПВХ', 'eng' => 'PVC film'],
                    '_relations_materialGroup_' => [
                        'upholstery_materials',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Пленка ПВХ',
                ],
                'leather_natural'          => [
                    'title'                 => ['rus' => 'Натуральная кожа', 'eng' => 'Natural leather'],
                    '_relations_hierarchy_' => [
                        'parent' => 'leather',
                    ],
                    'old_'                  => 'Кожа натуральная',
                    self::ATTR_ORIGIN_INDEX => 'Кожа натуральная',
                ],
                'leather_synthetic'        => [
                    'title'                 => ['rus' => 'Искусственная кожа', 'eng' => 'Synthetic leather'],
                    '_relations_hierarchy_' => [
                        'parent' => 'leather',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Искусственная кожа',
                ],
                'leather_synthetic_eco'    => [
                    'title'                 => ['rus' => 'Эко кожа', 'eng' => 'Eco leather'],
                    '_relations_hierarchy_' => [
                        'parent' => 'leather_synthetic',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'ЭКО кожа',
                ],

                'suede'                                => [
                    'title'                     => ['rus' => 'Замша', 'eng' => 'Suede'],
                    '_relations_materialGroup_' => [
                        'upholstery_materials',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Замша',
                ],
                'suede_natural'                        => [
                    'title'                 => ['rus' => 'Натуральная замша', 'eng' => 'Natural suede'],
                    '_relations_hierarchy_' => [
                        'parent' => 'suede',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Натуральная кожа',
                ],
                'suede_synthetic'                      => [
                    'title'                 => ['rus' => 'Искусственная замша', 'eng' => 'Synthetic suede'],
                    '_relations_hierarchy_' => [
                        'parent' => 'suede',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Искусственная замша',
                ],
                'foam_and_foam_rubber'                 => [
                    'title'                     => ['rus' => 'ППУ / поролон', 'eng' => 'Foam rubber'],
                    '_relations_materialGroup_' => [
                        'upholstery_materials',
                    ],
                ],
                'foam_rubber_polyurethane'             => [
                    'title'                 => ['rus' => 'ППУ', 'eng' => 'Polyurethane foam'],
                    '_relations_hierarchy_' => [
                        'parent' => 'foam_and_foam_rubber',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'ППУ',
                ],
                'foam_rubber_polyurethane_with_memory' => [
                    'title'                 => ['rus' => 'ППУ с эффектом памяти', 'eng' => 'Polyurethane foam with memory option'],
                    '_relations_hierarchy_' => [
                        'parent' => 'foam_rubber_polyurethane',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'ППУ с эффектом памяти',
                ],
                'foam_and_foam_rubber_foam'            => [
                    'title'                 => ['rus' => 'Поролон', 'eng' => 'Foam'],
                    '_relations_hierarchy_' => [
                        'parent' => 'foam_and_foam_rubber',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Поролон',
                ],
                'latex'                                => [
                    'title'                     => ['rus' => 'Латекс', 'eng' => 'Latex'],
                    '_relations_materialGroup_' => [
                        'element_filler',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Латекс',
                ],
                'latex_natural'                        => [
                    'title'                 => ['rus' => 'Латекс натуральный', 'eng' => 'Natural latex'],
                    '_relations_hierarchy_' => [
                        'parent' => 'latex',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Латекс натуральный',
                ],
                'latex_synthetic'                      => [
                    'title'                 => ['rus' => 'Латекс искусственный', 'eng' => 'Synthetic latex'],
                    '_relations_hierarchy_' => [
                        'parent' => 'latex',
                    ],
                    self::ATTR_ORIGIN_INDEX => 'Латекс искусственный',
                ],
                'coconut_coir'                         => [
                    'title'                     => ['rus' => 'Кокосовая койра', 'eng' => 'Coconut Coir'],
                    '_relations_materialGroup_' => [
                        'element_filler',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Кокосовая койра',
                ],
                'styrofoam_in_granules'                => [
                    'title'                     => ['rus' => 'Пенополистирол в гранулах', 'eng' => 'Styrofoam in granules'],
                    '_relations_materialGroup_' => [
                        'element_filler',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Пенополистирол в гранулах',
                ],
                'goose_down'                           => [
                    'title'                     => ['rus' => 'Гусиный пух', 'eng' => 'Goose down'],
                    '_relations_materialGroup_' => [
                        'element_filler',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Гусиный пух',
                ],
                'felt'                                 => [
                    'title'                     => ['rus' => 'Войлок', 'eng' => 'Felt'],
                    '_relations_materialGroup_' => [
                        'element_filler',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Войлок',
                ],
                'polyester'                            => [
                    'title'                     => ['rus' => 'Синтепон', 'eng' => 'Polyester'],
                    '_relations_materialGroup_' => [
                        'element_filler',
                    ],
                    self::ATTR_ORIGIN_INDEX     => 'Синтепон',
                ],
            ],
        ],
    ];

    /**
     * Console controller description (default) method
     */
    public function actionIndex()
    {
        $this->stdout("\nДля загрузки стандартного набора данных категории \"Материалы\" наберите в консоли:\n");
        $this->stdout("php yii material/import\n\n", Console::FG_YELLOW);
        $this->stdout("\nДля загрузки набора данных в обнуленную систему наберите в консоли:\n");
        $this->stdout("php yii material/reset-db\n", Console::FG_YELLOW);
        $this->stdout("php yii migrate/up\n", Console::FG_YELLOW);
        $this->stdout("php yii furni/init\n", Console::FG_YELLOW);
        $this->stdout("php yii material/import\n", Console::FG_YELLOW);
    }

    /**
     * IMPORT ACTION
     * @return bool
     * @throws Exception|\Throwable
     */
    public function actionImport(): bool
    {


        #---------------------------------------------------------------------------------------------------------------
        // --. ЛИРИКА
        $this->initDomainData(); // @baseImportController
        $this->prepareTMPDataTables();
        #---------------------------------------------------------------------------------------------------------------

        #---------------------------------------------------------------------------------------------------------------
        // -. обработка контекста классов импортируемых данных .. создание маппинга - $this->elementClassNameSchema
        foreach ($this->data as $elementClassDataKey => $elementClassData) {

            $className = $elementClassData['context'] . '\\' . $elementClassDataKey;
            $fullClassName = ClassAndContextHelper::getFullClassNameByModelClass($className);

            $this->elementClassNameSchema[$elementClassDataKey] = $fullClassName;
            $this->elementClassNameSchemaReverted[$fullClassName] = $elementClassDataKey;
        }
        #---------------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        // 1. обработка всех классов импортируемых данных .. создание маппинга - $this->elementClassMapping
        foreach ($this->data as $elementClassDataKey => $elementClassData) {
            $elementClassAR = ElementClassRecord::findOne([
                'name' => $elementClassData['context'] . '\\' . $elementClassData['name'],
            ]);
            if (!$elementClassAR) {
                throw new Exception('No elementClassAR');
            }
            $this->elementClassMappingReverted[$elementClassDataKey] = $elementClassAR->id;
            $this->elementClassMapping[$elementClassAR->id] = $elementClassDataKey;
        }
        #---------------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        // 2. обработка типов атрубитов импортируемых данных .. создание маппинга - $this->propertyTypeMapping
        $this->updatePropertyTypeMapping();
        #---------------------------------------------------------------------------------------------------------------
        if (!$this->propertyMappingReverted) {
            foreach (PropertyRecord::find()->all() as $property) {
                $this->propertyMappingReverted[$property->sysname] = $property->id;
                $this->propertyMapping[$property->id] = $property->sysname;
            }
        }
        #---------------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        // 3. обработка атрубитов импортируемых данных .. создание маппинга - $this->propertyMapping (+propertyMappingList)
        foreach ($this->attributesSchema as $elementClassDataKey => $elementClassDataStack) {
            foreach ($elementClassDataStack as $elementClassData) {
                dump($elementClassData);
                foreach ($elementClassData['attributes'] as $attributeIndex) {
                    if (!isset($this->attributeParams[$attributeIndex])) {
                        throw new Exception('No attributeParams');
                    }
                    $attributeParams = $this->attributeParams[$attributeIndex];
                    if (!$attributeParams['type'] || !isset($this->propertyTypeMappingReverted[$attributeParams['type']])) {
                        if (!isset($this->propertyTypeMappingReverted[self::ATTR_TYPES_TRANSFORMED[$attributeParams['type']]])) {
                            throw new Exception('PropertyType is wrong');
                        }
                        $attributeParams['type'] = self::ATTR_TYPES_TRANSFORMED[$attributeParams['type']];
                    }
                    $propertyTypeId = $this->propertyTypeMappingReverted[$attributeParams['type']];
                    echo $propertyTypeId . ' - ' . $attributeParams['type'] . PHP_EOL;
                    // обработка атрубитов импортируемых данных .. создание маппинга - $this->propertyMapping
                    // обработка атрубитов импортируемых данных .. создание маппинга - $this->propertyMappingList
                    $this->checkProperty($attributeIndex, $attributeParams, $propertyTypeId);
                }
            }
        }
        #---------------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        // 4. обработка элементов импортируемых данных .. создание маппинга - $this->elementMapping
        foreach ($this->data as $elementClassDataKey => $elementClassData) {
            dump($elementClassData);
            $elementClassId = $this->elementClassMappingReverted[$elementClassDataKey];
            foreach ($elementClassData['elements'] as $elementKey => $elementAttributes) {
                $this->checkElement($elementClassId, $elementKey, $elementAttributes);
            }
        }
        #---------------------------------------------------------------------------------------------------------------

        #---------------------------------------------------------------------------------------------------------------
        // 5. работа с шаблонами материалов
        $relTemplate = $this->propertyMappingReverted['Material2MaterialTemplate'];
        foreach ($this->attributesSchema['material'] as $dataRow) {

            $materialTemplate = MaterialTemplate::findOne(['name'=>$dataRow['template']]);
            $templateElementId = $materialTemplate->id;

            // 5. привязка материалов к шаблонам материалов
            foreach ($dataRow['elements'] as $element) {
                $elementId = $this->elementMappingReverted[$element]['element_id'];
                if (!PropertyValueBigintRecord::findOne([
                    'element_id'  => $elementId,
                    'value'       => $templateElementId,
                    'property_id' => $relTemplate,
                ])) {
                    $ff = (new AssociationRelationValue([
                        'elementId'  => $elementId,
                        'value'      => $templateElementId,
                        'propertyId' => $relTemplate,
                    ]));
                    $ff->save();
                }
            }

        }
        #---------------------------------------------------------------------------------------------------------------


        #---------------------------------------------------------------------------------------------------------------
        // 6. работа с группами материалов
        $relGroup = $this->propertyMappingReverted['Material2MaterialGroup'];
        foreach ($this->data['material']['elements'] as $elementName => $dataRow) {
            $elementId = $this->elementMappingReverted[$elementName]['element_id'];
            if (isset($dataRow['_relations_materialGroup_'])) {
                foreach ($dataRow['_relations_materialGroup_'] as $groupName) {
                    $groupElementId = $this->elementMappingReverted[$groupName]['element_id'];
                    if (!PropertyValueBigintRecord::findOne([
                        'element_id'  => $elementId,
                        'value'       => $groupElementId,
                        'property_id' => $relGroup,
                    ])) {
                        $ff = (new AssociationRelationValue([
                            'elementId'  => $elementId,
                            'value'      => $groupElementId,
                            'propertyId' => $relGroup,
                        ]));
                        $ff->save();
                    }
                }
            }
        }
        #---------------------------------------------------------------------------------------------------------------

        #---------------------------------------------------------------------------------------------------------------
        // 7. работа с группами материалов
        $relHierarchy = $this->propertyMappingReverted['Material_hierarchy'];
        foreach ($this->data['material']['elements'] as $elementName => $dataRow) {
            $elementId = $this->elementMappingReverted[$elementName]['element_id'];

            if (isset($dataRow[self::ATTR_ORIGIN_INDEX])) {
                $this->elementMappingReverted[$elementName][self::ATTR_ORIGIN_INDEX] = $dataRow[self::ATTR_ORIGIN_INDEX];
                $this->elementMapping[$elementId][self::ATTR_ORIGIN_INDEX] = $dataRow[self::ATTR_ORIGIN_INDEX];
            }

            if (isset($dataRow['_relations_hierarchy_']['parent'])) {
                $parentElementId = $this->elementMappingReverted[$dataRow['_relations_hierarchy_']['parent']]['element_id'];
                if (!PropertyValueBigintRecord::findOne([
                    'element_id'  => $parentElementId,
                    'value'       => $elementId,
                    'property_id' => $relHierarchy,
                    'is_parent'   => true,
                ])) {
                    if (!(new Material([
                        'id' => $parentElementId,
                    ]))->bindChild($relHierarchy, $elementId)) {
                        throw new Exception('Bind was not created');
                    }
                }
            }
        }
        #---------------------------------------------------------------------------------------------------------------


        foreach ($this->elementMappingReverted as $key => $row) {
            if (isset($row[self::ATTR_ORIGIN_INDEX])) {
                $connection = Yii::$app->import;
                $connection->createCommand()
                    ->insert($this->tmpTableName, [
                        'element_id'                   => $row['element_id'],
                        $this->tmpTableFieldOriginName => $row[self::ATTR_ORIGIN_INDEX],
                    ])->execute();
            }
        }
        return $this->actionImportFinish();

        /**
         * print_r($this->elementClassNameSchemaReverted);
         * print_r($this->elementClassMappingReverted);
         * print_r($this->propertyTypeMappingReverted);
         * print_r($this->propertyMappingReverted);
         * print_r($this->propertyMappingList);
         * print_r($this->elementMappingReverted);
         * return $this->actionImportFinish();
         */

    }

    /**
     * Prepare temporary tables
     */
    public function prepareTMPDataTables()
    {
        $tb = $this->tmpTableName;
        $this->stdout("\n", Console::FG_YELLOW);
        $this->queryForImportNonSafe('DROP TABLE IF EXISTS ' . $tb);
        $this->stdOutColored(['1. DROP TABLE IF EXISTS ', '"' . $tb . '"']);
        $this->queryForImportNonSafe('CREATE TABLE ' . $tb . ' (element_id bigint, ' .
            $this->tmpTableFieldOriginName . ' ' . $this->tmpTableFieldOriginType . ')');
        $this->stdOutColored(['2. CREATED TABLE', '"' . $tb . '"']);
        $this->queryForImportNonSafe('TRUNCATE TABLE ' . $tb);
        $this->stdOutColored(['3. TRUNCATED TABLE', '"' . $tb . '"']);
    }

    /**
     * Make query to import db non-safe
     */
    protected function queryForImportNonSafe($query)
    {
        Yii::$app->import->createCommand($query)->queryAll();
    }

    /**
     * Make stdout with Colors
     */
    protected function stdOutColored($array)
    {
        $this->stdout($array[0], Console::FG_YELLOW);
        $this->stdout(" ");
        $this->stdout($array[1], Console::FG_PURPLE);
        $this->stdout("\n");
    }

    /**
     * updatePropertyMapping
     */
    public function updatePropertyTypeMapping()
    {
        if (!$this->propertyTypeMapping) {
            $records = PropertyTypeRecord::find()->all();
            if ($records) {
                foreach ($records as $record) {
                    /** @var PropertyTypeRecord $record */
                    $this->propertyTypeMappingReverted[$record->name] = $record->id;
                    $this->propertyTypeMapping[$record->id] = $record->name;
                }
            }
        }
    }

    /**
     * Add property if not exists
     * @param array $attributeParams
     * @param string $attributeIndex
     * @param int $propertyTypeId
     * @throws Exception|\yii\db\Exception|\Throwable
     */
    public function checkProperty(string $attributeIndex, array $attributeParams, int $propertyTypeId)
    {

        /** @var Property $property */
        try {
            $property = Property::findOne(['sysname' => $attributeIndex]);
        } catch (\Exception $e) {
            $property = new Property([
                'name'           => $attributeParams['name'],
                'sysname'        => $attributeIndex,
                'description'    => $attributeParams['description'],
                'isSpecific'     => false,
                'sortOrder'      => 1000,
                'propertyTypeId' => $propertyTypeId,
            ]);
            $property->save();
        }


        $this->propertyMappingReverted[$property->sysname] = $property->id;
        $this->propertyMapping[$property->id] = $property->sysname;

        if ($attributeParams['type'] == self::ATTR_TYPE_LIST) {
            if ($attributeParams['variants']) {
                $property->setListItems($attributeParams['variants']);
                $list = $property->getListItems();

                foreach ($list as $item) {
                    $this->propertyMappingList[$property->id][$item->id] = [
                        'value' => $item->item,
                        'label' => $item->label,
                    ];
                }
            }
        }

    }

    /**
     * Add element if not exists
     * @param int $elementClassId
     * @param string $elementKey
     * @param array $elementAttributes
     */
    public function checkElement(int $elementClassId, string $elementKey, array $elementAttributes)
    {
        $element = ElementRecord::find()->where([
            'is_active' => true,
        ])->leftJoin('element2element_class', 'element2element_class.element_id = element.id')
            ->andWhere(['element2element_class.element_class_id' => $elementClassId])
            ->leftJoin('property_value_string', 'property_value_string.element_id = element.id')
            ->andWhere(['property_value_string.property_id' => $this->propertyMappingReverted['title']])
            ->andWhere(['property_value_string.value' => $elementAttributes['title']])
            ->one();

        if (!$element) {
            /** @var Element $element */
            $element = new $this->elementClassNameSchema[$this->elementClassMapping[$elementClassId]]();
            $properties = [];

            foreach ($elementAttributes as $elementAttributeKey => $elementAttributeValue) {
                if (isset($this->attributeParams[$elementAttributeKey]) &&
                    isset(self::ATTR_TYPES[$this->attributeParams[$elementAttributeKey]['type']])) {
                    $properties[$elementAttributeKey] = $elementAttributeValue;
                }
            }

            $data = [
                'sortOrder' => 1000,
                'isActive'  => true,
            ];

            if ($properties) {
                $data['properties'] = $elementAttributes;
            }

            $element->load($data, '');
            $element->save();
        }

        $this->elementMapping[$elementClassId][$element->id] = $elementKey;
        $this->elementMappingReverted[$elementKey] = ['element_id'       => $element->id,
                                                      'element_class_id' => $elementClassId];
    }

    /**
     * Finish import action
     * @return bool
     */
    public function actionImportFinish(): bool
    {
        $this->stdout("FINISH!", Console::FG_YELLOW);
        $this->stdout("\n\n", Console::FG_YELLOW);
        return true;
    }

    public function actionResetDb()
    {
        foreach ($this->dbConnections as $dbConnection) {
            $db = Yii::$app->$dbConnection;
            $schemas = $db->schema->getTableSchemas();

            // Drop all foreign keys
            foreach ($schemas as $schema) {
                if ($schema->foreignKeys) {
                    foreach ($schema->foreignKeys as $name => $foreignKey) {
                        $db->createCommand()->dropForeignKey($name, $schema->name)->execute();
                    }
                }
            }

            // Drop all tables
            foreach ($schemas as $schema) {
                $db->createCommand()->dropTable($schema->name)->execute();
            }
        }

        $this->stdOutColored(['1. All foreign keys were', '"dropped"']);
        $this->stdOutColored(['1. All tables were', '"dropped"']);
        return $this->actionImportFinish();
    }

}
