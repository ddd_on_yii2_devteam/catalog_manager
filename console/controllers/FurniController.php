<?php

namespace console\controllers;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models;
use Yii;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\Console;

class FurniController extends Controller
{
    private $contextMap = null;
    private $elementClassMap = null;
    private $propertyTypeMap = null;
    private $propertyMap = null;

    public function actionIndex()
    {
        $help = "\nДля инициализации базы наберите в консоли:\n" .
            "php yii furni/init\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionInit()
    {
        $this->loadExistingData();

        $this->addContexts();
        $this->addElementClasses();
        $this->addPropertyTypes();
        $this->addProperties();
        $this->addProperty2elementClass();

        $this->syncColors();
    }

    private function syncColors()
    {
        Yii::$app->dbSearch->createCommand("TRUNCATE TABLE property_value_color")->queryOne();
        $property = Yii::$app->dbSearch->createCommand("SELECT id FROM property WHERE sysname='colors'")->queryOne();
        if (empty($property['id'])) {
            return false;
        }
        $property_id = $property['id'];

        $colors = Yii::$app->dbMedia->createCommand("SELECT * FROM color")->queryAll();

        foreach ($colors as $color) {
            Yii::$app->dbSearch->createCommand()->insert('{{%property_value_color}}', [
                'property_id' => $property_id,
                'value'       => $color['id'],
                'parent_id'   => 0,
            ])->execute();
            $this->stdout('Add color ' . $color['code'] . ': ');
            $this->stdout($color['id'] . PHP_EOL, Console::FG_CYAN);
        }

        $searchColors = Yii::$app->dbSearch->createCommand("SELECT * FROM property_value_color")->queryAll();
        foreach ($searchColors as $searchColor) {
            $parents[$searchColor['value']] = $searchColor['id'];
        }

        foreach ($colors as $color) {
            $parent_id = trim($color['parent_id']);
            if ($parent_id) {
                Yii::$app->dbSearch->createCommand()->update('{{%property_value_color}}', [
                    'parent_id' => $parents[$parent_id],
                ], ['value' => $color['id']])->execute();
            }
        }
        return true;
    }

    private function addContexts()
    {
        foreach ((array)$this->getBaseContexts() as $_ => $contextName) {

            if (!isset($this->contextMap[$contextName])) {
                $context = new models\ContextRecord(['name' => $contextName]);
                $context->save();
                $this->stdoutContextCreate($context);
                $this->contextMap[$context->name] = $context->id;
            }

        }
    }

    private function addElementClasses()
    {
        $elementClasses = [];

        foreach ($this->getElementClasses() as $contextName => $contextElementClasses) {

            foreach ($contextElementClasses as $contextElementClass) {
                $name = $contextName . '\\' . $contextElementClass['name'];
                if (!isset($this->elementClassMap[$name])) {
                    $elementClasses[] = [
                        'context_id'  => $this->contextMap[$contextName],
                        'name'        => $name,
                        'description' => $contextElementClass['description'] ?? '',
                        'parent'      => $contextElementClass['parent'] ?? null,
                    ];
                }
            }

        }

        while (true) {
            $elementClassesWithUnknownParent = $this->createElementClasses($elementClasses);

            if (empty($elementClassesWithUnknownParent)) {
                break;
            }

            if (count($elementClassesWithUnknownParent) == count($elementClasses)) {
                throw new ErrorException("Can't find parent");
            }

            $elementClasses = $elementClassesWithUnknownParent;
        }
    }

    private function createElementClasses($elementClassesData)
    {
        $elementClasses = [];
        $elementClassesWithUnknownParent = [];


        foreach ($elementClassesData as $elementClass) {

            if ($parent = $elementClass['parent']) {
                $parentId = $this->elementClassMap[$parent['context'] . '\\' . $parent['name']] ?? null;

                if (!$parentId) {
                    $elementClassesWithUnknownParent[] = $elementClass;
                } else {
                    $elementClass['parent'] = $parentId;
                    $elementClasses[] = $elementClass;
                }

            } else {
                $elementClass['parent'] = 0;
                $elementClasses[] = $elementClass;
            }

        }

        foreach ((array)$elementClasses as $elementClass) {
            $newElementClass = new ElementClass([
                'contextId'   => $elementClass['context_id'],
                'name'        => $elementClass['name'],
                'description' => $elementClass['description'],
                'parentId'    => $elementClass['parent'],
            ]);

            $newElementClass->save();
            $this->stdoutElementClassCreate($newElementClass);
            $this->elementClassMap[$newElementClass->name] = $newElementClass->id;

        }

        return $elementClassesWithUnknownParent;
    }

    private function addPropertyTypes()
    {
        $propertyTypes = [];

        foreach ($this->getPropertyTypes() as $propertyType) {

            if (!isset($this->propertyTypeMap[$propertyType['name']])) {
                $propertyTypes[] = [
                    'name'         => $propertyType['name'],
                    'parent'       => $propertyType['parent'] ?? null,
                    'elementClass' => isset($propertyType['elementClass'])
                        ? $this->elementClassMap[$propertyType['elementClass']]
                        : null,
                ];
            }

        }

        while (true) {
            $propertyTypesWithUnknownParent = $this->createPropertyTypes($propertyTypes);

            if (empty($propertyTypesWithUnknownParent)) {
                break;
            }

            if (count($propertyTypesWithUnknownParent) == count($propertyTypes)) {
                throw new ErrorException("Can't find parent");
            }

            $propertyTypes = $propertyTypesWithUnknownParent;
        }

    }

    private function createPropertyTypes($propertyTypesData)
    {
        $propertyTypes = [];
        $propertyTypesWithUnknownParent = [];

        foreach ($propertyTypesData as $propertyType) {

            if ($parent = $propertyType['parent']) {
                $parentId = $this->propertyTypeMap[$parent] ?? null;

                if (!$parentId) {
                    $propertyTypesWithUnknownParent[] = $propertyType;
                } else {
                    $propertyType['parent'] = $parentId;
                    $propertyTypes[] = $propertyType;
                }

            } else {
                $propertyType['parent'] = 0;
                $propertyTypes[] = $propertyType;
            }

        }

        foreach ((array)$propertyTypes as $propertyType) {
            $newPropertyType = new models\PropertyTypeRecord([
                'name'             => $propertyType['name'],
                'parent_id'        => $propertyType['parent'],
                'element_class_id' => $propertyType['elementClass'],
            ]);

            $newPropertyType->save();
            $this->stdoutPropertyTypeCreate($newPropertyType);
            $this->propertyTypeMap[$newPropertyType->name] = $newPropertyType->id;

        }

        return $propertyTypesWithUnknownParent;
    }


    private function addProperties()
    {
        foreach ((array)$this->getProperties() as $property) {
            $propertyName = $property['name'];
            $propertySysname = $property['sysname'];

            if (!isset($this->propertyMap[$propertySysname])) {

                $newProperty = new Property([
                    'name'            => $propertyName,
                    'sysname'         => $propertySysname,
                    'description'     => $property['description'],
                    'propertyUnitId'  => $property['property_unit_id'],
                    'isSpecific'      => $property['is_specific'],
                    'propertyTypeId'  => $this->propertyTypeMap[$property['property_type']],
                    'propertyGroupId' => $property['property_group_id'],
                    'sortOrder'       => $property['sort_order'],
                ]);
                $newProperty->save();

                $this->stdoutPropertyCreate($newProperty);
                $this->propertyMap[$newProperty->sysname] = $newProperty->id;
            }

        }

    }

    private function addProperty2elementClass()
    {


        foreach ((array)$this->getProperty2elementClasses() as $property2elementClass) {
            /**
             * @var models\Property2elementClassRecord $newProperty2elementClass
             */
            $newProperty2elementClass = new models\Property2elementClassRecord([
                'element_class_id' => $this->elementClassMap[$property2elementClass['elementClass']] ?? 0,
                'property_id'      => $this->propertyMap[$property2elementClass['propertySysname']] ?? 0,
                'is_parent'        => $property2elementClass['parent'],
            ]);

            try {
                $res = $newProperty2elementClass->save();
                if ($res !== true) {
                    dump($property2elementClass['elementClass']);
                    dump($property2elementClass['propertySysname']);
                    dump($this->elementClassMap);
                    dump($this->propertyMap);
                    dump($res);
                    die;
                }
                $this->stdoutroperty2elementClassCreate($property2elementClass);
            } catch (\Exception $exception) {

            }

        }

    }

    private function loadExistingData()
    {
        // load Contexts
        $currentContexts = models\ContextRecord::find()
            ->select(['id', 'name'])
            ->indexBy('name')
            ->all();

        foreach ($currentContexts as $key => $currentContext) {
            $this->contextMap[$key] = $currentContext->id;
        }

        // load ElementClasses
        $currentElementClasses = models\ElementClassRecord::find()
            ->select(['id', 'name'])
            ->indexBy('name')
            ->all();

        foreach ($currentElementClasses as $key => $currentElementClass) {
            $this->elementClassMap[$key] = $currentElementClass->id;
        }

        // load PropertyType
        $currentPropertyTypes = models\PropertyTypeRecord::find()
            ->select(['id', 'name'])
            ->indexBy('name')
            ->all();

        foreach ($currentPropertyTypes as $key => $currentPropertyType) {
            $this->propertyTypeMap[$key] = $currentPropertyType->id;
        }

        // load Property
        $currentProperties = models\PropertyRecord::find()
            ->select(['id', 'sysname'])
            ->indexBy('sysname')
            ->all();

        foreach ($currentProperties as $key => $currentProperty) {
            $this->propertyMap[$key] = $currentProperty->id;
        }

    }

    private function getBaseContexts()
    {
        return Yii::$app->params['entities']['contexts'];
    }

    private function getElementClasses()
    {
        return Yii::$app->params['entities']['elementClasses'];
    }

    private function getPropertyTypes()
    {
        return Yii::$app->params['entities']['propertyTypes'];
    }

    private function getProperties()
    {
        return Yii::$app->params['entities']['properties'];
    }

    private function getProperty2elementClasses()
    {
        return Yii::$app->params['entities']['property2elementClasses'];
    }

    private function stdoutContextCreate(models\ContextRecord $context)
    {
        $this->stdout('Context ');
        $this->stdout($context->name, Console::FG_CYAN);
        $this->stdout(' was created with id = ');
        $this->stdout($context->id, Console::FG_CYAN);
        $this->stdout("\n");
    }

    private function stdoutElementClassCreate(ElementClass $newElementClass)
    {
        $this->stdout('ElementClass ');
        $this->stdout($newElementClass->name, Console::FG_CYAN);
        $this->stdout(' was created with id = ');
        $this->stdout($newElementClass->id, Console::FG_CYAN);
        $this->stdout("\n");
    }

    private function stdoutPropertyTypeCreate(models\PropertyTypeRecord $newPropertyType)
    {
        $this->stdout('PropertyType ');
        $this->stdout($newPropertyType->name, Console::FG_CYAN);
        $this->stdout(' was created with id = ');
        $this->stdout($newPropertyType->id, Console::FG_CYAN);
        $this->stdout("\n");
    }

    private function stdoutPropertyCreate(Property $newProperty)
    {
        $this->stdout('Property ');
        $this->stdout($newProperty->sysname, Console::FG_CYAN);
        $this->stdout(' was created with id = ');
        $this->stdout($newProperty->id, Console::FG_CYAN);
        $this->stdout("\n");
    }

    private function stdoutroperty2elementClassCreate(array $newProperty2elementClass)
    {
        $this->stdout('Add property ');
        $this->stdout($newProperty2elementClass['propertySysname'], Console::FG_CYAN);
        $this->stdout(' to element ');
        $this->stdout($newProperty2elementClass['elementClass'], Console::FG_CYAN);
        $this->stdout(' as ');
        $this->stdout($newProperty2elementClass['parent'] ? 'parent' : 'child', Console::FG_CYAN);
        $this->stdout("\n");
    }

}
