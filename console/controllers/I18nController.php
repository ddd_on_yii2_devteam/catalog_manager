<?php

namespace console\controllers;

use commonprj\components\core\models\i18n\SourceRecord;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use Yii;

class I18nController extends Controller
{

    public $migration = <<<HTML
<?php

use yii\db\Migration;

/**
 * Class --class--
 */
class --class-- extends Migration {

    public function init()
    {
        \$this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_--lang--
        \$this->createTable('{{%string_--lang--}}', [
            'id' => \$this->bigPrimaryKey(),
            'source_id' => \$this->integer()->notNull(),
            'source_recordset_id' => \$this->bigInteger()->notNull(),
            'value' => \$this->string(255),
            'property_id' => \$this->integer(),
        ]);
        \$this->createIndex('IXFK-string_--lang--_source', '{{%string_--lang--}}', 'source_id');
        \$this->addForeignKey('FK-string_--lang--_source', '{{%string_--lang--}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_--lang--
        \$this->createTable('{{%text_--lang--}}', [
            'id' => \$this->bigPrimaryKey(),
            'source_id' => \$this->integer()->notNull(),
            'source_recordset_id' => \$this->bigInteger()->notNull(),
            'value' => \$this->text(),
            'property_id' => \$this->integer(),

        ]);
        \$this->createIndex('IXFK-text_--lang--_source', '{{%text_--lang--}}', 'source_id');
        \$this->addForeignKey('FK-text_--lang--_source', '{{%text_--lang--}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_--lang--
        \$this->dropTable('{{%string_--lang--}}');
        // 2. удаляем таблицу text_--lang--
        \$this->dropTable('{{%text_--lang--}}');

        return true;
    }
}
HTML;

    public function actionIndex()
    {
        $help = "\nДля создания миграции для новой интернационализации наберите в консоли:\n" .
            "php yii i18n/create {lang}\n\n" .
            "например, для английского языка команда будет выглядеть так:\n" .
            "php yii i18n/create eng\n\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCreate($lang)
    {
        $class = date('\mymd_His') . "_create_{$lang}_structure";
        $search = ['--class--', '--lang--'];
        $replace = [$class, $lang];
        $migration = str_replace($search, $replace, $this->migration);
        $migrationPath = \Yii::getAlias('@app/migrations');
        $file = $migrationPath . DIRECTORY_SEPARATOR . $class . ".php";
        if ($this->confirm("Создать миграцию '$file'?")) {

            FileHelper::createDirectory($migrationPath);
            file_put_contents($file, $migration);
            $this->stdout("Новая миграция успешно создана.\n", Console::FG_GREEN);
        }
    }

    public function actionSource()
    {
        $i18nSource = Yii::$app->params['i18n'];

        foreach ($i18nSource['source'] as $dbComponent => $sources) {
            $db = Yii::$app->{$dbComponent};
            $dbName = $this->getDsnAttribute('dbname', $db->dsn);

            foreach ($sources as $item) {
                $params = [
                    'db_name'    => $dbName,
                    'table_name' => $item['table_name'],
                    'field_name' => $item['field_name'],
                    'is_string'  => $item['is_string'] ?? 1,
                ];

                $this->addSourceRecord($params);
            }

        }
    }

    public function actionTranslate()
    {
        //Example

        $text = 'Описание описания';

        $localization = Yii::$app->localization;
        $sourceRecordsetId = 53;
        $sourceId = 2;
        $propertyId = null;
        $lang = 'rus';

        $localization->addTranslation($sourceId, $sourceRecordsetId, $propertyId, $text, $lang);

    }


    public function actionUntr()
    {
        //Example
        $localization = Yii::$app->localization;

        $items = $localization->getUntranslatedItems();

        dump($items);
    }

    private function addSourceRecord($params)
    {
        $isSourceExist = SourceRecord::find()->where($params)->count();

        if (!$isSourceExist) {
            $source = new SourceRecord($params);
            $res = $source->save();
            if ($res) {
                $this->stdoutSourceCreate($source);
            }
        }
    }

    private function getDsnAttribute($name, $dsn)
    {
        if (preg_match('/' . $name . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }

    private function stdoutSourceCreate(SourceRecord $source)
    {
        $this->stdout('Source ');
        $this->stdout($source->db_name . '.', Console::FG_CYAN);
        $this->stdout($source->table_name . '.', Console::FG_CYAN);
        $this->stdout($source->field_name, Console::FG_CYAN);
        $this->stdout(' was created as ');
        $this->stdout($source->is_string ? 'string' : 'text', Console::FG_CYAN);
        $this->stdout(' with id = ');
        $this->stdout($source->id, Console::FG_CYAN);
        $this->stdout("\n");
    }

}
