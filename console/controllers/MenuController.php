<?php

namespace console\controllers;

use commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue;
use commonprj\components\core\entities\elementCategory\ElementCategory;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\components\core\entities\menu\Menu;
use commonprj\components\core\entities\property\Property;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use yii\console\Controller;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;

class MenuController extends Controller
{

    protected $properties = null;

    public function actionIndex()
    {
        $help = "\nДля загрузки начальных данных:\n" .
            "php yii menu/create\n\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCreate()
    {
        $menuStructures = $this->getMenusStructures();

        foreach ($this->getMenus() as $menuData) {
            $elementClass = ElementClass::find()->where(['name' => $menuData['elementClass']])->one();

            $data = [
                'elementClassId' => $elementClass->id,
                'name'           => $menuData['name'],
            ];

            $menu = Menu::find()->where($data)->one();

            if (!$menu) {
                $menu = new Menu($data);
                $menu->save();
                $this->stdoutEntityCreate($menu);
            }

            $menuStructure = $menuStructures[$menu->name] ?? [];

            if ($menuStructure) {
                $this->addBranch($menu->id, $menuStructure);
            }

        }
    }

    protected function addBranch($menuId, $branch, $parentId = null, $level = 0)
    {
        foreach ($branch as $menuElement) {
            $this->addMenuItem($menuId, $menuElement, $parentId, $level);
        }

    }

    protected function addMenuItem($menuId, $menuElement, $parentId, $level = 0)
    {
        $sysname = $menuElement['sysname'] ?? '';

        $elementCategory = ElementCategory::find()
            ->where([
                'sysname' => $sysname,
            ])
            ->one();

        $variantData = $menuElement['variant'] ?? null;

        if (!$elementCategory) {
            $elementCategory = new ElementCategory([
                'sysname' => $sysname,
            ]);
            $propertyVariantId = $this->addVariant(null, $variantData);

        } else {
            $propertyVariantId = $this->addVariant($elementCategory->propertyVariantId, $variantData);
        }

        if ($propertyVariantId) {
            $data = [
                'name'              => $menuElement['name'],
                'parentId'          => $parentId,
                'menuId'            => $menuId,
                'isActive'          => 1,
                'propertyVariantId' => $propertyVariantId,
                'sortOrder'         => 100,
                'level'             => $level,

            ];

            $elementCategory->load($data, '');
            $result = $elementCategory->save();

            if ($result !== true) {
                dump($elementCategory);
                die;
            }

            $children = $menuElement['children'] ?? null;

            if ($children && $result === true) {
                $level = $level + 1;
                $this->addBranch($menuId, $children, $elementCategory->id, $level);
            }
        }

        return $elementCategory->id ?? null;
    }

    protected function getPropertyId($sysname)
    {
        if (!isset($this->properties[$sysname])) {
            $property = Property::findOne(['sysname' => $sysname]);

            if (!$property) {
                throw new NotFoundHttpException("Property {$sysname} Not Found");
            }

            $this->properties[$sysname] = $property->id;
        }

        return $this->properties[$sysname];
    }

    protected function addVariant($id = null, $data)
    {
        if (!$id && !$data) {
            throw new NotFoundHttpException("Variant Not Found");
        }

        if ($id) {
            $propertyVariant = PropertyVariant::findOne($id);
        } else {
            $propertyVariant = new PropertyVariant();
        }

        if ($data) {
            $propertyId = $this->getPropertyId($data['propertySysname']);
            $propertyValueIds = $data['valuesIds'] ?? null;

            $propertyValueIds = is_array($propertyValueIds) && count($propertyValueIds) == 1
                ? reset($propertyValueIds) : $propertyValueIds;

            $propertyMultiplicityId = is_array($propertyValueIds)
                ? AbstractPropertyValue::MULTIPLICITY_ARRAY
                : AbstractPropertyValue::MULTIPLICITY_SCALAR;

            if ($propertyId && $propertyValueIds) {
                $propertyVariant->propertyId = $propertyId;
                $propertyVariant->propertyMultiplicityId = $propertyMultiplicityId;
                $propertyVariant->propertyValueId = $propertyValueIds;

                $result = $propertyVariant->save();

                if ($result !== true) {
                    dump($propertyVariant);
                    die;
                }
            }
        }

        return $propertyVariant->id ?? null;
    }

    private function stdoutEntityCreate($entity)
    {
        $this->stdout("Source ");
        $this->stdout(get_class($entity), Console::FG_CYAN);
        $this->stdout(" create with id = ");
        $this->stdout($entity->id, Console::FG_CYAN);
        $this->stdout("\n");
    }

    private function getMenus()
    {
        return [
            [
                'elementClass' => 'catalog\ProductModel',
                'name'         => 'rus-Main',
            ],
        ];
    }

    private function getMenusStructures()
    {
        return [
            'rus-Main' => [
                [
                    'name'     => ['eng' => 'Furniture', 'rus' => 'Жилая мебель'],
                    'sysname'  => 'furniture',
                    'variant'  => [
                        'propertySysname' => 'functionalArea',
                        'valuesIds'       => [1],
                    ],
                    'children' => [
                        [
                            'name'     => ['eng' => 'Living room', 'rus' => 'Гостиная'],
                            'sysname'  => 'living_room',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => 2,
                            ],
                            'children' => [
                                [
                                    'name'     => ['eng' => 'Sofas & Loveseats', 'rus' => 'Диваны и мини диваны'],
                                    'sysname'  => 'sofas_&_loveseats',
                                    'variant'  => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [150],
                                    ],
                                    'children' => [
                                        [
                                            'name'    => ['eng' => 'Sofas', 'rus' => 'Диваны прямые'],
                                            'sysname' => 'sofas',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [151],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Corner Sofas', 'rus' => 'Диваны угловые'],
                                            'sysname' => 'corner_sofas',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [152],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Sectional Sofas', 'rus' => 'Диваны модульные'],
                                            'sysname' => 'sectional_sofas',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [153],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Sofas with ottomans', 'rus' => 'Диваны с оттоманкой'],
                                            'sysname' => 'sofas_with_ottomans',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [154],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Loveseats', 'rus' => 'Мини-диваны'],
                                            'sysname' => 'loveseats',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [156],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Recliner sofas', 'rus' => 'Диваны-реклайнеры'],
                                            'sysname' => 'recliner_sofas',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [155],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'name'     => ['eng' => 'Armchairs & Chairs', 'rus' => 'Кресла и стулья'],
                                    'sysname'  => 'armchairs_&_chairs',
                                    'variant'  => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [175],
                                    ],
                                    'children' => [
                                        [
                                            'name'    => ['eng' => 'Armchairs', 'rus' => 'Кресла'],
                                            'sysname' => 'armchairs',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [176],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Chairs', 'rus' => 'Стулья'],
                                            'sysname' => 'chairs',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [276],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Tables', 'rus' => 'Столы'],
                                    'sysname' => 'tables',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Wardrobes & Shelvings', 'rus' => 'Шкафы и Стеллажи'],
                                    'sysname' => 'wardrobes_&_shelvings',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1, 75],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Dressers & Drawers', 'rus' => 'Комоды и Тумбы'],
                                    'sysname' => 'dressers_&_drawers',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [50, 25],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Living room Sets', 'rus' => 'Комплекты для гостиной'],
                                    'sysname' => 'living_room_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Living room Lighting', 'rus' => 'Освещение для гостиной'],
                                    'sysname' => 'living_room_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Living room Decor & Accessories', 'rus' => 'Декор и аксессуары для гостиной'],
                                    'sysname' => 'living_room_decor_&_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Ottomans & Benches', 'rus' => 'Оттоманки и Банкетки'],
                                    'sysname' => 'ottomans_&_benches',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [200, 225],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Bedroom', 'rus' => 'Спальня'],
                            'sysname'  => 'bedroom',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [3],
                            ],
                            'children' => [
                                [
                                    'name'     => ['eng' => 'Beds & Headboards', 'rus' => 'Кровати и изголовья'],
                                    'sysname'  => 'beds_&_headboards',
                                    'variant'  => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                    'children' => [
                                        [
                                            'name'    => ['eng' => 'Single Beds (70 cm - 120 cm)', 'rus' => 'Односпальные кровати (ширина до 120 см)'],
                                            'sysname' => 'single_beds',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [271],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Small Double Beds (120 cm - 150 cm)', 'rus' => 'Полуторные кровати (ширина 120 см - 150 см)'],
                                            'sysname' => 'small_double_beds',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [251],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'name'     => ['eng' => 'Mattresses', 'rus' => 'Матрасы'],
                                    'sysname'  => 'mattresses_&_mattress_toppers',
                                    'variant'  => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                    'children' => [
                                        [
                                            'name'    => ['eng' => 'Mattresses', 'rus' => 'Матрасы'],
                                            'sysname' => 'mattresses',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [263],
                                            ],
                                        ],
                                        [
                                            'name'    => ['eng' => 'Mattress toppers', 'rus' => 'Наматрасники'],
                                            'sysname' => 'mattress_toppers',
                                            'variant' => [
                                                'propertySysname' => 'furnitureType',
                                                'valuesIds'       => [267],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bedroom Wardrobes & Shelvings', 'rus' => 'Шкафы и Полки'],
                                    'sysname' => 'bedroom_wardrobes_&_shelvings',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bedroom seating', 'rus' => 'Банкетки, Скамейки и пуфы'],
                                    'sysname' => 'bedroom_seating',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [225],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bedroom Tables', 'rus' => 'Столики для спальни'],
                                    'sysname' => 'bedroom_tables',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Dressers & Chests', 'rus' => 'Комоды'],
                                    'sysname' => 'dressers_&_chests',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [50],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bedroom sets', 'rus' => 'Комплекты для спальни'],
                                    'sysname' => 'bedroom_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bedroom Lighting', 'rus' => 'Освещение в спальне'],
                                    'sysname' => 'bedroom_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bedroom Decor & Accessories', 'rus' => 'Декор и аксессуары для спальни'],
                                    'sysname' => 'bedroom_decor_&_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Bathroom', 'rus' => 'Ванная комната'],
                            'sysname'  => 'bathroom',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [5],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Bathroom Vanities & Sets', 'rus' => 'Комплекты для ванной и тумбы под раковину'],
                                    'sysname' => 'bathroom_vanities_&_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [123],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bathroom Cabinets & Shelvings', 'rus' => 'Шкафы и Полки для ванной'],
                                    'sysname' => 'bathroom_cabinets_&_shelvings',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bathroom Mirrors', 'rus' => 'Зеркала для ванной комнаты'],
                                    'sysname' => 'bathroom_mirrors',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bathroom Lighting', 'rus' => 'Освещение в ванной комнате'],
                                    'sysname' => 'bathroom_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bathroom Accessories', 'rus' => 'Аксессуары для ванной комнаты'],
                                    'sysname' => 'bathroom_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Kitchen & Dining', 'rus' => 'Кухня и Столовая'],
                            'sysname'  => 'kitchen_&_dining',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [4],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Dining Tables & Sets', 'rus' => 'Обеденные столы и комплекты'],
                                    'sysname' => 'dining_tables_&_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Dining Chairs & Benches', 'rus' => 'Стулья и диваны'],
                                    'sysname' => 'dining_chairs_&_benches',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [275],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Dining Storage', 'rus' => 'Хранение на кухне'],
                                    'sysname' => 'dining_storage',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kitchen Lighting', 'rus' => 'Освещение для кухни'],
                                    'sysname' => 'kitchen_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kitchen Decor & Accessories', 'rus' => 'Декор и аксессуары для кухни и Столовой'],
                                    'sysname' => 'kitchen_decor_&_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Entryway', 'rus' => 'Прихожая'],
                            'sysname'  => 'entryway',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [6],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Wardrobes & Dressers', 'rus' => 'Шкафы и Комоды'],
                                    'sysname' => 'wardrobes_&_dressers',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Shoe Storage', 'rus' => 'Хранение обуви'],
                                    'sysname' => 'shoe_storage',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [25],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Console Tables', 'rus' => 'Консольные столы'],
                                    'sysname' => 'console_tables',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [25],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Benches & Seating', 'rus' => 'Банкетки, Скамейки и Пуфы'],
                                    'sysname' => 'benches_&_seating',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [225],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Racks & Wall Shelves', 'rus' => 'Вешалки и Полки'],
                                    'sysname' => 'racks_&_wall_shelves',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [325],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Entryway Sets', 'rus' => 'Комплекты для прихожей'],
                                    'sysname' => 'entryway_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Entryway Lighting', 'rus' => 'Освещение для прихожей'],
                                    'sysname' => 'Entryway Lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Entryway Decor &  Accessories', 'rus' => 'Декор и аксессуары для прихожей'],
                                    'sysname' => 'Entryway Decor &  Accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Home Office & Library', 'rus' => 'Рабочий кабинет и Библиотека'],
                            'sysname'  => 'home_office_&_library',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [7],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Office Desks & Chairs', 'rus' => 'Письменные столы и стулья'],
                                    'sysname' => 'office_desks_&_chairs',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300, 275],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bookcases & Shelvings', 'rus' => 'Книжные шкафы и Стеллажи'],
                                    'sysname' => 'bookcases_&_shelvings',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Office Cabinets, Chests & Media Stands', 'rus' => 'Офисные шкафы, Комоды и Тумбы'],
                                    'sysname' => 'office_cabinets,_chests_&_media_stands',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1, 25, 50],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Office Sets', 'rus' => 'Комплекты для кабинета'],
                                    'sysname' => 'office_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Home Office Lighting', 'rus' => 'Освещение для кабинета'],
                                    'sysname' => 'home_office_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Home Office Accessories', 'rus' => 'Аксессуары для кабинета'],
                                    'sysname' => 'home_office_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Baby room', 'rus' => 'Комната для малышей'],
                            'sysname'  => 'baby_room',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [9],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Cribs, Bassinets & Mattresses', 'rus' => 'Кровати детские, колыбели и Матрасы'],
                                    'sysname' => 'cribs,_bassinets_&_mattresses',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Dressers & Changings', 'rus' => 'Комоды и пеленальные столы'],
                                    'sysname' => 'dressers_&_changings',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [50],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'High Chairs & Gliders', 'rus' => 'Стулья и кресла'],
                                    'sysname' => 'high_chairs_&_gliders',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [275],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Nursery Sets', 'rus' => 'Комплекты мебели для малышей'],
                                    'sysname' => 'nursery_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Baby Safety', 'rus' => 'Безопасность'],
                                    'sysname' => 'baby_safety',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Baby room Decor & Accessories', 'rus' => 'Декор и аксессуары для комнаты малышей'],
                                    'sysname' => 'baby_room_decor_&_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Baby lighting', 'rus' => 'Освещение для комнаты малышей'],
                                    'sysname' => 'baby_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],

                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Kids room', 'rus' => 'Комната для детей'],
                            'sysname'  => 'kids_room',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [10],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Kids Beds', 'rus' => 'Детские кровати'],
                                    'sysname' => 'kids_beds',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids Wardrobes, Dressers & Shelvings', 'rus' => 'Детские Шкафы, Комоды и Стеллажи'],
                                    'sysname' => 'kids_wardrobes,_dressers_&_shelvings',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1, 50, 75],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Toy Storage', 'rus' => 'Хранение игрушек'],
                                    'sysname' => 'toy_storage',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [400],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Playroom', 'rus' => 'Игровая'],
                                    'sysname' => 'playroom',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [400],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids Tables', 'rus' => 'Детские столы'],
                                    'sysname' => 'kids_tables',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids Seating', 'rus' => 'Детские Стулья, пуфы, скамейки'],
                                    'sysname' => 'kids_seating',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids Sofas & Armchairs', 'rus' => 'Детские диваны и кресла'],
                                    'sysname' => 'kids_sofas_&_armchairs',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [200],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids room Sets', 'rus' => 'Комплекты для детской комнаты'],
                                    'sysname' => 'kids_room_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids Mattresses', 'rus' => 'Детские матрасы'],
                                    'sysname' => 'kids_mattresses',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids room Decor &  Accessories', 'rus' => 'Декор и аксессуары для детской комнаты'],
                                    'sysname' => 'kids_room_decor_&_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [400],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Kids lighting', 'rus' => 'Освещение для детской комнаты'],
                                    'sysname' => 'kids_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Teens room', 'rus' => 'Комната для подростков'],
                            'sysname'  => 'teens_room',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [11],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Teens Beds', 'rus' => 'Кровати для подростков'],
                                    'sysname' => 'teens_beds',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens Mattresses', 'rus' => 'Матрасы для подростков'],
                                    'sysname' => 'teens_mattresses',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens Wardrobes, Dressers & Shelvings', 'rus' => 'Шкафы, Комоды и Стеллажи для подростков'],
                                    'sysname' => 'teens_wardrobes,_dressers_&_shelvings',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [1, 50, 75],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens Tables & Chairs', 'rus' => 'Столы и стулья для подростков'],
                                    'sysname' => 'teens_tables_&_chairs',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens Sofas & Armchairs', 'rus' => 'Диваны и кресла для подростков'],
                                    'sysname' => 'teens_sofas_&_armchairs',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [150],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens room Sets', 'rus' => 'Комплекты для комнаты подростков'],
                                    'sysname' => 'teens_room_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens Mattresses', 'rus' => 'Матрасы для подростков'],
                                    'sysname' => 'teens_mattresses',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [250],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens room Decor & Accessories', 'rus' => 'Декор и аксессуары для комнаты подростков'],
                                    'sysname' => 'teens_room_decor_&_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Teens Lighting', 'rus' => 'Освещение для комнаты подростков'],
                                    'sysname' => 'teens_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Game & Entertainment', 'rus' => 'Игры и Развлечения'],
                            'sysname'  => 'game_&_entertainment',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [15],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Billiards', 'rus' => 'Биллиард'],
                                    'sysname' => 'billiards',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bowling', 'rus' => 'Боулинг'],
                                    'sysname' => 'bowling',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Casino', 'rus' => 'Казино'],
                                    'sysname' => 'casino',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Cinema', 'rus' => 'Кинотеатр'],
                                    'sysname' => 'cinema',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Others', 'rus' => 'Другое'],
                                    'sysname' => 'others',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Gaming Tables', 'rus' => 'Игровые столы'],
                                    'sysname' => 'gaming_tables',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Game Rooms Lighting', 'rus' => 'Освещение для игровых зон'],
                                    'sysname' => 'game_rooms_lighting',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [475],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Game & Entertainment Accessories', 'rus' => 'Аксессуары для игровых зон'],
                                    'sysname' => 'game_&_entertainment_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Bar & Wine', 'rus' => 'Бар и Винная комната'],
                            'sysname'  => 'bar_&_wine',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [13],
                            ],
                            'children' => [

                                [
                                    'name'    => ['eng' => 'Bar Tables', 'rus' => 'Барыне столы и стойки'],
                                    'sysname' => 'bar_tables',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [300],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bar Stools', 'rus' => 'Барыне стулья'],
                                    'sysname' => 'bar_stools',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [275],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Wine Storage', 'rus' => 'Хранение вина'],
                                    'sysname' => 'wine_storage',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [50],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bar Sets', 'rus' => 'Комплекты для бара'],
                                    'sysname' => 'bar_sets',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [125],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Bar Decor & Accessories', 'rus' => 'Декор и аксессуары для бара'],
                                    'sysname' => 'bar_decor_&_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Sport & Fitness', 'rus' => 'Спорт и Фитнес'],
                            'sysname'  => 'sport_&_fitness',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [14],
                            ],
                            'children' => [

                                [
                                    'name'    => ['eng' => 'Training equipment', 'rus' => 'Тренажеры'],
                                    'sysname' => 'training_equipment',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Sports equipment', 'rus' => 'Спортивное оборудование'],
                                    'sysname' => 'sports_equipment',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],

                                [
                                    'name'    => ['eng' => 'Fitness equipment', 'rus' => 'Фитнес оборудование'],
                                    'sysname' => 'fitness_equipment',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Swimming pools', 'rus' => 'Бассейны'],
                                    'sysname' => 'swimming_pools',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Sport & Fitness Accessories', 'rus' => 'Аксессуары для спорта и фитнеса'],
                                    'sysname' => 'sport_&_fitness_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [350],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Storage & Organization', 'rus' => 'Хранение'],
                            'sysname'  => 'storage_&_organization',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [12],
                            ],
                            'children' => [

                                [
                                    'name'    => ['eng' => 'Pantry & Laundry', 'rus' => 'Кладовая и постирочная'],
                                    'sysname' => 'pantry_&_laundry',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Garage', 'rus' => 'Гараж'],
                                    'sysname' => 'garage',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],

                                [
                                    'name'    => ['eng' => 'Storage Accessories', 'rus' => 'Аксессуары для хранения'],
                                    'sysname' => 'storage_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'name'     => ['eng' => 'Closet room', 'rus' => 'Гардеробная комната'],
                            'sysname'  => 'closet_room',
                            'variant'  => [
                                'propertySysname' => 'functionalArea',
                                'valuesIds'       => [8],
                            ],
                            'children' => [
                                [
                                    'name'    => ['eng' => 'Closet Systems & Organazers', 'rus' => 'Система хранения для гардеробной'],
                                    'sysname' => 'closet_systems_&_organazers',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Closet room Tables & Dressers', 'rus' => 'Столы и Комоды для гардеробной комнаты'],
                                    'sysname' => 'closet_room_tables_&_dressers',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],

                                [
                                    'name'    => ['eng' => 'Closet Seating', 'rus' => 'Оттоманки, банкетки, Пуфы'],
                                    'sysname' => 'closet_seating',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                                [
                                    'name'    => ['eng' => 'Closet Accessories', 'rus' => 'Аксессуары для гардеробной комнаты'],
                                    'sysname' => 'closet_accessories',
                                    'variant' => [
                                        'propertySysname' => 'furnitureType',
                                        'valuesIds'       => [100],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],


        ];
    }


}
