<?php

namespace console\controllers;

use commonprj\components\crm\entities\manufacturer\Manufacturer;
use console\models\Company\AddressImportMapping;
use console\models\Company\CompanyImportMapping;
use commonprj\components\core\models\ElementClassRecord;
use commonprj\components\core\models\PropertyRecord;
use commonprj\components\crm\entities\address\Address;
use commonprj\components\crm\entities\seller\Seller;
use console\models\Company\Location;
use console\models\Company\Organization;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;

class CompanyController extends BaseImportController
{
    const ADDRESS = 'address';
    const SELLER = 'seller';
    const MANUFACTURER = 'manufacturer';

    const MANUFACTURER_TYPE = 1;
    const SELLER_TYPE = 2;

    public $clear;

    private $domainModels = [
        self::ADDRESS      => [
            'context' => 'crm',
            'name'    => 'Address',
        ],
        self::SELLER       => [
            'context' => 'crm',
            'name'    => 'Seller',
        ],
        self::MANUFACTURER => [
            'context' => 'crm',
            'name'    => 'Manufacturer',
        ],
    ];


    private $baseProperties = [
        self::ADDRESS =>
            [
                'fullAddress',
                'country',
                'postalCode',
                'city',
                'description',
                'addressType',
                'geoLocation',
            ],

        self::SELLER => [
            'legalName',
            'tradeMark',
            'country',
            'organizationCode',
            'site',
            'description',
        ],

        self::MANUFACTURER => [
            'legalName',
            'tradeMark',
            'country',
            'organizationCode',
            'site',
            'description',

        ],
    ];

    private $fieldsGroup = [
        self::ADDRESS      => [
            'property' => [
                'fullAddress' => 'address',
                'country'     => 'getCountryCode',
                'postalCode'  => 'postal_code',
                'city'        => 'locality',
                'geoLocation' => [
                    'coordinates_latitude',
                    'coordinates_longitude',
                ],
                'addressType' => 'getLocationTypeId',
                'description' => 'description',
            ],
        ],
        self::SELLER       => [
            'property' => [
                'legalName'        => "getLegalName",
                'tradeMark'        => "getTradeMark",
                'country'          => "getCountry",
                'organizationCode' => "code",
                'site'             => "site",
                'description'      => "getDescription",
            ],
        ],
        self::MANUFACTURER => [
            'property' => [
                'legalName'        => "getLegalName",
                'tradeMark'        => "getTradeMark",
                'country'          => "getCountry",
                'organizationCode' => "code",
                'site'             => "site",
                'description'      => "getDescription",
            ],
        ],
    ];

    private $fieldsToPoperties = null;

    public function options($actionID)
    {
        return ['clear'];
    }

    public function actionIndex()
    {
        $help = "\nДля добавления компаний наберите в консоли:\n" .
            "php yii company/create\n\n" .
            "\nДля привязки проадвцов:\n" .
            "php yii company/bind\n\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCreate()
    {
        $this->createSupportImportTables();

        if ($this->clear) {
            $this->clearSupportImportTables();
        }

        $this->initDomainData();
        $this->initEntityData();

        $organizations = Organization::find()
            ->all();

        foreach ($organizations as $organization) {
            $this->addCompany($organization);
        }
        $locations = Location::find()
            ->all();

        foreach ($locations as $location) {
            $this->addLocation($location);
        }

//        if ($this->confirm(
//            "Are you sure you want to bind data!")) {
            $this->actionBindAddress();
            $this->actionBindManufacturer();
//        } else {
//            $this->stdout("Action was cancelled by user. Nothing has been performed.\n");
//        }

        $this->stdout("TOTAL: " . count($organizations) . " Company\n", Console::FG_GREEN);
        $this->stdout("------------\n", Console::FG_BLACK);
        $this->stdout("TOTAL: " . count($locations) . " Address\n", Console::FG_GREEN);
        $this->stdout("------------\n", Console::FG_BLACK);

    }

    public function actionBindAddress()
    {
        $organizations = Organization::find()
            ->all();

        foreach ($organizations as $organization) {
            $this->addAddressToOrganization($organization);
        }
    }

    public function actionBindManufacturer()
    {
        $organizations = Organization::find()
            ->where(['type' => self::SELLER_TYPE])
            ->all();

        foreach ((array)$organizations as $organization) {
            $this->addManufacturersToSeller($organization);
        }
    }

    public function addCompany(Organization $organization)
    {
        $organizationType = $organization->type;
        $element_id = $this->findCompany($organization->id);
        $locationData = $this->getDataForEntity($organization->toArray(),
            $organizationType == self::MANUFACTURER_TYPE ? self::MANUFACTURER : self::SELLER);

        $company = $this->getCompanyInstance($organizationType);
        $company->isActive = $organization->activated;


        if ($element_id) {
            $this->stdoutEntityExist($organization->id, $element_id, 'Company');
        } else {
            $propertyValues = $this->renderPropertyValues($locationData['property'], $organization);

            $company->save();

            $result = $company->saveProperties($propertyValues);

            if ($result) {
                (new CompanyImportMapping([
                    'element_id' => $company->id,
                    'origin_id'  => $organization->id,
                ]))->save();
            }
            $this->stdoutAddCompany($organization->id, $company->id);
        }
    }

    public function addLocation(Location $location)
    {
        $element_id = $this->findAddress($location->id);

        if (!$element_id) {
            $locationData = $this->getDataForEntity($location, self::ADDRESS);
            $address = new Address(['isActive' => true]);

            $propertyValues = $this->renderPropertyValues($locationData['property'], $location);

            $address->save();
            $result = $address->saveProperties($propertyValues);
            $element_id = $address->id;

            if ($result) {
                (new AddressImportMapping([
                    'element_id' => $address->id,
                    'origin_id'  => $location->id,
                ]))->save();
            }

            $this->stdoutAddAddress($location->id, $element_id);
        } else {
            $this->stdoutEntityExist($location->id, $element_id, 'Address');
        }

        return $element_id;
    }

    public function addAddressToOrganization(Organization $organization)
    {
        $locationsQuery = $organization->getLocations();
        $locations = $locationsQuery
            ->select('id')
            ->all();

        $organizationType = $organization->type;
        $company = $this->getCompanyInstance($organizationType);
        $element_id = $this->findCompany($organization->id);
        $company = $company->findOne(['id' => $element_id]);

        foreach ((array)$locations as $location) {
            $addressId = $this->findAddress($location->id);
            $result[$location->id] = $addressId ? $company->bindAddress($addressId) : false;
            $this->stdoutBindElementToParentElement($addressId, $company->id, 'Address', 'Company');
        }

        return $result ?? null;
    }

    public function addManufacturersToSeller(Organization $organization)
    {
        $element_id = $this->findCompany($organization->id);
        $seller = new Seller();
        $seller = $seller->findOne(['id' => $element_id]);

        $manufacturers = $organization->getManufacturers()->all();

        foreach ((array)$manufacturers as $manufacturer) {
            $manufacturerId = $this->findCompany($manufacturer->id);
            $result[$manufacturer->id] = $manufacturerId ? $seller->bindManufacturer($manufacturerId) : false;
            $this->stdoutBindElementToParentElement($manufacturerId, $seller->id, 'Manufacturer', 'Seller');
        }


    }

    protected function getLegalName(...$args)
    {
        /** @var Organization $organization */
        $organization = $args[0];

        return $organization->legal_name;
    }

    protected function getTradeMark(...$args)
    {
        /** @var Organization $organization */
        $organization = $args[0];

        $translationsQuery = $organization->type == self::SELLER_TYPE
            ? $organization->getSellerTranslations()
            : $organization->getManufacturerTranslations();

        $translations = $translationsQuery->indexBy('locale')->all();

        $result = [];
        foreach ($translations as $translation) {
            $result[$this->normalizeLang($translation->locale)] = $translation->name;
        }

        if ($organization->legal_name) {
            $result['rus'] = $organization->legal_name;
        }

        return $result;
    }

    protected function getCountry(...$args)
    {
        /** @var Organization $organization */
        $organization = $args[0];

        return (array)strtolower($organization->country);
    }

    protected function getCountryCode(...$args)
    {
        $location = $args[0];
        return (array)strtolower($location->country_code);
    }

    protected function getDescription(...$args)
    {
        /** @var Organization $organization */
        $organization = $args[0];

        $translationsQuery = $organization->type == self::SELLER_TYPE
            ? $organization->getSellerTranslations()
            : $organization->getManufacturerTranslations();

        $translations = $translationsQuery->indexBy('locale')->all();

        $result = [];
        foreach ($translations as $translation) {
            if ($translation->description) {
                $result[$this->normalizeLang($translation->locale)] = $translation->description;
            }
        }

        return $result;
    }

    protected function getLocationTypeId(...$args)
    {
        $location = $args[0];
        return [$location->type == 9 ? 4 : $location->type];
    }

    private function getDataForEntity($data, $objectType)
    {
        foreach ($this->fieldsToPoperties[$objectType] as $fieldGroup => $propMap) {

            foreach ($propMap as $propName => $propId) {
                if (is_array($propId)) {
                    $result[$fieldGroup][key($propId)][reset($propId)] = $data[$propName];
                } else {
                    $result[$fieldGroup][$propId] = $this->getPropertyValue($data, $propName);
                }
            }
        }

        return $result ?? null;
    }

    private function getCompanyInstance($type)
    {
        if ($type == self::MANUFACTURER_TYPE) {
            $model = new Manufacturer();
        } elseif ($type == self::SELLER_TYPE) {
            $model = new Seller();
        }

        return $model;
    }

    private function getPropertyValue($data, $propName)
    {
        if (method_exists($this, $propName)) {
            $value = [$this, $propName];
        } elseif (isset($data[$propName])) {
            $value = $data[$propName] == '\N' ? null : $data[$propName];
        }

        return $value ?? null;
    }

    private function initEntityData()
    {
        foreach ($this->domainModels as $index => $elementClass) {
            $elementClassId = $this->getElementClassId($elementClass['context'], $elementClass['name']);
            $this->initProperties($elementClassId, $index);
        }
    }

    private function initProperties($elementClassId, $modelName)
    {
        $existingProps = PropertyRecord::find()->indexBy('sysname')->all();
        $properties = [];
        $propertiesIds = [];

        if (!isset($this->baseProperties[$modelName])) {
            throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
        }

        foreach ($this->baseProperties[$modelName] as $needProperty) {
            $property = $this->getProperty($existingProps[$needProperty]);
            $properties[$property->sysname] = $property;
            $propertiesIds[] = $property->id;
        }

        foreach ($this->fieldsGroup[$modelName] as $fieldGroup => $fieldsMapping) {

            foreach ($fieldsMapping as $propName => $fieldName) {

                if (is_array($fieldName)) {
                    foreach ($fieldName as $key => $value) {
                        $this->fieldsToPoperties[$modelName][$fieldGroup][$value] = [$properties[$propName]->id => $key];
                    }
                } else {
                    $this->fieldsToPoperties[$modelName][$fieldGroup][$fieldName] = $properties[$propName]->id;
                }

            }

        }
    }

    /**
     * Получаем ID Element_class, если не существует - добавялем
     * @param $context
     * @param $name
     * @return int
     */
    private function getElementClassId($context, $name)
    {
        $elementClass = ElementClassRecord::findOne([
            'name' => $className = $context . '\\' . $name,
        ]);

        if (!$elementClass) {
            throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
        }

        return $elementClass->id ?? null;
    }

    protected function clearSupportImportTables()
    {
        $query = 'TRUNCATE TABLE company_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'TRUNCATE TABLE address_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();
    }

    private function normalizeLang($locale)
    {
        $langMap = [
            'ru' => 'rus',
            'en' => 'eng',
            'zh' => 'zho',
            'uk' => 'ukr',
            'pl' => 'pol',
            'tr' => 'tur',
            'it' => 'ita',
        ];

        return $langMap[$locale];
    }

    private function stdoutEntityExist($originId, $elementId, $entity)
    {
        $this->stdout($entity . ' with id ');
        $this->stdout($originId, Console::FG_CYAN);
        $this->stdout(' as Element ');
        $this->stdout($elementId, Console::FG_GREEN);
        $this->stdout(" exist \n");
    }


    private function stdoutAddCompany($organizationId, $elementId)
    {
        $this->stdout('Add Company with id ');
        $this->stdout($organizationId, Console::FG_CYAN);
        $this->stdout(' as Element ');
        $this->stdout($elementId, Console::FG_GREEN);
        $this->stdout("\n");
    }

    private function stdoutAddAddress($addressId, $elementId)
    {
        $this->stdout('Add Address with id ');
        $this->stdout($addressId, Console::FG_CYAN);
        $this->stdout(' as Element ');
        $this->stdout($elementId, Console::FG_GREEN);
        $this->stdout("\n");
    }

    private function stdoutBindElementToParentElement($childId, $parentId, $childName = 'child', $parentName = 'parent')
    {
        $this->stdout("Bind to $parentName ");
        $this->stdout($parentId, Console::FG_CYAN);
        $this->stdout(" $childName with id = ");
        $this->stdout($childId, Console::FG_CYAN);
        $this->stdout("\n");
    }

}
