<?php

namespace console\controllers;

use commonprj\components\core\entities\property\Property;
use yii\console\Controller;
use yii\helpers\Console;

class ListItemsController extends Controller
{

    public function actionIndex()
    {
        $help = "\nДля добавления списка наберите в консоли:\n" .
            "php yii list-items/create\n\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCreate()
    {
        $this->addCountry();

        $this->addMaterialVazy();
        $this->addKonstruktsiyaKnizhnogoShkafa();
        $this->addNalichieVKabinete();
        $this->addVidPodstavki();
        $this->addNalichieVDetskoy();
        $this->addKonfiguratsiya();
        $this->addVidOsveshcheniya();
        $this->addMehanizmReklayner();
        $this->addSposobUstanovki();
        $this->addKolichestvoDverey();
        $this->addKrovatOptsii();
        $this->addMatrasOptsii();
        $this->addUglovoeRaspolozhenie();
        $this->addPoverhnost();
        $this->addFunktsionalnyeZony();
        $this->addMaterialIzgotovleniyaKarkasa();
        $this->addTeksturaDrevesiny();
        $this->addPodlokotnikMaterial();
        $this->addPodlokotnikNalichie();
        $this->addNapolnitelMatrasaKrovati();
        $this->addOsnovaFasadovKorpusnoyMebeli();
        $this->addTsvet();
        $this->addStil();
        $this->addNapolnitelDlyaMyagkoyMebeli();
        $this->addKonstruktsiyaMatrasa();
        $this->addKolichestvoNozhekStola();
        $this->addFormaStola();
        $this->addRaskladnoyMehanizmStola();
        $this->addTipMyagkoyMebeli();
        $this->addRaskladnoyMehanizm();
        $this->addVidyMaterialovNozhekStola();
        $this->addKolichestvoMest();
        $this->addProductCatalog();
        $this->addPattern();
        $this->addAddressType();
        $this->addTypeOfOptions();
        $this->addHidingPower();
        $this->addGlossDegree();
        $this->addFabricStructure();
        $this->addWearResistance();
        $this->addLanguage();
        $this->addCurrency();
        $this->addSegment();
        $this->addPopularity();
        $this->addGrade();
        $this->addColorResistance();
        $this->addPvcFilmFinishing();
        $this->addPvcFilmType();
        $this->addGlassBrand();
        $this->addGlassProperty();
        $this->addGlassDecoration();
        $this->addGlassKind();
        $this->addFacadeDesign();
        $this->addAdditionalFinishingOptions();
        $this->addTexture();
        $this->addPatternMaterial();
    }

    public function addCountry()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'country']);
        $property->setListItems($this->getCountry());

        $this->stdoutAddList('country', count($this->getCountry()));
    }

    public function addMaterialVazy()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'materialVazy']);
        $property->setListItems($this->getMaterialVazy());

        $this->stdoutAddList('materialVazy', count($this->getMaterialVazy()));
    }

    public function addKonstruktsiyaKnizhnogoShkafa()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'konstruktsiyaKnizhnogoShkafa']);
        $property->setListItems($this->getKonstruktsiyaKnizhnogoShkafa());

        $this->stdoutAddList('konstruktsiyaKnizhnogoShkafa', count($this->getKonstruktsiyaKnizhnogoShkafa()));
    }

    public function addNalichieVKabinete()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'nalichieVKabinete']);
        $property->setListItems($this->getNalichieVKabinete());

        $this->stdoutAddList('nalichieVKabinete', count($this->getNalichieVKabinete()));
    }

    public function addVidPodstavki()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'vidPodstavki']);
        $property->setListItems($this->getVidPodstavki());

        $this->stdoutAddList('vidPodstavki', count($this->getVidPodstavki()));
    }

    public function addNalichieVDetskoy()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'nalichieVDetskoy']);
        $property->setListItems($this->getNalichieVDetskoy());

        $this->stdoutAddList('nalichieVDetskoy', count($this->getNalichieVDetskoy()));
    }

    public function addKonfiguratsiya()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'konfiguratsiya']);
        $property->setListItems($this->getKonfiguratsiya());

        $this->stdoutAddList('konfiguratsiya', count($this->getKonfiguratsiya()));
    }

    public function addVidOsveshcheniya()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'vidOsveshcheniya']);
        $property->setListItems($this->getVidOsveshcheniya());

        $this->stdoutAddList('vidOsveshcheniya', count($this->getVidOsveshcheniya()));
    }

    public function addMehanizmReklayner()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'mehanizmReklayner']);
        $property->setListItems($this->getMehanizmReklayner());

        $this->stdoutAddList('mehanizmReklayner', count($this->getMehanizmReklayner()));
    }

    public function addSposobUstanovki()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'sposobUstanovki']);
        $property->setListItems($this->getSposobUstanovki());

        $this->stdoutAddList('sposobUstanovki', count($this->getSposobUstanovki()));
    }

    public function addKolichestvoDverey()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'kolichestvoDverey']);
        $property->setListItems($this->getKolichestvoDverey());

        $this->stdoutAddList('kolichestvoDverey', count($this->getKolichestvoDverey()));
    }

    public function addKrovatOptsii()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'krovatOptsii']);
        $property->setListItems($this->getKrovatOptsii());

        $this->stdoutAddList('krovatOptsii', count($this->getKrovatOptsii()));
    }

    public function addMatrasOptsii()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'matrasOptsii']);
        $property->setListItems($this->getMatrasOptsii());

        $this->stdoutAddList('matrasOptsii', count($this->getMatrasOptsii()));
    }

    public function addUglovoeRaspolozhenie()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'uglovoeRaspolozhenie']);
        $property->setListItems($this->getUglovoeRaspolozhenie());

        $this->stdoutAddList('uglovoeRaspolozhenie', count($this->getUglovoeRaspolozhenie()));
    }

    public function addPoverhnost()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'poverhnost']);
        $property->setListItems($this->getPoverhnost());

        $this->stdoutAddList('poverhnost', count($this->getPoverhnost()));
    }

    public function addFunktsionalnyeZony()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'functionalArea']);
        $property->setListItems($this->getFunktsionalnyeZony());

        $this->stdoutAddList('functionalArea', count($this->getFunktsionalnyeZony()));
    }

    public function addMaterialIzgotovleniyaKarkasa()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'materialIzgotovleniyaKarkasa']);
        $property->setListItems($this->getMaterialIzgotovleniyaKarkasa());

        $this->stdoutAddList('materialIzgotovleniyaKarkasa', count($this->getMaterialIzgotovleniyaKarkasa()));
    }

    public function addTeksturaDrevesiny()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'teksturaDrevesiny']);
        $property->setListItems($this->getTeksturaDrevesiny());

        $this->stdoutAddList('teksturaDrevesiny', count($this->getTeksturaDrevesiny()));
    }

    public function addPodlokotnikMaterial()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'podlokotnikMaterial']);
        $property->setListItems($this->getPodlokotnikMaterial());

        $this->stdoutAddList('podlokotnikMaterial', count($this->getPodlokotnikMaterial()));
    }

    public function addPodlokotnikNalichie()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'podlokotnikNalichie']);
        $property->setListItems($this->getPodlokotnikNalichie());

        $this->stdoutAddList('podlokotnikNalichie', count($this->getPodlokotnikNalichie()));
    }

    public function addNapolnitelMatrasaKrovati()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'napolnitelMatrasaKrovati']);
        $property->setListItems($this->getNapolnitelMatrasaKrovati());

        $this->stdoutAddList('napolnitelMatrasaKrovati', count($this->getNapolnitelMatrasaKrovati()));
    }

    public function addOsnovaFasadovKorpusnoyMebeli()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'osnovaFasadovKorpusnoyMebeli']);
        $property->setListItems($this->getOsnovaFasadovKorpusnoyMebeli());

        $this->stdoutAddList('osnovaFasadovKorpusnoyMebeli', count($this->getOsnovaFasadovKorpusnoyMebeli()));
    }

    public function addTsvet()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'tsvet']);
        $property->setListItems($this->getTsvet());

        $this->stdoutAddList('tsvet', count($this->getTsvet()));
    }

    public function addStil()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'stil']);
        $property->setListItems($this->getStil());

        $this->stdoutAddList('stil', count($this->getStil()));
    }

    public function addNapolnitelDlyaMyagkoyMebeli()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'napolnitelDlyaMyagkoyMebeli']);
        $property->setListItems($this->getNapolnitelDlyaMyagkoyMebeli());

        $this->stdoutAddList('napolnitelDlyaMyagkoyMebeli', count($this->getNapolnitelDlyaMyagkoyMebeli()));
    }

    public function addKonstruktsiyaMatrasa()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'konstruktsiyaMatrasa']);
        $property->setListItems($this->getKonstruktsiyaMatrasa());

        $this->stdoutAddList('konstruktsiyaMatrasa', count($this->getKonstruktsiyaMatrasa()));
    }

    public function addKolichestvoNozhekStola()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'kolichestvoNozhekStola']);
        $property->setListItems($this->getKolichestvoNozhekStola());

        $this->stdoutAddList('kolichestvoNozhekStola', count($this->getKolichestvoNozhekStola()));
    }

    public function addFormaStola()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'formaStola']);
        $property->setListItems($this->getFormaStola());

        $this->stdoutAddList('formaStola', count($this->getFormaStola()));
    }

    public function addRaskladnoyMehanizmStola()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'raskladnoyMehanizmStola']);
        $property->setListItems($this->getRaskladnoyMehanizmStola());

        $this->stdoutAddList('raskladnoyMehanizmStola', count($this->getRaskladnoyMehanizmStola()));
    }

    public function addTipMyagkoyMebeli()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'tipMyagkoyMebeli']);
        $property->setListItems($this->getTipMyagkoyMebeli());

        $this->stdoutAddList('tipMyagkoyMebeli', count($this->getTipMyagkoyMebeli()));
    }

    public function addRaskladnoyMehanizm()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'raskladnoyMehanizm']);
        $property->setListItems($this->getRaskladnoyMehanizm());

        $this->stdoutAddList('raskladnoyMehanizm', count($this->getRaskladnoyMehanizm()));
    }

    public function addVidyMaterialovNozhekStola()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'vidyMaterialovNozhekStola']);
        $property->setListItems($this->getVidyMaterialovNozhekStola());

        $this->stdoutAddList('vidyMaterialovNozhekStola', count($this->getVidyMaterialovNozhekStola()));
    }

    public function addKolichestvoMest()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'kolichestvoMest']);
        $property->setListItems($this->getKolichestvoMest());

        $this->stdoutAddList('kolichestvoMest', count($this->getKolichestvoMest()));
    }

    public function addProductCatalog()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'furnitureType']);
        $property->setListItems($this->getProductCatalog());

        $this->stdoutAddList('furnitureType', count($this->getProductCatalog()));
    }

    public function addPattern()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'pattern']);
        $property->setListItems($this->getPattern());

        $this->stdoutAddList('pattern', count($this->getPattern()));
    }

    public function addAddressType()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'addressType']);
        $property->setListItems($this->getAddressType());

        $this->stdoutAddList('addressType', count($this->getAddressType()));
    }

    public function addTypeOfOptions()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'typeOfOption']);
        $property->setListItems($this->getTypeOfOption());

        $this->stdoutAddList('typeOfOptions', count($this->getTypeOfOption()));
    }

    public function addHidingPower()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'hidingPower']);
        $property->setListItems($this->getHidingPower());

        $this->stdoutAddList('hidingPower', count($this->getHidingPower()));
    }

    public function addGlossDegree()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'glossDegree']);
        $property->setListItems($this->getGlossDegree());

        $this->stdoutAddList('glossDegree', count($this->getGlossDegree()));
    }

    public function addFabricStructure()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'fabricStructure']);
        $property->setListItems($this->getFabricStructure());

        $this->stdoutAddList('fabricStructure', count($this->getFabricStructure()));
    }

    public function addWearResistance()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'wearResistance']);
        $property->setListItems($this->getWearResistance());

        $this->stdoutAddList('wearResistance', count($this->getWearResistance()));
    }

    public function addLanguage()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'language']);
        $property->setListItems($this->getLanguage());

        $this->stdoutAddList('language', count($this->getLanguage()));
    }

    public function addCurrency()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'currency']);
        $property->setListItems($this->getCurrency());

        $this->stdoutAddList('currency', count($this->getCurrency()));
    }

    public function addSegment()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'segment']);
        $property->setListItems($this->getSegment());

        $this->stdoutAddList('segment', count($this->getSegment()));
    }


    public function addGrade()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'grade']);
        $property->setListItems($this->getGrade());

        $this->stdoutAddList('grade', count($this->getGrade()));
    }


    public function addPopularity()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'popularity']);
        $property->setListItems($this->getPopularity());

        $this->stdoutAddList('popularity', count($this->getPopularity()));
    }

    private function getCountry()
    {
        return $this->allCountryList();
    }

    private function addColorResistance()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'colorResistance']);
        $property->setListItems($this->getColorResistance());

        $this->stdoutAddList('colorResistance', count($this->getColorResistance()));
    }

    private function addPvcFilmFinishing()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'pvcFilmFinishing']);
        $property->setListItems($this->getPvcFilmFinishing());

        $this->stdoutAddList('pvcFilmFinishing', count($this->getPvcFilmFinishing()));
    }

    private function addPvcFilmType()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'pvcFilmType']);
        $property->setListItems($this->getPvcFilmType());

        $this->stdoutAddList('pvcFilmType', count($this->getPvcFilmType()));
    }

    private function addGlassBrand()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'glassBrand']);
        $property->setListItems($this->getGlassBrand());

        $this->stdoutAddList('glassBrand', count($this->getGlassBrand()));
    }

    private function addGlassProperty()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'glassProperty']);
        $property->setListItems($this->getGlassProperty());

        $this->stdoutAddList('glassProperty', count($this->getGlassProperty()));
    }

    private function addGlassDecoration()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'glassDecoration']);
        $property->setListItems($this->getGlassDecoration());

        $this->stdoutAddList('glassDecoration', count($this->getGlassDecoration()));
    }

    private function addGlassKind()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'glassKind']);
        $property->setListItems($this->getGlassKind());

        $this->stdoutAddList('glassKind', count($this->getGlassKind()));
    }

    private function addFacadeDesign()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'facadeDesign']);
        $property->setListItems($this->getFacadeDesign());

        $this->stdoutAddList('facadeDesign', count($this->getFacadeDesign()));
    }

    private function addAdditionalFinishingOptions()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'additionalFinishingOptions']);
        $property->setListItems($this->getAdditionalFinishingOptions());

        $this->stdoutAddList('additionalFinishingOptions', count($this->getAdditionalFinishingOptions()));
    }

    private function addTexture()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'texture']);
        $property->setListItems($this->getTexture());

        $this->stdoutAddList('texture', count($this->getTexture()));
    }

    private function addPatternMaterial()
    {
        /** @var Property $property */
        $property = Property::findOne(['sysname' => 'patternMaterial']);
        $property->setListItems($this->getPatternMaterial());

        $this->stdoutAddList('patternMaterial', count($this->getPatternMaterial()));
    }



    private function stdoutAddList($name, $count)
    {
        $this->stdout('Add list ');
        $this->stdout($name, Console::FG_CYAN);
        $this->stdout(' with ');
        $this->stdout($count, Console::FG_GREEN);
        $this->stdout(" items\n");
    }

    private function allCountryList()
    {
        return ['au' => 'Австралия',
                'at' => 'Австрия',
                'az' => 'Азербайджан',
                'al' => 'Албания',
                'dz' => 'Алжир',
                'vi' => 'Американские Виргинские острова',
                'as' => 'Американское Самоа',
                'ai' => 'Ангилья',
                'ao' => 'Ангола',
                'ad' => 'Андорра',
                'aq' => 'Антарктида',
                'ag' => 'Антигуа и Барбуда',
                'ar' => 'Аргентина',
                'am' => 'Армения',
                'aw' => 'Аруба',
                'af' => 'Афганистан',
                'bs' => 'Багамские острова',
                'bd' => 'Бангладеш',
                'bb' => 'Барбадос',
                'bh' => 'Бахрейн',
                'bz' => 'Белиз',
                'by' => 'Белоруссия',
                'be' => 'Бельгия',
                'bj' => 'Бенин',
                'bm' => 'Бермуды',
                'bg' => 'Болгария',
                'bo' => 'Боливия',
                'bq' => 'Бонайре, Синт-Эстатиус и Саба',
                'ba' => 'Босния и Герцеговина',
                'bw' => 'Ботсвана',
                'br' => 'Бразилия',
                'io' => 'Британская территория в Индийском океане',
                'vg' => 'Британские Виргинские острова',
                'bn' => 'Бруней',
                'bv' => 'Буве',
                'bf' => 'Буркина-Фасо',
                'bi' => 'Бурунди',
                'bt' => 'Бутан',
                'vu' => 'Вануату',
                'va' => 'Ватикан',
                'gb' => 'Великобритания',
                'hu' => 'Венгрия',
                've' => 'Венесуэла',
                'tl' => 'Восточный Тимор (Тимор-Лешти)',
                'vn' => 'Вьетнам',
                'ga' => 'Габон',
                'ht' => 'Гаити',
                'gy' => 'Гайана',
                'gm' => 'Гамбия',
                'gh' => 'Гана',
                'gp' => 'Гваделупа',
                'gt' => 'Гватемала',
                'gn' => 'Гвинея',
                'gw' => 'Гвинея-Бисау',
                'de' => 'Германия',
                'gg' => 'Гернси',
                'gi' => 'Гибралтар',
                'hn' => 'Гондурас',
                'hk' => 'Гонконг',
                'gd' => 'Гренада',
                'gl' => 'Гренландия',
                'gr' => 'Греция',
                'ge' => 'Грузия',
                'gu' => 'Гуам',
                'dk' => 'Дания',
                'cd' => 'Демократическая Республика Конго',
                'je' => 'Джерси',
                'dj' => 'Джибути',
                'dm' => 'Доминика',
                'do' => 'Доминикана',
                'eg' => 'Египет',
                'zm' => 'Замбия',
                'eh' => 'Западная Сахара (Сахарская Арабская Демократическая Республика)',
                'zw' => 'Зимбабве',
                'il' => 'Израиль',
                'im' => 'остров Мэн',
                'in' => 'Индия',
                'id' => 'Индонезия, Бали',
                'jo' => 'Иордания',
                'iq' => 'Ирак',
                'ir' => 'Иран',
                'ie' => 'Ирландия',
                'is' => 'Исландия',
                'es' => 'Испания',
                'it' => 'Италия',
                'ye' => 'Йемен',
                'cv' => 'Кабо-Верде (Острова Зеленого Мыса)',
                'kz' => 'Казахстан',
                'ky' => 'Каймановы острова',
                'kh' => 'Камбоджа',
                'cm' => 'Камерун',
                'ca' => 'Канада',
                'qa' => 'Катар',
                'ke' => 'Кения',
                'cy' => 'Кипр',
                'kg' => 'Киргизия',
                'ki' => 'Кирибати',
                'cn' => 'Китай',
                'kp' => 'КНДР',
                'cc' => 'Кокосовые острова (Килинг)',
                'co' => 'Колумбия',
                'km' => 'Коморские острова',
                'kr' => 'Корея',
                'cr' => 'Коста-Рика',
                'ci' => 'Кот-д’Ивуар (Берег Слоновой Кости)',
                'cu' => 'Куба',
                'kw' => 'Кувейт',
                'cw' => 'Кюрасао',
                'la' => 'Лаос',
                'lv' => 'Латвия',
                'ls' => 'Лесото',
                'lr' => 'Либерия',
                'lb' => 'Ливан',
                'ly' => 'Ливия',
                'lt' => 'Литва',
                'li' => 'Лихтенштейн',
                'lu' => 'Люксембург',
                'mu' => 'Маврикий',
                'mr' => 'Мавритания',
                'mg' => 'Мадагаскар',
                'yt' => 'Майотта',
                'mo' => 'Макао (Аомынь)',
                'mk' => 'Македония',
                'mw' => 'Малави',
                'my' => 'Малайзия',
                'ml' => 'Мали',
                'mv' => 'Мальдивы',
                'mt' => 'Мальта',
                'ma' => 'Марокко',
                'mq' => 'Мартиника',
                'mh' => 'Маршалловы острова',
                'mx' => 'Мексика',
                'mz' => 'Мозамбик',
                'md' => 'Молдавия',
                'mc' => 'Монако',
                'mn' => 'Монголия',
                'ms' => 'Монтсеррат',
                'mm' => 'Мьянма (Бирма)',
                'na' => 'Намибия',
                'nr' => 'Науру',
                'np' => 'Непал',
                'ne' => 'Нигер',
                'ng' => 'Нигерия',
                'an' => 'Нидерландские Антильские острова',
                'nl' => 'Нидерланды (Голландия)',
                'ni' => 'Никарагуа',
                'nu' => 'Ниуэ',
                'nz' => 'Новая Зеландия',
                'nc' => 'Новая Каледония',
                'no' => 'Норвегия',
                'ae' => 'ОАЭ',
                'om' => 'Оман',
                'nf' => 'Остров Норфолк',
                'cx' => 'Остров Рождества',
                'sh' => 'Остров Святой Елены',
                'hm' => 'Остров Херд и острова Макдональд',
                'ck' => 'Острова Кука',
                'pn' => 'Острова Питкэрн',
                'tc' => 'Острова Тёркс и Кайкос',
                'wf' => 'Острова Уоллис и Футуна',
                'pk' => 'Пакистан',
                'pw' => 'Палау',
                'ps' => 'Палестина',
                'pa' => 'Панама',
                'pg' => 'Папуа',
                'py' => 'Парагвай',
                'pe' => 'Перу',
                'pl' => 'Польша',
                'pt' => 'Португалия',
                'pr' => 'Пуэрто-Рико',
                'cg' => 'Республика Конго',
                're' => 'Реюньон',
                'ru' => 'Россия',
                'rw' => 'Руанда',
                'ro' => 'Румыния',
                'sv' => 'Сальвадор',
                'ws' => 'Самоа',
                'sm' => 'Сан-Марино',
                'st' => 'Сан-Томе и Принсипи',
                'sa' => 'Саудовская Аравия',
                'sz' => 'Свазиленд',
                'mp' => 'Северные Марианские острова',
                'sc' => 'Сейшелы',
                'pm' => 'Сен-Пьер и Микелон',
                'sn' => 'Сенегал',
                'vc' => 'Сент-Винсент и Гренадины',
                'kn' => 'Сент-Китс и Невис',
                'lc' => 'Сент-Люсия',
                'rs' => 'Сербия',
                'sg' => 'Сингапур',
                'sy' => 'Сирия',
                'sk' => 'Словакия',
                'si' => 'Словения',
                'sb' => 'Соломоновы острова',
                'so' => 'Сомали',
                'sd' => 'Судан',
                'sr' => 'Суринам',
                'us' => 'США',
                'sl' => 'Сьерра-Леоне',
                'tj' => 'Таджикистан',
                'th' => 'Таиланд',
                'tw' => 'Тайвань',
                'tz' => 'Танзания',
                'tg' => 'Того',
                'tk' => 'Токелау',
                'to' => 'Тонга',
                'tt' => 'Тринидад и Тобаго',
                'tv' => 'Тувалу',
                'tn' => 'Тунис',
                'tm' => 'Туркмения',
                'tr' => 'Турция',
                'ug' => 'Уганда',
                'uz' => 'Узбекистан',
                'ua' => 'Украина',
                'uy' => 'Уругвай',
                'fo' => 'Фарерские острова',
                'fm' => 'Федеративные Штаты Микронезии',
                'fj' => 'Фиджи',
                'ph' => 'Филиппины',
                'fi' => 'Финляндия',
                'fk' => 'Фолклендские (Мальвинские) острова',
                'fr' => 'Франция',
                'gf' => 'Французская Гвиана',
                'pf' => 'Французская Полинезия (Таити)',
                'tf' => 'Французские Южные и Антарктические Территории',
                'hr' => 'Хорватия',
                'cf' => 'Центральноафриканская Республика',
                'td' => 'Чад',
                'me' => 'Черногория',
                'cz' => 'Чехия',
                'cl' => 'Чили, о. Пасхи',
                'ch' => 'Швейцария',
                'se' => 'Швеция',
                'sj' => 'Шпицберген и Ян-Майен',
                'lk' => 'Шри-Ланка (Цейлон)',
                'ec' => 'Эквадор, Галапагосы',
                'gq' => 'Экваториальная Гвинея',
                'er' => 'Эритрея',
                'ee' => 'Эстония',
                'et' => 'Эфиопия',
                'za' => 'ЮАР',
                'gs' => 'Южная Георгия и Южные Сандвичевы острова',
                'jm' => 'Ямайка',
                'jp' => 'Япония',
        ];
    }

    private function getMaterialVazy()
    {
        return [
            1 => 'Стекло',
            2 => 'Фарфор',
            3 => 'Керамика',
            4 => 'Латунь',
            5 => 'Медь',
            6 => 'Бронза',
            7 => 'Хрусталь',
        ];
    }

    private function getKonstruktsiyaKnizhnogoShkafa()
    {
        return [
            1 => 'Открытый',
            2 => 'С фасадами',
            3 => 'Комбинированный',
        ];
    }

    private function getNalichieVKabinete()
    {
        return [
            1 => 'Стол',
            2 => 'Стул/Кресло',
            3 => 'Тумба',
            4 => 'Шкаф',
        ];
    }

    private function getVidPodstavki()
    {
        return [
            1 => 'Кухонная',
            2 => 'Цветочная',
            3 => 'Для системного блока',
            4 => 'Декоративная',
            5 => 'Медиа',
            6 => 'Для лампы',
        ];
    }

    private function getNalichieVDetskoy()
    {
        return [
            1 => 'Кровать',
            2 => 'Стол',
            3 => 'Шкаф',
        ];
    }

    private function getKonfiguratsiya()
    {
        return [
            1 => 'Прямая',
            2 => 'Угловая',
            3 => 'П-образная',
            4 => 'Т-образная',
            5 => 'Закругленная',
            6 => 'Другая',
        ];
    }

    private function getVidOsveshcheniya()
    {
        return [
            1  => 'Потолочное освещение',
            2  => 'Напольное освещение',
            3  => 'Настенное освещение',
            4  => 'Освещение в ванной',
            5  => 'Кухонная подсветка',
            6  => 'Настольные лампы',
            7  => 'Рабочие лампы',
            8  => 'Декоративное освещение',
            9  => 'Светильники для детей 0-7 лет',
            10 => 'Светильники для детей 8-12 лет',
        ];
    }

    private function getMehanizmReklayner()
    {
        return [
            1 => 'Электрический',
            2 => 'Механический',
            3 => 'Пневматический',
        ];
    }

    private function getSposobUstanovki()
    {
        return [
            1 => 'Напольный',
            2 => 'Настенный',
        ];
    }

    private function getKolichestvoDverey()
    {
        return [
            1 => 'Одна',
            2 => 'Две',
            3 => 'Три',
            4 => 'Четыре и более',
        ];
    }

    private function getKrovatOptsii()
    {
        return [
            1 => 'Двухъярусная кровать',
            2 => 'Дополнительное выдвижное спальное место',
        ];
    }

    private function getMatrasOptsii()
    {
        return [
            1 => 'Оборачиваемый: зима-лето',
            2 => 'Оборачиваемый: мягкий-жесткий',
            3 => 'Поставляется свёрнутым в рулон',
            4 => 'Ортопедический',
            5 => 'Двухсторонний',
        ];
    }

    private function getUglovoeRaspolozhenie()
    {
        return [
            1 => 'Правое',
            2 => 'Левое',
        ];
    }

    private function getPoverhnost()
    {
        return [
            1 => 'Гальваника',
            2 => 'Хромирование',
        ];
    }

    private function getFunktsionalnyeZony()
    {
        return [
            [
                'item'  => 1,
                'label' => ['rus' => 'Жилая мебель', 'eng' => 'Residential furniture'],
                'children' => [
                    [
                        'item'     => 2,
                        'label'    => ['rus' => 'Гостиная', 'eng' => 'Living Room',],
                    ],
                    [
                        'item'     => 3,
                        'label'    => ['rus' => 'Спальня', 'eng' => 'Bedroom',],
                    ],
                    [
                        'item'     => 4,
                        'label'    => ['rus' => 'Кухня и Столовая', 'eng' => 'Kitchen & Dining',],
                    ],
                    [
                        'item'     => 5,
                        'label'    => ['rus' => 'Ванная Комната', 'eng' => 'Bathroom',],
                    ],
                    [
                        'item'     => 6,
                        'label'    => ['rus' => 'Прихожая', 'eng' => 'Entryway',],
                    ],
                    [
                        'item'     => 7,
                        'label'    => ['rus' => 'Рабочий кабинет', 'eng' => 'Home Office & Library',],
                    ],
                    [
                        'item'     => 8,
                        'label'    => ['rus' => 'Гардеробная комната', 'eng' => 'Closet Room',],
                    ],
                    [
                        'item'     => 9,
                        'label'    => ['rus' => 'Комната для новорожденных', 'eng' => 'Baby Room',],
                    ],
                    [
                        'item'     => 10,
                        'label'    => ['rus' => 'Комната для детей', 'eng' => 'Kids Room',],
                    ],
                    [
                        'item'     => 11,
                        'label'    => ['rus' => 'Комната для подростков', 'eng' => 'Teens Room',],
                    ],
                    [
                        'item'     => 12,
                        'label'    => ['rus' => 'Хранение', 'eng' => 'Storage & Organisation',],
                    ],
                    [
                        'item'     => 13,
                        'label'    => ['rus' => 'Бар и Винная комната', 'eng' => 'Bar & Wine',],
                    ],
                    [
                        'item'     => 14,
                        'label'    => ['rus' => 'Спорт и Фитнес', 'eng' => 'Sport & Fitness',],
                    ],
                    [
                        'item'     => 15,
                        'label'    => ['rus' => 'Игры и Развлечения', 'eng' => 'Game & Entertainment',],
                    ],
                ]
            ],
        ];

//        return [
//            1  => 'Жилая мебель',
//            2  => 'Кухня',
//            3  => 'Прихожая',
//            4  => 'Гостиная',
//            5  => 'Столовая',
//            6  => 'Рабочий кабинет',
//            7  => 'Кладовая',
//            8  => 'Спальня',
//            9  => 'Ванная комната',
//            10 => 'Для новорожденных',
//            11 => 'Для детей',
//            12 => 'Игровая, кинотеатр',
//            13 => 'Мебель для бизнеса',
//            14 => 'Детские учреждения',
//            15 => 'Образовательные учреждения',
//            16 => 'Культурно-развлекательные учреждения',
//            17 => 'Физкультурные и спортивные учреждения',
//            18 => 'Организации общественного питания',
//            19 => 'Учреждения общественного транспорта',
//            20 => 'Медицинские учреждения',
//            21 => 'Административно-офисные помещения',
//            22 => 'Торговые учреждения',
//            23 => 'Коммерческие учреждения',
//            24 => 'Прочие',
//            25 => 'Мебель для улицы',
//            26 => 'Для дачи и сада',
//            27 => 'Для открытых кафе и ресторанов',
//            28 => 'Для парков и скверов',
//            29 => 'Кейтеринг',
//            30 => 'Для пляжа',
//            31 => 'Для открытых торговых площадок',
//            32 => 'Детская уличная мебель',
//            33 => 'Гардеробная',
//            34 => 'Прочее',
//            35 => 'Для подростков',
//        ];
    }

    private function getMaterialIzgotovleniyaKarkasa()
    {
        return [
            1 => 'Массив',
            2 => 'Фанера',
            3 => 'ЛДСП',
            4 => 'Металл',
            5 => 'МДФ',
            6 => 'Стекловолокно',
            7 => 'Пластик',
        ];
    }

    private function getTeksturaDrevesiny()
    {
        return [
            1 => 'Видимая',
            2 => 'Невидимая',
        ];
    }

    private function getPodlokotnikMaterial()
    {
        return [
            1 => 'Жёсткий, деревянный',
            2 => 'Мягкий, материал обивки',
            3 => 'Жесткий, металлический',
        ];
    }

    private function getPodlokotnikNalichie()
    {
        return [
            1 => 'Отсутствует',
            2 => 'Правый',
            3 => 'Левый',
        ];
    }

    private function getNapolnitelMatrasaKrovati()
    {
        return [
            1 => 'Латекс натуральный',
            2 => 'Латекс искусственный',
            3 => 'ППУ с эффектом памяти',
            4 => 'ППУ или поролон',
            5 => 'Кокосовая койра',
            6 => 'Войлок',
            7 => 'Хлопок',
            8 => 'Сизаль',
        ];
    }

    private function getOsnovaFasadovKorpusnoyMebeli()
    {
        return [
            1 => 'Металл',
            2 => 'Зеркала',
            3 => 'Стекло',
            4 => 'ЛДСП',
            5 => 'МДФ',
            6 => 'Массив',
        ];
    }

    private function getTsvet()
    {
        return [
            1  => 'Красный',
            2  => 'Зелёный',
            3  => 'Синий',
            4  => 'Жёлтый',
            5  => 'Оранжевый',
            6  => 'Розовый',
            7  => 'Голубой',
            8  => 'Бордовый',
            9  => 'Коричневый',
            10 => 'Фиолетовый',
            11 => 'Салатовый',
            12 => 'Чёрный',
            13 => 'Серый',
            14 => 'Белый',
            15 => 'Бежевый',
            16 => 'Охра',
            17 => 'Лазурит',
            18 => 'Чай с молоком',
            19 => 'Насыщенный серый',
            20 => 'Тёмно зелёный',
            21 => 'Сливовый',
            22 => 'Фисташковый',
        ];
    }

    private function getStil()
    {
        return [
            1  => 'Классический модерн (американский)',
            2  => 'Бохо шик',
            3  => 'Альпийский (шале)',
            4  => 'Классический',
            5  => 'Прованс',
            6  => 'Арт де ко',
            7  => 'Рустик',
            8  => 'Кантри',
            9  => 'Английский',
            10 => 'Винтаж',
            11 => 'Этнический',
            12 => 'Японский',
            13 => 'Скандинавский',
            14 => 'Эклектика',
            15 => 'Индустриальный',
            16 => 'Хайтек',
            17 => 'Минимализм',
            18 => 'Лофт',
            19 => 'Модерн',
            20 => 'Шебби шик',
            21 => 'Ампир',
            22 => 'Контемпорари',
            23 => 'Поп-Арт',
            24 => 'Китч',
            25 => 'Ретро',
            26 => 'Техно',
            27 => 'Средиземноморский',
            28 => 'Промышленный',
            29 => 'Гранж',
            30 => 'Барокко',
            31 => 'Рококо',
            32 => 'Китайский',
            33 => 'Африканский',
            34 => 'Египетский',
            35 => 'Греческий',
            36 => 'Античный',
            37 => 'Восточный',
            38 => 'Арабский',
            39 => 'Исламский',
            40 => 'Марокканский',
            41 => 'Индийский',
            42 => 'Арт-нуово',
        ];
    }

    private function getNapolnitelDlyaMyagkoyMebeli()
    {
        return [
            1 => 'ППУ',
            2 => 'Пружинный блок',
            3 => 'Пружины + ППУ',
            4 => 'Гусиный пух',
            5 => 'Синтепон',
            6 => 'Войлок',
            7 => 'Пенополистирол в гранулах',
        ];
    }

    private function getKonstruktsiyaMatrasa()
    {
        return [
            1 => 'Зависимый пружинный блок',
            2 => 'Независимый пружинный блок',
            3 => 'Беспружинный',
        ];
    }

    private function getKolichestvoNozhekStola()
    {
        return [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => 'Более 4-х',
            6 => 'Нет',
        ];
    }

    private function getFormaStola()
    {
        return [
            1  => 'Прямоугольный',
            2  => 'Квадратный',
            3  => 'Круглый',
            4  => 'Овальный',
            5  => 'Треугольный',
            6  => 'Т-образный',
            7  => 'П-образный',
            8  => 'Угловой',
            9  => 'Полукруглый',
            10 => 'Нестандартная',
        ];
    }

    private function getRaskladnoyMehanizmStola()
    {
        return [
            1 => 'Книжка',
            2 => 'Раздвижной синхронизированный',
            3 => 'Трансформер',
            4 => 'Раздвижной (несинхронизированный)',
            5 => 'Без механизма',
        ];
    }

    private function getTipMyagkoyMebeli()
    {
        return [
            1 => 'Софа',
            2 => 'Тахта',
            3 => 'Кушетка',
        ];
    }

    private function getRaskladnoyMehanizm()
    {
        return [
            1  => 'Книжка',
            2  => 'Дельфин',
            3  => 'Клик-клак',
            4  => 'Телескоп',
            5  => 'Конкорд',
            6  => 'Аккордеон',
            7  => 'Выдвижной',
            8  => 'Францзуская раскладушка',
            9  => 'Американская раскладушка',
            10 => 'Седафлекс',
            11 => 'Конрад',
            12 => 'Еврокнижка',
            13 => 'Пума',
            14 => 'Пантограф',
            15 => 'Без механизма',
            16 => 'Еврософа',
            17 => 'Нью Роллер',
            18 => 'Сезам',
            19 => 'Поворотный',
            20 => 'Караван',
            21 => 'Слон',
            22 => 'Ходри (Hodry) с электроприводом',
            23 => 'Опора-вяз',
            24 => 'Бумеранг',
            25 => 'Софа',
            26 => 'Итальянская раскладушка',
            27 => 'Трансформер',
        ];
    }

    private function getVidyMaterialovNozhekStola()
    {
        return [
            1 => 'Массив',
            2 => 'Металл',
            3 => 'МДФ',
            4 => 'ЛДСП',
            5 => 'Пластик',
            6 => 'Стекло',
        ];
    }

    private function getKolichestvoMest()
    {
        return [
            1 => '2',
            2 => '3',
            3 => 'Более 3-х',
        ];
    }

    private function getProductCatalog()
    {

        return [
            // 1. Шкафы
            [
                'item'  => 1,
                'label' => ['rus' => 'Шкафы'],
                'children' => [
                    [
                        'item'     => 2,
                        'label'    => ['rus' => 'Шкафы распашные'],
                    ],
                    [
                        'item'     => 3,
                        'label'    => ['rus' => 'Шкафы книжные'],
                    ],
                    [
                        'item'     => 4,
                        'label'    => ['rus' => 'Шкафы навесные'],
                    ],
                    [
                        'item'     => 5,
                        'label'    => ['rus' => 'Шкафы-купе'],
                    ],
                    [
                        'item'     => 6,
                        'label'    => ['rus' => 'Шкафы для кабинета'],
                    ],
                    [
                        'item'     => 7,
                        'label'    => ['rus' => 'Шкафы Винные'],
                    ],
                    [
                        'item'     => 8,
                        'label'    => ['rus' => 'Буфеты'],
                    ],
                    [
                        'item'     => 9,
                        'label'    => ['rus' => 'Витрины'],
                    ],
                    [
                        'item'     => 10,
                        'label'    => ['rus' => 'Шкафы для ванн'],
                    ],
                    [
                        'item'     => 11,
                        'label'    => ['rus' => 'Шкафы напольные для ванн'],
                    ],
                    [
                        'item'     => 12,
                        'label'    => ['rus' => 'Шкафы детские'],
                    ],
                    [
                        'item'     => 13,
                        'label'    => ['rus' => 'Шкафы книжные детские'],
                    ],
                    [
                        'item'     => 14,
                        'label'    => ['rus' => 'Шкафы для обуви'],
                    ],
                    [
                        'item'     => 15,
                        'label'    => ['rus' => 'Шкафы для подростков'],
                    ],
                    [
                        'item'     => 16,
                        'label'    => ['rus' => 'Шкафы для инструментов'],
                    ],
                    [
                        'item'     => 17,
                        'label'    => ['rus' => 'Шкафы для инструментов навесные'],
                    ],
                    [
                        'item'     => 18,
                        'label'    => ['rus' => 'Кулеры для вина'],
                    ],
                ]
            ],



            // 2. Тумбы
            [
                'item'  => 25,
                'label' => ['rus' => 'Тумбы'],
                'children' => [
                    [
                        'item'     => 26,
                        'label'    => ['rus' => 'Тумбы'],
                    ],
                    [
                        'item'     => 27,
                        'label'    => ['rus' => 'Тумбы для обуви'],
                    ],
                    [
                        'item'     => 28,
                        'label'    => ['rus' => 'Прикроватные Тумбы'],
                    ],
                    [
                        'item'     => 29,
                        'label'    => ['rus' => 'Тумбы для TV и медиа'],
                    ],
                    [
                        'item'     => 30,
                        'label'    => ['rus' => 'Тумбы для кабинета'],
                    ],
                    [
                        'item'     => 31,
                        'label'    => ['rus' => 'Кухонные тумбы'],
                    ],
                    [
                        'item'     => 32,
                        'label'    => ['rus' => 'Кухонный остров'],
                    ],
                    [
                        'item'     => 33,
                        'label'    => ['rus' => 'Тумба для вина'],
                    ],
                    [
                        'item'     => 34,
                        'label'    => ['rus' => 'Тумбы под раковину'],
                    ],
                    [
                        'item'     => 35,
                        'label'    => ['rus' => 'Тумбы детские'],
                    ],
                    [
                        'item'     => 36,
                        'label'    => ['rus' => 'Тумбы для подростков'],
                    ],
                ]
            ],

            // 3. Комоды
            [
                'item'  => 50,
                'label' => ['rus' => 'Комоды'],
                'children' => [
                    [
                        'item'     => 51,
                        'label'    => ['rus' => 'Комоды'],
                    ],
                    [
                        'item'     => 52,
                        'label'    => ['rus' => 'Комоды высокие'],
                    ],
                    [
                        'item'     => 53,
                        'label'    => ['rus' => 'Комоды для кабинета'],
                    ],
                    [
                        'item'     => 54,
                        'label'    => ['rus' => 'Картотеки'],
                    ],
                    [
                        'item'     => 55,
                        'label'    => ['rus' => 'Секретеры'],
                    ],
                    [
                        'item'     => 56,
                        'label'    => ['rus' => 'Комод для вина'],
                    ],
                    [
                        'item'     => 57,
                        'label'    => ['rus' => 'Комоды детские'],
                    ],
                    [
                        'item'     => 58,
                        'label'    => ['rus' => 'Детские высокие комоды'],
                    ],
                    [
                        'item'     => 59,
                        'label'    => ['rus' => 'Пеленальные столики'],
                    ],
                    [
                        'item'     => 60,
                        'label'    => ['rus' => 'Пеленальные комоды'],
                    ],
                    [
                        'item'     => 61,
                        'label'    => ['rus' => 'Острова для гардеробной'],
                    ],
                    [
                        'item'     => 62,
                        'label'    => ['rus' => 'Комоды для подростков'],
                    ],
                    [
                        'item'     => 63,
                        'label'    => ['rus' => 'Высокие комоды для подростков'],
                    ],
                ]
            ],

            // 4. Полки и Стеллажи
            [
                'item'  => 75,
                'label' => ['rus' => 'Полки и Стеллажи'],
                'children' => [
                    [
                        'item'     => 76,
                        'label'    => ['rus' => 'Навесные полки'],
                    ],
                    [
                        'item'     => 77,
                        'label'    => ['rus' => 'Стеллажи'],
                    ],
                    [
                        'item'     => 78,
                        'label'    => ['rus' => 'Стеллажи Винные'],
                    ],
                    [
                        'item'     => 79,
                        'label'    => ['rus' => 'Стеллажи для ванн'],
                    ],
                    [
                        'item'     => 80,
                        'label'    => ['rus' => 'Полки для ванн'],
                    ],
                    [
                        'item'     => 81,
                        'label'    => ['rus' => 'Стеллажи детские'],
                    ],
                    [
                        'item'     => 82,
                        'label'    => ['rus' => 'Полки детские'],
                    ],
                    [
                        'item'     => 83,
                        'label'    => ['rus' => 'Кухонные Полки'],
                    ],
                    [
                        'item'     => 84,
                        'label'    => ['rus' => 'Полки для обуви'],
                    ],
                    [
                        'item'     => 85,
                        'label'    => ['rus' => 'Навесные полки с ключками'],
                    ],
                    [
                        'item'     => 86,
                        'label'    => ['rus' => 'Стеллажи для обуви'],
                    ],
                    [
                        'item'     => 87,
                        'label'    => ['rus' => 'Металличские стеллажы'],
                    ],
                ]
            ],

            // 5. Модульные системы
            [
                'item'  => 100,
                'label' => ['rus' => 'Модульные системы'],
                'children' => [
                    [
                        'item'     => 101,
                        'label'    => ['rus' => 'Стенки'],
                    ],
                    [
                        'item'     => 102,
                        'label'    => ['rus' => 'Модульные прихожие'],
                    ],
                    [
                        'item'     => 103,
                        'label'    => ['rus' => 'Библиотеки'],
                    ],
                    [
                        'item'     => 104,
                        'label'    => ['rus' => 'Модульные системы для гардеробной'],
                    ],
                    [
                        'item'     => 105,
                        'label'    => ['rus' => 'Модульные системы для детской'],
                    ],
                    [
                        'item'     => 106,
                        'label'    => ['rus' => 'Детские стенки'],
                    ],
                    [
                        'item'     => 107,
                        'label'    => ['rus' => 'Стенки для подростков'],
                    ],
                    [
                        'item'     => 108,
                        'label'    => ['rus' => 'Открытые системы хранения'],
                    ],
                ]
            ],

            // 6. Комплекты
            [
                'item'  => 125,
                'label' => ['rus' => 'Комплекты'],
                'children' => [
                    [
                        'item'     => 126,
                        'label'    => ['rus' => 'Комплекты для кабинета'],
                    ],
                    [
                        'item'     => 127,
                        'label'    => ['rus' => 'Комплекты мебели для ванной комнаты'],
                    ],
                    [
                        'item'     => 128,
                        'label'    => ['rus' => 'Комплекты мягкой мебели (sofa & armchair sets)'],
                    ],
                    [
                        'item'     => 129,
                        'label'    => ['rus' => 'Кухонные уголки'],
                    ],
                    [
                        'item'     => 130,
                        'label'    => ['rus' => 'Комплекты для прихожей'],
                    ],
                    [
                        'item'     => 131,
                        'label'    => ['rus' => 'Комплекты для спальни'],
                    ],
                    [
                        'item'     => 132,
                        'label'    => ['rus' => 'Комплекты детской мебели'],
                    ],
                    [
                        'item'     => 133,
                        'label'    => ['rus' => 'Туалетные столики (комплекты)'],
                    ],
                    [
                        'item'     => 134,
                        'label'    => ['rus' => 'Комплекты Комод и Зеркало'],
                    ],
                    [
                        'item'     => 135,
                        'label'    => ['rus' => 'Комплекты для ТВ и медиа'],
                    ],
                    [
                        'item'     => 136,
                        'label'    => ['rus' => 'Комплекты для кухни'],
                    ],
                    [
                        'item'     => 137,
                        'label'    => ['rus' => 'Комплекты мебели для малышей'],
                    ],
                    [
                        'item'     => 138,
                        'label'    => ['rus' => 'Комплект детский (стол+стулья)'],
                    ],
                    [
                        'item'     => 139,
                        'label'    => ['rus' => 'Комплекты мебели для подростков'],
                    ],
                ]
            ],

            // 8. Диваны
            [
                'item'  => 150,
                'label' => ['rus' => 'Диваны'],
                'children' => [
                    [
                        'item'     => 151,
                        'label'    => ['rus' => 'Диваны прямые'],
                    ],
                    [
                        'item'     => 152,
                        'label'    => ['rus' => 'Диваны угловые'],
                    ],
                    [
                        'item'     => 153,
                        'label'    => ['rus' => 'Диваны модульные'],
                    ],
                    [
                        'item'     => 154,
                        'label'    => ['rus' => 'Диваны с оттоманкой'],
                    ],
                    [
                        'item'     => 155,
                        'label'    => ['rus' => 'Диваны реклайнеры'],
                    ],
                    [
                        'item'     => 156,
                        'label'    => ['rus' => 'Мини диваны'],
                    ],
                    [
                        'item'     => 157,
                        'label'    => ['rus' => 'Кушетки'],
                    ],
                    [
                        'item'     => 158,
                        'label'    => ['rus' => 'Диваны кухонные'],
                    ],
                    [
                        'item'     => 159,
                        'label'    => ['rus' => 'Диваны детские'],
                    ],
                    [
                        'item'     => 160,
                        'label'    => ['rus' => 'Кушетки детские'],
                    ],
                    [
                        'item'     => 161,
                        'label'    => ['rus' => 'Диваны для подостков'],
                    ],
                ]
            ],

            // 9. Кресла
            [
                'item'  => 175,
                'label' => ['rus' => 'Кресла'],
                'children' => [
                    [
                        'item'      => 176,
                        'label'     => ['rus' => 'Кресла'],
                    ],
                    [
                        'item'      => 177,
                        'label'     => ['rus' => 'Кресла реклайнеры'],
                    ],
                    [
                        'item'      => 178,
                        'label'     => ['rus' => 'Кресла качалки'],
                    ],
                    [
                        'item'      => 179,
                        'label'     => ['rus' => 'Кресла мешки'],
                    ],
                    [
                        'item'      => 180,
                        'label'     => ['rus' => 'Кресла с подставкой для ног'],
                    ],
                    [
                        'item'      => 181,
                        'label'     => ['rus' => 'Кресла качалки для кормления'],
                    ],
                    [
                        'item'      => 182,
                        'label'     => ['rus' => 'Кресла детские'],
                    ],
                    [
                        'item'      => 183,
                        'label'     => ['rus' => 'Кресла мешки детские'],
                    ],
                    [
                        'item'      => 184,
                        'label'     => ['rus' => 'Кресла для подростков'],
                    ],
                ]
            ],


            //10. Оттоманки и Пуфы
            [
                'item'  => 200,
                'label' => ['rus' => 'Оттоманки и Пуфы'],
                'children' => [
                    [
                        'item'      => 201,
                        'label'     => ['rus' => 'Оттоманки'],
                    ],
                    [
                        'item'      => 202,
                        'label'     => ['rus' => 'Пуфы'],
                    ],
                    [
                        'item'      => 203,
                        'label'     => ['rus' => 'Детские пуфы'],
                    ],
                    [
                        'item'      => 204,
                        'label'     => ['rus' => 'Подставки для ног'],
                    ],
                ]
            ],


            // 11. Банкетки и Скамейки
            [
                'item'  => 225,
                'label' => ['rus' => 'Банкетки и Скамейки'],
                'children' => [
                    [
                        'item'      => 226,
                        'label'     => ['rus' => 'Банкетки'],
                    ],
                    [
                        'item'      => 227,
                        'label'     => ['rus' => 'Скамейки'],
                    ],
                    [
                        'item'      => 228,
                        'label'     => ['rus' => 'Скамейки для обуви'],
                    ],
                    [
                        'item'      => 229,
                        'label'     => ['rus' => 'Скамейки с ящиком'],
                    ],
                ]
            ],

            // 12. Кровати и Матрасы
            [
                'item'  => 250,
                'label' => ['rus' => 'Кровати и Матрасы'],
                'children' => [
                    [
                        'item'     => 251,
                        'label'    => ['rus' => 'Полуторные кровати'],
                    ],
                    [
                        'item'     => 252,
                        'label'    => ['rus' => 'Двуспальные кровати Queen size'],
                    ],
                    [
                        'item'     => 253,
                        'label'    => ['rus' => 'Двуспальные кровати King size'],
                    ],
                    [
                        'item'     => 254,
                        'label'    => ['rus' => 'Двухъярусные кровати'],
                    ],
                    [
                        'item'     => 255,
                        'label'    => ['rus' => 'Кровати-чердаки детские'],
                    ],
                    [
                        'item'     => 256,
                        'label'    => ['rus' => 'Кровати-чердаки'],
                    ],
                    [
                        'item'     => 257,
                        'label'    => ['rus' => 'Кровати для новорожденных'],
                    ],
                    [
                        'item'     => 258,
                        'label'    => ['rus' => 'Колыбели'],
                    ],
                    [
                        'item'     => 259,
                        'label'    => ['rus' => 'Люльки'],
                    ],
                    [
                        'item'     => 260,
                        'label'    => ['rus' => 'Кровати трансформеры'],
                    ],
                    [
                        'item'     => 261,
                        'label'    => ['rus' => 'Кровати детские'],
                    ],
                    [
                        'item'     => 262,
                        'label'    => ['rus' => 'Кровати для подростков'],
                    ],
                    [
                        'item'     => 263,
                        'label'    => ['rus' => 'Матрасы'],
                    ],
                    [
                        'item'     => 264,
                        'label'    => ['rus' => 'Изголовья'],
                    ],
                    [
                        'item'     => 265,
                        'label'    => ['rus' => 'Каркасы кроватей'],
                    ],
                    [
                        'item'     => 266,
                        'label'    => ['rus' => 'Основания для матрасов'],
                    ],
                    [
                        'item'     => 267,
                        'label'    => ['rus' => 'Наматрасники'],
                    ],
                    [
                        'item'     => 268,
                        'label'    => ['rus' => 'Матрасы для новорожденных'],
                    ],
                    [
                        'item'     => 269,
                        'label'    => ['rus' => 'Матрасы детские'],
                    ],
                    [
                        'item'     => 270,
                        'label'    => ['rus' => 'Наматрасники детские'],
                    ],
                    [
                        'item'     => 271,
                        'label'    => ['rus' => 'Односпальные кровати'],
                    ],
                ]
            ],

            // 13. Стулья
            [
                'item'  => 275,
                'label' => ['rus' => 'Стулья'],
                'children' => [
                    [
                        'item'      => 276,
                        'label'     => ['rus' => 'Стулья'],
                    ],
                    [
                        'item'      => 277,
                        'label'     => ['rus' => 'Складные стулья'],
                    ],
                    [
                        'item'      => 278,
                        'label'     => ['rus' => 'Барные стулья'],
                    ],
                    [
                        'item'      => 279,
                        'label'     => ['rus' => 'Табуреты'],
                    ],
                    [
                        'item'      => 280,
                        'label'     => ['rus' => 'Рабочие кресла'],
                    ],
                    [
                        'item'      => 281,
                        'label'     => ['rus' => 'Детские рабочие кресла'],
                    ],
                    [
                        'item'      => 282,
                        'label'     => ['rus' => 'Детские стулья'],
                    ],
                    [
                        'item'      => 283,
                        'label'     => ['rus' => 'Стульчики для кормления'],
                    ],
                    [
                        'item'      => 284,
                        'label'     => ['rus' => 'Табуреты детские'],
                    ],
                    [
                        'item'      => 285,
                        'label'     => ['rus' => 'Рабочие кресла для подростков'],
                    ],
                    [
                        'item'      => 286,
                        'label'     => ['rus' => 'Игровые кресла'],
                    ],
                    [
                        'item'      => 287,
                        'label'     => ['rus' => 'Табуреты для ванн'],
                    ],
                ]
            ],

            // 14. Столы
            [
                'item'  => 300,
                'label' => ['rus' => 'Столы'],
                'children' => [
                    [
                        'item'     => 301,
                        'label'    => ['rus' => 'Обеденные столы'],
                    ],
                    [
                        'item'     => 302,
                        'label'    => ['rus' => 'Туалетные столики'],
                    ],
                    [
                        'item'     => 303,
                        'label'    => ['rus' => 'Компьютерные столы'],
                    ],
                    [
                        'item'     => 304,
                        'label'    => ['rus' => 'Письменные столы'],
                    ],
                    [
                        'item'     => 305,
                        'label'    => ['rus' => 'Кофейные столики'],
                    ],
                    [
                        'item'     => 306,
                        'label'    => ['rus' => 'Журнальные столики'],
                    ],
                    [
                        'item'     => 307,
                        'label'    => ['rus' => 'Приставные столики'],
                    ],
                    [
                        'item'     => 308,
                        'label'    => ['rus' => 'Сервировочные столики'],
                    ],
                    [
                        'item'     => 309,
                        'label'    => ['rus' => 'Консольные столики'],
                    ],
                    [
                        'item'     => 310,
                        'label'    => ['rus' => 'Столы трансформеры'],
                    ],
                    [
                        'item'     => 311,
                        'label'    => ['rus' => 'Детские столы'],
                    ],
                    [
                        'item'     => 312,
                        'label'    => ['rus' => 'Письменные столы для подростков'],
                    ],
                    [
                        'item'     => 313,
                        'label'    => ['rus' => 'Парты'],
                    ],
                    [
                        'item'     => 314,
                        'label'    => ['rus' => 'Компьютерные столы для подростков'],
                    ],
                    [
                        'item'     => 315,
                        'label'    => ['rus' => 'Раскладные столы'],
                    ],
                    [
                        'item'     => 316,
                        'label'    => ['rus' => 'Чертежные столы'],
                    ],
                    [
                        'item'     => 317,
                        'label'    => ['rus' => 'Барные столы'],
                    ],
                    [
                        'item'     => 318,
                        'label'    => ['rus' => 'Барные стойки'],
                    ],
                    [
                        'item'     => 319,
                        'label'    => ['rus' => 'Передвижные бары'],
                    ],
                    [
                        'item'     => 320,
                        'label'    => ['rus' => 'Игровые столы'],
                    ],
                ]
            ],

            // 15. Вешалки
            [
                'item'  => 325,
                'label' => ['rus' => 'Вешалки'],
                'children' => [
                    [
                        'item'     => 326,
                        'label'    => ['rus' => 'Прихожие'],
                    ],
                    [
                        'item'     => 327,
                        'label'    => ['rus' => 'Напольные вешалки'],
                    ],
                    [
                        'item'     => 328,
                        'label'    => ['rus' => 'Стойки для одежды'],
                    ],
                    [
                        'item'     => 329,
                        'label'    => ['rus' => 'Вешалки для костюма'],
                    ],
                    [
                        'item'     => 330,
                        'label'    => ['rus' => 'Детские вешалки для одежды'],
                    ],
                ]
            ],

            // 16. Аксессуары
            [
                'item'  => 350,
                'label' => ['rus' => 'Аксессуары'],
                'children' => [
                    [
                        'item'     => 351,
                        'label'    => ['rus' => 'Зеркала настенные'],
                    ],
                    [
                        'item'     => 352,
                        'label'    => ['rus' => 'Зеркала для ванн'],
                    ],
                    [
                        'item'     => 353,
                        'label'    => ['rus' => 'Зеркала напольные'],
                    ],
                    [
                        'item'     => 354,
                        'label'    => ['rus' => 'Корзины для белья'],
                    ],
                    [
                        'item'     => 355,
                        'label'    => ['rus' => 'Перегородки'],
                    ],
                    [
                        'item'     => 356,
                        'label'    => ['rus' => 'Каминные порталы'],
                    ],
                    [
                        'item'     => 357,
                        'label'    => ['rus' => 'Подставки'],
                    ],
                    [
                        'item'     => 358,
                        'label'    => ['rus' => 'Подставки под журналы'],
                    ],
                    [
                        'item'     => 359,
                        'label'    => ['rus' => 'Подставки под ноутбук'],
                    ],
                    [
                        'item'     => 360,
                        'label'    => ['rus' => 'Зарядные станции настольные'],
                    ],
                    [
                        'item'     => 361,
                        'label'    => ['rus' => 'Органайзеры для стола'],
                    ],
                    [
                        'item'     => 362,
                        'label'    => ['rus' => 'Вазы'],
                    ],
                    [
                        'item'     => 363,
                        'label'    => ['rus' => 'Подушки на стулья'],
                    ],
                    [
                        'item'     => 364,
                        'label'    => ['rus' => 'Коврики для ванн'],
                    ],
                    [
                        'item'     => 365,
                        'label'    => ['rus' => 'Часы'],
                    ],
                    [
                        'item'     => 366,
                        'label'    => ['rus' => 'Ковры'],
                    ],
                    [
                        'item'     => 367,
                        'label'    => ['rus' => 'Органайзеры для украшений'],
                    ],
                    [
                        'item'     => 368,
                        'label'    => ['rus' => 'Крючки'],
                    ],
                    [
                        'item'     => 369,
                        'label'    => ['rus' => 'Чехлы для обуви'],
                    ],
                    [
                        'item'     => 370,
                        'label'    => ['rus' => 'Подставки под чемодан'],
                    ],
                    [
                        'item'     => 371,
                        'label'    => ['rus' => 'Органайзер для украшений'],
                    ],
                    [
                        'item'     => 372,
                        'label'    => ['rus' => 'Подвесные органайзеры'],
                    ],
                    [
                        'item'     => 373,
                        'label'    => ['rus' => 'Разделители полок'],
                    ],
                    [
                        'item'     => 374,
                        'label'    => ['rus' => 'Коробки'],
                    ],
                    [
                        'item'     => 375,
                        'label'    => ['rus' => 'Корзины'],
                    ],
                    [
                        'item'     => 376,
                        'label'    => ['rus' => 'Плечики для одежды'],
                    ],
                    [
                        'item'     => 377,
                        'label'    => ['rus' => 'Стремянки'],
                    ],
                    [
                        'item'     => 378,
                        'label'    => ['rus' => 'Сушилки для белья'],
                    ],
                    [
                        'item'     => 379,
                        'label'    => ['rus' => 'Стойки для бутылок'],
                    ],
                    [
                        'item'     => 380,
                        'label'    => ['rus' => 'Ящики'],
                    ],
                    [
                        'item'     => 381,
                        'label'    => ['rus' => 'Сундуки'],
                    ],
                ]
            ],


            // 17. Аксессуары для детской
            [
                'item'  => 400,
                'label' => ['rus' => 'Аксессуары для детской'],
                'children' => [
                    [
                        'item'     => 401,
                        'label'    => ['rus' => 'Манежы'],
                    ],
                    [
                        'item'     => 402,
                        'label'    => ['rus' => 'Детские перегородки'],
                    ],
                    [
                        'item'     => 403,
                        'label'    => ['rus' => 'Пеленальные столики накладки'],
                    ],
                    [
                        'item'     => 404,
                        'label'    => ['rus' => 'Матрасы для пеленальных столиков'],
                    ],
                    [
                        'item'     => 405,
                        'label'    => ['rus' => 'Ящики дл игрушек'],
                    ],
                    [
                        'item'     => 406,
                        'label'    => ['rus' => 'Корзины для игрушек'],
                    ],
                    [
                        'item'     => 407,
                        'label'    => ['rus' => 'Органайзеры для игрушек'],
                    ],
                    [
                        'item'     => 408,
                        'label'    => ['rus' => 'Часы детские'],
                    ],
                    [
                        'item'     => 409,
                        'label'    => ['rus' => 'Подушки декоративные для детской'],
                    ],
                    [
                        'item'     => 410,
                        'label'    => ['rus' => 'Детский декор стен'],
                    ],
                    [
                        'item'     => 411,
                        'label'    => ['rus' => 'Детские ступеньки'],
                    ],
                ]
            ],

            // 18. Игровая мебель детская
            [
                'item'  => 425,
                'label' => ['rus' => ' Игровая мебель детская'],
                'children' => [
                    [
                        'item'     => 426,
                        'label'    => ['rus' => 'Шведские стенки'],
                    ],
                    [
                        'item'     => 427,
                        'label'    => ['rus' => 'Игровые комплексы'],
                    ],
                ]
            ],

            // 19. Текстиль
            [
                'item'  => 450,
                'label' => ['rus' => 'Текстиль'],
                'children' => [
                    [
                        'item'     => 451,
                        'label'    => ['rus' => 'Пледы'],
                    ],
                    [
                        'item'     => 452,
                        'label'    => ['rus' => 'Покрывала'],
                    ],
                    [
                        'item'     => 453,
                        'label'    => ['rus' => 'Подушки декоративные'],
                    ],
                ]
            ],

            // 20. Освещение
            [
                'item'  => 475,
                'label' => ['rus' => 'Освещение'],
                'children' => [
                    [
                        'item'     => 476,
                        'label'    => ['rus' => 'Потолочные светильники'],
                    ],
                    [
                        'item'     => 477,
                        'label'    => ['rus' => 'Подвесные светильники'],
                    ],
                    [
                        'item'     => 478,
                        'label'    => ['rus' => 'Люстры'],
                    ],
                    [
                        'item'     => 479,
                        'label'    => ['rus' => 'Потолочные вентиляторы'],
                    ],
                    [
                        'item'     => 480,
                        'label'    => ['rus' => 'Настольные светильники'],
                    ],
                    [
                        'item'     => 481,
                        'label'    => ['rus' => 'Настольные лампы'],
                    ],
                    [
                        'item'     => 482,
                        'label'    => ['rus' => 'Настенные светильники'],
                    ],
                    [
                        'item'     => 483,
                        'label'    => ['rus' => 'Точечные светильники'],
                    ],
                    [
                        'item'     => 484,
                        'label'    => ['rus' => 'Напольные светильники'],
                    ],
                    [
                        'item'     => 485,
                        'label'    => ['rus' => 'Потолочные светильники детские'],
                    ],
                    [
                        'item'     => 486,
                        'label'    => ['rus' => 'Подвесные светильники детские'],
                    ],
                    [
                        'item'     => 487,
                        'label'    => ['rus' => 'Настольные светильники детские'],
                    ],
                    [
                        'item'     => 488,
                        'label'    => ['rus' => 'Настольные лампы детские'],
                    ],
                    [
                        'item'     => 489,
                        'label'    => ['rus' => 'Настенные светильники детские'],
                    ],
                    [
                        'item'     => 490,
                        'label'    => ['rus' => 'Напольные светильники детские'],
                    ],
                    [
                        'item'     => 491,
                        'label'    => ['rus' => 'Ночники детские'],
                    ],
                ]
            ],

        ];

//        return [
//            1   => 'Хранение',
//            2   => 'Диваны прямые',
//            3   => 'Диваны угловые',
//            4   => 'Кресла',
//            5   => 'Банкетки',
//            6   => 'Комплекты мягкой мебели',
//            7   => 'Ванные комнаты',
//            8   => 'Шкафы детские',
//            9   => 'Зеркала',
//            10  => 'Манежи',
//            11  => 'Тумбы',
//            12  => 'Комоды детские',
//            13  => 'Кресла-реклайнеры',
//            14  => 'Шкафы для ванн',
//            15  => 'Зеркала для ванн',
//            16  => 'Кровати и матрасы',
//            17  => 'Односпальные кровати',
//            18  => 'Двуспальные кровати',
//            19  => 'Матрасы',
//            20  => 'Вешалки',
//            21  => 'Туалетные столики',
//            22  => 'Стулья',
//            23  => 'Складные стулья',
//            24  => 'Табуретки',
//            25  => 'Перегородки',
//            26  => 'Шкафы под мойку',
//            27  => 'Барные стулья',
//            28  => 'Тумбочки детские',
//            29  => 'Мягкая мебель',
//            30  => 'Детская мебель',
//            31  => 'Кровати детские',
//            32  => 'Матрасы детские',
//            33  => 'Детские столы',
//            34  => 'Комоды',
//            35  => 'Колыбели',
//            36  => 'Шкафы-купе',
//            37  => 'Игровая мебель',
//            38  => 'Диваны детские',
//            39  => 'Шкафы распашные',
//            40  => 'Модульные системы для детской',
//            41  => 'Пеленальные столики',
//            42  => 'Комплекты детской мебели',
//            43  => 'Предметы интерьера',
//            44  => 'Пуфы',
//            45  => 'Детские стулья',
//            46  => 'Стеллажи',
//            47  => 'Столы',
//            48  => 'Компьютерные столы',
//            //49  => 'Стулья',
//            50  => 'Кухонные уголки',
//            51  => 'Обеденные столы',
//            52  => 'Письменные столы',
//            53  => 'Кофейные столики',
//            54  => 'Журнальные столы',
//            55  => 'Консольные столики',
//            56  => 'Диваны модульные',
//            57  => 'Диваны с оттоманкой',
//            58  => 'Диваны-реклайнеры',
//            59  => 'Кресла-качалки',
//            60  => 'Кресла-мешки',
//            61  => 'Скамейки мягкие',
//            62  => 'Мини-диваны',
//            63  => 'Кушетки',
//            64  => 'Корзины',
//            65  => 'Настенные полки',
//            66  => 'Секретеры',
//            67  => 'Буфеты',
//            68  => 'Шкафы навесные',
//            //69  => 'Стеллажи',
//            70  => 'Стенки',
//            71  => 'Тумбочки прикроватные',
//            72  => 'Кровати двухъярусные детские',
//            73  => 'Сервировочные столики',
//            74  => 'Тумбы под ТВ',
//            75  => 'Прихожие',
//            76  => 'Витрины',
//            77  => 'Каминные порталы',
//            78  => 'Полуторные кровати',
//            79  => 'Часы',
//            80  => 'Освещение',
//            81  => 'Тумбы для обуви',
//            82  => 'Открытые полки',
//            83  => 'Комплекты мебели для ванной комнаты',
//            84  => 'Подставки',
//            85  => 'Кровати-чердаки детские',
//            86  => 'Приставные столики',
//            87  => 'Скамейки',
//            88  => 'Кабинеты',
//            89  => 'Шкафы книжные',
//            90  => 'Стеллажи детские',
//            91  => 'Стульчики для кормления',
//            92  => 'Кресла-качалки для кормления',
//            93  => 'Столы письменные для подростков',
//            94  => 'Парты',
//            95  => 'Компьютерные столы для подростков',
//            96  => 'Детские рабочие кресла',
//            97  => 'Кресла детские',
//            98  => 'Кресла-мешки детские',
//            99  => 'Кушетки детские',
//            100 => 'Детские пуфы',
//            101 => 'Кроватки для новорожденных',
//            102 => 'Вазы',
//        ];
    }

    private function getPattern()
    {
        return [
            1 => 'Однотонные',
            2 => 'Многотонные',
            3 => 'Клетка',
            4 => 'Полоска',
            5 => 'Цветочный рисунок',
            6 => 'Рисунок с геометрическими фигурами',
            7 => 'Пейзажи, животные',
            8 => 'Детский рисунок',
            9 => 'Сложный рисунок',
        ];
    }

    private function getAddressType()
    {
        return [
            1 => 'Юридический',
            2 => 'Почтовый',
            3 => 'Фактический',
            4 => 'Другое',
        ];
    }

    private function getLanguage()
    {
        return [
            'eng-US' => 'Английский',
            'rus-RU' => 'Русский',
            'zho-ZH' => 'Китайский',
            'ukr-UK' => 'Украинский',
            'pol-PL' => 'Польский',
            'tur-TR' => 'Турецкий',
            'ita-IT' => 'Итальянский',
            'ger-DE' => 'Немецкий',
        ];
    }


    private function getTypeOfOption()
    {
        return [
            'Property' => 'Property',
            'Material' => 'Material',
            'PriceCategory' => 'PriceCategory',
        ];
    }
    private function getHidingPower()
    {
        return [
            1 => 'Прозрачная',
            2 => 'Непрозрачная',
            3 => 'Открытые поры',
            4 => 'Закрытые поры',
        ];
    }

    private function getGlossDegree()
    {
        return [
            1 => 'Высокий глянец',
            2 => 'Обычный глянец',
            3 => 'Полуглянец',
            4 => 'Полуматовый',
            5 => 'Матовый',
            6 => 'Глубокоматовый',
        ];
    }

    private function getFabricStructure()
    {
        return [
            1 => 'Натуральные',
            2 => 'Синтетические',
            3 => 'Искусственные',
            4 => 'Смешанные',
        ];
    }

    private function getWearResistance()
    {
        return [
            1 => 'Декоративные. Менее 10 000 циклов',
            2 => 'Деликатные. От 10 000 до 15 000 циклов',
            3 => 'Повседневной эксплуатации. От 15 000 до 25 000 циклов',
            4 => 'Интенсивной эксплуатации. От 25 000 до 30 000 циклов',
            5 => 'Коммерческое использование. Выше 30 000 циклов',
        ];
    }

    private function getCurrency()
    {
        return [
            'USD' => 'Доллар США',
            'RUB' => 'Российский рубль',
            'BYN' => 'Белорусский рубль',
            'GBP' => 'Британский фунт стерлингов',
            'EUR' => 'Евро',
            'PLN' => 'Польский злотый',
            'UAH' => 'Украинская гривна',
            'CNY' => 'Китайский юань',
            'TRY' => 'Турецкая лира',
            'DKK' => 'Датская крона',
            'SEK' => 'Шведская крона',
            'MXN' => 'Мексиканское песо (edited)',
        ];
    }

    private function getSegment()
    {
        return [
            1 => 'Эконом',
            2 => 'Эконом +',
            3 => 'Доступный',
            4 => 'Средний',
            5 => 'Высокий',
            6 => 'Элитный',
            7 => 'Эксклюзивный',

        ];
    }

    private function getGrade()
    {
        return [
            1 => 'Эконом',
            2 => 'Средний уровень',
            3 => 'Качественное',
            4 => 'Высокое качество',
            5 => 'Эксклюзивное',
        ];
    }

    private function getPopularity()
    {
        return [
            1 => 'Новый товар',
            2 => 'Не было продаж',
            3 => 'Единичные продажи',
            4 => 'Часто продаваемый',
            5 => 'Повышенный спрос',
            6 => 'Хит продаж',
        ];
    }

    private function getColorResistance()
    {
        return [
            '1' => 'Неустойчивые <3',
            '2' => 'Невысокая устойчивость 3-4',
            '3' => 'Устойчивые 4-5',
            '4' => 'Высокая устойчивость >5',
        ];
    }

    private function getPvcFilmFinishing()
    {
        return [
            1 => 'Однотонная',
            2 => 'С рисунком',
            3 => 'Софт-тач',
            4 => '3Д',
            5 => 'Голограмма',
            6 => 'Патина',
            7 => 'Тиснение',
        ];
    }

    private function getPvcFilmType()
    {
        return [
            1 => 'Матовая',
            2 => 'Глянцевая',
            3 => 'Текстурная',
            4 => 'Зеркальная',
        ];
    }

    private function getGlassBrand()
    {
        return [
            1 => 'Лакобель',
            2 => 'Стемалит',
            3 => 'Оракал',
        ];
    }

    private function getGlassProperty()
    {
        return [
            1 => 'Закаленное',
            2 => 'Ударопрочное',
            3 => 'Термоустйчивое',
        ];
    }

    private function getGlassDecoration()
    {
        return [
            1 => 'Деколь',
            2 => 'Фьюзинг',
            3 => 'Шелкография',
            4 => 'Витраж',
            5 => 'Покраска',
            6 => 'Триплекс',
            7 => 'Термопечать',
            8 => 'Лакобель',
        ];
    }

    private function getGlassKind()
    {
        return [
            1 => 'Прозрачное',
            2 => 'Цветное',
            3 => 'Тонированное',
            4 => 'Зеркальное',
        ];
    }

    private function getFacadeDesign()
    {
        return [
            1 => 'Рамочный',
            2 => 'Сплошной',
        ];
    }
    private function getAdditionalFinishingOptions()
    {
        return [
            1 => 'Антикатура (искусственное старение)',
            2 => 'Софт-тач',
            3 => 'Паспарту',
            4 => 'Аэрография',
            5 => 'Фотопечать',
            6 => 'Инкрустация',
        ];
    }
    private function getTexture()
    {
        return [
            1 => 'Слабовыраженная',
            2 => 'Кольца',
            3 => 'Полосы',
            4 => 'Пламя',
            5 => 'Птичий глаз',
            6 => 'Скрипичная спинка',
            7 => 'Муар',
        ];
    }
    private function getPatternMaterial()
    {
        return [
            1  => 'Однотонные',
            2  => 'Многотонные',
            3  => 'Клетка',
            4  => 'Полоска',
            5  => 'Цветочный рисунок',
            6  => 'Рисунок с геометрическими фигурами',
            7  => 'Пейзажи, животные',
            8  => 'Детский рисунок',
            9  => 'Сложный рисунок',
            10 => 'Камень',
            11 => 'Вензеля',
            12 => 'Рогожка',
        ];
    }
}
