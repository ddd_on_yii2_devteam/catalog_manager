<?php

namespace console\controllers;

use commonprj\components\core\models\i18n\SourceRecord;
use commonprj\components\crm\entities\employee\Employee;
use commonprj\components\crm\entities\manufacturer\Manufacturer;
use yii\console\Controller;
use yii\helpers\Console;

class SeederController extends Controller
{

    public function actionIndex()
    {
        $help = "\nДля загрузки начальных данных:\n" .
            "php yii seeder/create\n\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCreate()
    {
        $employee = new Employee();
        $employeeData = [
            'isActive'   => true,
            'sortOrder'  => 500,
            'properties' => [
                "country"  => 'ru',
                "language" => 'rus-RU',
                "currency" => 'RUB',
                "password" => '123',
                "email"    => 'admin@admin.com',
            ],
        ];

        $employee->load($employeeData, '');
        $employee->save();


        $manufacturer = new Manufacturer();
        $manufacturerData = [
            'isActive'   => true,
            'sortOrder'  => 500,
            'properties' => [
                "legalName"        => 'Отрада-А',
                "description"      => 'admin furniprice',
                "country"          => 'ru',
                "organizationCode" => 'Furni.00000',
                "tradeMark"        => 'Furniprice',
            ],
        ];

        $manufacturer->load($manufacturerData, '');
        $manufacturer->save();

        $employee->bindCompany($manufacturer->id);

        $this->stdoutSourceCreate($employee, $manufacturer);
    }

    private function stdoutSourceCreate($employee, $manufacturer)
    {
        $this->stdout('Add employee ');
        $this->stdout($employee->id, Console::FG_CYAN);
        $this->stdout(' with manufacturer ');
        $this->stdout($manufacturer->id, Console::FG_GREEN);
        $this->stdout(" items\n");
    }

}
