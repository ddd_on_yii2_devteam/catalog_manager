<?php

namespace console\controllers;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\ContextRecord;
use commonprj\components\core\models\PropertyTypeRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use console\models\Company;
use console\models\Material;
use console\models\Product\ProductImportMapping;
use Yii;
use yii\console\Controller;

class BaseImportController extends Controller
{

    protected $baseContexts = [
        'catalog',
        'crm',
        'tcm',
        'core',
    ];
    private $contexts = null;
    private $propertyTypes = null;

    public function findCompany($id) {
        $company = Company\CompanyImportMapping::findOne(['origin_id' => $id]);

        return $company->element_id ?? null;
    }

    public function findAddress($id) {
        $address = Company\AddressImportMapping::findOne(['origin_id' => $id]);

        return $address->element_id ?? null;
    }

    public function findPriceCategory($id)
    {
        $category = Material\MaterialCategoryImportMapping::findOne(['origin_id' => $id]);

        return $category->element_id ?? null;
    }

    public function findMaterialCollection($id)
    {
        $materialCollection = Material\MaterialCollectionImportMapping::findOne(['origin_id' => $id]);

        return $materialCollection->element_id ?? null;
    }

    public function findMaterial($name)
    {
        $material = Material\MaterialImportMapping::findOne(['origin_name' => $name]);

        return $material->element_id ?? null;
    }

    public function findColoration($id)
    {
        $coloration = Material\ColorationImportMapping::findOne(['origin_id' => $id]);

        return $coloration->element_id ?? null;
    }

    public function findProduct($id)
    {
        $coloration = ProductImportMapping::findOne(['origin_id' => $id]);

        return $coloration->element_id ?? null;
    }

    protected function initDomainData() {
        $this->initContexts();
    }

    protected function initContexts() {
        foreach ($this->baseContexts as $baseContext) {
            $context = ContextRecord::findOne(['name' => $baseContext]);
            $this->contexts[$baseContext] = $context->id;
        }
    }

    protected function getContextIdByName($name) {
        return $this->contexts[$name];
    }

    protected function addProperty($name, $propParams)
    {
        //dump($propParams);
        //die;
        $property = new Property([
            'name'           => $name,
            'sysname'        => $propParams['sysname'],
            'description'    => $propParams['description'],
            'isSpecific'     => false,
            'sortOrder'      => 1000,
            'propertyTypeId' => $this->getPropertyTypeIdByType($propParams['property_type'] ?? 'string'),
        ]);
        $property->save();

        return $property;
    }

    protected function getProperty($PropertyRecord) {
        $property = new Property();
        $property->setAttributes(BaseDBRepository::arrayKeysUnderscore2CamelCase($PropertyRecord->attributes), false);

        return $property;
    }

    protected function getPropertyTypeIdByType($propTypeName) {
        if (empty($this->propertyTypes)) {
            $propTypes = PropertyTypeRecord::find()->indexBy('name')->all();

            foreach ($propTypes as $propName => $propType) {
                $this->propertyTypes[$propName] = $propType['id'];
            }
        }

        return $this->propertyTypes[$propTypeName];
    }

    protected function createSupportImportTables() {
        $query = 'CREATE TABLE IF NOT EXISTS company_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
                ->createCommand($query)
                ->queryAll();

        $query = 'CREATE TABLE IF NOT EXISTS address_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
                ->createCommand($query)
                ->queryAll();
    }

    protected function getSupportImportDb() {
        return Yii::$app->import;
    }

    protected function renderPropertyValues(array $properties, ...$args)
    {
        $result = [];

        foreach ($properties as $propertyId => $propertyValue) {
            if (is_array($propertyValue) && is_callable($propertyValue)) {
                $result[$propertyId] = call_user_func_array($propertyValue, $args);
            } else {
                if ($propertyValue){
                    $result[$propertyId] = $propertyValue;
                }
            }
        }
        return array_filter($result);
    }

}
