<?php

namespace console\controllers;

use commonprj\components\catalog\entities\productModel\ProductModel;
use commonprj\components\core\entities\property\Property;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class SegmentController extends Controller
{

    private $eco = 0;
    private $eco_plus = 0.25;
    private $affordable = 0.46;
    private $medium = 0.64;
    private $high = 0.79;
    private $elite = 0.90;
    private $exclusive = 0.97;

    public function actionIndex()
    {
        $help = "\nДля загрузки начальных данных:\n" .
            "php yii segment/calc\n\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCalc()
    {
        $this->stdout(date('Y.m.d H:i:s') . " Start" . PHP_EOL, Console::FG_YELLOW);
        $property = Property::findOne(['sysname' => 'furnitureType']);
        $price = Property::findOne(['sysname' => 'price']);
        $this->stdout(date('Y.m.d H:i:s') . " property is load" . PHP_EOL, Console::FG_YELLOW);
        $furnityreTypeTree = $property->getListItems();
        $this->stdout(date('Y.m.d H:i:s') . " getListItems" . PHP_EOL, Console::FG_YELLOW);
        foreach ($furnityreTypeTree as $parent) {
            $childrens = [$parent->id];
            foreach ($parent->children as $children) {
                $childrens[] = $children->id;
            }
            // получаем список всех элементов в одной из категорий
            $sql = "SELECT *, CASE
                    WHEN sum2 < sum*{$this->eco_plus} THEN 1
                    WHEN sum2 < sum*{$this->affordable} THEN 2
                    WHEN sum2 < sum*{$this->medium} THEN 3
                    WHEN sum2 < sum*{$this->high} THEN 4
                    WHEN sum2 < sum*{$this->elite} THEN 5
                    WHEN sum2 < sum*{$this->exclusive} THEN 6
                ELSE
                    7
                END AS segment
                FROM (
                    SELECT *,SUM(value_in_base_currency) OVER () AS sum,SUM(value_in_base_currency) OVER (ORDER BY value_in_base_currency,pe.element_id) AS sum2 
                    FROM property_value_money pvm
                    INNER JOIN property_value2element pe 
                      ON pvm.id=pe.property_value_id AND pvm.property_id=pe.property_id
                    INNER JOIN property_value2element pe2 
                      ON pe2.element_id = pe.element_id AND pe2.property_id={$property->id}
                    WHERE pvm.property_id={$price->id} AND pe2.property_value_id IN (" . implode(',', $childrens) . ") 
                    ORDER BY value_in_base_currency,pe.element_id ASC
                ) t";
            echo PHP_EOL.PHP_EOL.$sql.PHP_EOL.PHP_EOL;
            $rows = Yii::$app->dbSearch->createCommand($sql)->queryAll();
            foreach ($rows as $row) {
                $model = ProductModel::find()->withProperties(false)->where(['id' => $row['element_id']])->one();
                $model->load(['properties' => [
                        "segment" => [$row['segment']],
                    ]],'');
                echo $model->id . ' set segment: ' . $row['segment'] .", sum: {$sum}, sum2: {$sum2}". PHP_EOL;
                $model->save();
            }
        }

    }
}