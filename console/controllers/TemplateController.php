<?php

namespace console\controllers;

use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;
use commonprj\components\core\models\PropertyRecord;
use yii\base\Module;
use yii\helpers\Console;

class TemplateController extends BaseImportController
{

    public $properties = [
        'pvcFilm' => [
            'pvcFilmThickness',
            'pvcFilmType',
            'pvcFilmFinishing'
        ],
        'glass'   => [
            'glassBrand',
            'glassKind',
            'glassDecoration',
            'glassProperty'
        ],
        'wood'    => [
            'additionalFinishingOptions',
            'glossDegree',
            'hidingPower',
            'texture',
        ],
        'fabric'  => [
            'patternMaterial',
            'getFireResistance',
            'getWearResistance',
            'getFabricDensity',
            'getColorResistance',
            'getFabricStructure',
        ],


    ];

    public $rus = [
        'pvcFilm' => 'Шаблон для пленки ПВХ',
        'glass'   => 'Шаблон для стекла',
        'wood'    => 'Шаблон для дерева',
        'fabric'  => 'Шаблон для тканей',
        'metal'  => 'Шаблон для металла',
    ];

    public function __construct(string $id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function options($actionID)
    {
        return ['continue'];
    }

    public function actionIndex()
    {
        $help = "\nДля добавления шаблонов по видам товаров наберите в консоли:\n" .
            "php yii template/create\n\n" .
            "\nДля привязки шаблонов:\n" .
            "php yii template/bind\n\n";
        $this->stdout($help, Console::FG_GREEN);

    }

    public function actionCreate()
    {
        foreach ($this->properties as $name => $prop) {
            $this->stdout("{$name}: " . count($prop) . "\n", Console::FG_GREEN);
            $model = new MaterialTemplate(['isActive' => true]);
            $ids = PropertyRecord::find()->select(['id'])->where(['sysname' => $prop])->asArray()->column();

            $data = [
                'isActive'   => true,
                "properties" => [
                    "properties" => $ids,
                    "title"      => [
                        'rus' => $this->rus[$name],
                        'eng' => $name,
                    ],
                    'name' => $name,
                ]
            ];
            $model->load($data, '');
            $model->save();
        }

        $this->stdout("TOTAL: " . count($this->properties) . "\n", Console::FG_GREEN);
        $this->stdout("------------\n", Console::FG_BLACK);
    }
}