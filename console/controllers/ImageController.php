<?php

namespace console\controllers;

use Yii;
use yii\base\Module;
use yii\helpers\Console;

class ImageController extends BaseImportController
{

    public function __construct(string $id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        $help = "\nДля импорта изображений наберите в консоли:\n" .
                "php yii image/create\n\n" .
                $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCreate()
    {
        $this->createSupportImportTables();

        if (!$this->confirm("Start import images?")) {
            die;
        }

        $sql = "SELECT
            p.id,
            GROUP_CONCAT(DISTINCT (i.name) SEPARATOR ';') image
        FROM
            product p
                INNER JOIN
            product_image i ON p.id = i.product_id group by p.id
        ";
        $products = Yii::$app->db_mcore->createCommand($sql)->queryAll();
        foreach ($products as $product) {
            $this->getImage($product);
        }

        $this->stdout("------------\n", Console::FG_BLACK);
    }

    protected function getImage(...$args)
    {
        $product = $args[0];
        $img = [];
        $this->stdout("{$product['id']}: \n", Console::FG_GREEN);
        if (!empty($product['image'])) {
            $images = explode(';', $product['image']);
            $images = array_unique($images);
            foreach ($images as $image) {
                if (!($data = $this->check('/tmp/' . $image))) {
                    $content = @file_get_contents('http://content.furniprice.com/product/' . $image);
                    if (!$content) {
                        continue;
                    }
                    file_put_contents('/tmp/' . $image, $content);
                    $data = $this->upload('/tmp/' . $image);
                }
                if (!empty($data->id)) {
                    $img[] = $data->id . '.' . $data->format;
                }
            }
        }
        return $img;
    }

    private function check($file)
    {
        $args['id'] = md5($file);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://media-service.furniprice.local/image/' . $args['id']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $jsondata = json_decode($data);
        curl_close($ch);
        if (isset($jsondata->id) && !is_null($jsondata->id)) {
            $this->stdout('    -> Image exists: ' . $jsondata->id . '.' . $jsondata->format . PHP_EOL, Console::FG_GREEN);
            return $jsondata;
        }
        return false;
    }

    private function upload($file)
    {
        $args['id'] = md5($file);
        $args['image'] = new \CurlFile($file);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://media-service.furniprice.local/image');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $jsondata = json_decode(curl_exec($ch));
        curl_close($ch);
        if (!empty($jsondata->id)) {
            $this->stdout('        done: ' . $jsondata->id . '.' . $jsondata->format . PHP_EOL, Console::FG_YELLOW);
        } else {
            $this->stdout('        something wrong! ' . $file . PHP_EOL, Console::FG_RED);
        }
        return $jsondata ?? null;
    }

    private function stdoutText($text, $id, $elementId)
    {
        $this->stdout($text);
        $this->stdout($id, Console::FG_CYAN);
        $this->stdout(' as Element ');
        $this->stdout($elementId, Console::FG_GREEN);
        $this->stdout("\n");
    }

}
