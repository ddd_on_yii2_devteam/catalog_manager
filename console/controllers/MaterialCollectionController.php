<?php

namespace console\controllers;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\components\core\models\ElementClassRecord;
use commonprj\components\core\models\PropertyRecord;
use console\models\material\ColorationImportMapping;
use console\models\material\MaterialCatalog;
use console\models\material\MaterialCategory;
use console\models\Material\MaterialCategoryImportMapping;
use console\models\material\MaterialCollectionImportMapping;
use console\models\material\MaterialDepiction;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\web\NotFoundHttpException;

class MaterialCollectionController extends BaseImportController
{

    const MATERIALCOLLECTION = 'materialCollection';
    const PRICECATEGORY = 'priceCategory';
    const PRODUCTMATERIAL = 'productMaterial';

    public $continue;
    private $domainModels = [
        self::MATERIALCOLLECTION              => [
            'context' => 'catalog',
            'name'    => 'MaterialCollection',
        ],
        self::PRICECATEGORY      => [
            'context' => 'catalog',
            'name'    => 'PriceCategory',
        ],
        self::PRODUCTMATERIAL    => [
            'context' => 'catalog',
            'name'    => 'ProductModel',
        ],
    ];
    private $baseProperties = [
        self::MATERIALCOLLECTION              =>
        [
            'name',
            'MaterialCollection2PriceCategory',
            'MaterialCollection2Coloration',
            'MaterialCollection2Manufacturer',
        ],
        self::PRICECATEGORY      => [
            'name',
            'PriceCategory2Manufacturer',
        ],
        self::PRODUCTMATERIAL    => [
            'name',
            'vendorCode',
            'image',
            'colors',
            'pattern',
            'ProductMaterial2Manufacturer',
        ],
    ];
    private $fieldsGroup = [
        self::MATERIALCOLLECTION              => [
            'property' => [
                'name' => 'name',
            ],
        ],
        self::PRICECATEGORY      => [
            'property' => [
                'name' => "name",
            ],
        ],
        self::PRODUCTMATERIAL    => [
            'property' => [
                'name'       => 'name',
                'vendorCode' => 'code',
                'image'      => 'getImageName',
                'colors'     => 'getColorFromRal',
                'pattern'    => 'pattern_type',
            ],
        ],
    ];
    private $fieldsToPoperties = null;

    public function options($actionID)
    {
        return ['continue'];
    }

    public function actionIndex()
    {
        $help = "\nДля создания импорта наберите в консоли:\n" .
            "php yii material-collection/create\n\n";
        $this->stdout($help, Console::FG_GREEN);
    }

    public function actionCreate()
    {
        $this->createSupportImportTables();

        if ($this->confirm("Are you sure you want to clear ALL import data?")) {
            $this->clearSupportImportTables();
        }

        $this->initDomainData();
        $this->initEntityData();

        // materialCollection
        $materialCatalogs = MaterialCatalog::find()
            ->all();

        foreach ((array)$materialCatalogs as $materialCatalog) {
            $this->addMaterialCatalog($materialCatalog);
        }

        // priceCategory
        $categories = MaterialCategory::find()
            ->all();

        foreach ((array)$categories as $category) {
            $this->addPriceCategory($category);
        }

        // coloration
        $pics = MaterialDepiction::find()
            ->all();

        foreach ((array)$pics as $pic) {
            $this->addColoration($pic);
        }


        // bind Data
        if ($this->confirm("Are you sure you want to bind data?")) {

            $this->actionBindMaterialCollection();
            $this->actionBindPriceCategory();
            //$this->actionBindColoration();
            $this->actionBindMaterial();
        } else {
            $this->stdout("Action was cancelled by user. Nothing has been performed.\n");
        }


        $this->stdout("TOTAL: " . count($materialCatalogs) . " materialCollection\n", Console::FG_GREEN);
        $this->stdout("TOTAL: " . count($categories) . " priceCategory\n", Console::FG_GREEN);
        $this->stdout("TOTAL: " . count($pics) . " coloration\n", Console::FG_GREEN);
        $this->stdout("------------\n", Console::FG_BLACK);
    }

    public function actionBindMaterialCollection()
    {
        // materialCollection
        $materialCatalogs = MaterialCatalog::find()
            ->all();

        foreach ((array)$materialCatalogs as $materialCatalog) {
            $this->bindMaterialCollection2Manufacturer($materialCatalog);
        }
        foreach ((array)$materialCatalogs as $materialCatalog) {
            $this->bindMaterialCollection2PriceCategory($materialCatalog);
        }
    }

    public function actionBindPriceCategory()
    {
        // priceCategory
        $categories = MaterialCategory::find()
            ->all();

        foreach ((array)$categories as $category) {
            $this->bindPriceCategory2Manufacturer($category);
        }
    }

    public function actionBindColoration()
    {
        // coloration
        $pics = MaterialDepiction::find()
            ->all();

        foreach ((array)$pics as $pic) {
            $this->bindColoration2Manufacturer($pic);
        }
        foreach ((array)$pics as $pic) {
            $this->bindColoration2MaterialCollection($pic);
        }
    }

    public function actionBindMaterial()
    {
        $query = 'SELECT
               mc.id,pvt.value
            FROM
               property_value_translation pvt
                   INNER JOIN
               property_value pv ON pvt.translatable_id = pv.id
                      INNER JOIN
               property p ON pv.property_id = p.id
                      INNER JOIN
               material_type_property mtp ON mtp.property_id = p.id
                      INNER JOIN
               material_type mt ON mt.id = mtp.material_type_id
                      INNER JOIN
               material_directory md ON md.type_id = mt.id
                      INNER JOIN
               material_catalog mc ON mc.directory_id = md.id
            WHERE
               pvt.id >1 and locale="ru" and pvt.value in ("Массив","Ясень","Бук","Берёза","Вишня","Тополь","Сосна","Орех","Липа","Пихта","Тик","Махагон","Дуб","Кедр","Клён","Красное дерево","Фанера","Шпон","МДФ","ЛДСП","Искусственный камень","Стекло","Стекловолокно","Мрамор/Гранит","Керамическая плитка","Пластик","Металл","Текстиль","Вельвет","Скотчгард","Шенилл","Микрофибра","Флок","Жаккард","Микрожаккард","Велюр","Искусственная кожа","ЭКО кожа","Замша","Искусственная замша") and type=7';
        $mc2m = Yii::$app->get('db_mcore')
            ->createCommand($query)
            ->queryAll();

        foreach ($mc2m as $row) {
            $this->bindMaterialCollection2Material($row);
        }
    }

    public function addMaterialCatalog(MaterialCatalog $element)
    {
        $element_id = $this->findMaterialCollection($element->id);
        $entityData = $this->getDataForEntity($element->toArray(), self::MATERIALCOLLECTION);
        // если элемент не создан
        if (!$element_id) {
            $model = new MaterialCollection(['isActive' => $element->active]);
            $propertyValues = $this->renderPropertyValues($entityData['property'], $element->toArray());

            $model->save();
            $result = $model->saveProperties($propertyValues);

            $element_id = $model->id;
            if ($result) {
                (new MaterialCollectionImportMapping([
                    'element_id' => $model->id,
                    'origin_id'  => $element->id,
                ]))->save();
            }

            $this->stdoutText('  -> Add MaterialCollection with id ', $element->id, $element_id);
        } else {
            $this->stdoutTextExist('  -> Exist MaterialCollection with id ', $element->id);
        }

        return $element_id ?? null;
    }

    public function addPriceCategory(MaterialCategory $element)
    {
        $element_id = $this->findPriceCategory($element->id);
        $entityData = $this->getDataForEntity($element->toArray(), self::PRICECATEGORY);
        // если элемент не создан
        if (!$element_id) {
            $model = new PriceCategory(['isActive' => true]);
            $propertyValues = $this->renderPropertyValues($entityData['property'], $element->toArray());

            $model->save();
            $result = $model->saveProperties($propertyValues);
            $element_id = $model->id;

            if ($result) {
                (new MaterialCategoryImportMapping([
                    'element_id' => $model->id,
                    'origin_id'  => $element->id,
                ]))->save();
            }
            $this->stdoutText('  -> Add PriceCategory with id ', $element->id, $result['element_id']);
        } else {
            $this->stdoutTextExist('  -> Exist PriceCategory with id ', $element->id);
        }

        return $element_id ?? null;
    }

    public function addColoration(MaterialDepiction $element)
    {
        $element_id = $this->findColoration($element->id);
        $entityData = $this->getDataForEntity($element->toArray(), self::PRODUCTMATERIAL);
        // если элемент не создан
        if (!$element_id) {
            $model = new ProductMaterial(['isActive' => true]);
            $propertyValues = $this->renderPropertyValues($entityData['property'], $element->toArray());

            $model->save();
            $result = $model->saveProperties($propertyValues);
            $element_id = $model->id;

            if ($result) {
                (new ColorationImportMapping([
                    'element_id' => $element_id,
                    'origin_id'  => $element->id,
                ]))->save();
            }
            $this->stdoutText('  -> Add Coloration with id ', $element->id, $element_id);
        } else {
            $this->stdoutTextExist('  -> Exist Coloration with id ', $element->id);
        }

        return $element_id ?? null;
    }

    public function bindColoration2Manufacturer(MaterialDepiction $element)
    {

        $manufacturer_id = $this->findCompany($element->catalog->category->directory->manufacturer->id);
        $element_id = $this->findColoration($element->id);
        $model = new ProductMaterial();
        $coloration = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $coloration->bindManufacturer($manufacturer_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $coloration->id, $manufacturer_id);

        return $result ?? null;
    }

    public function bindMaterialCollection2Manufacturer(MaterialCatalog $element)
    {
        $manufacturer_id = $this->findCompany($element->category->directory->manufacturer->id);
        $element_id = $this->findMaterialCollection($element->id);
        $model = new MaterialCollection();
        $materialCollection = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $materialCollection->bindManufacturer($manufacturer_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $materialCollection->id, $manufacturer_id);

        return $result ?? null;
    }

    public function bindMaterialCollection2PriceCategory(MaterialCatalog $element)
    {
        $category_id = $this->findPriceCategory($element->price_category_id);
        $element_id = $this->findMaterialCollection($element->id);
        $model = new MaterialCollection();
        $materialCollection = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $materialCollection->bindPriceCategory($category_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $materialCollection->id, $category_id);

        return $result ?? null;
    }

    public function bindPriceCategory2Manufacturer(MaterialCategory $element)
    {
        $manufacturer_id = $this->findCompany($element->directory->manufacturer->id);
        $element_id = $this->findPriceCategory($element->id);
        $model = new PriceCategory();
        $priceCategory = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $priceCategory->bindManufacturer($manufacturer_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $priceCategory->id, $manufacturer_id);

        return $result ?? null;
    }

    public function bindColoration2MaterialCollection(MaterialDepiction $element)
    {
        $coloration_id = $this->findColoration($element->id);
        $element_id = $this->findMaterialCollection($element->catalog_id);
        $model = new MaterialCollection();
        $materialCollection = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $materialCollection->bindColoration($coloration_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $coloration_id, $materialCollection->id);

        return $result ?? null;
    }

    public function bindMaterialCollection2Material($element)
    {
        $material_id = $this->findMaterial($element['value']);
        $element_id = $this->findMaterialCollection($element['id']);
        $model = new MaterialCollection();
        $materialCollection = $model->findOne(['id' => $element_id]);

        $result = $element_id ? $materialCollection->bindMaterial($material_id) : false;
        $this->stdoutBindAssociatedElements(__FUNCTION__, $material_id, $materialCollection->id);

        return $result ?? null;
    }

    private function getDataForEntity($data, $objectType)
    {
        foreach ($this->fieldsToPoperties[$objectType] as $fieldGroup => $propMap) {

            foreach ($propMap as $propName => $propId) {
                if (is_array($propId)) {
                    $result[$fieldGroup][key($propId)][reset($propId)] = $data[$propName];
                } else {
                    $result[$fieldGroup][$propId] = $this->getPropertyValue($data, $propName);
                }
            }
        }

        return $result ?? null;
    }

    protected function getImageName(...$args)
    {
        $depiction = $args[0];
        if ($depiction['image_name']) {
            $p1 = substr($depiction['image_name'], 20, 2);
            $p2 = substr($depiction['image_name'], 17, 3);
            if (!($data = $this->check('/tmp/' . $depiction['image_name']))) {
                $content = @file_get_contents('http://content.furniprice.ru/catalog/materials/' . $p1 . '/' . $p2 . '/' . $depiction['image_name']);
                if (!$content) {
                    return '';
                }
                file_put_contents('/tmp/' . $depiction['image_name'], $content);
                $data = $this->upload('/tmp/' . $depiction['image_name']);
            }
            return $data->id ? $data->id . '.' . $data->format : '';
        }
        return '';
    }

    protected function getColorFromRal(...$args)
    {
        $depiction = $args[0];
        if ($depiction['colors']) {
            $colors = explode(',', trim($depiction['colors'], ','));
            $sql = 'select * from color where code in (\'' . implode('\',\'', $colors) . '\')';
            $rows = Yii::$app->dbMedia->createCommand($sql)->queryAll();
            $rals = ArrayHelper::getColumn($rows, 'id');
            $this->stdout(implode(',', $rals) . PHP_EOL, Console::FG_GREEN);
            return $rals ?? [];
        }
        return [];
    }

    private function check($file)
    {
        $args['id'] = md5($file);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://media-service.furniprice.local/image/' . $args['id']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $jsondata = json_decode(curl_exec($ch));
        curl_close($ch);
        if (isset($jsondata->id) && !is_null($jsondata->id)) {
            $this->stdout('    -> Image exists: ' . $jsondata->id . '.' . $jsondata->format . PHP_EOL, Console::FG_GREEN);
            return $jsondata;
        }
        return false;
    }

    private function upload($file)
    {
        $args['id'] = md5($file);
        $args['image'] = new \CurlFile($file);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://media-service.furniprice.local/image');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $jsondata = json_decode(curl_exec($ch));
        curl_close($ch);
        $this->stdout('    done: ' . $jsondata->id . '.' . $jsondata->format . PHP_EOL, Console::FG_YELLOW);
        return $jsondata;
    }

    private function getPropertyValue($data, $propName)
    {
        if (method_exists($this, $propName)) {
            $value = [$this, $propName];
        } elseif (isset($data[$propName])) {
            $value = $data[$propName] == '\N' ? null : $data[$propName];
        }

        return $value ?? null;
    }

    private function initEntityData()
    {
        foreach ($this->domainModels as $index => $elementClass) {
            $elementClassId = $this->getElementClassId($elementClass['context'], $elementClass['name']);
            $this->initProperties($elementClassId, $index);
        }
    }

    private function initProperties($elementClassId, $modelName)
    {
        $existingProps = PropertyRecord::find()->indexBy('sysname')->all();
        $properties = [];
        $propertiesIds = [];

        if (!isset($this->baseProperties[$modelName])) {
            throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
        }

        foreach ($this->baseProperties[$modelName] as $needProperty) {
            $property = $this->getProperty($existingProps[$needProperty]);
            $properties[$property->sysname] = $property;
            $propertiesIds[] = $property->id;
        }

        foreach ($this->fieldsGroup[$modelName] as $fieldGroup => $fieldsMapping) {

            foreach ($fieldsMapping as $propName => $fieldName) {

                if (is_array($fieldName)) {
                    foreach ($fieldName as $key => $value) {
                        $this->fieldsToPoperties[$modelName][$fieldGroup][$value] = [$properties[$propName]->id => $key];
                    }
                } else {
                    $this->fieldsToPoperties[$modelName][$fieldGroup][$fieldName] = $properties[$propName]->id;
                }
            }
        }
    }

    /**
     * Получаем ID Element_class, если не существует - добавялем
     * @param $context
     * @param $name
     * @return int
     */
    private function getElementClassId($context, $name)
    {
        $elementClass = ElementClassRecord::findOne([
            'name' => $className = $context . '\\' . $name,
        ]);

        if (!$elementClass) {
            throw new NotFoundHttpException(basename(__FILE__, '.php') . __LINE__);
        }

        return $elementClass->id ?? null;
    }

    protected function createSupportImportTables()
    {
        $query = 'CREATE TABLE IF NOT EXISTS materialcollection_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'CREATE TABLE IF NOT EXISTS materialcategory_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'CREATE TABLE IF NOT EXISTS coloration_mapping (element_id bigint, origin_id bigint)';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();
    }

    protected function clearSupportImportTables()
    {
        $query = 'TRUNCATE TABLE materialcollection_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'TRUNCATE TABLE materialcategory_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();

        $query = 'TRUNCATE TABLE coloration_mapping';
        $this->getSupportImportDb()
            ->createCommand($query)
            ->queryAll();
    }

    private function stdoutText($text, $id, $elementId)
    {
        $this->stdout($text);
        $this->stdout($id, Console::FG_CYAN);
        $this->stdout(' as Element ');
        $this->stdout($elementId, Console::FG_GREEN);
        $this->stdout("\n");
    }

    private function stdoutTextExist($text, $id)
    {
        $this->stdout($text);
        $this->stdout($id, Console::FG_YELLOW);
        $this->stdout("\n");
    }

    private function stdoutBindElementToParentElement($childId, $parentId, $childName = 'child', $parentName = 'parent')
    {
        $this->stdout("Bind to $parentName ");
        $this->stdout($parentId, Console::FG_CYAN);
        $this->stdout(" $childName with id = ");
        $this->stdout($childId, Console::FG_CYAN);
        $this->stdout("\n");
    }

    private function stdoutBindAssociatedElements($function, $childId, $parentId)
    {
        $this->stdout("{$function}: ");
        $this->stdout($childId, Console::FG_CYAN);
        $this->stdout(" <==> ");
        $this->stdout($parentId, Console::FG_CYAN);
        $this->stdout("\n");
    }

}
