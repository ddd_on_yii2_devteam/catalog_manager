<?php
return [
    ['elementClass' => 'crm\Manufacturer', 'propertySysname' => 'PriceCategory2Manufacturer', 'parent' => true],
    ['elementClass' => 'catalog\PriceCategory', 'propertySysname' => 'PriceCategory2Manufacturer', 'parent' => false],

    ['elementClass' => 'catalog\MaterialCollection', 'propertySysname' => 'ProductMaterial2MaterialCollection', 'parent' => true],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'ProductMaterial2MaterialCollection', 'parent' => false],
    ['elementClass' => 'catalog\PriceCategory', 'propertySysname' => 'ProductMaterial2PriceCategory', 'parent' => true],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'ProductMaterial2PriceCategory', 'parent' => false],
    ['elementClass' => 'catalog\Material', 'propertySysname' => 'ProductMaterial2Material', 'parent' => true],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'ProductMaterial2Material', 'parent' => false],
    ['elementClass' => 'crm\Manufacturer', 'propertySysname' => 'ProductMaterial2Manufacturer', 'parent' => true],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'ProductMaterial2Manufacturer', 'parent' => false],

    ['elementClass' => 'crm\Manufacturer', 'propertySysname' => 'MaterialCollection2Manufacturer', 'parent' => true],
    ['elementClass' => 'catalog\MaterialCollection', 'propertySysname' => 'MaterialCollection2Manufacturer', 'parent' => false],

    ['elementClass' => 'catalog\Product', 'propertySysname' => 'Product2ProductMaterial', 'parent' => true],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'Product2ProductMaterial', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'ProductModel2ProductMaterial', 'parent' => true],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'ProductModel2ProductMaterial', 'parent' => false],
    ['elementClass' => 'catalog\Component', 'propertySysname' => 'Component_hierarchy', 'parent' => true],

    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'Component2ProductModel_modularComponent', 'parent' => true],
    ['elementClass' => 'catalog\Component', 'propertySysname' => 'Component2ProductModel_modularComponent', 'parent' => false],

    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'Component2ProductModel_constructionElement', 'parent' => true],
    ['elementClass' => 'catalog\Component', 'propertySysname' => 'Component2ProductModel_constructionElement', 'parent' => false],

    ['elementClass' => 'crm\Manufacturer', 'propertySysname' => 'ProductModel2Manufacturer', 'parent' => true],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'ProductModel2Manufacturer', 'parent' => false],

    ['elementClass' => 'catalog\Component', 'propertySysname' => 'Material2Component', 'parent' => true],
    ['elementClass' => 'catalog\Material', 'propertySysname' => 'Material2Component', 'parent' => false],

    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'Material2ProductModel', 'parent' => true],
    ['elementClass' => 'catalog\Material', 'propertySysname' => 'Material2ProductModel', 'parent' => false],

    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'Product2ProductModel', 'parent' => true],
    ['elementClass' => 'catalog\Product', 'propertySysname' => 'Product2ProductModel', 'parent' => false],

    ['elementClass' => 'catalog\Material', 'propertySysname' => 'Material_hierarchy', 'parent' => true],

    ['elementClass' => 'catalog\Material', 'propertySysname' => 'Material_composition', 'parent' => true],
    # MaterialGroup
    ['elementClass' => 'catalog\MaterialGroup', 'propertySysname' => 'Material2MaterialGroup', 'parent' => true],
    ['elementClass' => 'catalog\Material', 'propertySysname' => 'Material2MaterialGroup', 'parent' => false],
    ['elementClass' => 'catalog\MaterialGroup', 'propertySysname' => 'title', 'parent' => false],
    ['elementClass' => 'catalog\MaterialGroup', 'propertySysname' => 'ProductMaterial2MaterialGroup', 'parent' => false],
    #MaterialTemplate
    ['elementClass' => 'catalog\MaterialTemplate', 'propertySysname' => 'Material2MaterialTemplate', 'parent' => true],
    ['elementClass' => 'catalog\Material', 'propertySysname' => 'Material2MaterialTemplate', 'parent' => false],
    ['elementClass' => 'catalog\MaterialTemplate', 'propertySysname' => 'title', 'parent' => false],
    ['elementClass' => 'catalog\MaterialTemplate', 'propertySysname' => 'name', 'parent' => false],

    ['elementClass' => 'catalog\Product', 'propertySysname' => 'ProductRequest2Product', 'parent' => true],
    ['elementClass' => 'crm\ProductRequest', 'propertySysname' => 'ProductRequest2Product', 'parent' => false],

    // Crm
    ['elementClass' => 'crm\Manufacturer', 'propertySysname' => 'Seller2Manufacturer', 'parent' => true],
    ['elementClass' => 'crm\Seller', 'propertySysname' => 'Seller2Manufacturer', 'parent' => false],

    ['elementClass' => 'crm\Seller', 'propertySysname' => 'ProductRequest2Seller', 'parent' => true],
    ['elementClass' => 'crm\ProductRequest', 'propertySysname' => 'ProductRequest2Seller', 'parent' => false],

    ['elementClass' => 'crm\Customer', 'propertySysname' => 'ProductRequest2Customer', 'parent' => true],
    ['elementClass' => 'crm\ProductRequest', 'propertySysname' => 'ProductRequest2Customer', 'parent' => false],

    ['elementClass' => 'crm\Employee', 'propertySysname' => 'ProductOffer2Employee', 'parent' => true],
    ['elementClass' => 'crm\ProductOffer', 'propertySysname' => 'ProductOffer2Employee', 'parent' => false],

    ['elementClass' => 'crm\ProductRequest', 'propertySysname' => 'ProductOffer2ProductRequest', 'parent' => true],
    ['elementClass' => 'crm\ProductOffer', 'propertySysname' => 'ProductOffer2ProductRequest', 'parent' => false],

    ['elementClass' => 'crm\Company', 'propertySysname' => 'Employee2Company', 'parent' => true],
    ['elementClass' => 'crm\Employee', 'propertySysname' => 'Employee2Company', 'parent' => false],

    ['elementClass' => 'crm\Address', 'propertySysname' => 'Company2Address', 'parent' => true],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'Company2Address', 'parent' => false],

    ['elementClass' => 'crm\Address', 'propertySysname' => 'User2Address', 'parent' => true],
    ['elementClass' => 'crm\User', 'propertySysname' => 'User2Address', 'parent' => false],

    ['elementClass' => 'crm\CompanyState', 'propertySysname' => 'Company2CompanyState', 'parent' => true],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'Company2CompanyState', 'parent' => false],

    ['elementClass' => 'crm\CompanyRole', 'propertySysname' => 'Company2CompanyRole', 'parent' => true],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'Company2CompanyRole', 'parent' => false],

    ['elementClass' => 'crm\UserRole', 'propertySysname' => 'User2UserRole', 'parent' => true],
    ['elementClass' => 'crm\User', 'propertySysname' => 'User2UserRole', 'parent' => false],

    ['elementClass' => 'crm\AccessSpecification', 'propertySysname' => 'User2AccessSpecification', 'parent' => true],
    ['elementClass' => 'crm\User', 'propertySysname' => 'User2AccessSpecification', 'parent' => false],

    ['elementClass' => 'crm\CompanyRole', 'propertySysname' => 'UserRole2CompanyRole', 'parent' => true],
    ['elementClass' => 'crm\UserRole', 'propertySysname' => 'UserRole2CompanyRole', 'parent' => false],

    ['elementClass' => 'crm\UserRole', 'propertySysname' => 'Permission2UserRole', 'parent' => true],
    ['elementClass' => 'crm\Permission', 'propertySysname' => 'Permission2UserRole', 'parent' => false],

    ['elementClass' => 'crm\PermissionGroup', 'propertySysname' => 'Permission2PermissionGroup', 'parent' => true],
    ['elementClass' => 'crm\Permission', 'propertySysname' => 'Permission2PermissionGroup', 'parent' => false],

    ['elementClass' => 'crm\PermissionGroup', 'propertySysname' => 'CompanyState2PermissionGroup', 'parent' => true],
    ['elementClass' => 'crm\CompanyState', 'propertySysname' => 'CompanyState2PermissionGroup', 'parent' => false],

    ['elementClass' => 'crm\PermissionGroup', 'propertySysname' => 'CompanyRole2PermissionGroup', 'parent' => true],
    ['elementClass' => 'crm\CompanyRole', 'propertySysname' => 'CompanyRole2PermissionGroup', 'parent' => false],

    ['elementClass' => 'core\ElementClass', 'propertySysname' => 'PermissionGroup2ElementClass', 'parent' => true],
    ['elementClass' => 'crm\PermissionGroup', 'propertySysname' => 'PermissionGroup2ElementClass', 'parent' => false],

    ['elementClass' => 'crm\Address', 'propertySysname' => 'fullAddress', 'parent' => false],

    ['elementClass' => 'crm\Address', 'propertySysname' => 'country', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'postalCode', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'district', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'city', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'street', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'house', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'flat', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'description', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'addressType', 'parent' => false],
    ['elementClass' => 'crm\Address', 'propertySysname' => 'geoLocation', 'parent' => false],

    ['elementClass' => 'crm\Company', 'propertySysname' => 'legalName', 'parent' => false],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'organizationCode', 'parent' => false],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'country', 'parent' => false],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'site', 'parent' => false],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'description', 'parent' => false],
    ['elementClass' => 'crm\Company', 'propertySysname' => 'tradeMark', 'parent' => false],

    #user
    ['elementClass' => 'crm\User', 'propertySysname' => 'language', 'parent' => false],
    ['elementClass' => 'crm\User', 'propertySysname' => 'currency', 'parent' => false],
    ['elementClass' => 'crm\User', 'propertySysname' => 'country', 'parent' => false],
    ['elementClass' => 'crm\User', 'propertySysname' => 'email', 'parent' => false],
    ['elementClass' => 'crm\User', 'propertySysname' => 'password', 'parent' => false],

    #employee
    ['elementClass' => 'crm\Employee', 'propertySysname' => 'position', 'parent' => false],


    ['elementClass' => 'catalog\MaterialCollection', 'propertySysname' => 'name', 'parent' => false],
    ['elementClass' => 'catalog\PriceCategory', 'propertySysname' => 'name', 'parent' => false],
    #ProductMaterial
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'name', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'pattern', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'colors', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'vendorCode', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'image', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'texture', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'colorResistance', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'wearResistance', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'fireResistance', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'fabricDensity', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'fabricStructure', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'pvcFilmFinishing', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'pvcFilmType', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'pvcFilmThickness', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'glassBrand', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'glassProperty', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'glassDecoration', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'glassKind', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'facadeDesign', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'additionalFinishingOptions', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'hidingPower', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'glossDegree', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'patternMaterial', 'parent' => false],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'ProductMaterial2MaterialGroup', 'parent' => false],
    #material
    ['elementClass' => 'catalog\Material', 'propertySysname' => 'patternMaterial', 'parent' => false],
    ['elementClass' => 'catalog\Material', 'propertySysname' => 'title', 'parent' => false],
    #ProductModel
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'name', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'image', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'price', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'country', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'vendorCode', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'vendorCodeMpn', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'materialVazy', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'konstruktsiyaKnizhnogoShkafa', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieVKabinete', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'vidPodstavki', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieVDetskoy', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'konfiguratsiya', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'vidOsveshcheniya', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'mehanizmReklayner', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'sposobUstanovki', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'kolichestvoDverey', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'krovatOptsii', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'matrasOptsii', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'uglovoeRaspolozhenie', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'poverhnost', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'functionalArea', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'materialIzgotovleniyaKarkasa', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'teksturaDrevesiny', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'podlokotnikMaterial', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'podlokotnikNalichie', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'napolnitelMatrasaKrovati', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'osnovaFasadovKorpusnoyMebeli', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'tsvet', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'stil', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'napolnitelDlyaMyagkoyMebeli', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'konstruktsiyaMatrasa', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'kolichestvoNozhekStola', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'formaStola', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'raskladnoyMehanizmStola', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'tipMyagkoyMebeli', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'raskladnoyMehanizm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'vidyMaterialovNozhekStola', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'kolichestvoMest', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'razmer', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieOtdeleniyaDlyaObuvi', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieVeshalki', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieZerkala', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'sRamkoyBagetom', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'spalnoeMestoVtorogoYarusaShirinaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'spalnoeMestoVtorogoYarusaDlinaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'mehanizmVrashcheniya', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'skladnoyStul', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'barnyyStul', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'podlokotnikiStula', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'glubinaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'dlinaVRazlozhennomSostoyaniiMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichiePodgolovnika', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichiePodemnogoMehanizma', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieOrtopedicheskogoOsnovaniya', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieVydvizhnogoYashchika', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieTumby', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'myagkayaSpinka', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieKoles', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'nalichieYashchikaDlyaBelya', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'vysotaSidenyaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'spalnoeMestoShirinaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'spalnoeMestoDlinaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'vysotaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'shirinaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'dlinaMm', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'description', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'furnitureType', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'grade', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'segment', 'parent' => false],
    ['elementClass' => 'catalog\ProductModel', 'propertySysname' => 'popularity', 'parent' => false],


    ['elementClass' => 'catalog\Material', 'propertySysname' => 'title', 'parent' => false],

    ['elementClass' => 'catalog\MaterialTemplate', 'propertySysname' => 'properties', 'parent' => false],
    ['elementClass' => 'catalog\MaterialTemplate', 'propertySysname' => 'properties', 'parent' => false],
    #ProductMaterial
    ['elementClass' => 'catalog\Component', 'propertySysname' => 'ProductMaterial2Component', 'parent' => true],
    ['elementClass' => 'catalog\ProductMaterial', 'propertySysname' => 'ProductMaterial2Component', 'parent' => false],
    ['elementClass' => 'catalog\Component', 'propertySysname' => 'title', 'parent' => false],

    // Option
    ['elementClass' => 'catalog\Option', 'propertySysname' => 'title', 'parent' => false],
    ['elementClass' => 'catalog\Option', 'propertySysname' => 'propertyId', 'parent' => false],
    ['elementClass' => 'catalog\Option', 'propertySysname' => 'materialGroupId', 'parent' => false],
    ['elementClass' => 'catalog\Option', 'propertySysname' => 'typeOfOption', 'parent' => false],
    ['elementClass' => 'catalog\Option', 'propertySysname' => 'isAdditional', 'parent' => false],
    ['elementClass' => 'catalog\Option', 'propertySysname' => 'Variant2Option', 'parent' => false],

    // Variant
    ['elementClass' => 'catalog\Variant', 'propertySysname' => 'title', 'parent' => false],
    ['elementClass' => 'catalog\Variant', 'propertySysname' => 'extraCharge', 'parent' => false],
    ['elementClass' => 'catalog\Variant', 'propertySysname' => 'configuration', 'parent' => false],

    ['elementClass' => 'catalog\Product', 'propertySysname' => 'Variant2Product', 'parent' => false],
    ['elementClass' => 'catalog\Variant', 'propertySysname' => 'Variant2Product', 'parent' => false],
    ['elementClass' => 'catalog\Variant', 'propertySysname' => 'Variant2Option', 'parent' => false],
];

