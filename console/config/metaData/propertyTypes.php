<?php
return [
    [
        'name' => 'property',
    ],
    [
        'name' => 'relation',
    ],
    [
        'name'         => 'associationRelation',
        'parent'       => 'relation',
        'elementClass' => 'core\AssociationRelationValue',
    ],
    [
        'name'         => 'aggregationRelation',
        'parent'       => 'relation',
        'elementClass' => 'core\AggregationRelationValue',
    ],
    [
        'name'         => 'hierarchyRelation',
        'parent'       => 'relation',
        'elementClass' => 'core\HierarchyRelationValue',
    ],
    [
        'name'         => 'boolean',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueBoolean',
    ],
    [
        'name'         => 'int',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueInt',
    ],
    [
        'name'         => 'bigint',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueBigint',
    ],
    [
        'name'         => 'float',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueFloat',
    ],
    [
        'name'         => 'string',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueString',
    ],
    [
        'name'         => 'text',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueText',
    ],
    [
        'name'         => 'date',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueDate',
    ],
    [
        'name'         => 'timestamp',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueTimestamp',
    ],
    [
        'name'         => 'geolocation',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueGeolocation',
    ],
    [
        'name'         => 'list',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueListItem',
    ],
    [
        'name'         => 'array',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyArrayValue',
    ],
    [
        'name'         => 'range',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyRangeValue',
    ],
    [
        'name'         => 'json',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueJson',
    ],
    [
        'name'         => 'color',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueColor',
    ],
    [
        'name'         => 'tree',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueTreeItem',
    ],
    [
        'name'         => 'money',
        'parent'       => 'property',
        'elementClass' => 'core\PropertyValueMoney',
    ],
];
