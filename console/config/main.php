<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'),
    ['entities' =>
         [
             'properties' => require(__DIR__ . '/metaData/properties.php'),
             'contexts' => require(__DIR__ . '/metaData/contexts.php'),
             'elementClasses' => require(__DIR__ . '/metaData/elementClasses.php'),
             'propertyTypes' => require(__DIR__ . '/metaData/propertyTypes.php'),
             'property2elementClasses' => require(__DIR__ . '/metaData/property2elementClasses.php'),
         ],
    ]
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'autocomplete'],
    'controllerNamespace' => 'console\controllers',
    'language'   => 'rus-RU',
    'components'          => [
        'log' => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'autocomplete' => [
            'class' => 'iiifx\Yii2\Autocomplete\Component',
            'config' => [
                '@common/config/main.php', # <-- config list
                '@common/config/main-local.php',
                '@console/config/main.php',
                '@console/config/main-local.php',
                '@api/config/main.php',
                '@api/config/main-local.php',
            ],
        ],
    ],
    'params'              => $params,
];
