<?php

namespace console\models\Product;

use Yii;

/**
 * This is the model class for table "product_property_directory_category".
 *
 * @property int $id
 * @property int $product_property_directory_id
 * @property int $price_category_id
 * @property string $amount
 * @property string $currency (DC2Type:currency)
 *
 * @property MaterialPriceCategory $priceCategory
 * @property ProductPropertyDirectory $productPropertyDirectory
 */
class ProductPropertyDirectoryCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_property_directory_category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_property_directory_id', 'price_category_id'], 'integer'],
            [['amount'], 'string', 'max' => 19],
            [['currency'], 'string', 'max' => 255],
            [['price_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialPriceCategory::className(), 'targetAttribute' => ['price_category_id' => 'id']],
            [['product_property_directory_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductPropertyDirectory::className(), 'targetAttribute' => ['product_property_directory_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_property_directory_id' => 'Product Property Directory ID',
            'price_category_id' => 'Price Category ID',
            'amount' => 'Amount',
            'currency' => 'Currency',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceCategory()
    {
        return $this->hasOne(MaterialPriceCategory::className(), ['id' => 'price_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyDirectory()
    {
        return $this->hasOne(ProductPropertyDirectory::className(), ['id' => 'product_property_directory_id']);
    }
}
