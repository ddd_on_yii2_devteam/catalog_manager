<?php

namespace console\models\Product;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $manufacturer_id
 * @property string $sku
 * @property string $mpn
 * @property string $created_at
 * @property int $primary_category_id
 * @property string $price_amount
 * @property string $price_currency (DC2Type:currency)
 * @property string $updated_at
 * @property string $sketch_name
 * @property string $sketch_original_name
 * @property string $sketch_mime_type
 * @property int $sketch_size
 * @property int $activated
 * @property int $activated_by_id
 * @property int $created_by_id
 * @property int $updated_by_id
 * @property int $deleted_by_id
 * @property string $activated_at
 * @property string $texture_name
 * @property string $texture_original_name
 * @property string $texture_mime_type
 * @property int $texture_size
 * @property int $score
 * @property int $recommended
 *
 * @property Users $updatedBy
 * @property Organization $manufacturer
 * @property Users $createdBy
 * @property Catalog $primaryCategory
 * @property Users $deletedBy
 * @property Users $activatedBy
 * @property ProductAdditionalInfo $productAdditionalInfo
 * @property ProductCatalog[] $productCatalogs
 * @property Catalog[] $catalogs
 * @property ProductFavorite[] $productFavorites
 * @property ProductHistoricalPrice[] $productHistoricalPrices
 * @property ProductImage[] $productImages
 * @property ProductProperty[] $productProperties
 * @property ProductQuality $productQuality
 * @property ProductTranslation[] $productTranslations
 * @property ProductZone[] $productZones
 * @property Zone[] $zones
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'created_at', 'primary_category_id', 'updated_at', 'activated', 'score', 'recommended'], 'required'],
            [['manufacturer_id', 'primary_category_id', 'sketch_size', 'activated', 'activated_by_id', 'created_by_id', 'updated_by_id', 'deleted_by_id', 'texture_size', 'score', 'recommended'], 'integer'],
            [['created_at', 'updated_at', 'activated_at'], 'safe'],
            [['sku', 'mpn', 'price_currency', 'sketch_name', 'sketch_original_name', 'sketch_mime_type', 'texture_name', 'texture_original_name', 'texture_mime_type'], 'string', 'max' => 255],
            [['price_amount'], 'string', 'max' => 19],
            [['updated_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by_id' => 'id']],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
            [['created_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by_id' => 'id']],
            [['primary_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catalog::className(), 'targetAttribute' => ['primary_category_id' => 'id']],
            [['deleted_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deleted_by_id' => 'id']],
            [['activated_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['activated_by_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacturer_id' => 'Manufacturer ID',
            'sku' => 'Sku',
            'mpn' => 'Mpn',
            'created_at' => 'Created At',
            'primary_category_id' => 'Primary Category ID',
            'price_amount' => 'Price Amount',
            'price_currency' => 'Price Currency',
            'updated_at' => 'Updated At',
            'sketch_name' => 'Sketch Name',
            'sketch_original_name' => 'Sketch Original Name',
            'sketch_mime_type' => 'Sketch Mime Type',
            'sketch_size' => 'Sketch Size',
            'activated' => 'Activated',
            'activated_by_id' => 'Activated By ID',
            'created_by_id' => 'Created By ID',
            'updated_by_id' => 'Updated By ID',
            'deleted_by_id' => 'Deleted By ID',
            'activated_at' => 'Activated At',
            'texture_name' => 'Texture Name',
            'texture_original_name' => 'Texture Original Name',
            'texture_mime_type' => 'Texture Mime Type',
            'texture_size' => 'Texture Size',
            'score' => 'Score',
            'recommended' => 'Recommended',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'updated_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Organization::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrimaryCategory()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'primary_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'deleted_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'activated_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAdditionalInfo()
    {
        return $this->hasOne(ProductAdditionalInfo::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCatalogs()
    {
        return $this->hasMany(ProductCatalog::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogs()
    {
        return $this->hasMany(Catalog::className(), ['id' => 'catalog_id'])->viaTable('product_catalog', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFavorites()
    {
        return $this->hasMany(ProductFavorite::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductHistoricalPrices()
    {
        return $this->hasMany(ProductHistoricalPrice::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProperties()
    {
        return $this->hasMany(ProductProperty::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductQuality()
    {
        return $this->hasOne(ProductQuality::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations()
    {
        return $this->hasMany(ProductTranslation::className(), ['translatable_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductZones()
    {
        return $this->hasMany(ProductZone::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZones()
    {
        return $this->hasMany(Zone::className(), ['id' => 'zone_id'])->viaTable('product_zone', ['product_id' => 'id']);
    }
}
