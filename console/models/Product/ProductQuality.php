<?php

namespace console\models\Product;

use Yii;

/**
 * This is the model class for table "product_quality".
 *
 * @property int $id
 * @property int $product_id
 * @property int $grade
 * @property int $popularity
 * @property int $duration
 * @property int $segment
 *
 * @property Product $product
 */
class ProductQuality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_quality';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'grade', 'popularity', 'duration', 'segment'], 'integer'],
            [['grade', 'popularity', 'segment'], 'required'],
            [['product_id'], 'unique'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'grade' => 'Grade',
            'popularity' => 'Popularity',
            'duration' => 'Duration',
            'segment' => 'Segment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
