<?php

namespace console\models\Product;

use Yii;

/**
 * This is the model class for table "product_property_directory".
 *
 * @property int $id
 * @property int $value_id
 *
 * @property ProductProperty $id0
 * @property MaterialDirectory $value
 * @property ProductPropertyDirectoryCategory[] $productPropertyDirectoryCategories
 */
class ProductPropertyDirectory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_property_directory';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'value_id'], 'integer'],
            [['id'], 'unique'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductProperty::className(), 'targetAttribute' => ['id' => 'id']],
            [['value_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialDirectory::className(), 'targetAttribute' => ['value_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value_id' => 'Value ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ProductProperty::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(MaterialDirectory::className(), ['id' => 'value_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyDirectoryCategories()
    {
        return $this->hasMany(ProductPropertyDirectoryCategory::className(), ['product_property_directory_id' => 'id']);
    }
}
