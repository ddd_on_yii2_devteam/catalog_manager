<?php

namespace console\models\Product;

use Yii;

/**
 * This is the model class for table "product_property".
 *
 * @property int $id
 * @property int $property_id
 * @property int $product_id
 * @property string $created_at
 * @property int $type
 *
 * @property Product $product
 * @property Property $property
 * @property ProductPropertyArea $productPropertyArea
 * @property ProductPropertyBoolean $productPropertyBoolean
 * @property ProductPropertyDirectory $productPropertyDirectory
 * @property ProductPropertyEnum $productPropertyEnum
 * @property ProductPropertyInteger $productPropertyInteger
 * @property ProductPropertyLength $productPropertyLength
 * @property ProductPropertySet $productPropertySet
 * @property ProductPropertyString $productPropertyString
 * @property ProductPropertyText $productPropertyText
 * @property ProductPropertyVolume $productPropertyVolume
 */
class ProductProperty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_property';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'product_id', 'created_at', 'type'], 'required'],
            [['property_id', 'product_id', 'type'], 'integer'],
            [['created_at'], 'safe'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyArea()
    {
        return $this->hasOne(ProductPropertyArea::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyBoolean()
    {
        return $this->hasOne(ProductPropertyBoolean::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyDirectory()
    {
        return $this->hasOne(ProductPropertyDirectory::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyEnum()
    {
        return $this->hasOne(ProductPropertyEnum::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyInteger()
    {
        return $this->hasOne(ProductPropertyInteger::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyLength()
    {
        return $this->hasOne(ProductPropertyLength::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertySet()
    {
        return $this->hasOne(ProductPropertySet::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyString()
    {
        return $this->hasOne(ProductPropertyString::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyText()
    {
        return $this->hasOne(ProductPropertyText::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPropertyVolume()
    {
        return $this->hasOne(ProductPropertyVolume::className(), ['id' => 'id']);
    }
}
