<?php

namespace console\models\Material;

use Yii;

/**
 * This is the model class for table "material_mapping_named".
 *
 * @property integer $element_id
 * @property integer $origin_id
 */
class MaterialImportMapping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_mapping_named';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('import');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['element_id'], 'integer'],
            [['origin_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'element_id' => 'Element ID',
            'origin_id'  => 'Origin Name',
        ];
    }
}
