<?php

namespace console\models\material;

use Yii;

/**
 * This is the model class for table "material_depiction".
 *
 * @property integer $id
 * @property integer $catalog_id
 * @property integer $available
 * @property string $code
 * @property string $name
 * @property string $updated_at
 * @property string $image_name
 * @property string $image_original_name
 * @property string $image_mime_type
 * @property integer $image_size
 * @property string $texture_name
 * @property string $texture_original_name
 * @property string $texture_mime_type
 * @property integer $texture_size
 * @property integer $position
 * @property string $colors
 * @property integer $pattern_type
 *
 * @property MaterialCatalog $catalog
 */
class MaterialDepiction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_depiction';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catalog_id', 'available', 'image_size', 'texture_size', 'position', 'pattern_type'], 'integer'],
            [['available', 'updated_at', 'position', 'pattern_type'], 'required'],
            [['updated_at'], 'safe'],
            [['colors'], 'string'],
            [['code', 'name', 'image_name', 'image_original_name', 'image_mime_type', 'texture_name', 'texture_original_name', 'texture_mime_type'], 'string', 'max' => 255],
            [['catalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialCatalog::className(), 'targetAttribute' => ['catalog_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catalog_id' => 'Catalog ID',
            'available' => 'Available',
            'code' => 'Code',
            'name' => 'Name',
            'updated_at' => 'Updated At',
            'image_name' => 'Image Name',
            'image_original_name' => 'Image Original Name',
            'image_mime_type' => 'Image Mime Type',
            'image_size' => 'Image Size',
            'texture_name' => 'Texture Name',
            'texture_original_name' => 'Texture Original Name',
            'texture_mime_type' => 'Texture Mime Type',
            'texture_size' => 'Texture Size',
            'position' => 'Position',
            'colors' => 'Colors',
            'pattern_type' => 'Pattern Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalog()
    {
        return $this->hasOne(MaterialCatalog::className(), ['id' => 'catalog_id']);
    }
}
