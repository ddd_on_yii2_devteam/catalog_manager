<?php

namespace console\models\material;

use console\models\Company\Organization;
use console\models\material\MaterialCategory;
use console\models\Product\ProductPropertyDirectory;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "material_directory".
 *
 * @property integer $id
 * @property integer $manufacturer_id
 * @property string $name
 * @property integer $type_id
 * @property integer $activated_by_id
 * @property integer $created_by_id
 * @property integer $updated_by_id
 * @property integer $deleted_by_id
 * @property integer $activated
 * @property string $activated_at
 *
 * @property MaterialCategory[] $materialCategories
 * @property Users $updatedBy
 * @property Organization $manufacturer
 * @property Users $createdBy
 * @property MaterialType $type
 * @property Users $deletedBy
 * @property Users $activatedBy
 * @property ProductPropertyDirectory[] $productPropertyDirectories
 */
class MaterialDirectory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_directory';
    }

    /**
     * @return Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_id', 'type_id', 'activated_by_id', 'created_by_id', 'updated_by_id', 'deleted_by_id', 'activated'], 'integer'],
            [['name', 'activated'], 'required'],
            [['activated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['updated_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by_id' => 'id']],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
            [['created_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['deleted_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deleted_by_id' => 'id']],
            [['activated_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['activated_by_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacturer_id' => 'Manufacturer ID',
            'name' => 'Name',
            'type_id' => 'Type ID',
            'activated_by_id' => 'Activated By ID',
            'created_by_id' => 'Created By ID',
            'updated_by_id' => 'Updated By ID',
            'deleted_by_id' => 'Deleted By ID',
            'activated' => 'Activated',
            'activated_at' => 'Activated At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMaterialCategories()
    {
        return $this->hasMany(MaterialCategory::className(), ['directory_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
//    public function getUpdatedBy()
//    {
//        return $this->hasOne(Users::className(), ['id' => 'updated_by_id']);
//    }

    /**
     * @return ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Organization::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return ActiveQuery
     */
//    public function getCreatedBy()
//    {
//        return $this->hasOne(Users::className(), ['id' => 'created_by_id']);
//    }

    /**
     * @return ActiveQuery
     */
//    public function getType()
//    {
//        return $this->hasOne(MaterialType::className(), ['id' => 'type_id']);
//    }

    /**
     * @return ActiveQuery
     */
//    public function getDeletedBy()
//    {
//        return $this->hasOne(Users::className(), ['id' => 'deleted_by_id']);
//    }

    /**
     * @return ActiveQuery
     */
//    public function getActivatedBy()
//    {
//        return $this->hasOne(Users::className(), ['id' => 'activated_by_id']);
//    }

    /**
     * @return ActiveQuery
     */
//    public function getProductPropertyDirectories()
//    {
//        return $this->hasMany(ProductPropertyDirectory::className(), ['value_id' => 'id']);
//    }
}
