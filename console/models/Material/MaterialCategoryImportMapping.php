<?php

namespace console\models\Material;

use Yii;

/**
 * This is the model class for table "company_mapping".
 *
 * @property integer $element_id
 * @property integer $origin_id
 */
class MaterialCategoryImportMapping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'materialcategory_mapping';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('import');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['element_id', 'origin_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'element_id' => 'Element ID',
            'origin_id' => 'Origin ID',
        ];
    }
}
