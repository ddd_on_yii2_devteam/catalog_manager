<?php

namespace console\models\material;

use Yii;

/**
 * This is the model class for table "material_category".
 *
 * @property integer $id
 * @property integer $directory_id
 * @property string $name
 * @property string $created_at
 *
 * @property MaterialCatalog[] $materialCatalogs
 * @property MaterialDirectory $directory
 * @property ProductPropertyDirectoryCategory[] $productPropertyDirectoryCategories
 */
class MaterialCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_price_category';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['directory_id'], 'integer'],
            [['name', 'created_at'], 'required'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['directory_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialDirectory::className(), 'targetAttribute' => ['directory_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'directory_id' => 'Directory ID',
            'name' => 'Name',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialCatalogs()
    {
        return $this->hasMany(MaterialCatalog::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectory()
    {
        return $this->hasOne(MaterialDirectory::className(), ['id' => 'directory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getProductPropertyDirectoryCategories()
//    {
//        return $this->hasMany(ProductPropertyDirectoryCategory::className(), ['category_id' => 'id']);
//    }
}
