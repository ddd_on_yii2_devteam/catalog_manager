<?php

namespace console\models\material;

use Yii;

/**
 * This is the model class for table "material_catalog".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $active
 * @property string $name
 *
 * @property MaterialCategory $category
 * @property MaterialCatalogPropertyValue[] $materialCatalogPropertyValues
 * @property PropertyValue[] $propertyValues
 * @property MaterialDepiction[] $materialDepictions
 */
class MaterialCatalog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_catalog';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_category_id', 'active'], 'integer'],
            [['active', 'name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['price_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialCategory::className(), 'targetAttribute' => ['price_category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_category_id' => 'Price Category ID',
            'active' => 'Active',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(MaterialCategory::className(), ['id' => 'price_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialCatalogPropertyValues()
    {
        return $this->hasMany(MaterialCatalogPropertyValue::className(), ['material_catalog_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyValues()
    {
        return $this->hasMany(PropertyValue::className(), ['id' => 'property_value_id'])->viaTable('material_catalog_property_value', ['material_catalog_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialDepictions()
    {
        return $this->hasMany(MaterialDepiction::className(), ['catalog_id' => 'id']);
    }
}
