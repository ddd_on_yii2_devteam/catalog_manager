<?php

namespace console\models\Company;

use Yii;

/**
 * This is the model class for table "organization".
 *
 * @property integer $id
 * @property string $legal_name
 * @property string $country
 * @property string $code
 * @property string $site
 * @property string $created_at
 * @property integer $type
 * @property integer $activated
 * @property integer $activated_by_id
 * @property string $activated_at
 * @property integer $created_by_id
 * @property integer $updated_by_id
 * @property integer $deleted_by_id
 *
 * @property BankDetails[] $bankDetails
 * @property Contract[] $contracts
 * @property ManufacturerTranslation[] $manufacturerTranslations
 * @property MaterialDirectory[] $materialDirectories
 * @property Users $updatedBy
 * @property Users $createdBy
 * @property Users $deletedBy
 * @property Users $activatedBy
 * @property OrganizationContact[] $organizationContacts
 * @property OrganizationImage[] $organizationImages
 * @property OrganizationLocation[] $organizationLocations
 * @property Location[] $locations
 * @property Product[] $products
 * @property SellerManufacturer[] $sellerManufacturers
 * @property SellerManufacturer[] $sellerManufacturers0
 * @property Organization[] $manufacturers
 * @property Organization[] $sellers
 * @property SellerTranslation[] $sellerTranslations
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'code', 'created_at', 'type', 'activated'], 'required'],
            [['created_at', 'activated_at'], 'safe'],
            [['type', 'activated', 'activated_by_id', 'created_by_id', 'updated_by_id', 'deleted_by_id'], 'integer'],
            [['legal_name', 'code', 'site'], 'string', 'max' => 255],
            [['country'], 'string', 'max' => 2],
            [['updated_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by_id' => 'id']],
            [['created_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by_id' => 'id']],
            [['deleted_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deleted_by_id' => 'id']],
            [['activated_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['activated_by_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'legal_name' => 'Legal Name',
            'country' => 'Country',
            'code' => 'Code',
            'site' => 'Site',
            'created_at' => 'Created At',
            'type' => 'Type',
            'activated' => 'Activated',
            'activated_by_id' => 'Activated By ID',
            'activated_at' => 'Activated At',
            'created_by_id' => 'Created By ID',
            'updated_by_id' => 'Updated By ID',
            'deleted_by_id' => 'Deleted By ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankDetails()
    {
        return $this->hasMany(BankDetails::className(), ['organization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContracts()
    {
        return $this->hasMany(Contract::className(), ['organization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturerTranslations()
    {
        return $this->hasMany(ManufacturerTranslation::className(), ['translatable_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialDirectories()
    {
        return $this->hasMany(MaterialDirectory::className(), ['manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'updated_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'deleted_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'activated_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationContacts()
    {
        return $this->hasMany(OrganizationContact::className(), ['organization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationImages()
    {
        return $this->hasMany(OrganizationImage::className(), ['organization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationLocations()
    {
        return $this->hasMany(OrganizationLocation::className(), ['organization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['id' => 'location_id'])->viaTable('organization_location', ['organization_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellerManufacturers()
    {
        return $this->hasMany(SellerManufacturer::className(), ['seller_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellerManufacturers0()
    {
        return $this->hasMany(SellerManufacturer::className(), ['manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturers()
    {
        return $this->hasMany(Organization::className(), ['id' => 'manufacturer_id'])->viaTable('seller_manufacturer', ['seller_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellers()
    {
        return $this->hasMany(Organization::className(), ['id' => 'seller_id'])->viaTable('seller_manufacturer', ['manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellerTranslations()
    {
        return $this->hasMany(SellerTranslation::className(), ['translatable_id' => 'id']);
    }
}
