<?php

namespace console\models\Company;

use Yii;

/**
 * This is the model class for table "location".
 *
 * @property integer $id
 * @property string $address
 * @property string $postal_code
 * @property string $country_code
 * @property string $locality
 * @property string $created_at
 * @property string $coordinates_latitude
 * @property string $coordinates_longitude
 * @property integer $type
 * @property string $description
 * @property integer $created_by_id
 * @property integer $updated_by_id
 * @property integer $deleted_by_id
 *
 * @property Users $updatedBy
 * @property Users $createdBy
 * @property Users $deletedBy
 * @property OrganizationLocation $organizationLocation
 * @property Organization[] $organizations
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'postal_code', 'country_code', 'locality', 'created_at', 'type'], 'required'],
            [['created_at'], 'safe'],
            [['coordinates_latitude', 'coordinates_longitude'], 'number'],
            [['type', 'created_by_id', 'updated_by_id', 'deleted_by_id'], 'integer'],
            [['address', 'postal_code', 'locality', 'description'], 'string', 'max' => 255],
            [['country_code'], 'string', 'max' => 2],
            [['updated_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['updated_by_id' => 'id']],
            [['created_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['created_by_id' => 'id']],
            [['deleted_by_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deleted_by_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'postal_code' => 'Postal Code',
            'country_code' => 'Country Code',
            'locality' => 'Locality',
            'created_at' => 'Created At',
            'coordinates_latitude' => 'Coordinates Latitude',
            'coordinates_longitude' => 'Coordinates Longitude',
            'type' => 'Type',
            'description' => 'Description',
            'created_by_id' => 'Created By ID',
            'updated_by_id' => 'Updated By ID',
            'deleted_by_id' => 'Deleted By ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'updated_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'created_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'deleted_by_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizationLocation()
    {
        return $this->hasOne(OrganizationLocation::className(), ['location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrganizations()
    {
        return $this->hasMany(Organization::className(), ['id' => 'organization_id'])->viaTable('organization_location', ['location_id' => 'id']);
    }
}
