<?php

namespace console\models\Company;

use Yii;

/**
 * This is the model class for table "seller_manufacturer".
 *
 * @property integer $id
 * @property integer $seller_id
 * @property integer $manufacturer_id
 * @property string $created_at
 *
 * @property Organization $seller
 * @property Organization $manufacturer
 * @property SellerManufacturerCatalog[] $sellerManufacturerCatalogs
 * @property Catalog[] $catalogs
 */
class SellerManufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seller_manufacturer';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seller_id', 'manufacturer_id'], 'integer'],
            [['created_at'], 'required'],
            [['created_at'], 'safe'],
            [['seller_id', 'manufacturer_id'], 'unique', 'targetAttribute' => ['seller_id', 'manufacturer_id'], 'message' => 'The combination of Seller ID and Manufacturer ID has already been taken.'],
            [['seller_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['seller_id' => 'id']],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seller_id' => 'Seller ID',
            'manufacturer_id' => 'Manufacturer ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeller()
    {
        return $this->hasOne(Organization::className(), ['id' => 'seller_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Organization::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellerManufacturerCatalogs()
    {
        return $this->hasMany(SellerManufacturerCatalog::className(), ['seller_manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogs()
    {
        return $this->hasMany(Catalog::className(), ['id' => 'catalog_id'])->viaTable('seller_manufacturer_catalog', ['seller_manufacturer_id' => 'id']);
    }
}
