<?php

namespace console\models\Company;

use Yii;

/**
 * This is the model class for table "manufacturer_translation".
 *
 * @property integer $id
 * @property integer $translatable_id
 * @property string $name
 * @property string $description
 * @property string $locale
 * @property integer $name_transliterated
 *
 * @property Organization $translatable
 */
class ManufacturerTranslation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_translation';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_mcore');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['translatable_id', 'name_transliterated'], 'integer'],
            [['name', 'locale', 'name_transliterated'], 'required'],
            [['description'], 'string'],
            [['name', 'locale'], 'string', 'max' => 255],
            [['translatable_id', 'locale'], 'unique', 'targetAttribute' => ['translatable_id', 'locale'], 'message' => 'The combination of Translatable ID and Locale has already been taken.'],
            [['translatable_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['translatable_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'translatable_id' => 'Translatable ID',
            'name' => 'Name',
            'description' => 'Description',
            'locale' => 'Locale',
            'name_transliterated' => 'Name Transliterated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslatable()
    {
        return $this->hasOne(Organization::className(), ['id' => 'translatable_id']);
    }
}
