<?php

use yii\db\Migration;

/**
 * Class m171130_083545_core_structure
 */
class m171130_083545_core_structure extends Migration
{

    /**
     * Import Core DB structure by default DB connection
     */
    public function safeUp()
    {

        // 1. СОЗДАЕМ БАЗОВЫЕ ТАБЛИЦЫ
        // 1.1. создаем таблицу context
        $this->createTable('{{%context}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(50)->notNull()->unique(),
        ]);
        $this->alterColumn('{{%context}}', 'id', 'smallint');
        $this->createIndex('UN_context', '{{%context}}', ['name'], true);
        // 1.2. создаем таблицу element_class
        $this->createTable('{{%element_class}}', [
            'id'          => $this->primaryKey(),
            'context_id'  => $this->smallInteger()->notNull(),
            'name'        => $this->string(255)->notNull()->unique(),
            'description' => $this->text(),
            'parent_id'   => $this->integer()->defaultValue(0)->notNull(),
        ]);
        $this->createIndex('UN_element_class_name', '{{%element_class}}', ['name'], true);
        $this->createIndex('IXFK_class_context', '{{%element_class}}', 'context_id');
        $this->addForeignKey('FK_class_context', '{{%element_class}}', 'context_id', '{{%context}}', 'id');
        // 1.3. создаем таблицу property_type
        $this->createTable('{{%property_type}}', [
            'id'               => $this->primaryKey(),
            'name'             => $this->string(50)->notNull()->unique(),
            'parent_id'        => $this->smallInteger(),
            'element_class_id' => $this->integer(),
        ]);
        $this->alterColumn('{{%property_type}}', 'id', 'smallint');
        $this->createIndex('UN_property_type_name', '{{%property_type}}', ['name'], true);
        $this->createIndex('IXFK_property_type_element_class', '{{%property_type}}', ['id', 'element_class_id']);
        $this->addForeignKey('FK_property_type_element_class', '{{%property_type}}', 'element_class_id', '{{%element_class}}', 'id');
        // 1.4. создаем таблицу property_group
        $this->createTable('{{%property_group}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(255)->notNull()->unique(),
            'sort_order' => $this->integer()->defaultValue(1000)->notNull(),
        ]);
        $this->createIndex('UN_property_group_name', '{{%property_group}}', ['name'], true);
        $this->insert('{{%property_group}}', array(
            'name' => 'Габаритные размеры',
        ));
        // 1.5. создаем таблицу property_unit
        $this->createTable('{{%property_unit}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(50)->notNull()->unique(),
        ]);
        $this->createIndex('UN_property_unit_name', '{{%property_unit}}', ['name'], true);
        $this->insert('{{%property_unit}}', array(
            'name' => 'mm',
        ));
        // 1.6. создаем таблицу element
        $this->createTable('{{%element}}', [
            'id'         => $this->bigPrimaryKey(),
            'sort_order' => $this->integer()->defaultValue(1000)->notNull(),
            'is_active'  => $this->boolean()->defaultValue(false)->notNull(),
        ]);
        #==========================================================================================

        // 2. СОЗДАЕМ СВЯЗУЮЩИЕ ТАБЛИЦЫ ДЛЯ БАЗОВЫХ ТАБЛИЦ
        // 2.1. базовая таблица property (имеет свойства связующей, поэтому в этом разделе)
        $this->createTable('{{%property}}', [
            'id'                => $this->primaryKey(),
            'name'              => $this->string(255)->notNull()->unique(),
            'sysname'           => $this->string(50)->notNull()->unique(),
            'description'       => $this->text(),
            'property_unit_id'  => $this->integer(),
            'is_specific'       => $this->boolean()->defaultValue(false)->notNull(),
            'property_type_id'  => $this->smallInteger()->notNull(),
            'property_group_id' => $this->integer(),
            'sort_order'        => $this->smallInteger()->defaultValue(1000)->notNull(),
        ]);
        $this->createIndex('UN_property_name', '{{%property}}', ['name'], true);
        $this->createIndex('IXFK_property_property_group', '{{%property}}', ['id', 'property_group_id']);
        $this->createIndex('IXFK_property_property_type', '{{%property}}', ['id', 'property_type_id']);
        $this->createIndex('IXFK_property_property_unit', '{{%property}}', ['id', 'property_unit_id']);
        $this->addForeignKey('FK_property_property_group', '{{%property}}', 'property_group_id', '{{%property_group}}', 'id');
        $this->addForeignKey('FK_property_property_type', '{{%property}}', 'property_type_id', '{{%property_type}}', 'id');
        $this->addForeignKey('FK_property_property_unit', '{{%property}}', 'property_unit_id', '{{%property_unit}}', 'id');
        // 2.2. связующая таблица property2element_class
        $this->createTable('{{%property2element_class}}', [
            'element_class_id' => $this->integer()->notNull(),
            'property_id'      => $this->integer()->notNull(),
            'is_parent'        => $this->boolean()->defaultValue(false)->notNull(),
        ]);
        $this->addPrimaryKey('PK_property2element_class', '{{%property2element_class}}', ['element_class_id', 'property_id']);
        $this->createIndex('IXFK_property2element_class_class', '{{%property2element_class}}', 'element_class_id');
        $this->createIndex('IXFK_property2element_class_property', '{{%property2element_class}}', 'property_id');
        $this->addForeignKey('FK_property2element_class_class', '{{%property2element_class}}', 'element_class_id', '{{%element_class}}', 'id');
        $this->addForeignKey('FK_property2element_class_property', '{{%property2element_class}}', 'property_id', '{{%property}}', 'id');
        // 2.3. связующая таблица element2element_class
        $this->createTable('{{%element2element_class}}', [
            'element_class_id' => $this->integer()->notNull(),
            'element_id'       => $this->bigInteger()->notNull(),
        ]);
        $this->addPrimaryKey('PK_element2element_class', '{{%element2element_class}}', ['element_class_id', 'element_id']);
        $this->createIndex('IXFK_element2element_class_class', '{{%element2element_class}}', 'element_class_id');
        $this->createIndex('IXFK_element2element_class_element', '{{%element2element_class}}', 'element_id');
        $this->addForeignKey('FK_element2element_class_class', '{{%element2element_class}}', 'element_class_id', '{{%element_class}}', 'id');
        $this->addForeignKey('FK_element2element_class_element', '{{%element2element_class}}', 'element_id', '{{%element}}', 'id');
        #==========================================================================================

        // 3. СОЗДАЕМ СВЯЗУЮЩИЕ ТАБЛИЦЫ ДЛЯ БАЗОВОЙ ТАБЛИЦЫ property (таблицы хранения данных)
        // 3.1. создаем таблицу для property - тип данных boolean
        $this->createTable('{{%property_value_boolean}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->boolean()->notNull()->defaultValue(false),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('UN_property_value_boolean', '{{%property_value_boolean}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_boolean_property', '{{%property_value_boolean}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_boolean_property', '{{%property_value_boolean}}', 'property_id', '{{%property}}', 'id');
        // 3.2. создаем таблицу для property - тип данных int
        $this->createTable('{{%property_value_int}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->integer()->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('UN_property_value_int', '{{%property_value_int}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_int_property', '{{%property_value_int}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_int_property', '{{%property_value_int}}', 'property_id', '{{%property}}', 'id');
        // 3.3. создаем таблицу для property - тип данных bigInteger
        $this->createTable('{{%property_value_bigint}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->bigInteger()->notNull()->unsigned(),
            'element_id'  => $this->bigInteger()->notNull(),
            'is_parent'   => $this->boolean()->defaultValue(false)->notNull(),
        ]);
        $this->createIndex('UN_property_value_bigint', '{{%property_value_bigint}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_property_bigint', '{{%property_value_bigint}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_property_bigint', '{{%property_value_bigint}}', 'property_id', '{{%property}}', 'id');
        // 3.4. создаем таблицу для property - тип данных float
        $this->createTable('{{%property_value_float}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->float()->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('UN_property_value_float', '{{%property_value_float}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_float_property', '{{%property_value_float}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_float_property', '{{%property_value_float}}', 'property_id', '{{%property}}', 'id');
        // 3.5. создаем таблицу для property - тип данных string
        $this->createTable('{{%property_value_string}}', [
            'id'                         => $this->bigPrimaryKey(),
            'property_id'                => $this->integer()->notNull(),
            'value'                      => $this->string(255)->notNull(),
            'element_id'                 => $this->bigInteger()->notNull(),
            'searchdb_property_value_id' => $this->bigInteger(),
        ]);
        $this->createIndex('UN_string_property_element_value', '{{%property_value_string}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_string_property', '{{%property_value_string}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_string_property', '{{%property_value_string}}', 'property_id', '{{%property}}', 'id');
        // 3.6. создаем таблицу для property - тип данных text
        $this->createTable('{{%property_value_text}}', [
            'id'                         => $this->bigPrimaryKey(),
            'property_id'                => $this->integer()->notNull(),
            'value'                      => $this->text()->notNull(),
            'element_id'                 => $this->bigInteger()->notNull(),
            'searchdb_property_value_id' => $this->bigInteger(),
        ]);
        $this->createIndex('UN_property_value_text', '{{%property_value_text}}', ['property_id', 'element_id'], true);
        $this->createIndex('IXFK_property_value_text_property', '{{%property_value_text}}', ['id', 'property_id']);
        $this->addForeignKey('IXFK_property_value_text_property', '{{%property_value_text}}', 'property_id', '{{%property}}', 'id');
        // 3.7. создаем таблицу для property - тип данных date
        $this->createTable('{{%property_value_date}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->date()->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('UN_property_value_date', '{{%property_value_date}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_date_property', '{{%property_value_date}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_date_property', '{{%property_value_date}}', 'property_id', '{{%property}}', 'id');
        // 3.8. создаем таблицу для property - тип данных timestamp
        $this->createTable('{{%property_value_timestamp}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->timestamp()->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('UN_property_value_timestamp', '{{%property_value_timestamp}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_timestamp_property', '{{%property_value_timestamp}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_timestamp_property', '{{%property_value_timestamp}}', 'property_id', '{{%property}}', 'id');
        // 3.9. создаем таблицу для property - тип данных geolocation
        $this->createTable('{{%property_value_geolocation}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->addColumn('{{%property_value_geolocation}}', '{{%value}}', 'point NOT NULL');
        // $this->createIndex('UN_property_value_geolocation', '{{%property_value_geolocation}}', ['value'], true);
        $this->createIndex('IXFK_property_value_geolocation_property', '{{%property_value_geolocation}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_geolocation_property', '{{%property_value_geolocation}}', 'property_id', '{{%property}}', 'id');

        // создаем таблицу для property - тип данных char (color)
        $this->createTable('{{%property_value_color}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->char(7)->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('UN_property_value_color', '{{%property_value_color}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_color_property', '{{%property_value_color}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_color_property', '{{%property_value_color}}', 'property_id', '{{%property}}', 'id');


        #==========================================================================================

        // 4. СОЗДАЕМ СВЯЗУЮЩИЕ ТАБЛИЦЫ ДЛЯ БАЗОВОЙ ТАБЛИЦЫ property (прочие таблицы)
        // 4.1. создаем таблицу property_range
        $this->createTable('{{%property_range}}', [
            'id'                         => $this->bigPrimaryKey(),
            'name'                       => $this->string(255)->notNull()->unique(),
            'property_id'                => $this->integer()->notNull(),
            'from_value_id'              => $this->bigInteger(),
            'to_value_id'                => $this->bigInteger(),
            'element_id'                 => $this->bigInteger()->notNull(),
            'searchdb_property_value_id' => $this->bigInteger(),
        ]);
        $this->createIndex('UN_property_range_property_name', '{{%property_range}}', ['property_id', 'name'], true);
        $this->createIndex('UN_property_range_property_value', '{{%property_range}}', ['property_id', 'from_value_id', 'to_value_id'], true);
        $this->createIndex('IXFK_property_range_property', '{{%property_range}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_range_property', '{{%property_range}}', 'property_id', '{{%property}}', 'id');
        // 4.2. создаем таблицу property_array
        $this->createTable('{{%property_array}}', [
            'id'                         => $this->bigPrimaryKey(),
            'name'                       => $this->string(255)->notNull(),
            'property_id'                => $this->integer()->notNull(),
            'element_id'                 => $this->bigInteger()->notNull(),
            'searchdb_property_value_id' => $this->bigInteger(),
        ]);
        $this->addColumn('{{%property_array}}', 'value_ids', 'int8[] NOT NULL');
        $this->createIndex('UN_property_array_name', '{{%property_array}}', ['property_id', 'name'], true);
        $this->createIndex('UN_property_array_value', '{{%property_array}}', ['property_id', 'value_ids']);
        $this->createIndex('IXFK_property_array_property', '{{%property_array}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_array_property', '{{%property_array}}', 'property_id', '{{%property}}', 'id');
        // 4.3. создаем таблицу property_list_item
        $this->createTable('{{%property_list_item}}', [
            'id'                         => $this->bigPrimaryKey(),
            'property_id'                => $this->integer()->notNull(),
            'item'                       => $this->string(255)->notNull(),
            'label'                      => $this->string(255)->notNull(),
            'sort_order'                 => $this->bigInteger()->defaultValue(0),
            'searchdb_property_value_id' => $this->bigInteger(),
        ]);
        $this->createIndex('UN_property_property_value_list_item', '{{%property_list_item}}', ['property_id', 'item'], true);
        $this->createIndex('UN_property_property_list_item_label', '{{%property_list_item}}', ['property_id', 'label'], true);
        $this->createIndex('IXFK_property_property_list_item', '{{%property_list_item}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_property_list_item', '{{%property_list_item}}', 'property_id', '{{%property}}', 'id');
        // 4.4. создаем таблицу property_value_list_item
        $this->createTable('{{%property_value_list_item}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->bigInteger()->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('IXFK_property_value_list_item_property_list_item', '{{%property_value_list_item}}', ['id', 'property_id']);
        $this->createIndex('IXFK_property_value_property_list_item', '{{%property_value_list_item}}', ['id', 'value']);
        $this->addForeignKey('FK_property_value_property_list_item', '{{%property_value_list_item}}', 'property_id', '{{%property}}', 'id');
        $this->addForeignKey('FK_property_value_list_item_property_list_item', '{{%property_value_list_item}}', 'value', '{{%property_list_item}}', 'id');
        #==========================================================================================

        // 5. СОЗДАЕМ ПРОЧИЕ СВЯЗЫВАЮЩИЕ ТАБЛИЦЫ
        // 5.1. связующая таблица property_variant
        $this->createTable('{{%property_variant}}', [
            'id'                       => $this->bigPrimaryKey(),
            'property_id'              => $this->integer()->notNull(),
            'property_multiplicity_id' => $this->smallInteger()->notNull(),
            'property_value_id'        => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('IXFK_property_variant_property', '{{%property_variant}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_variant_property', '{{%property_variant}}', 'property_id', '{{%property}}', 'id');
        // 5.2. связующая таблица element_category
        $this->createTable('{{%element_category}}', [
            'id'                  => $this->primaryKey(),
            'name'                => $this->string(255)->notNull()->unique(),
            'sysname'             => $this->string(50)->unique(),
            'description'         => $this->text(),
            'parent_id'           => $this->integer(),
            'is_parent'           => $this->boolean()->defaultValue(false)->notNull(),
            'root_id'             => $this->integer(),
            'is_active'           => $this->boolean()->defaultValue(false)->notNull(),
            'element_class_id'    => $this->integer()->notNull(),
            'property_variant_id' => $this->bigInteger()->notNull(),
            'sort_order'          => $this->integer()->defaultValue(1000)->notNull(),
            'level'               => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);
        $this->createIndex('UN_element_category_name', '{{%element_category}}', ['name'], true);
        $this->createIndex('UN_element_category_sysname', '{{%element_category}}', ['sysname'], true);
        $this->createIndex('IXFK_element_category_element_category', '{{%element_category}}', ['id', 'parent_id']);
        $this->createIndex('IXFK_element_category_element_class', '{{%element_category}}', ['id', 'element_class_id']);
        $this->createIndex('IXFK_element_category_property_variant', '{{%element_category}}', ['id', 'property_variant_id']);
        $this->addForeignKey('FK_element_category_element_category', '{{%element_category}}', 'parent_id', '{{%element_category}}', 'id');
        $this->addForeignKey('FK_element_category_element_class', '{{%element_category}}', 'element_class_id', '{{%element_class}}', 'id');
        $this->addForeignKey('FK_element_category_property_variant', '{{%element_category}}', 'property_variant_id', '{{%property_variant}}', 'id');
        #==========================================================================================

        // XX. вывод результата
        echo "m171130_083545_core_structure was just successfully migrated. \n";
        return true;
        #==========================================================================================

    }

    /**
     * Delete Core DB structure by default DB connection
     */
    public function safeDown()
    {
        // 5. УДАЛЯЕМ ПРОЧИЕ СВЯЗЫВАЮЩИЕ ТАБЛИЦЫ
        $this->dropTable('{{%element_category}}');
        $this->dropTable('{{%property_variant}}');

        // 4. УДАЛЯЕМ СВЯЗУЮЩИЕ ТАБЛИЦЫ ДЛЯ БАЗОВОЙ ТАБЛИЦЫ property (прочие таблицы)
        $this->dropTable('{{%property_value_list_item}}');
        $this->dropTable('{{%property_list_item}}');
        $this->dropTable('{{%property_array}}');
        $this->dropTable('{{%property_range}}');

        // 3. СОЗДАЕМ СВЯЗУЮЩИЕ ТАБЛИЦЫ ДЛЯ БАЗОВОЙ ТАБЛИЦЫ property (таблицы хранения данных)
        $this->dropTable('{{%property_value_geolocation}}');
        $this->dropTable('{{%property_value_timestamp}}');
        $this->dropTable('{{%property_value_date}}');
        $this->dropTable('{{%property_value_text}}');
        $this->dropTable('{{%property_value_string}}');
        $this->dropTable('{{%property_value_float}}');
        $this->dropTable('{{%property_value_bigint}}');
        $this->dropTable('{{%property_value_int}}');
        $this->dropTable('{{%property_value_boolean}}');
        $this->dropTable('{{%property_value_color}}');

        // 2. УДАЛЯЕМ СВЯЗУЮЩИЕ ТАБЛИЦЫ ДЛЯ БАЗОВЫХ ТАБЛИЦ
        $this->dropTable('{{%element2element_class}}');
        $this->dropTable('{{%property2element_class}}');
        $this->dropTable('{{%property}}');

        // 1. удаляем базовые таблицы
        $this->dropTable('{{%element}}');
        $this->dropTable('{{%property_unit}}');
        $this->dropTable('{{%property_group}}');
        $this->dropTable('{{%property_type}}');
        $this->dropTable('{{%element_class}}');
        $this->dropTable('{{%context}}');

        // XX. вывод результата
        echo "m171130_083545_core_structure was just successfully reverted. \n";
        return true;
        #==========================================================================================

    }

}