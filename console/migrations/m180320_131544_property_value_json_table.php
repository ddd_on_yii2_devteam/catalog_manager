<?php

use yii\db\Migration;

/**
 * Class m180320_131544_property_value_json_table
 */
class m180320_131544_property_value_json_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%property_value_json}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => 'jsonb',
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('UN_json_property_element_value', '{{%property_value_json}}', ['property_id', 'element_id', 'value'], true);
        $this->createIndex('IXFK_property_value_json_property', '{{%property_value_json}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_value_json_property', '{{%property_value_json}}', 'property_id', '{{%property}}', 'id');

        echo "m180320_131544_property_value_json_table created.\n";
    }

    public function down()
    {
        $this->dropTable('{{%property_value_json}}');
        echo "m180320_131544_property_value_json_table reverted.\n";
    }

}
