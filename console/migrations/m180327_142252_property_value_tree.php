<?php

use yii\db\Migration;

/**
 * Class m180327_142252_property_value_tree
 */
class m180327_142252_property_value_tree extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%property_tree_item}}', [
            'id'                         => $this->bigPrimaryKey(),
            'property_id'                => $this->integer()->notNull(),
            'item'                       => $this->string(255)->notNull(),
            'label'                      => $this->string(255)->notNull(),
            'parent_id'                  => $this->bigInteger(),
            'sort_order'                 => $this->bigInteger()->defaultValue(0),
            'searchdb_property_value_id' => $this->bigInteger(),
        ]);

        $this->createIndex('UN_property_property_value_tree_item', '{{%property_tree_item}}', ['property_id', 'item'], true);
        $this->createIndex('IXFK_property_property_tree_item', '{{%property_tree_item}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_property_tree_item', '{{%property_tree_item}}', 'property_id', '{{%property}}', 'id');

        $this->createTable('{{%property_value_tree_item}}', [
            'id'          => $this->bigPrimaryKey(),
            'property_id' => $this->integer()->notNull(),
            'value'       => $this->bigInteger()->notNull(),
            'element_id'  => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('IXFK_property_value_tree_item_property_tree_item', '{{%property_value_tree_item}}', ['id', 'property_id']);
        $this->createIndex('IXFK_property_value_property_tree_item', '{{%property_value_tree_item}}', ['id', 'value']);
        $this->addForeignKey('FK_property_value_property_tree_item', '{{%property_value_tree_item}}', 'property_id', '{{%property}}', 'id');
        $this->addForeignKey('FK_property_value_tree_item_property_tree_item', '{{%property_value_tree_item}}', 'value', '{{%property_tree_item}}', 'id');
        #==========================================================================================
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

        $this->dropTable('{{%property_value_tree_item}}');
        $this->dropTable('{{%property_tree_item}}');

        echo "m180327_142252_property_value_tree cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180327_142252_property_value_tree cannot be reverted.\n";

        return false;
    }
    */
}
