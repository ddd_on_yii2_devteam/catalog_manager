<?php

use yii\db\Migration;

/**
 * Class m171214_103605_init_i18n_structure
 */
class m171214_103605_init_i18n_structure extends Migration
{

    /**
     * Turn to use "dbI18n" DB connection
     */
    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * Import i19n DB structure by "dbI18n" DB connection
     */
    public function safeUp()
    {

        // 1. СОЗДАЕМ ТАБЛИЦЫ ДЛЯ РУССКОГО ЯЗЫКА
        // 1.1. создаем базовую таблицу
        $this->createTable('{{%source}}', [
            'id' => $this->primaryKey(),
            'db_name' => $this->string(255)->notNull(),
            'table_name' => $this->string(255)->notNull(),
            'field_name' => $this->string(255)->notNull(),
            'is_string' => $this->boolean()->defaultValue(true),
        ]);

        #==========================================================================================

        // XX. вывод результата
        echo "m171214_103605_init_i18n_structure.php was just successfully migrated.\n";
        return true;
        #==========================================================================================

    }

    /**
     * Delete i18n DB structure by "dbI18n" DB connection
     */
    public function safeDown()
    {
        // 1. УДАЛЯЕМ ТАБЛИЦЫ
        $this->dropTable('{{%source}}');

        echo "m171214_103605_init_i18n_structure was just successfully reverted.\n";
        return true;

    }
}
