<?php

use yii\db\Migration;

/**
 * Handles the creation of table `property_validation_rule`.
 * Has foreign keys to the tables:
 *
 * - `property_variant_id`
 */
class m180305_124526_create_property_validation_rule_table extends Migration
{

    public function init()
    {
        $this->db = 'dbSearch';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        // create property_validation_rule
        $this->createTable('{{%property_validation_rule}}', [
            'id'                  => $this->primaryKey(),
            'property_variant_id' => $this->bigInteger(),
            'data'                => 'jsonb',

        ]);

        $this->createIndex('IXFK_property_validation_rule_property_variant', '{{%property_validation_rule}}', ['id', 'property_variant_id']);
        $this->addForeignKey('FK_property_validation_rule_property_variant', '{{%property_validation_rule}}', 'property_variant_id', '{{%property_variant}}', 'id');

        // create template
        $this->createTable('{{%template}}', [
            'id'                  => $this->primaryKey(),
            'element_class_id'    => $this->integer(),
            'property_variant_id' => $this->bigInteger(),
        ]);
        $this->addColumn('{{%template}}', 'property_ids', 'int4[]');

        $this->addForeignKey('FK_template_element_class', '{{%template}}', 'element_class_id', '{{%element_class}}', 'id');
        $this->addForeignKey('FK_template_property_variant', '{{%template}}', 'property_variant_id', '{{%property_variant}}', 'id');

        $this->createIndex('IXFK_template_element_class', '{{%template}}', ['id', 'element_class_id']);
        $this->createIndex('IXFK_template_property_variant', '{{%template}}', ['id', 'property_variant_id']);

        // addColumn to property
        $this->addColumn('{{%property}}', 'parent_id', $this->integer());
        $this->addColumn('{{%property}}', 'is_parent', $this->boolean()->defaultValue(false)->notNull());

        // addColumn to element_category
        $this->addColumn('{{%element_category}}', 'image_id', $this->string(32));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%element_category}}', 'image_id');

        $this->dropColumn('{{%property}}', 'is_parent');
        $this->dropColumn('{{%property}}', 'parent_id');

        // drops foreign key for table `property_variant_id`
/*        $this->dropForeignKey(
                'FK_template_element_class', 'FK_template_property_variant'
        );
        $this->dropIndex(
                'IXFK_template_element_class', 'IXFK_template_property_variant'
        );*/

        $this->dropTable('{{%template}}');


/*        $this->dropForeignKey(
                'FK_property_validation_rule_property_variant'
        );
        $this->dropIndex(
                'IXFK_property_validation_rule_property_variant'
        );*/

        $this->dropTable('{{%property_validation_rule}}');
    }

}
