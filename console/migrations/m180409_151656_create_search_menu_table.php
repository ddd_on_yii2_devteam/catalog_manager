<?php

use yii\db\Migration;

/**
 * Handles the creation of table `core_menu`.
 */
class m180409_151656_create_search_menu_table extends Migration
{
    /**
     * Turn to use "dbSearch" DB connection
     */
    public function init()
    {
        $this->db = 'dbSearch';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex('IXFK_element_category_element_class', '{{%element_category}}');
        $this->dropColumn('{{%element_category}}', 'element_class_id');
        $this->dropColumn('{{%element_category}}', 'is_parent');
        $this->dropColumn('{{%element_category}}', 'root_id');

        $this->addColumn('{{%element_category}}', 'menu_id', $this->integer());


        // 1. создаем таблицу menu
        $this->createTable('{{%menu}}', [
            'id'               => $this->bigPrimaryKey(),
            'element_class_id' => $this->integer()->notNull(),
            'name'             => $this->string(255)->notNull(),
        ]);
        $this->createIndex('IXFK-menu_element_class', '{{%menu}}', 'element_class_id');
        $this->addForeignKey('FK-menu_element_class', '{{%menu}}', 'element_class_id', '{{%element_class}}', 'id');

        $this->addForeignKey('FK-element_category_menu', '{{%element_category}}', 'menu_id', '{{%menu}}', 'id');

        $this->dropIndex('UN_element_category_name', 'element_category');

        $this->dropColumn('{{%element_category}}', 'name');
        $this->addColumn('{{%element_category}}', 'name', $this->string(255)->notNull());

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%element_category}}', 'name');
        $this->addColumn('{{%element_category}}', 'name', $this->string(255)->notNull()->unique());

        $this->createIndex('UN_element_category_name', 'element_category', ['name'], true);

        $this->dropColumn('{{%element_category}}', 'menu_id');
        $this->dropTable('{{%menu}}');

        $this->addColumn('{{%element_category}}', 'element_class_id', $this->integer()->notNull());
        $this->addColumn('{{%element_category}}', 'is_parent', $this->boolean()->defaultValue(false)->notNull());
        $this->addColumn('{{%element_category}}', 'root_id', $this->integer());


        $this->createIndex('IXFK_element_category_element_class', '{{%element_category}}', ['id', 'element_class_id']);

        return true;
    }

}
