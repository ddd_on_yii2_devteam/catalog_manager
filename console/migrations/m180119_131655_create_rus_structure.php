<?php

use yii\db\Migration;

/**
 * Class m180119_131655_create_rus_structure
 */
class m180119_131655_create_rus_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_rus
        $this->createTable('{{%string_rus}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_rus_source', '{{%string_rus}}', 'source_id');
        $this->addForeignKey('FK-string_rus_source', '{{%string_rus}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_rus
        $this->createTable('{{%text_rus}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_rus_source', '{{%text_rus}}', 'source_id');
        $this->addForeignKey('FK-text_rus_source', '{{%text_rus}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_rus
        $this->dropTable('{{%string_rus}}');
        // 2. удаляем таблицу text_rus
        $this->dropTable('{{%text_rus}}');

        return true;
    }
}