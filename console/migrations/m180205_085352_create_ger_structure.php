<?php

use yii\db\Migration;

/**
 * Class m180205_085352_create_ger_structure
 */
class m180205_085352_create_ger_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_ger
        $this->createTable('{{%string_ger}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_ger_source', '{{%string_ger}}', 'source_id');
        $this->addForeignKey('FK-string_ger_source', '{{%string_ger}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_ger
        $this->createTable('{{%text_ger}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_ger_source', '{{%text_ger}}', 'source_id');
        $this->addForeignKey('FK-text_ger_source', '{{%text_ger}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_ger
        $this->dropTable('{{%string_ger}}');
        // 2. удаляем таблицу text_ger
        $this->dropTable('{{%text_ger}}');

        return true;
    }
}