<?php

use yii\db\Migration;

/**
 * Class m180125_151229_create_ukr_structure
 */
class m180125_151229_create_ukr_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_ukr
        $this->createTable('{{%string_ukr}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_ukr_source', '{{%string_ukr}}', 'source_id');
        $this->addForeignKey('FK-string_ukr_source', '{{%string_ukr}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_ukr
        $this->createTable('{{%text_ukr}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_ukr_source', '{{%text_ukr}}', 'source_id');
        $this->addForeignKey('FK-text_ukr_source', '{{%text_ukr}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_ukr
        $this->dropTable('{{%string_ukr}}');
        // 2. удаляем таблицу text_ukr
        $this->dropTable('{{%text_ukr}}');

        return true;
    }
}