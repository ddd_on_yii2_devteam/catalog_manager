<?php

use yii\db\Migration;

/**
 * Class m180125_151246_create_ita_structure
 */
class m180125_151246_create_ita_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_ita
        $this->createTable('{{%string_ita}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_ita_source', '{{%string_ita}}', 'source_id');
        $this->addForeignKey('FK-string_ita_source', '{{%string_ita}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_ita
        $this->createTable('{{%text_ita}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_ita_source', '{{%text_ita}}', 'source_id');
        $this->addForeignKey('FK-text_ita_source', '{{%text_ita}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_ita
        $this->dropTable('{{%string_ita}}');
        // 2. удаляем таблицу text_ita
        $this->dropTable('{{%text_ita}}');

        return true;
    }
}