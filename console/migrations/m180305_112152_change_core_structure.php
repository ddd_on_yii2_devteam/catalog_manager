<?php

use yii\db\Migration;

/**
 * Class m180305_112152_change_core_structure
 */
class m180305_112152_change_core_structure extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // 1. удаляем таблицу element_category
        $this->dropTable('{{%element_category}}');

        // 2. удаляем таблицу property_variant
        $this->dropTable('{{%property_variant}}');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // 1. связующая таблица property_variant
        $this->createTable('{{%property_variant}}', [
            'id'                       => $this->bigPrimaryKey(),
            'property_id'              => $this->integer()->notNull(),
            'property_multiplicity_id' => $this->smallInteger()->notNull(),
            'property_value_id'        => $this->bigInteger()->notNull(),
        ]);
        $this->createIndex('IXFK_property_variant_property', '{{%property_variant}}', ['id', 'property_id']);
        $this->addForeignKey('FK_property_variant_property', '{{%property_variant}}', 'property_id', '{{%property}}', 'id');

        // 2. связующая таблица element_category
        $this->createTable('{{%element_category}}', [
            'id'                  => $this->primaryKey(),
            'name'                => $this->string(255)->notNull()->unique(),
            'sysname'             => $this->string(50)->unique(),
            'description'         => $this->text(),
            'parent_id'           => $this->integer(),
            'is_parent'           => $this->boolean()->defaultValue(false)->notNull(),
            'root_id'             => $this->integer(),
            'is_active'           => $this->boolean()->defaultValue(false)->notNull(),
            'element_class_id'    => $this->integer()->notNull(),
            'property_variant_id' => $this->bigInteger()->notNull(),
            'sort_order'          => $this->integer()->defaultValue(1000)->notNull(),
            'level'               => $this->smallInteger()->defaultValue(0)->notNull(),
        ]);
        $this->createIndex('UN_element_category_name', '{{%element_category}}', ['name'], true);
        $this->createIndex('UN_element_category_sysname', '{{%element_category}}', ['sysname'], true);
        $this->createIndex('IXFK_element_category_element_category', '{{%element_category}}', ['id', 'parent_id']);
        $this->createIndex('IXFK_element_category_element_class', '{{%element_category}}', ['id', 'element_class_id']);
        $this->createIndex('IXFK_element_category_property_variant', '{{%element_category}}', ['id', 'property_variant_id']);
        $this->addForeignKey('FK_element_category_element_category', '{{%element_category}}', 'parent_id', '{{%element_category}}', 'id');
        $this->addForeignKey('FK_element_category_element_class', '{{%element_category}}', 'element_class_id', '{{%element_class}}', 'id');
        $this->addForeignKey('FK_element_category_property_variant', '{{%element_category}}', 'property_variant_id', '{{%property_variant}}', 'id');
        
        echo "m180305_112152_change_core_structure reverted.\n";
        return true;
    }

}
