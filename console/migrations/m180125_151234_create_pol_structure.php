<?php

use yii\db\Migration;

/**
 * Class m180125_151234_create_pol_structure
 */
class m180125_151234_create_pol_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_pol
        $this->createTable('{{%string_pol}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_pol_source', '{{%string_pol}}', 'source_id');
        $this->addForeignKey('FK-string_pol_source', '{{%string_pol}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_pol
        $this->createTable('{{%text_pol}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_pol_source', '{{%text_pol}}', 'source_id');
        $this->addForeignKey('FK-text_pol_source', '{{%text_pol}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_pol
        $this->dropTable('{{%string_pol}}');
        // 2. удаляем таблицу text_pol
        $this->dropTable('{{%text_pol}}');

        return true;
    }
}