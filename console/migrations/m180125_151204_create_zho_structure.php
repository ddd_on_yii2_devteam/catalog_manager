<?php

use yii\db\Migration;

/**
 * Class m180125_151204_create_zho_structure
 */
class m180125_151204_create_zho_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_zho
        $this->createTable('{{%string_zho}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_zho_source', '{{%string_zho}}', 'source_id');
        $this->addForeignKey('FK-string_zho_source', '{{%string_zho}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_zho
        $this->createTable('{{%text_zho}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_zho_source', '{{%text_zho}}', 'source_id');
        $this->addForeignKey('FK-text_zho_source', '{{%text_zho}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_zho
        $this->dropTable('{{%string_zho}}');
        // 2. удаляем таблицу text_zho
        $this->dropTable('{{%text_zho}}');

        return true;
    }
}