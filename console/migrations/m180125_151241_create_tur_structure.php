<?php

use yii\db\Migration;

/**
 * Class m180125_151241_create_tur_structure
 */
class m180125_151241_create_tur_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_tur
        $this->createTable('{{%string_tur}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_tur_source', '{{%string_tur}}', 'source_id');
        $this->addForeignKey('FK-string_tur_source', '{{%string_tur}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_tur
        $this->createTable('{{%text_tur}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_tur_source', '{{%text_tur}}', 'source_id');
        $this->addForeignKey('FK-text_tur_source', '{{%text_tur}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_tur
        $this->dropTable('{{%string_tur}}');
        // 2. удаляем таблицу text_tur
        $this->dropTable('{{%text_tur}}');

        return true;
    }
}