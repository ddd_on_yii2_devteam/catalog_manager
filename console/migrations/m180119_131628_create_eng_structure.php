<?php

use yii\db\Migration;

/**
 * Class m180119_131628_create_eng_structure
 */
class m180119_131628_create_eng_structure extends Migration {

    public function init()
    {
        $this->db = 'dbI18n';
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function safeUp() {

        // 1. создаем таблицу string_eng
        $this->createTable('{{%string_eng}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->string(255),
            'property_id' => $this->integer(),
        ]);
        $this->createIndex('IXFK-string_eng_source', '{{%string_eng}}', 'source_id');
        $this->addForeignKey('FK-string_eng_source', '{{%string_eng}}', 'source_id', '{{%source}}', 'id');

        // 1. создаем таблицу text_eng
        $this->createTable('{{%text_eng}}', [
            'id' => $this->bigPrimaryKey(),
            'source_id' => $this->integer()->notNull(),
            'source_recordset_id' => $this->bigInteger()->notNull(),
            'value' => $this->text(),
            'property_id' => $this->integer(),

        ]);
        $this->createIndex('IXFK-text_eng_source', '{{%text_eng}}', 'source_id');
        $this->addForeignKey('FK-text_eng_source', '{{%text_eng}}', 'source_id', '{{%source}}', 'id');

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {

        // 1. удаляем таблицу string_eng
        $this->dropTable('{{%string_eng}}');
        // 2. удаляем таблицу text_eng
        $this->dropTable('{{%text_eng}}');

        return true;
    }
}