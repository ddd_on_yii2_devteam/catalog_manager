<?php

use yii\db\Migration;

/**
 * Class m180330_133520_property_value_money
 */
class m180330_133520_property_value_money extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%property_value_money}}', [
            'id'                    => $this->bigPrimaryKey(),
            'property_id'           => $this->integer()->notNull(),
            'value'                 => $this->integer()->notNull(),
            'element_id'            => $this->bigInteger()->notNull(),
            'currency_list_item_id' => $this->bigInteger()->notNull(),
        ]);

        $this->addForeignKey('FK_property_value_money_property', '{{%property_value_money}}', 'property_id', '{{%property}}', 'id');
        $this->createIndex('IXFK_property_value_money_property', '{{%property_value_money}}', ['id', 'property_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%property_value_money}}');

        echo "m180330_133520_property_value_money cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180330_133520_property_value_money cannot be reverted.\n";

        return false;
    }
    */
}
