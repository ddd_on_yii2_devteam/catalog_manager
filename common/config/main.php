<?php

use commonprj\components\catalog\entities as CatalogEntities;
use commonprj\components\core\entities as CoreEntities;
use commonprj\components\crm\entities as CrmEntities;

$params = array_merge(
    ['i18n' => require(__DIR__ . '/i18n.php')]
);

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language'   => 'eng-US',
    'components' => [

        // CORE
        'elementRepository'                  => [
            'class' => CoreEntities\element\ElementDBRepository::class,
        ],
        'elementClassRepository'             => [
            'class' => CoreEntities\elementClass\ElementClassDBRepository::class,
        ],
        'relationGroupRepository'            => [
            'class' => CoreEntities\relationGroup\RelationGroupDBRepository::class,
        ],
        'relationClassRepository'            => [
            'class' => CoreEntities\relationClass\RelationClassDBRepository::class,
        ],
        'propertyRepository'                 => [
            'class' => CoreEntities\property\PropertyDBRepository::class,
        ],
        'elementCategoryRepository'          => [
            'class' => CoreEntities\elementCategory\ElementCategoryDBRepository::class,
        ],
        'propertyListItemRepository'         => [
            'class' => CoreEntities\propertyListItem\PropertyListItemDBRepository::class,
        ],
        'propertyTreeItemRepository'         => [
            'class' => CoreEntities\propertyTreeItem\PropertyTreeItemDBRepository::class,
        ],
        'propertyGroupRepository'            => [
            'class' => CoreEntities\propertyGroup\PropertyGroupDBRepository::class,
        ],
        'propertyUnitRepository'             => [
            'class' => CoreEntities\propertyUnit\PropertyUnitDBRepository::class,
        ],
        'propertyTypeRepository'             => [
            'class' => CoreEntities\propertyType\PropertyTypeDBRepository::class,
        ],
        'menuRepository'             => [
            'class' => CoreEntities\menu\MenuDBRepository::class,
        ],

        // Property Value Repositories
        'propertyArrayValueRepository'       => [
            'class' => CoreEntities\propertyArrayValue\PropertyArrayValueDBRepository::class,
        ],
        'propertyRangeValueRepository'       => [
            'class' => CoreEntities\propertyRangeValue\PropertyRangeValueDBRepository::class,
        ],
        'propertyValueBooleanRepository'     => [
            'class' => CoreEntities\propertyValueBoolean\PropertyValueBooleanDBRepository::class,
        ],
        'propertyValueFloatRepository'       => [
            'class' => CoreEntities\propertyValueFloat\PropertyValueFloatDBRepository::class,
        ],
        'propertyValueGeolocationRepository' => [
            'class' => CoreEntities\propertyValueGeolocation\PropertyValueGeolocationDBRepository::class,
        ],
        'propertyValueIntRepository'         => [
            'class' => CoreEntities\propertyValueInt\PropertyValueIntDBRepository::class,
        ],
        'propertyValueStringRepository'      => [
            'class' => CoreEntities\propertyValueString\PropertyValueStringDBRepository::class,
        ],
        'propertyValueColorRepository'       => [
            'class' => CoreEntities\propertyValueColor\PropertyValueColorDBRepository::class,
        ],
        'propertyValueTextRepository'        => [
            'class' => CoreEntities\propertyValueText\PropertyValueTextDBRepository::class,
        ],
        'propertyValueTimeStampRepository'   => [
            'class' => CoreEntities\propertyValueTimeStamp\PropertyValueTimeStampDBRepository::class,
        ],
        'propertyValueBigintRepository'      => [
            'class' => CoreEntities\propertyValueBigint\PropertyValueBigintDBRepository::class,
        ],
        'propertyValueDateRepository'        => [
            'class' => CoreEntities\propertyValueDate\PropertyValueDateDBRepository::class,
        ],
        'propertyValueListRepository'        => [
            'class' => CoreEntities\propertyValueListItem\PropertyValueListItemDBRepository::class,
        ],
        'propertyValueTreeRepository'        => [
            'class' => CoreEntities\propertyValueTreeItem\PropertyValueTreeItemDBRepository::class,
        ],
        'propertyValueJsonRepository'        => [
            'class' => CoreEntities\propertyValueJson\PropertyValueJsonDBRepository::class,
        ],
        'propertyValueMoneyRepository'        => [
            'class' => CoreEntities\propertyValueMoney\PropertyValueMoneyDBRepository::class,
        ],

        'propertyVariantRepository'        => [
            'class' => CoreEntities\propertyVariant\PropertyVariantDBRepository::class,
        ],
        'propertyValidationRuleRepository'        => [
            'class' => CoreEntities\propertyValidationRule\PropertyValidationRuleDBRepository::class,
        ],
        'templateRepository' => [
            'class' => CoreEntities\template\TemplateDBRepository::class,
        ],

        'abstractPropertyValueRepository'    => [
            'class' => CoreEntities\abstractPropertyValue\AbstractPropertyValueDBRepository::class,
        ],
        'relationValueRepository'            => [
            'class' => CoreEntities\relationValue\RelationValueDBRepository::class,
        ],
        'hierarchyRelationValueRepository'   => [
            'class' => CoreEntities\hierarchyRelationValue\HierarchyRelationValueDBRepository::class,
        ],
        'aggregationRelationValueRepository' => [
            'class' => CoreEntities\aggregationRelationValue\AggregationRelationValueDBRepository::class,
        ],
        'associationRelationValueRepository' => [
            'class' => CoreEntities\associationRelationValue\AssociationRelationValueDBRepository::class,
        ],

        // CATALOG
        'materialRepository'                 => [
            'class' => CatalogEntities\material\MaterialDBRepository::class,
        ],
        'productModelRepository'             => [
            'class' => CatalogEntities\productModel\ProductModelDBRepository::class,
        ],
        'materialCollectionRepository'       => [
            'class' => CatalogEntities\materialCollection\MaterialCollectionDBRepository::class,
        ],
        'priceCategoryRepository'            => [
            'class' => CatalogEntities\priceCategory\PriceCategoryDBRepository::class,
        ],
        'productMaterialRepository'          => [
            'class' => CatalogEntities\productMaterial\ProductMaterialDBRepository::class,
        ],
        'materialTemplateRepository'         => [
            'class' => CatalogEntities\materialTemplate\MaterialTemplateDBRepository::class,
        ],
        'materialGroupRepository'            => [
            'class' => CatalogEntities\materialGroup\MaterialGroupDBRepository::class,
        ],
        'optionRepository'                   => [
            'class' => CatalogEntities\option\OptionDBRepository::class,
        ],
        'variantRepository'                  => [
            'class' => CatalogEntities\variant\VariantDBRepository::class,
        ],
        // CRM
        'addressRepository'                  => [
            'class' => CrmEntities\address\AddressDBRepository::class,
        ],
        'companyRepository'                  => [
            'class' => CrmEntities\company\CompanyDBRepository::class,
        ],
        'manufacturerRepository'             => [
            'class' => CrmEntities\manufacturer\ManufacturerDBRepository::class,
        ],
        'sellerRepository'                   => [
            'class' => CrmEntities\seller\SellerDBRepository::class,
        ],
        'customerRepository'                 => [
            'class' => CrmEntities\customer\CustomerDBRepository::class,
        ],
        'employeeRepository'                 => [
            'class' => CrmEntities\employee\EmployeeDBRepository::class,
        ],
        'abstractUserRepository'             => [
            'class' => CrmEntities\abstractUser\AbstractUserDBRepository::class,
        ],

        //Services
        'propertyService'                    => [
            'class' => commonprj\services\PropertyService::class,
        ],
        'searchService'                      => [
            'class' => \commonprj\services\search\SearchService::class,
        ],


        // Services
        'localization'                       => [
            'class' => commonprj\services\LocalizationService::class,
        ],
        'currency'                       => [
            'class' => commonprj\services\CurrencyService::class,
        ],

        'i18n' => [
            'translations' => [
                'entity*' => [
                    'class'          => 'commonprj\extendedStdComponents\FurniPhpMessageSource',
                    'sourceLanguage' => 'default',
                    'fileMap'        => [
                        'entity' => 'entity.php',
                    ],
                    "basePath"       => "@commonprj/messages",
                ],
            ],
        ],

    ],

    'params' => $params,
];
