<?php
/**
 * @package            FurniPrice
 * @subpackage         Core
 * @category           DBRepository
 * @created            15.01.2018
 * @author             Vasiliy Konakov
 * @refactoredBy       Vasiliy Konakov
 * @updated            15.01.2018
 */

namespace common\extendedStdComponents;

use yii;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\components\core\entities\element\Element;
use yii\web\HttpException;
use yii\helpers\Inflector;

/**
 * Class ViewChildren
 * @package common\extendedStdComponents
 */
class ViewChildren extends BaseAction
{

    var $condition = [];

    /**
     * Find all children elements related to basic element by property type "aggregation|hierarchy"
     * @return array null|Element[] (domain layer objects)
     * @throws HttpException
     */
    public function run(): array
    {

        // 1. Check access
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        // 2. Get query params
        $queryParams = yii::$app->getRequest()->getQueryParams();
        foreach ($queryParams as $key => $queryParam) {
            $this->condition[Inflector::underscore($key)] = $queryParam;
        }

        // 3. Get result
        $element = new Element([
            'id' => $this->condition['element_id'],
        ]);
        $result = $element->getChildren($this->condition['property_id']);

        #-----------------------
        var_dump($result);
        exit;
        # return true;

    }

}
