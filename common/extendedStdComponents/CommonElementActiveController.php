<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 29.06.2016
 */

namespace common\extendedStdComponents;

use commonprj\extendedStdComponents\BaseActiveController;

/**
 * ActiveController implements a common set of actions for supporting RESTful access to ActiveRecord.
 *
 * The class of the ActiveRecord should be specified via [[modelClass]], which must implement [[\yii\db\ActiveRecordInterface]].
 * By default, the following actions are supported:
 *
 * - `index`: list of models
 * - `view`: return the details of a model
 * - `create`: create a new model
 * - `update`: update an existing model
 * - `delete`: delete an existing model
 * - `options`: return the allowed HTTP methods
 *
 * You may disable some of these actions by overriding [[actions()]] and unsetting the corresponding actions.
 *
 * To add a new action, either override [[actions()]] by appending a new action class or write a new action method.
 * Make sure you also override [[verbs()]] to properly declare what HTTP methods are allowed by the new action.
 *
 * You should usually override [[checkAccess()]] to check whether the current user has the privilege to perform
 * the specified action against the specified model.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CommonElementActiveController extends BaseActiveController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        $defaultActions = parent::actions();
        $currentActions = [
            'viewElementElementClasses'     => [
                'class'       => 'common\extendedStdComponents\ViewElementElementClassesAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementChildren'           => [
                'class'       => 'common\extendedStdComponents\ViewElementChildrenAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementIsParent'           => [
                'class'       => 'common\extendedStdComponents\ViewElementIsParentAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementParent'             => [
                'class'       => 'common\extendedStdComponents\ViewElementParentAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementModels'             => [
                'class'       => 'common\extendedStdComponents\ViewElementModelsAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementProperties'         => [
                'class'       => 'common\extendedStdComponents\ViewElementPropertiesAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementRootIds'            => [
                'class'       => 'common\extendedStdComponents\ViewElementRootIdsAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementPropertyValue'           => [
                'class'       => 'common\extendedStdComponents\ViewElementPropertyValueAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementsByPropertyValues'           => [
                'class'       => 'common\extendedStdComponents\ViewElementsByPropertyValuesAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createElementChild'            => [
                'class'       => 'common\extendedStdComponents\CreateElementChildAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createElementPropertyRelation' => [
                'class'       => 'common\extendedStdComponents\CreateElementPropertyRelationAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createElementProperty' => [
                'class'       => 'common\extendedStdComponents\CreateElementPropertyAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createElement2ElementClass'    => [
                'class'       => 'common\extendedStdComponents\CreateElement2ElementClassAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario'    => $this->createScenario,
            ],
            'updateElementPropertyRelation'    => [
                'class'       => 'common\extendedStdComponents\UpdateElementPropertyRelationAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
                'scenario'    => $this->createScenario,
            ],
            'deleteElementPropertyValue'    => [
                'class'       => 'common\extendedStdComponents\DeleteElementPropertyValueAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteElement2ElementClass'    => [
                'class'       => 'common\extendedStdComponents\DeleteElement2ElementClassAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createBindChildren'            => [
                'class'       => 'common\extendedStdComponents\BindChildrenAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'updateBindChild'            => [
                'class'       => 'common\extendedStdComponents\BindChildAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'updateBindToParent'            => [
                'class'       => 'common\extendedStdComponents\BindToParentAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'updateUnbindChild'            => [
                'class'       => 'common\extendedStdComponents\UnbindChildAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'updateUnbindAllChildren'            => [
                'class'       => 'common\extendedStdComponents\UnbindAllChildrenAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
        $resultActions = array_merge($defaultActions, $currentActions);

        $resultActions = array_merge($resultActions, $this->addActions());

        return $resultActions;
    }

    protected function addActions() : array
    {
        return [];
    }
}
