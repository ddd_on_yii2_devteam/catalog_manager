<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 26.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\PropertyRelationRecord;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;

/**
 * Class DeletePropertyValueAction
 * @package common\extendedStdComponents
 */
class DeleteElementPropertyValueAction extends BaseAction
{
    /**
     * @param $id
     * @param $propertyId
     * @throws HttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function run($id, $propertyId)
    {
        /**
         * @var Property $property
         */
        $property = (new Property())->findOne([
            'condition' => ['id' => $propertyId]
        ]);
        $model = $property->getValueByElementId($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        if ($model) {
            if ($model->delete() === false) {
                throw new HttpException(500, 'Failed to delete the object for unknown reason.');
            }

            Yii::$app->getResponse()->setStatusCode(204);
        } else {
            Yii::$app->getResponse()->setStatusCode(404);
        }
    }
}