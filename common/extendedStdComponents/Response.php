<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/22/2018
 * Time: 5:14 PM
 */

namespace common\extendedStdComponents;

use Yii;
use yii\web\Response as OriginResponse;
use commonprj\services\LocalizationService;

class Response extends OriginResponse
{
    /**
     * Name of header with language
     * @var string
     */
    public $languageHeaderAttribute = 'Content-Language';

    /**
     * Name of header with country
     * @var string
     */
    public $countryHeaderAttribute = 'Content-Country';

    /**
     * Name of header with currency
     * @var string
     */
    public $currencyHeaderAttribute = 'Content-Currency';


    public function init()
    {
        parent::init();

        /** @var LocalizationService $localizationService */
        $localizationService = Yii::$app->localization;

        $this->headers->set($this->languageHeaderAttribute, $localizationService->getCurrentIsoLang());
        $this->headers->set($this->countryHeaderAttribute, 'US');
        $this->headers->set($this->currencyHeaderAttribute, 'usd');
    }
}