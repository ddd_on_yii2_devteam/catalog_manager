<?php

namespace common\extendedStdComponents\catalog\component;

use commonprj\components\catalog\entities\component\Component;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Component * @package api\controllers
 */
class ViewMaterialsAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Component $model
         */
        $model = $this->findModel($id);

        return $model->getMaterials() ?? [];
    }

}