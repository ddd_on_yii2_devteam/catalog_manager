<?php

namespace common\extendedStdComponents\catalog\materialCollection;

use commonprj\components\catalog\entities\materialCollection\MaterialCollection;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class MaterialCollection * @package api\controllers
 */
class ViewManufacturerAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var MaterialCollection $model
         */
        $model = $this->findModel($id);

        return $model->getManufacturer();
    }

}