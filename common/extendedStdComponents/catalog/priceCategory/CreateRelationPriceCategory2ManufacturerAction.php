<?php

namespace common\extendedStdComponents\catalog\priceCategory;

use commonprj\components\catalog\entities\priceCategory\PriceCategory;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class PriceCategory * @package api\controllers
 */
class CreateRelationPriceCategory2ManufacturerAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var PriceCategory $model
         */
        $model = $this->findModel($id);

        return $model->bindManufacturer($elementId);
    }

}