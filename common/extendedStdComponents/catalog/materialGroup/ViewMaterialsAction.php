<?php

namespace common\extendedStdComponents\catalog\materialGroup;

use commonprj\components\catalog\entities\materialGroup\MaterialGroup;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class MaterialGroup * @package api\controllers
 */
class ViewMaterialsAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var MaterialGroup $model
         */
        $model = $this->findModel($id);

        return $model->getMaterials() ?? [];
    }

}