<?php

namespace common\extendedStdComponents\catalog\material;

use commonprj\components\catalog\entities\material\Material;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class Material * @package api\controllers
 */
class ViewMaterialTemplateAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\catalog\entities\materialTemplate\MaterialTemplate|null
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Material $model
         */
        $model = $this->findModel($id);

        $isInHierarchy = Yii::$app->getRequest()->getQueryParam('isInHierarchy');

        if ($isInHierarchy && $isInHierarchy == 'true') {
            return $model->getMaterialTemplateInHierarchy();
        }

        return $model->getMaterialTemplate();
    }

}