<?php

namespace common\extendedStdComponents\catalog\material;

use commonprj\components\catalog\entities\material\Material;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Material * @package api\controllers
 */
class CreateRelationMaterial2MaterialTemplateAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Material $model
         */
        $model = $this->findModel($id);

        return $model->bindMaterialTemplate($elementId);
    }

}