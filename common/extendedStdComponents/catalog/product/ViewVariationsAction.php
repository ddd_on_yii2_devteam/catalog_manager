<?php

namespace common\extendedStdComponents\catalog\product;

use commonprj\components\catalog\entities\product\Product;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Product * @package api\controllers
 */
class ViewVariationsAction extends BaseAction
{

    /**
     * @param int $id
     * @return array
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id): array 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Product $model
         */
        $model = $this->findModel($id);

        return $model->getVariations() ?? [];
    }

}