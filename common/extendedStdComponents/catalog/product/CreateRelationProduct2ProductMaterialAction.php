<?php

namespace common\extendedStdComponents\catalog\product;

use commonprj\components\catalog\entities\product\Product;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class Product * @package api\controllers
 */
class CreateRelationProduct2ProductMaterialAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Product $model
         */
        $model = $this->findModel($id);

        return $model->bindProductMaterial($elementId);
    }

}