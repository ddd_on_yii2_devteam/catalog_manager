<?php

namespace common\extendedStdComponents\catalog\productMaterial;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ProductMaterial * @package api\controllers
 */
class ViewMaterialAction extends BaseAction
{

    /**
     * @param int $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductMaterial $model
         */
        $model = $this->findModel($id);

        return $model->getMaterial();
    }

}