<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.03.2018
 * Time: 16:37
 */

namespace common\extendedStdComponents\catalog\productMaterial;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewMaterialGroupAction
 * @package common\extendedStdComponents\catalog\productMaterial
 */
class ViewMaterialGroupAction extends BaseAction
{
    /**
     * @param int $id
     * @return \commonprj\components\catalog\entities\materialGroup\MaterialGroup
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductMaterial $model
         */
        $model = $this->findModel($id);

        return $model->getMaterialGroup();
    }
}