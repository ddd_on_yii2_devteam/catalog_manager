<?php

namespace common\extendedStdComponents\catalog\productMaterial;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ProductMaterial * @package api\controllers
 */
class ViewManufacturerAction extends BaseAction
{

    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id) 
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductMaterial $model
         */
        $model = $this->findModel($id);

        return $model->getManufacturer();
    }

}