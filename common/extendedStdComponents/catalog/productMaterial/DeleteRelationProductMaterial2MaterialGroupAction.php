<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.03.2018
 * Time: 16:24
 */

namespace common\extendedStdComponents\catalog\productMaterial;

use commonprj\components\catalog\entities\productMaterial\ProductMaterial;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class deleteRelationProductMaterial2MaterialGroupAction
 * @package common\extendedStdComponents\catalog\productMaterial
 */
class DeleteRelationProductMaterial2MaterialGroupAction extends BaseAction
{
    /**
     * @param int $id
     * @param int $elementId
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $elementId)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ProductMaterial $model
         */
        $model = $this->findModel($id);

        return $model->unbindMaterialGroup($elementId);
    }
}