<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\extendedStdComponents\catalog\productModel;

use commonprj\extendedStdComponents\EntityDataProvider;
use commonprj\services\search\ReturnFilter;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * IndexAction implements the API endpoint for listing multiple models.
 *
 * For more details and usage information on IndexAction, see the [guide article on rest controllers](guide:rest-controllers).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexAction extends \common\extendedStdComponents\IndexAction
{
    /**
     * This method is called right before `run()` is executed.
     * You may override this method to do preparation work for the action run.
     * If the method returns false, it will cancel the action.
     *
     * @return bool whether to run the action.
     */
    protected function beforeRun()
    {
        $result = parent::beforeRun();

        $controller = $this->controller;
        $controller->setSerializerParams([
            'collectionEnvelope' => 'items',
            'metaEnvelope'       => 'pager',
            'expandParam'        => 'with',
            'returnFilters'      => new ReturnFilter([
                'country',
                'functionalArea',
                'stil',
                'price',
                'furnitureType',
            ]),

        ]);

        return $result;
    }


    /**
     * @return ActiveDataProvider
     */
    public function run()
    {

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        return $this->prepareDataProvider();
    }

    /**
     * Prepares the data provider that should return the requested collection of the models.
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        $filter = null;
        if ($this->dataFilter !== null) {
            $this->dataFilter = Yii::createObject($this->dataFilter);
            if ($this->dataFilter->load($requestParams)) {
                $filter = $this->dataFilter->build(false);
                if ($filter === false) {
                    return $this->dataFilter;
                }
            }
        }

        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this, $filter);
        }

        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->modelClass;

        $query = $modelClass::find();

        if (!empty($filter)) {
            $query->andWhere($filter);
        }

        return Yii::createObject([
            'class'      => EntityDataProvider::className(),
            'query'      => $query,
            'pagination' => [
                'params' => $requestParams,
            ],
            'sort'       => [
                'params' => $requestParams,
            ],
        ]);
    }
}
