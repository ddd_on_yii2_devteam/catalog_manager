<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 11.07.2016
 */

namespace common\extendedStdComponents;

use yii\data\ActiveDataProvider;
use commonprj\components\core\entities;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertiesAction
 * @package common\extendedStdComponents
 */
class ViewElementPropertiesAction extends BaseAction
{
    /**
     * @param $id
     * @return entities\property\Property[]|mixed|ActiveDataProvider
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var entities\element\Element $model */
        $model = new $this->modelClass();
        $condition['condition'] = ['id' => $id];
        $model = $model->findOne($condition);

        return $model->getProperties();
    }
}