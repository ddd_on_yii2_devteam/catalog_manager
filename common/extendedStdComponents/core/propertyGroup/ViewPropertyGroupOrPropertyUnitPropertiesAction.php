<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 17:22
 */

namespace common\extendedStdComponents\core\propertyGroup;

use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyGroupProperties
 * @package common\extendedStdComponents\core\propertyGroup
 */
class ViewPropertyGroupOrPropertyUnitPropertiesAction extends BaseAction
{
    /**
     * @param $id
     * @return array|\commonprj\components\core\entities\property\Property[]
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $entity = $this->findModel($id);

        return $entity->getProperties();
    }
}