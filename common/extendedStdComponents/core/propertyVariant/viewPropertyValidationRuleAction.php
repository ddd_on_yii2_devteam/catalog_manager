<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.02.2018
 * Time: 15:47
 */

namespace common\extendedStdComponents\core\propertyVariant;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyAction
 * @package common\extendedStdComponents\core\propertyVariant
 */
class ViewPropertyValidationRuleAction extends BaseAction
{
    /**
     * @param $id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $model = $this->findModel($id);

        return $model->getPropertyValidationRule();
    }
}