<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.02.2018
 * Time: 15:47
 */

namespace common\extendedStdComponents\core\propertyVariant;

use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyValueAction
 * @package common\extendedStdComponents\core\propertyVariant
 */
class ViewPropertyValueAction extends BaseAction
{
    /**
     * @param $id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var PropertyValue $propertyValue
         */
        $model = $this->findModel($id);

        return $model->getPropertyValue();
    }
}