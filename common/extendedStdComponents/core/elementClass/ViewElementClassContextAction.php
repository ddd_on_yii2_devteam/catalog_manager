<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.02.2018
 * Time: 18:39
 */

namespace common\extendedStdComponents\core\elementClass;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewElementClassContextAction
 * @package common\extendedStdComponents\core\elementClass
 */
class ViewElementClassContextAction extends BaseAction
{
    /**
     * @param $id
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ElementClass $elementClass
         */
        $elementClass = $this->findModel($id);

        return $elementClass->getContext();
    }
}