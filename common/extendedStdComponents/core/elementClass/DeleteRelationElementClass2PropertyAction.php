<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.02.2018
 * Time: 18:50
 */

namespace common\extendedStdComponents\core\elementClass;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class DeleteRelationElementClass2PropertyAction
 * @package common\extendedStdComponents\core\elementClass
 */
class DeleteRelationElementClass2PropertyAction extends BaseAction
{
    /**
     * @param $id
     * @return bool
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ElementClass $elementClass
         */
        $elementClass = $this->findModel($id);
        $propertyId = \Yii::$app->request->getQueryParam('propertyId');

        return $elementClass->unbindProperty($propertyId);
    }
}