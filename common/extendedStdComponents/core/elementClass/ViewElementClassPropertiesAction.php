<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.02.2018
 * Time: 18:44
 */

namespace common\extendedStdComponents\core\elementClass;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewElementClassPropertiesAction
 * @package common\extendedStdComponents\core\elementClass
 */
class ViewElementClassPropertiesAction extends BaseAction
{
    /**
     * @param $id
     * @return array
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ElementClass $elementClass
         */
        $elementClass = $this->findModel($id);

        return $elementClass->getProperties();
    }
}