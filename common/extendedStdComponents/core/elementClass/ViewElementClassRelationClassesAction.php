<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.02.2018
 * Time: 18:45
 */

namespace common\extendedStdComponents\core\elementClass;

use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewElementClassRelationClassesAction
 * @package common\extendedStdComponents\core\elementClass
 */
class ViewElementClassRelationClassesAction extends BaseAction
{
    /**
     * @param $id
     * @return \commonprj\components\core\entities\property\Property[]
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ElementClass $elementClass
         */
        $elementClass = $this->findModel($id);

        return $elementClass->getRelationClasses();
    }
}