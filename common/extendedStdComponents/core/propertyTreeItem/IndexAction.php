<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\extendedStdComponents\core\propertyTreeItem;

use Yii;
use commonprj\components\core\entities\propertyTreeItem\PropertyTreeItem;


/**
 * IndexAction implements the API endpoint for listing multiple models.
 *
 * For more details and usage information on IndexAction, see the [guide article on rest controllers](guide:rest-controllers).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexAction extends \common\extendedStdComponents\IndexAction
{
    /**
     * @return array|mixed|\yii\data\ActiveDataProvider
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        if (Yii::$app->getRequest()->get('view') == 'tree') {

            $propertyId = Yii::$app->getRequest()->get('propertyId');
            /** @var PropertyTreeItem $model */
            $model = new $this->modelClass();
            return $model->getTree($propertyId);
        }

        return parent::run();

    }
}
