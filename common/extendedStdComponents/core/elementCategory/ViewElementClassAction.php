<?php

namespace common\extendedStdComponents\core\elementCategory;

use commonprj\components\core\entities\elementCategory\ElementCategory;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ElementCategory * @package api\controllers
 */
class ViewElementClassAction extends BaseAction
{

    /**
     * @param int $id
     * @return \commonprj\components\core\entities\elementClass\ElementClass
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ElementCategory $model
         */
        $model = $this->findModel($id);

        return $model->getElementClass();
    }

}