<?php

namespace common\extendedStdComponents\core\elementCategory;

use commonprj\components\core\entities\elementCategory\ElementCategory;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ElementCategory * @package api\controllers
 */
class ViewChildrenAction extends BaseAction
{

    /**
     * @param int $id
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var ElementCategory $model
         */
        $model = $this->findModel($id);

        return $model->getChildren();
    }

}