<?php

namespace common\extendedStdComponents\core\elementCategory;

use commonprj\components\core\entities\menu\Menu;
use commonprj\extendedStdComponents\BaseAction;
use Yii;


/**
 * IndexAction implements the API endpoint for listing multiple models.
 *
 * For more details and usage information on IndexAction, see the [guide article on rest controllers](guide:rest-controllers).
 */
class IndexAction extends BaseAction
{
    /**
     * @return array|mixed|\yii\data\ActiveDataProvider
     * @throws \yii\db\Exception
     * @throws \yii\web\HttpException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $menuId = Yii::$app->getRequest()->get('menuId') ?? null;
        $menusQuery = Menu::find();

        if ($menuId) {
            $menusQuery = $menusQuery->where(['id' => $menuId]);
        }

        $menus = $menusQuery->all();

        /** @var Menu $menu */
        foreach ((array)$menus as $menu) {
            $tries[]['menu'] = $menu;
            $tries[]['tree'] = array_values($menu->getElementCategories());
        }

        return $tries ?? [];
    }
}
