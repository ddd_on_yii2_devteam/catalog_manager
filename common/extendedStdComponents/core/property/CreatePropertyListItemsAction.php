<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 01.03.2018
 * Time: 11:55
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class CreatePropertyListItemsAction
 * @package common\extendedStdComponents\core\property
 */
class CreatePropertyListItemsAction extends BaseAction
{

    /**
     * @param $id
     * @return BaseCrudModel|\commonprj\extendedStdComponents\BaseCrudModel
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var Property $model */
        $model = $this->findModel($id);
        $attributes = Yii::$app->getRequest()->getBodyParams();

        if (empty($attributes['items'] || !is_array($attributes['items']))) {
            throw new HttpException(422, "Требуется массив listItems");
        }

        $result = $model->setListItems($attributes['items']);

        if (!$result) {
            if ($model->hasErrors()) {
                throw new HttpException(422, json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE));
            } else {
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        }

        return $result;
    }
}