<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.02.2018
 * Time: 15:46
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class ViewPropertyVariantsAction
 * @package common\extendedStdComponents\core\property
 */
class ViewPropertyVariantAction extends BaseAction
{
    /**
     * @param $id
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Property $property
         */
        $property = $this->findModel($id);

        $request = Yii::$app->getRequest();
        $propertyValueId = $request->getQueryParam('propertyValueId');
        $multiplicityId = $request->getQueryParam('multiplicityId',1);

        return $property->getPropertyVariant($propertyValueId,$multiplicityId);
    }
}