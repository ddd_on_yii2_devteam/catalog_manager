<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10.04.2018
 * Time: 12:30
 */

namespace common\extendedStdComponents\core\property;

use commonprj\components\core\entities\property\Property;
use commonprj\extendedStdComponents\BaseAction;
use Yii;

/**
 * Class ViewTemplateAction
 * @package common\extendedStdComponents\core\property
 */
class ViewTemplateAction extends BaseAction
{
    /**
     * @param $id
     * @return \commonprj\components\core\entities\template\Template|null
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Property $model
         */
        $model = $this->findModel($id);

        $request = Yii::$app->getRequest();
        $elementClassId = $request->getQueryParam('elementClassId');
        $propertyValueId = $request->getQueryParam('propertyValueId');
        $multiplicityId = $request->getQueryParam('multiplicityId');

        return $model->getTemplate($elementClassId, $propertyValueId, $multiplicityId);
    }
}