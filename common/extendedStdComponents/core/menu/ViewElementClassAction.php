<?php

namespace common\extendedStdComponents\core\menu;

use commonprj\components\core\entities\menu\Menu;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewPropertyVariantsAction
 * @package common\extendedStdComponents\core\menu
 */
class ViewElementClassAction extends BaseAction
{
    /**
     * @param $id
     * @return \commonprj\components\core\entities\elementClass\ElementClass
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /**
         * @var Menu $menu
         */
        $menu = $this->findModel($id);

        return $menu->getElementClass();
    }
}