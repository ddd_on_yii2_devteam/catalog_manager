<?php
/**
 * @package            FurniPrice
 * @subpackage         Core
 * @category           DBRepository
 * @created            21.12.2017
 * @author             Vasiliy Konakov
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class BindToParentAction
 * @package common\extendedStdComponents
 */
class BindToParentAction extends BaseAction
{
    /**
     * Binding an element to parent by relation property
     * @param int $id Child (this) ElementRecord id
     * @param int $propertyId PropertyRecord id
     * @param int $parentElementId Parent ElementRecord id
     * @return boolean SUCCESS OR FAIL
     */
    public function run(int $id, int $propertyId, int $parentElementId): bool
    {

        // 1. Check access
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var Element $model */
        $model = $this->findModel($id);
        return $model->bindToParent($propertyId, $parentElementId);
    }
}