<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 29.06.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\auth\auth\Auth;
use commonprj\components\core\entities\billing\client\ClientServiceRepository;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\services\Route;
use PDO;
use PDOException;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\BaseInflector;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AuthAction extends BaseAction
{
    /**
     * @return Model
     * @throws BadRequestHttpException
     */
    public function run($login, $pass)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        /** @var Auth $model */
        $model = new $this->modelClass();

//        if (!isset($bodyParams['login'])) {
//            $model->addError('login', 'Missing required parameters: login');
//        }
//
//        if (!isset($bodyParams['pass'])) {
//            $model->addError('pass', 'Missing required parameters: pass');
//        }

        if (!$model->hasErrors()) {
            $model->getSession($login, $pass);
        }

        return Json::decode($model->result);
    }
}
