<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 19.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\property\Property;
use commonprj\components\core\models\Property2elementClassRecord;
use commonprj\extendedStdComponents\BaseDBRepository;
use commonprj\extendedStdComponents\BaseAction;
use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * Class createPropertyClassAction
 * @package common\extendedStdComponents
 */
class CreatePropertyListItemsAction extends BaseAction
{
    /**
     * @var string the scenario to be assigned to the new model before it is validated and saved.
     */
    public $scenario = Model::SCENARIO_DEFAULT;

    /**
     * Creates a new model.
     * @param $id
     * @param $elementClassId
     * @throws HttpException
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var Property $model */
        $model = $this->findModel($id);
        $items = Yii::$app->getRequest()->getBodyParam('items', []);

        return $model->setListItems($items);
    }
}