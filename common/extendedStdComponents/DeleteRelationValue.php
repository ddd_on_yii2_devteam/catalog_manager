<?php
/**
 * @package            FurniPrice
 * @subpackage        Core
 * @category        DBRepository
 * @created            14.12.2017
 * @author            Vasiliy Konakov
 * @refactoredBy
 * @updated
 */

namespace common\extendedStdComponents;

use yii;
use yii\web\BadRequestHttpException;
use yii\helpers\Inflector;
use commonprj\components\core\entities\relationValue\RelationValue;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class DeleteRelationValue
 * @package common\extendedStdComponents
 */
class DeleteRelationValue extends BaseAction
{

    var $condition = [];

    /**
     * @return boolean
     * @throws BadRequestHttpException
     */
    public function run(): bool
    {

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $queryParams = yii::$app->getRequest()->getQueryParams();
        foreach ($queryParams as $key => $queryParam) {
            $this->condition[Inflector::underscore($key)] = $queryParam;
        }

        /** @var RelationValue $modelClass */
        $modelClass = new $this->modelClass();
        foreach ($queryParams as $queryParamKey => $queryParamValue) {
            $modelClass->$queryParamKey = $queryParamValue;
        }

        return $modelClass->delete($this->condition);

    }

}
