<?php
/**
 * @package            FurniPrice
 * @subpackage         Core
 * @category           DBRepository
 * @created            21.12.2017
 * @author             Vasiliy Konakov
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class UnbindChildAction
 * @package common\extendedStdComponents
 */
class UnbindChildAction extends BaseAction
{
    /**
     * Delete relation between elements identified by property
     * @param int $id Basic parent (this) ElementRecord id
     * @param int $propertyId PropertyRecord id
     * @param int $childElementId child ElementRecord id
     * @return boolean SUCCESS OR FAIL
     */
    public function run(int $id, int $propertyId, int $childElementId): bool
    {
        // 1. Check access
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }
        /** @var Element $model */
        $model = $this->findModel($id);
        return $model->unbindChild($propertyId, $childElementId);
    }
}