<?php

namespace common\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewElementChildrenAction
 * @package common\extendedStdComponents
 */
class ViewElementChildrenAction extends BaseAction
{
    /**
     * Find all children elements related to basic element by child-type relation property
     * @param int $id ElementRecord id
     * @param int|null $propertyId PropertyRecord id
     * @return array Element[] (domain layer objects)
     */
    public function run(int $id, int $propertyId = null): array
    {
        // 1. Check access
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }
        /** @var Element $model */
        $model = $this->findModel($id);
        return $model->getChildren($propertyId);
    }
}