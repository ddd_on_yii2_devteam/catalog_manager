<?php
/**
 * @package            FurniPrice
 * @subpackage         Core
 * @category           DBRepository
 * @created            21.12.2017
 * @author             Vasiliy Konakov
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewParentAction
 * @package common\extendedStdComponents
 */
class ViewElementParentAction extends BaseAction
{
    /**
     * Find parent element related to basic element identified by parent-type relation property
     * @param int $id ElementRecord id
     * @param int $propertyId PropertyRecord id
     * @return null|Element (domain layer object)
     */
    public function run(int $id, int $propertyId): ?Element
    {
        // 1. Check access
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }
        /** @var Element $model */
        $model = $this->findModel($id);
        return $model->getParent($propertyId);
    }
}