<?php

namespace common\extendedStdComponents;

use Yii;
use yii\web\HttpException;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\components\core\entities\property\Property;

/**
 * Class deletePropertyClass
 * @package common\extendedStdComponents
 */
class DeletePropertyClassAction extends BaseAction
{
    /**
     * @param $id
     * @param $elementClassId
     * @throws \yii\web\HttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id, $elementClassId)
    {
        /** @var Property $model */
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $result = $model->deletePropertyClass($elementClassId);
        if ($result) {
            Yii::$app->getResponse()->setStatusCode(204);
        } else {
            throw new HttpException(500, 'Failed to delete the object for unknown reason.');
        }

    }
}