<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 06.09.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\extendedStdComponents\BaseAction;

/**
 * Class ViewElementPropertyValueAction
 * @package common\extendedStdComponents
 */
class ViewElementPropertyValueAction extends BaseAction
{
    /**
     * @param $id
     * @param $propertyId
     * @return \commonprj\components\core\entities\abstractPropertyValue\AbstractPropertyValue
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($id, $propertyId)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var Element $model */
        $model = $this->findModel($id);

        return $model->getPropertyValue($propertyId);
    }
}