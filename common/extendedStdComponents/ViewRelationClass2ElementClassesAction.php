<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 25.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\relationClass\RelationClass;
use commonprj\extendedStdComponents\BaseAction;
use yii\data\ActiveDataProvider;

/**
 * Class ViewRelationClassClassesAction
 * @package common\extendedStdComponents
 */
class ViewRelationClass2ElementClassesAction extends BaseAction
{
    /**
     * @param int $id
     * @param $isRoot
     * @return mixed|ActiveDataProvider
     */
    public function run($id, $isRoot)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var RelationClass $model */
        $model = $this->findModel($id);

        return $model->getElementClassesByIsRoot($isRoot);
    }
}