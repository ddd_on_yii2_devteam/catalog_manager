<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 29.06.2016
 */

namespace common\extendedStdComponents;

use commonprj\services\search\SearchRequestParser;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\BaseInflector;
use commonprj\components\core\helpers\ClassAndContextHelper;
use commonprj\extendedStdComponents\BaseCrudModel;
use commonprj\extendedStdComponents\BaseAction;
use commonprj\components\core\entities\element\Element;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexAction extends BaseAction
{
    /**
     * @return array
     * @throws \Exception
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        if (!(new $this->modelClass()) instanceof Element) {
            return $this->notElementRun();
        }

        $parser = new SearchRequestParser(Yii::$app->getRequest());
        $searchParamsObject = $parser->getSearchParamsObject();
        $searchParamsObject->setModelClass($this->modelClass);

        $models = Yii::$app->searchService->findEntities($searchParamsObject);

        return $models;
    }

    /**
     * @return array|BaseCrudModel[]
     */
    public function notElementRun()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $queryParams = Yii::$app->getRequest()->getQueryParams();

        if (!empty($queryParams['variantTypeId'])) {
            $this->setModelClassByVariantType($queryParams['variantTypeId']);
            unset($queryParams['variantTypeId']);
        }

        /** @var BaseCrudModel $modelClass */
        $modelClass = new $this->modelClass();
        $isRecord = strpos($this->modelClass, 'Record');
        $whereParams = [];

        if (!$isRecord) {
            $queryParams = $this->fromStringNullToReal($queryParams);
            $keysToErase = [];

            foreach ($queryParams as $queryParam => $value) {
                if (strpos($queryParam, '!') == (strlen($queryParam) - 1)) {
                    // Создаем временный параметр для валидации
                    $queryParam = preg_replace('/!/', '', $queryParam);
                    $queryParams[$queryParam] = $value;
                    $keysToErase[] = $queryParam;
                }
            }

            $modelClass->setAttributes($queryParams, false);

            foreach ($keysToErase as $keyToErase) {
                unset($queryParams[$keyToErase]);
            }

            if (!$modelClass->validate()) {
                return $modelClass;
            } else {
                $whereParams = $this->getWhereParams($modelClass, $queryParams);
            }
        }

        // пагинация для не Element-ов
        $page = (!empty($queryParams['page'])) ? $queryParams['page'] : 1;
        $perPage = (!empty($queryParams['per-page'])) ? $queryParams['per-page'] : 50;

        $condition['pagination']['offset'] = ($page - 1) * $perPage;
        $condition['pagination']['limit'] = $perPage;

        $condition['condition'] = $whereParams;

        if ($modelClass instanceof Element) {
            $classId = ClassAndContextHelper::getClassId($this->modelClass);
            $condition['byClassId'] = $classId;

            return $modelClass->find($condition);
        } else {
            if ($isRecord) {
                /** @var ActiveRecord $modelClass */
                $records = $modelClass::find()->all();
                $result = [];

                foreach ($records as $record) {
                    $result[$record['id']] = $record;
                }

                return $result;
            } else {
                $condition['byClassId'] = false;
                return $modelClass->find($condition);
            }
        }
    }

    /**
     * @param $variantTypeId
     */
    private function setModelClassByVariantType($variantTypeId)
    {
        if ($this->modelClass === 'commonprj\components\core\entities\variant\Variant') {
            switch ($variantTypeId) {
                case 1:
                    $this->modelClass = 'commonprj\components\core\entities\propertyVariant\PropertyVariant';
                    break;
                case 2:
                    $this->modelClass = 'commonprj\components\core\entities\relationVariant\RelationVariant';
                    break;
            }
        }
    }

    /**
     * @param $queryParams
     * @return array
     */
    private function fromStringNullToReal($queryParams): array
    {
        //Метод может воникнуть конфликт в случае если зарезервированное слово null будет указано в БД в каченстве строки (в любом регистре). Это маловероятно, но если случится, то убрать последний параметр в функции substr_compare()
        foreach ($queryParams as $key => $value) {
            if (substr_compare($value, 'null', 0, null, true) === 0) {
                $queryParams[$key] = null;
            }
        }

        return $queryParams;
    }

    /**
     * @param $modelClass
     * @param $queryParams
     * @return array
     */
    private function getWhereParams($modelClass, $queryParams)
    {
        $whereParams = [];

        foreach ($modelClass as $key => $value) {
            if (in_array($key, array_keys($queryParams)) && in_array($key, $modelClass::ALLOWED_WHERE_PARAMS)) {
                $whereParams[BaseInflector::underscore($key)] = $value;
            }
        }

        return $whereParams;
    }
}
