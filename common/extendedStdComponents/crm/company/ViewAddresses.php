<?php

namespace common\extendedStdComponents\crm\company;

use commonprj\components\crm\entities\abstractCompany\AbstractCompany;
use commonprj\components\crm\entities\address\Address;
use commonprj\extendedStdComponents\BaseAction;


/**
 * Class ViewHierarchyChildren
 * @package common\extendedStdComponents
 */
class ViewAddresses extends BaseAction
{

    var $condition = [];

    /**
     * @param int id
     * @param int|null $typeId
     * @return array Address[] (domain layer objects)
     * @throws \yii\web\NotFoundHttpException
     */
    public function run(int $id, int $typeId = null): array
    {

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var AbstractCompany $model */
        $model = $this->findModel($id);


        return $model->getAddresses($typeId);
    }

}
