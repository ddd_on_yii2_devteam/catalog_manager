<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/22/2018
 * Time: 5:19 PM
 */

namespace common\extendedStdComponents;

use commonprj\services\CurrencyService;
use Yii;
use yii\web\Request as OriginRequest;
use commonprj\services\LocalizationService;

class Request extends OriginRequest
{
    /**
     * Name of header with language
     * @var string
     */
    public $languageHeaderAttribute = 'Accept-Language';

    /**
     * Name of header with country
     * @var string
     */
    public $countryHeaderAttribute = 'Accept-Country';

    /**
     * Name of header with currency
     * @var string
     */
    public $currencyHeaderAttribute = 'Accept-Currency';

    public function init()
    {
        parent::init();

        /** @var LocalizationService $localizationService */
        $localizationService = Yii::$app->localization;
        $localizationService->setCurrentLang($this->headers->get($this->languageHeaderAttribute));

        /** @var CurrencyService $currencyService */
        $currencyService = Yii::$app->currency;
        $currencyService->setCurrentCurrency($this->headers->get($this->currencyHeaderAttribute));

    }
}