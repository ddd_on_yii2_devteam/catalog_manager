<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 11.07.2016
 */

namespace common\extendedStdComponents;

use commonprj\components\core\entities\element\Element;
use commonprj\extendedStdComponents\BaseAction;
use yii\data\ActiveDataProvider;

/**
 * Class viewModelsAction
 * @package common\extendedStdComponents
 */
class ViewElementModelsAction extends BaseAction
{
    /**
     * @param $id
     * @return \commonprj\components\core\models\CL_T_CLIENTRecord[]|mixed|ActiveDataProvider
     */
    public function run($id)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /** @var Element $model */
        $model = $this->findModel($id);

        return $model->getModels();
    }
}