<?php
$params = array_merge(require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'));

$routes = require(__DIR__ . '/../../api/config/routes.php');

return [
    'id'                  => 'app-api',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => [
        'log',
        'media',
    ],
    'controllerNamespace' => 'api\controllers',
    'components'          => [
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 0 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'      => [
            'class'   => \common\extendedStdComponents\Request::class,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response'     => [
            'class'   => \common\extendedStdComponents\Response::class,
            'format'  => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => array_merge([
                'service/property/element-classes' => 'service/property-element-classes',
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/property-group',
                        'core/property-unit',
                        'core/property-type',
                    ],
                    'tokens'        => [
                        '{id}' => '<id:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/properties' => 'viewPropertyGroupOrPropertyUnitProperties',
                    ],
                    'pluralize'     => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/element-class',
                    ],
                    'tokens'        => [
                        '{id}'         => '<id:\\d[\\d,]*>',
                        '{propertyId}' => '<propertyId:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/context'                  => 'viewElementClassContextAction',
                        'GET {id}/properties'               => 'viewElementClassPropertiesAction',
                        'GET {id}/relation-classes'         => 'viewElementClassRelationClassesAction',
                        'POST {id}/property/{propertyId}'   => 'createRelationElementClass2Property',
                        'DELETE {id}/property/{propertyId}' => 'deleteRelationElementClass2PropertyAction',
                    ],
                    'pluralize'     => false,
                ],
                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => [
                        'core/element',
                        'core/property-variant',
                    ],
                    'pluralize'  => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/element-type',
                    ],
                    'tokens'        => [
                        '{id}'            => '<id:\\d[\\d,]*>',
                        '{elementTypeId}' => '<elementTypeId:\\d+>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/element-category' => 'viewElementTypeCategory',
                        'GET {id}/element-class'    => 'viewElementTypeClass',
                        'GET {id}/variant'          => 'viewElementTypeVariant',
                    ],
                    'pluralize'     => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/relation-class',
                    ],
                    'tokens'        => [
                        '{id}'              => '<id:\\d[\\d,]*>',
                        '{relationClassId}' => '<relationClassId:\\d+>',
                        '{elementClassId}'  => '<elementClassId:\\d+>',
                    ],
                    'extraPatterns' => [
                        'POST {id}/element-class/{elementClassId}' => 'createRelationClass2ElementClass',

                        'GET {id}/relation-groups' => 'viewRelationClassGroups',
                        'GET {id}/element-classes' => 'viewRelationClass2ElementClasses',

                        'DELETE {id}/element-class/{elementClassId}' => 'deleteRelationClass2ElementClass',
                    ],
                    'pluralize'     => false,
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/relation',
                    ],
                    'tokens'        => [
                        '{id}' => '<id:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/child' => 'viewRelationChilds',
                    ],
                    'pluralize'     => false,
                ],


                // FUNCTIONALITY @core\relation-value
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'core/relation-value',
                    ],
                    'tokens'        => [
                        '{elementId}'        => '<elementId:\\d[\\d,]*>',
                        '{propertyId}'       => '<propertyId:\\d[\\d,]*>',
                        '{relatedElementId}' => '<relatedElementId:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET bind-child/{elementId}/{propertyId}/{relatedElementId}'               => 'BindChild', // bool @bklv
                        'GET is-parent/{elementId}/{propertyId}'                                   => 'ViewIsParent', // bool @bklv
                        'POST bind-children/{elementId}/{propertyId}'                              => 'BindChildren', // bool @bklv (список relatedElementId в виде POST['relatedChildren'] = int[])
                        'POST bind-children-associated/{elementId}/{propertyId}'                   => 'BindAssociatedEntities', // bool @bklv (список associatedElementId в виде POST['associatedEntities'] = int[])
                        'GET parent/{elementId}/{propertyId}'                                      => 'ViewParent', // \commonprj\components\core\entities\element\Element @bklv
                        'GET children/{elementId}/{propertyId}'                                    => 'ViewChildren', // \commonprj\components\core\entities\element\Element[] @bklv
                        'GET hierarchy-children/{elementId}/{propertyId}'                          => 'ViewHierarchyChildren', // \commonprj\components\core\entities\element\Element[] @bklv
                        'GET bind-to-parent/{elementId}/{propertyId}/{relatedElementId}'           => 'BindToParent', // bool @bklv
                        'GET bind-entity-associated/{elementId}/{propertyId}/{relatedElementId}'   => 'BindAssociatedEntity', // bool @bklv
                        'GET unbind-child/{elementId}/{propertyId}/{relatedElementId}'             => 'UnbindChild',  // bool @bklv
                        'GET unbind-all-children/{elementId}/{propertyId}'                         => 'UnbindAllChildren',  // bool @bklv
                        'GET unbind-entity-associated/{elementId}/{propertyId}/{relatedElementId}' => 'UnbindAssociatedEntity',  // bool @bklv
                        'GET unbind-all-associated/{elementId}/{propertyId}'                       => 'UnbindAllAssociatedEntities',  // bool @bklv
                    ],
                    'pluralize'     => false,
                ],
                [
                    //Получение переводов
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'product-material',
                        'price-category',
                        'material-collection',
                        'product-model',
                        'core/property',
                    ],
                    'tokens'        => [
                        '{id}' => '<id:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/translation'         => 'getTranslationData',
                    ],
                    'pluralize'     => false,

                ],
            ], $routes),
        ],

    ],

    'params'  => $params,
    'modules' => [
        'core'  => [
            'class' => api\modules\core\Module::class,
        ],
        'media' => [
            'class'           => commonprj\modules\media\Module::class,
            'mediaServiceUrl' => 'http://media-service.furniprice.local/image',
            'recognizeColors' => 10,
            'maxSize'         => 1024 * 1024 * 2,
            'extensions'      => 'gif, png, jpg',
        ],
    ],
];
