<?php

return
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => [
            'core/property-validation-rule',
        ],
        'tokens'     => [
            '{id}' => '<id:\\d+>',
        ],

        'extraPatterns' => [

        ],
        'pluralize'     => false,
    ];