<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'product-model',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/manufacturer'          => 'viewManufacturer',
            'GET {id}/sellers'               => 'viewSellers',
            'GET {id}/product-materials'     => 'viewProductMaterials',
            'GET {id}/materials'             => 'viewMaterials',
            'GET {id}/modular-components'    => 'viewModularComponents',
            'GET {id}/construction-elements' => 'viewConstructionElements',
            'GET {id}/products'              => 'viewProducts',
            'GET {id}/variants'              => 'viewVariations',

            'POST {id}/manufacturer/{elementId}'         => 'createRelationProductModel2Manufacturer',
            'POST {id}/product-material/{elementId}'     => 'createRelationProductModel2ProductMaterial',
            'POST {id}/material/{elementId}'             => 'createRelationProductModel2Material',
            'POST {id}/modular-component/{elementId}'    => 'createRelationProductModel2ModularComponent',
            'POST {id}/construction-element/{elementId}' => 'createRelationProductModel2ConstructionElement',
            'POST {id}/variant/{elementId}'              => 'createRelationProductModel2Variation',

            'DELETE {id}/manufacturer/{elementId}'         => 'deleteRelationProductModel2Manufacturer',
            'DELETE {id}/product-material/{elementId}'     => 'deleteRelationProductModel2ProductMaterial',
            'DELETE {id}/material/{elementId}'             => 'deleteRelationProductModel2Material',
            'DELETE {id}/modular-component/{elementId}'    => 'deleteRelationProductModel2ModularComponent',
            'DELETE {id}/construction-element/{elementId}' => 'deleteRelationProductModel2ConstructionElement',
            'DELETE {id}/variant/{elementId}'              => 'deleteRelationProductModel2Variation',
        ],
        'pluralize'     => false,
    ];