<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'material',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/material-groups'                  => 'viewMaterialGroups',
                'GET {id}/material-template'                => 'viewMaterialTemplate',
                'GET {id}/components'                       => 'viewComponents',
                'GET {id}/product-models'                   => 'viewProductModels',
                'GET {id}/composition/children'             => 'viewCompositionMaterials',
                'GET {id}/composition/parents'              => 'viewCompositionParents',
                'GET {id}/hierarchy/children'               => 'viewHierarchyChildren',
                'GET {id}/hierarchy/parent'                 => 'viewHierarchyParent',
                'GET {id}/hierarchy/descendants'            => 'viewHierarchyDescendants',
                'GET {id}/full-list-in-hierarchy'           => 'viewFullListInHierarchy',

                'POST {id}/material-group/{elementId}'      => 'createRelationMaterial2MaterialGroup',
                'POST {id}/material-template/{elementId}'   => 'createRelationMaterial2MaterialTemplate',
                'POST {id}/composition/{elementId}'         => 'createRelationMaterial2CompositionMaterial',
                'POST {id}/hierarchy/{elementId}'           => 'createRelationMaterial2HierarchyChild',

                'DELETE {id}/material-group/{elementId}'    => 'deleteRelationMaterial2MaterialGroup',
                'DELETE {id}/material-template/{elementId}' => 'deleteRelationMaterial2MaterialTemplate',
                'DELETE {id}/composition/{elementId}'       => 'deleteRelationMaterial2CompositionMaterial',
                'DELETE {id}/hierarchy/{elementId}'         => 'deleteRelationMaterial2HierarchyChild',
            ],
            'pluralize'     => false,
];
