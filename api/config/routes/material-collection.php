<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'material-collection',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/manufacturer'                => 'viewManufacturer',
                'POST {id}/manufacturer/{elementId}'   => 'createRelationMaterialCollection2Manufacturer',
                'DELETE {id}/manufacturer/{elementId}' => 'deleteRelationMaterialCollection2Manufacturer',
            ],
            'pluralize'     => false,
];
