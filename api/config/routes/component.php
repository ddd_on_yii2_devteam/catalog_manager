<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'component',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/manufacturer'                    => 'viewManufacturer',
                'GET {id}/materials'                       => 'viewMaterials',
                'GET {id}/product-materials'               => 'viewProductMaterials',

                'POST {id}/manufacturer/{elementId}'       => 'createRelationComponent2Manufacturer',
                'POST {id}/material/{elementId}'           => 'createRelationComponent2Material',
                'POST {id}/product-material/{elementId}'   => 'createRelationComponent2ProductMaterial',

                'DELETE {id}/manufacturer/{elementId}'     => 'deleteRelationComponent2Manufacturer',
                'DELETE {id}/material/{elementId}'         => 'deleteRelationComponent2Material',
                'DELETE {id}/product-material/{elementId}' => 'deleteRelationComponent2ProductMaterial',
            ],
            'pluralize'     => false,
];
