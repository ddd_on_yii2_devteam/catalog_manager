<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'material-template',
            ],
            'tokens'        => [
                '{id}' => '<id:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/materials' => 'viewMaterials',
            ],
            'pluralize'     => false,
];
