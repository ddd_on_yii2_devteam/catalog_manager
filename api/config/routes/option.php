<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'option',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/variants' => 'viewVariants',
        ],
        'pluralize'     => false,
    ];