<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'core/element-category',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/children'         => 'viewChildren',
            'GET {id}/parent'           => 'viewParent',
            'GET {id}/element-class'    => 'viewElementClass',
            'GET {id}/property-variant' => 'viewPropertyVariant',


            'POST {id}/manufacturer/{elementId}'   => 'createRelationPriceCategory2Manufacturer',
            'DELETE {id}/manufacturer/{elementId}' => 'deleteRelationPriceCategory2Manufacturer',
        ],
        'pluralize'     => false,
    ];