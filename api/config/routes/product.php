<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'product',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/product-model'                   => 'viewProductModel',
                'GET {id}/product-materials'               => 'viewProductMaterials',
                'GET {id}/variations'                      => 'viewVariations',

                'POST {id}/product-model/{elementId}'      => 'createRelationProduct2ProductModel',
                'POST {id}/product-material/{elementId}'   => 'createRelationProduct2ProductMaterial',
                'POST {id}/variation/{elementId}'          => 'createRelationProduct2Variation',

                'DELETE {id}/product-model/{elementId}'    => 'deleteRelationProduct2ProductModel',
                'DELETE {id}/product-material/{elementId}' => 'deleteRelationProduct2ProductMaterial',
                'DELETE {id}/variation/{elementId}'        => 'deleteRelationProduct2Variation',
            ],
            'pluralize'     => false,
];
