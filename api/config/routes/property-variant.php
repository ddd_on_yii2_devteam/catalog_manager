<?php

return
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => [
            'core/property-variant',
        ],
        'tokens'     => [
            '{id}' => '<id:\\d+>',
        ],

        'extraPatterns' => [
            'GET {id}/property-validation-rule' => 'viewPropertyValidationRule',
            'GET {id}/element-categories'       => 'viewElementCategories',
            'GET {id}/property-value'           => 'viewPropertyValue',
            'GET {id}/property'                 => 'viewProperty',
        ],
        'pluralize'     => false,
    ];