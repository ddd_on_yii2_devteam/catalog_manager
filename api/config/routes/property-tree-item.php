<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'core/property-tree-item',
        ],
        'tokens' => [
            '{id}' => '<id:\\d[\\d,]*>',
        ],
        'extraPatterns' => [

        ],
        'pluralize'     => false,
    ];