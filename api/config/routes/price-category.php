<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'price-category',
            ],
            'tokens'        => [
                '{id}'        => '<id:\\d+>',
                '{elementId}' => '<elementId:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/manufacturer'                => 'viewManufacturer',
                'POST {id}/manufacturer/{elementId}'   => 'createRelationPriceCategory2Manufacturer',
                'DELETE {id}/manufacturer/{elementId}' => 'deleteRelationPriceCategory2Manufacturer',
            ],
            'pluralize'     => false,
];