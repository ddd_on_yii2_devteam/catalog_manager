<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'core/menu',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/element-class'    => 'viewElementClass',
            'GET {id}/element-categories' => 'viewElementCategories',
        ],
        'pluralize'     => false,
    ];