<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'core/property',
        ],
        'tokens'        => [
            '{id}' => '<id:\\d[\\d,]*>',
        ],
        'extraPatterns' => [
            'GET {id}/type'              => 'viewPropertyType',
            'GET {id}/values'            => 'viewPropertyValues',
            'GET {id}/element-classes'   => 'viewPropertyElementClasses',
            'GET {id}/property-variants' => 'viewPropertyVariants',
            'GET {id}/property-variant'  => 'viewPropertyVariant',
            'GET {id}/list-items'        => 'viewPropertyListItems',
            'GET {id}/template'          => 'viewTemplate',

            'POST {id}/list-items' => 'createPropertyListItems',
        ],
        'pluralize'     => false,
    ];
