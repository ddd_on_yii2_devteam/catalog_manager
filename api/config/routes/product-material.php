<?php

return
    [
        'class'         => 'yii\rest\UrlRule',
        'controller'    => [
            'product-material',
        ],
        'tokens'        => [
            '{id}'        => '<id:\\d+>',
            '{elementId}' => '<elementId:\\d+>',
        ],
        'extraPatterns' => [
            'GET {id}/manufacturer'        => 'viewManufacturer',
            'GET {id}/material'            => 'viewMaterial',
            'GET {id}/material-collection' => 'viewMaterialCollection',
            'GET {id}/material-group'      => 'viewMaterialGroup',
            'GET {id}/price-category'      => 'viewPriceCategory',
            'GET {id}/component'           => 'viewComponent',

            'POST {id}/manufacturer/{elementId}'        => 'createRelationProductMaterial2Manufacturer',
            'POST {id}/material/{elementId}'            => 'createRelationProductMaterial2Material',
            'POST {id}/material-collection/{elementId}' => 'createRelationProductMaterial2MaterialCollection',
            'POST {id}/price-category/{elementId}'      => 'createRelationProductMaterial2PriceCategory',
            'POST {id}/material-group/{elementId}'      => 'createRelationProductMaterial2MaterialGroup',

            'DELETE {id}/manufacturer/{elementId}'        => 'deleteRelationProductMaterial2Manufacturer',
            'DELETE {id}/material/{elementId}'            => 'deleteRelationProductMaterial2Material',
            'DELETE {id}/material-collection/{elementId}' => 'deleteRelationProductMaterial2MaterialCollection',
            'DELETE {id}/price-category/{elementId}'      => 'deleteRelationProductMaterial2PriceCategory',
            'DELETE {id}/material-group/{elementId}'      => 'deleteRelationProductMaterial2MaterialGroup',
        ],
        'pluralize'     => false,
    ];
