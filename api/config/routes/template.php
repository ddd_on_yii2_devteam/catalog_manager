<?php

return
    [
        'class'      => 'yii\rest\UrlRule',
        'controller' => [
            'core/template',
        ],
        'tokens'     => [
            '{id}' => '<id:\\d+>',
        ],

        'extraPatterns' => [

        ],
        'pluralize'     => false,
    ];