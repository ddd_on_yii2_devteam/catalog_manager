<?php

return
        [
            'class'         => 'yii\rest\UrlRule',
            'controller'    => [
                'material-group',
            ],
            'tokens'        => [
                '{id}' => '<id:\\d+>',
            ],
            'extraPatterns' => [
                'GET {id}/materials' => 'viewMaterials',
            ],
            'pluralize'     => false,
];
