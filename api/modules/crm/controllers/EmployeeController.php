<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\employee\Employee;

/**
 * Class EmployeeController
 * @package api\modules\crm\controllers
 */
class EmployeeController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Employee::class;

}