<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\customer\Customer;

/**
 * Class CustomerController
 * @package api\modules\crm\controllers
 */
class CustomerController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Customer::class;

}