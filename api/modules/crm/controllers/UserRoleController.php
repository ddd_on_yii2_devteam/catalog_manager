<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\userRole\UserRole;

/**
 * Class UserRoleController
 * @package api\modules\crm\controllers
 */
class UserRoleController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = UserRole::class;

}