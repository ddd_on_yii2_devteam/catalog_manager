<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\permission\Permission;

/**
 * Class PermissionController
 * @package api\modules\crm\controllers
 */
class PermissionController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Permission::class;

}