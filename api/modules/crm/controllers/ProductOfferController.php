<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\productOffer\ProductOffer;

/**
 * Class ProductOfferController
 * @package api\modules\crm\controllers
 */
class ProductOfferController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = ProductOffer::class;

}