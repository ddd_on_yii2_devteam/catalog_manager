<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\companyRole\CompanyRole;

/**
 * Class CompanyRoleController
 * @package api\modules\crm\controllers
 */
class CompanyRoleController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = CompanyRole::class;

}