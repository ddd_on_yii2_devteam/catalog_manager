<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\productRequest\ProductRequest;

/**
 * Class ProductRequestController
 * @package api\modules\crm\controllers
 */
class ProductRequestController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = ProductRequest::class;

}