<?php

namespace api\modules\crm\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

/**
 * Class PermissionGroupController
 * @package api\modules\crm\controllers
 */
class PermissionGroupController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = PermissionGroup::class;

}