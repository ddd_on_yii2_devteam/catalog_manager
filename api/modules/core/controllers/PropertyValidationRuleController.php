<?php

namespace api\modules\core\controllers;

use commonprj\components\core\entities\propertyValidationRule\PropertyValidationRule;
use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class PropertyValidationRuleController
 * @package api\modules\core\controllers
 */
class PropertyValidationRuleController extends BaseActiveController
{
    public $modelClass = PropertyValidationRule::class;

}