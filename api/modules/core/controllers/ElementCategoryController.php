<?php

namespace api\modules\core\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\core\elementCategory as ElementCategoryAction;
use commonprj\components\core\entities\elementCategory\ElementCategory;


/**
 * Class ElementCategoryController
 * @package api\controllers
 */
class ElementCategoryController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = ElementCategory::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'index' => [
                'class'       => ElementCategoryAction\IndexAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewChildren' => [
                'class'       => ElementCategoryAction\ViewChildrenAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewParent' => [
                'class'       => ElementCategoryAction\ViewParentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClass' => [
                'class'       => ElementCategoryAction\ViewElementClassAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyVariant' => [
                'class'       => ElementCategoryAction\ViewPropertyVariantAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}