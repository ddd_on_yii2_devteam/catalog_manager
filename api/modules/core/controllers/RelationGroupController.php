<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 08.07.2016
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class RelationGroupController
 * @package api\modules\core\controllers
 */
class RelationGroupController extends BaseActiveController
{
    public $modelClass = 'commonprj\components\core\entities\relationGroup\RelationGroup';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $defaultActions = parent::actions();
        $currentActions = [
            'viewRelationGroupRelationClass' => [
                'class'       => 'core\extendedStdComponents\ViewRelationGroupRelationClassAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
        $resultActions = array_merge($defaultActions, $currentActions);

        return $resultActions;
    }
}