<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 13.09.2016
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class SchemaElementController
 * @package api\modules\core\controllers
 */
class SchemaElementController extends BaseActiveController
{
    public $modelClass = 'commonprj\components\core\entities\element\Element';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'viewElementsBySchemaId' => [
                'class' => 'core\extendedStdComponents\ViewElementsBySchemaIdAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementVariantsBySchemaId' => [
                'class' => 'core\extendedStdComponents\ViewElementVariantsBySchemaIdAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createElementsBySchemaId' => [
                'class' => 'core\extendedStdComponents\CreateElementsBySchemaIdAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteElementsBySchemaId' => [
                'class' => 'core\extendedStdComponents\DeleteElementsBySchemaIdAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteVariantsBySchemaId' => [
                'class' => 'core\extendedStdComponents\DeleteVariantsBySchemaIdAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}