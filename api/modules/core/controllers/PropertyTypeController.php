<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 10.08.2016
 */

namespace api\modules\core\controllers;

use common\extendedStdComponents\core\propertyGroup\ViewPropertyGroupOrPropertyUnitPropertiesAction;
use commonprj\components\core\entities\propertyType\PropertyType;
use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class PropertyTypeController
 * @package api\modules\core\controllers
 */
class PropertyTypeController extends BaseActiveController
{
    public $modelClass = PropertyType::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index'                            => [
                'class'       => 'common\extendedStdComponents\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view'                             => [
                'class'       => 'common\extendedStdComponents\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyGroupOrPropertyUnitProperties'  => [
                'class'       => ViewPropertyGroupOrPropertyUnitPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}