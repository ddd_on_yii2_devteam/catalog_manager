<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 17:26
 */

namespace api\modules\core\controllers;

use common\extendedStdComponents\core\propertyGroup\ViewPropertyGroupOrPropertyUnitPropertiesAction;
use commonprj\components\core\entities\propertyGroup\PropertyGroup;
use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class PropertyGroupController
 * @package api\modules\core\controllers
 */
class PropertyGroupController extends BaseActiveController
{
    public $modelClass = PropertyGroup::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $defaultActions = parent::actions();
        $currentActions = [
            'viewPropertyGroupOrPropertyUnitProperties'  => [
                'class'       => ViewPropertyGroupOrPropertyUnitPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
        $resultActions = array_merge($defaultActions, $currentActions);

        return $resultActions;
    }
}