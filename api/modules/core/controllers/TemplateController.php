<?php

namespace api\modules\core\controllers;

use commonprj\components\core\entities\template\Template;
use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class TemplateController
 * @package api\modules\core\controllers
 */
class TemplateController extends BaseActiveController
{
    public $modelClass = Template::class;

}