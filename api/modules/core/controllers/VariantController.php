<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 20.09.2016
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class VariantController
 * @package api\modules\core\controllers
 */
class VariantController extends BaseActiveController
{
    public $modelClass = 'commonprj\components\core\entities\variant\Variant';

    public function actions()
    {
        $parentAction = parent::actions();
        $currentAction = [
            'viewVariantElementType' => [
                'class' => 'core\extendedStdComponents\ViewVariantElementTypeAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariantProperty' => [
                'class' => 'core\extendedStdComponents\ViewVariantPropertyAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariantPropertyValue' => [
                'class' => 'core\extendedStdComponents\ViewVariantPropertyValueAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariantRelatedElement' => [
                'class' => 'core\extendedStdComponents\ViewVariantRelatedElementAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariantRelationClass' => [
                'class' => 'core\extendedStdComponents\ViewVariantRelationClassAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariantSchemaElement' => [
                'class' => 'core\extendedStdComponents\ViewVariantSchemaElementAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];

        return array_merge($parentAction, $currentAction);
    }
}