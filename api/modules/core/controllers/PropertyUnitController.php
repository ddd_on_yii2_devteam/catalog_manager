<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 26.08.2016
 */

namespace api\modules\core\controllers;

use common\extendedStdComponents\core\propertyGroup\ViewPropertyGroupOrPropertyUnitPropertiesAction;
use commonprj\components\core\entities\propertyUnit\PropertyUnit;
use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class PropertyUnitController
 * @package api\modules\core\controllers
 */
class PropertyUnitController extends BaseActiveController
{
    public $modelClass = PropertyUnit::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $parentActions = parent::actions();
        $currentActions = [
            'viewPropertyGroupOrPropertyUnitProperties' => [
                'class' => ViewPropertyGroupOrPropertyUnitPropertiesAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];

        return array_merge($parentActions, $currentActions);
    }
}