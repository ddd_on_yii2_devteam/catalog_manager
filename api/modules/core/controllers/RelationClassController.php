<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 15.07.2016
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class RelationClassController
 * @package api\modules\core\controllers
 */
class RelationClassController extends BaseActiveController
{
    public $modelClass = 'commonprj\components\core\entities\relationClass\RelationClass';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $defaultActions = parent::actions();
        $currentActions = [
            'viewRelationClassGroups'          => [
                'class'       => 'core\extendedStdComponents\ViewRelationClassGroupsAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewRelationClass2ElementClasses' => [
                'class'       => 'core\extendedStdComponents\ViewRelationClass2ElementClassesAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationClass2ElementClass' => [
                'class'       => 'core\extendedStdComponents\DeleteRelationClass2ElementClassAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationClass2ElementClass' => [
                'class'       => 'core\extendedStdComponents\CreateRelationClass2ElementClassAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
        $resultActions = array_merge($defaultActions, $currentActions);

        return $resultActions;
    }
}