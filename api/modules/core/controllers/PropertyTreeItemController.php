<?php

namespace api\modules\core\controllers;

use common\extendedStdComponents\core\propertyTreeItem\IndexAction;
use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\core\entities\PropertyTreeItem\PropertyTreeItem;

/**
 * Class AddressController
 * @package api\controllers
 */
class PropertyTreeItemController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = PropertyTreeItem::class;

    protected function addActions(): array
    {
        return [
            'index'   => [
                'class'       => IndexAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }


}