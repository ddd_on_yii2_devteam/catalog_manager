<?php

namespace api\modules\core\controllers;

use common\extendedStdComponents\core\propertyVariant as PropertyVariantActions;
use commonprj\components\core\entities\propertyVariant\PropertyVariant;
use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class PropertyVariantController
 * @package api\modules\core\controllers
 */
class PropertyVariantController extends BaseActiveController
{
    public $modelClass = PropertyVariant::class;

    public function actions()
    {
        $defaultActions = parent::actions();
        $currentActions = [
            'viewElementCategories' => [
                'class'       => PropertyVariantActions\ViewElementCategoriesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProperty'         => [
                'class'       => PropertyVariantActions\ViewPropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyValue'        => [
                'class'       => PropertyVariantActions\ViewPropertyValueAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyValidationRule'           => [
                'class'       => PropertyVariantActions\ViewPropertyValidationRuleAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
        $resultActions = array_merge($defaultActions, $currentActions);

        return $resultActions;
    }

}