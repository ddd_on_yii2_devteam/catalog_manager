<?php

namespace api\modules\core\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\core\entities\propertyValueColor\PropertyValueColor;

/**
 * Class PropertyValueColorController
 * @package api\modules\core\controllers
 */
class PropertyValueColorController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = PropertyValueColor::class;

}