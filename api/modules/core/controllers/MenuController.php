<?php

namespace api\modules\core\controllers;

use commonprj\components\core\entities\menu\Menu;
use commonprj\extendedStdComponents\BaseActiveController;
use common\extendedStdComponents\core\menu as MenuActions;

/**
 * Class MenuController
 * @package api\modules\core\controllers
 */
class MenuController extends BaseActiveController
{
    public $modelClass = Menu::class;

    public function actions()
    {
        $parentAction = parent::actions();
        $currentAction = [
            'viewElementClass' => [
                'class' => MenuActions\viewElementClassAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementCategories' => [
                'class' => MenuActions\ViewElementCategoriesAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],

        ];

        return array_merge($parentAction, $currentAction);
    }
}