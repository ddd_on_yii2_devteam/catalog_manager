<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 29.07.2016
 */

namespace api\modules\core\controllers;

use common\extendedStdComponents\core\elementClass\CreateRelationElementClass2PropertyAction;
use common\extendedStdComponents\core\elementClass\DeleteRelationElementClass2PropertyAction;
use common\extendedStdComponents\core\elementClass\ViewElementClassContextAction;
use common\extendedStdComponents\core\elementClass\ViewElementClassPropertiesAction;
use common\extendedStdComponents\core\elementClass\ViewElementClassRelationClassesAction;
use commonprj\components\core\entities\elementClass\ElementClass;
use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class ElementClassController
 * @package api\modules\core\controllers
 */
class ElementClassController extends BaseActiveController
{
    public $modelClass = ElementClass::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
//        $defaultActions = parent::actions();

        $currentActions = [
            'index'                            => [
                'class'       => 'common\extendedStdComponents\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view'                             => [
                'class'       => 'common\extendedStdComponents\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationElementClass2Property'                             => [
                'class'       => CreateRelationElementClass2PropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationElementClass2PropertyAction'                             => [
                'class'       => DeleteRelationElementClass2PropertyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClassContextAction'                             => [
                'class'       => ViewElementClassContextAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClassPropertiesAction'                             => [
                'class'       => ViewElementClassPropertiesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementClassRelationClassesAction'                             => [
                'class'       => ViewElementClassRelationClassesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
//            'viewElementClass2RelationClasses' => [
//                'class'       => 'core\extendedStdComponents\ViewElementClass2RelationClassesAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
//            'viewElementClassProperties'       => [
//                'class'       => 'core\extendedStdComponents\ViewElementClassPropertiesAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
//            'viewElementClassByName'           => [
//                'class'       => 'core\extendedStdComponents\ViewElementClassByNameAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
//            'viewElementClassContext'          => [
//                'class'       => 'core\extendedStdComponents\ViewElementClassContextAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
//            'update'                           => [
//                'class'       => 'core\extendedStdComponents\UpdateAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//                'scenario'    => $this->updateScenario,
//            ],
//            'delete'                           => [
//                'class'       => 'core\extendedStdComponents\DeleteAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
//            'deleteElementClass2RelationClass' => [
//                'class'       => 'core\extendedStdComponents\DeleteElementClass2RelationClassAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
//            'createElementClass2RelationClass' => [
//                'class'       => 'core\extendedStdComponents\CreateElementClass2RelationClassAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
//            'createElementClass2Property'      => [
//                'class'       => 'core\extendedStdComponents\CreateElementClass2PropertyAction',
//                'modelClass'  => $this->modelClass,
//                'checkAccess' => [$this, 'checkAccess'],
//            ],
        ];

//        $resultActions = array_merge($defaultActions, $currentActions);

        return $currentActions;
    }
}