<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 19.07.2016
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;
use commonprj\components\core\entities\element\Element;

/**
 * Class ElementController
 * @package api\modules\core\controllers
 */
class ElementController extends BaseActiveController
{
    public $modelClass = Element::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class'       => 'core\extendedStdComponents\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view'  => [
                'class'       => 'core\extendedStdComponents\ViewAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}