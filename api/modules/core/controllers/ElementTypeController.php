<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 21.07.2016
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;

/**
 * Class ElementTypeController
 * @package api\modules\core\controllers
 */
class ElementTypeController extends BaseActiveController
{
    public $modelClass = 'commonprj\components\core\entities\elementType\ElementType';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $defaultActions = parent::actions();
        $currentActions = [
            'viewElementTypeCategory' => [
                'class'       => 'core\extendedStdComponents\ViewElementTypeCategoryAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementTypeClass'    => [
                'class'       => 'core\extendedStdComponents\ViewElementTypeClassAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewElementTypeVariant'    => [
                'class'       => 'core\extendedStdComponents\ViewElementTypeVariantAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
        $resultActions = array_merge($defaultActions, $currentActions);

        return $resultActions;
    }
}