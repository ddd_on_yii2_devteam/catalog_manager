<?php
/**
 * @package            FurniPrice
 * @subpackage        Common
 * @category        DBRepository
 * @created            14.12.2017
 * @author            Vasiliy Konakov
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;
use commonprj\components\core\entities\relationValue\RelationValue;

/**
 * Class RelationValueController
 * @package api\modules\core\controllers
 */
class RelationValueController extends BaseActiveController
{
    public $modelClass = RelationValue::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {

        return [
            'ViewChildren' => [
                'class' => 'core\extendedStdComponents\ViewChildren',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'ViewHierarchyChildren' => [
                'class' => 'core\extendedStdComponents\ViewHierarchyChildren',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'ViewParent' => [
                'class' => 'core\extendedStdComponents\ViewParent',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'ViewIsParent' => [
                'class' => 'core\extendedStdComponents\ViewIsParent',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'BindAssociatedEntity' => [
                'class' => 'core\extendedStdComponents\BindAssociatedEntity',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'BindAssociatedEntities' => [
                'class' => 'core\extendedStdComponents\BindAssociatedEntities',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'BindChild' => [
                'class' => 'core\extendedStdComponents\BindChild',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'UnbindChild' => [
                'class' => 'core\extendedStdComponents\UnbindChild',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'UnbindAssociatedEntity' => [
                'class' => 'core\extendedStdComponents\UnbindAssociatedEntity',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'UnbindAllChildren' => [
                'class' => 'core\extendedStdComponents\UnbindAllChildren',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'UnbindAllAssociatedEntities' => [
                'class' => 'core\extendedStdComponents\UnbindAllAssociatedEntities',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'BindChildren' => [
                'class' => 'core\extendedStdComponents\BindChildren',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'BindToParent' => [
                'class' => 'core\extendedStdComponents\BindToParent',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'DeleteRelationValue' => [
                'class' => 'core\extendedStdComponents\DeleteRelationValue',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }


}
