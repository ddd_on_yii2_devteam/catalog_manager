<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 18.07.2016
 */

namespace api\modules\core\controllers;

use common\extendedStdComponents\core\property as PropertyActions;
use commonprj\extendedStdComponents\BaseActiveController;
use commonprj\components\core\entities\property\Property;

/**
 * Class PropertyController
 * @package api\modules\core\controllers
 */
class PropertyController extends BaseActiveController
{
    public $modelClass = Property::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $defaultActions = parent::actions();
        $currentActions = [
            'viewPropertyValues'         => [
                'class'       => PropertyActions\ViewPropertyValuesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyVariants'        => [
                'class'       => PropertyActions\ViewPropertyVariantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyVariant'        => [
                'class'       => PropertyActions\ViewPropertyVariantAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyType'           => [
                'class'       => PropertyActions\ViewPropertyTypeAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyElementClasses' => [
                'class'       => PropertyActions\ViewPropertyElementClassesAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPropertyListItems'      => [
                'class'       => PropertyActions\ViewPropertyListItemsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createPropertyListItems'    => [
                'class'       => PropertyActions\CreatePropertyListItemsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewTemplate'      => [
                'class'       => PropertyActions\ViewTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
        $resultActions = array_merge($defaultActions, $currentActions);

        return $resultActions;
    }
}