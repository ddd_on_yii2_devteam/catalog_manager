<?php
/**
 * @package            FurniPrice
 * @subpackage        Common
 * @category        DBRepository
 * @created            14.12.2017
 * @author            Vasiliy Konakov
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;
use commonprj\components\core\entities\hierarchyRelationValue\HierarchyRelationValue;

/**
 * Class HierarchyRelationValueController
 * @package api\modules\core\controllers
 */
class HierarchyRelationValueController extends BaseActiveController
{
    public $modelClass = HierarchyRelationValue::class;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [

            'ViewHierarchyChildren' => [
                'class' => 'core\extendedStdComponents\ViewHierarchyChildren',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }


}
