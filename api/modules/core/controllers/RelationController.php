<?php
/**
 * Created by daSilva.Rodrigues
 * Date: 18.10.2016
 */

namespace api\modules\core\controllers;

use commonprj\extendedStdComponents\BaseActiveController;

class RelationController extends BaseActiveController
{
    public $modelClass = 'commonprj\components\core\entities\relationValue\RelationValue';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return  [
            'index' => [
                'class' => 'common\extendedStdComponents\IndexAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'view' => [
                'class' => 'common\extendedStdComponents\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewRelationChilds' => [
                'class' => 'common\extendedStdComponents\ViewRelationChildsAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }
}