<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\product as ProductAction;
use commonprj\components\catalog\entities\product\Product;


/**
 * Class ProductController
 * @package api\controllers
 */
class ProductController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Product::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'viewProductMaterials' => [
                'class'       => ProductAction\ViewProductMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProduct2ProductMaterial' => [
                'class'       => ProductAction\CreateRelationProduct2ProductMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProduct2ProductMaterial' => [
                'class'       => ProductAction\DeleteRelationProduct2ProductMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductModel' => [
                'class'       => ProductAction\ViewProductModelAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProduct2ProductModel' => [
                'class'       => ProductAction\CreateRelationProduct2ProductModelAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProduct2ProductModel' => [
                'class'       => ProductAction\DeleteRelationProduct2ProductModelAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariations' => [
                'class'       => ProductAction\ViewVariationsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProduct2Variation' => [
                'class'       => ProductAction\CreateRelationProduct2VariationAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProduct2Variation' => [
                'class'       => ProductAction\DeleteRelationProduct2VariationAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}