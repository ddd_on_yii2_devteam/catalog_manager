<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\priceCategory as PriceCategoryAction;
use commonprj\components\catalog\entities\priceCategory\PriceCategory;


/**
 * Class PriceCategoryController
 * @package api\controllers
 */
class PriceCategoryController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = PriceCategory::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'createRelationPriceCategory2Manufacturer' => [
                'class'       => PriceCategoryAction\CreateRelationPriceCategory2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationPriceCategory2Manufacturer' => [
                'class'       => PriceCategoryAction\DeleteRelationPriceCategory2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturer' => [
                'class'       => PriceCategoryAction\ViewManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}