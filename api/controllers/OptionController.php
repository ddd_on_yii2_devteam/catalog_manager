<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\option as OptionAction;
use commonprj\components\catalog\entities\option\Option;

/**
 * Class OptionController
 * @package api\controllers
 */
class OptionController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Option::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'viewVariants' => [
                'class'       => OptionAction\ViewVariantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}