<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.02.2018
 * Time: 18:24
 */

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\core\entities\property\Property;

class PropertyController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Property::class;
}