<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\materialCollection as MaterialCollectionAction;
use commonprj\components\catalog\entities\materialCollection\MaterialCollection;


/**
 * Class MaterialCollectionController
 * @package api\controllers
 */
class MaterialCollectionController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = MaterialCollection::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'createRelationMaterialCollection2Manufacturer' => [
                'class'       => MaterialCollectionAction\CreateRelationMaterialCollection2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterialCollection2Manufacturer' => [
                'class'       => MaterialCollectionAction\DeleteRelationMaterialCollection2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturer' => [
                'class'       => MaterialCollectionAction\ViewManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}