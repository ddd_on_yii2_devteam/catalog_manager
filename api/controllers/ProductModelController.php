<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\productModel as ProductModelAction;
use commonprj\components\catalog\entities\productModel\ProductModel;


/**
 * Class ProductModelController
 * @package api\controllers
 */
class ProductModelController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = ProductModel::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'createRelationProductModel2Manufacturer' => [
                'class'       => ProductModelAction\CreateRelationProductModel2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductModel2Manufacturer' => [
                'class'       => ProductModelAction\DeleteRelationProductModel2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturer' => [
                'class'       => ProductModelAction\ViewManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewSellers' => [
                'class'       => ProductModelAction\ViewSellersAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductModel2ProductMaterial' => [
                'class'       => ProductModelAction\CreateRelationProductModel2ProductMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductModel2ProductMaterial' => [
                'class'       => ProductModelAction\DeleteRelationProductModel2ProductMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductMaterials' => [
                'class'       => ProductModelAction\ViewProductMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductModel2Material' => [
                'class'       => ProductModelAction\CreateRelationProductModel2MaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductModel2Material' => [
                'class'       => ProductModelAction\DeleteRelationProductModel2MaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterials' => [
                'class'       => ProductModelAction\ViewMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductModel2ConstructionElement' => [
                'class'       => ProductModelAction\CreateRelationProductModel2ConstructionElementAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductModel2ConstructionElement' => [
                'class'       => ProductModelAction\DeleteRelationProductModel2ConstructionElementAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewConstructionElements' => [
                'class'       => ProductModelAction\ViewConstructionElementsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductModel2ModularComponent' => [
                'class'       => ProductModelAction\CreateRelationProductModel2ModularComponentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductModel2ModularComponent' => [
                'class'       => ProductModelAction\DeleteRelationProductModel2ModularComponentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewModularComponents' => [
                'class'       => ProductModelAction\ViewModularComponentsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProducts' => [
                'class'       => ProductModelAction\ViewProductsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductModel2Variation' => [
                'class'       => ProductModelAction\CreateRelationProductModel2VariantAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductModel2Variation' => [
                'class'       => ProductModelAction\DeleteRelationProductModel2VariantAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewVariations' => [
                'class'       => ProductModelAction\ViewVariantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],

            'index'   => [
                'class'       => 'common\extendedStdComponents\catalog\productModel\IndexAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}