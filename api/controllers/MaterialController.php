<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\material as MaterialAction;
use commonprj\components\catalog\entities\material\Material;


/**
 * Class MaterialController
 * @package api\controllers
 */
class MaterialController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Material::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'createRelationMaterial2MaterialGroup' => [
                'class'       => MaterialAction\CreateRelationMaterial2MaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2MaterialGroup' => [
                'class'       => MaterialAction\DeleteRelationMaterial2MaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterialGroups' => [
                'class'       => MaterialAction\ViewMaterialGroupsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterial2MaterialTemplate' => [
                'class'       => MaterialAction\CreateRelationMaterial2MaterialTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2MaterialTemplate' => [
                'class'       => MaterialAction\DeleteRelationMaterial2MaterialTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterialTemplate' => [
                'class'       => MaterialAction\ViewMaterialTemplateAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewComponents' => [
                'class'       => MaterialAction\ViewComponentsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductModels' => [
                'class'       => MaterialAction\ViewProductModelsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterial2CompositionChild' => [
                'class'       => MaterialAction\CreateRelationMaterial2CompositionChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2CompositionChild' => [
                'class'       => MaterialAction\DeleteRelationMaterial2CompositionChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompositionChildren' => [
                'class'       => MaterialAction\ViewCompositionChildrenAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewCompositionParents' => [
                'class'       => MaterialAction\ViewCompositionParentsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationMaterial2HierarchyChild' => [
                'class'       => MaterialAction\CreateRelationMaterial2HierarchyChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationMaterial2HierarchyChild' => [
                'class'       => MaterialAction\DeleteRelationMaterial2HierarchyChildAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewHierarchyChildren' => [
                'class'       => MaterialAction\ViewHierarchyChildrenAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewHierarchyParent' => [
                'class'       => MaterialAction\ViewHierarchyParentAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewHierarchyDescendants' => [
                'class'       => MaterialAction\ViewHierarchyDescendantsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewFullListInHierarchy' => [
                'class'       => MaterialAction\ViewFullListInHierarchyAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'index'   => [
                'class'       => MaterialAction\IndexAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}