<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\materialGroup as MaterialGroupAction;
use commonprj\components\catalog\entities\materialGroup\MaterialGroup;


/**
 * Class MaterialGroupController
 * @package api\controllers
 */
class MaterialGroupController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = MaterialGroup::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'viewMaterials' => [
                'class'       => MaterialGroupAction\ViewMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}