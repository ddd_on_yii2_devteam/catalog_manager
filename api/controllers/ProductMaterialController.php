<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\productMaterial as ProductMaterialAction;
use commonprj\components\catalog\entities\productMaterial\ProductMaterial;


/**
 * Class ProductMaterialController
 * @package api\controllers
 */
class ProductMaterialController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = ProductMaterial::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'createRelationProductMaterial2Manufacturer' => [
                'class'       => ProductMaterialAction\CreateRelationProductMaterial2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductMaterial2Manufacturer' => [
                'class'       => ProductMaterialAction\DeleteRelationProductMaterial2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturer' => [
                'class'       => ProductMaterialAction\ViewManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductMaterial2Material' => [
                'class'       => ProductMaterialAction\CreateRelationProductMaterial2MaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductMaterial2Material' => [
                'class'       => ProductMaterialAction\DeleteRelationProductMaterial2MaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterial' => [
                'class'       => ProductMaterialAction\ViewMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterialGroup' => [
                'class'       => ProductMaterialAction\ViewMaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductMaterial2MaterialCollection' => [
                'class'       => ProductMaterialAction\CreateRelationProductMaterial2MaterialCollectionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductMaterial2MaterialGroup' => [
                'class'       => ProductMaterialAction\CreateRelationProductMaterial2MaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductMaterial2MaterialCollection' => [
                'class'       => ProductMaterialAction\DeleteRelationProductMaterial2MaterialCollectionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewMaterialCollection' => [
                'class'       => ProductMaterialAction\ViewMaterialCollectionAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationProductMaterial2PriceCategory' => [
                'class'       => ProductMaterialAction\CreateRelationProductMaterial2PriceCategoryAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductMaterial2PriceCategory' => [
                'class'       => ProductMaterialAction\DeleteRelationProductMaterial2PriceCategoryAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationProductMaterial2MaterialGroup' => [
                'class'       => ProductMaterialAction\DeleteRelationProductMaterial2MaterialGroupAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewPriceCategory' => [
                'class'       => ProductMaterialAction\ViewPriceCategoryAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}