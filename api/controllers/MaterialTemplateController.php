<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\materialTemplate as MaterialTemplateAction;
use commonprj\components\catalog\entities\materialTemplate\MaterialTemplate;


/**
 * Class MaterialTemplateController
 * @package api\controllers
 */
class MaterialTemplateController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = MaterialTemplate::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'viewMaterials' => [
                'class'       => MaterialTemplateAction\ViewMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}