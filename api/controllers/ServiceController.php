<?php

namespace api\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

class ServiceController extends Controller
{
    public function actionPropertyElementClasses()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return \Yii::$app->propertyRepository->getProperty2elementClass();
    }

}