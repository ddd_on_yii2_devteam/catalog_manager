<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use common\extendedStdComponents\catalog\component as ComponentAction;
use commonprj\components\catalog\entities\component\Component;


/**
 * Class ComponentController
 * @package api\controllers
 */
class ComponentController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Component::class;

    /**
     * @inheritdoc
     */
    protected function addActions(): array
    {
        return [
            'viewMaterials' => [
                'class'       => ComponentAction\ViewMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationComponent2Material' => [
                'class'       => ComponentAction\CreateRelationComponent2MaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationComponent2Material' => [
                'class'       => ComponentAction\DeleteRelationComponent2MaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewManufacturer' => [
                'class'       => ComponentAction\ViewManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationComponent2Manufacturer' => [
                'class'       => ComponentAction\CreateRelationComponent2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationComponent2Manufacturer' => [
                'class'       => ComponentAction\DeleteRelationComponent2ManufacturerAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'viewProductMaterials' => [
                'class'       => ComponentAction\ViewProductMaterialsAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'createRelationComponent2ProductMaterial' => [
                'class'       => ComponentAction\CreateRelationComponent2ProductMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'deleteRelationComponent2ProductMaterial' => [
                'class'       => ComponentAction\DeleteRelationComponent2ProductMaterialAction::class,
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

}