<?php

namespace api\gii\generators\api;

use commonprj\components\core\entities\element\Element;
use commonprj\components\core\models\ElementClassRecord;
use Yii;
use yii\db\Query;
use yii\gii\CodeFile;

/**
 * This generator will generate one or multiple ActiveRecord classes for the specified database table.
 *
 * add 'generators' to main-local.php
 *
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => [],
        'generators' => [
            'entity' => ['class' => api\gii\generators\entity\Generator::class]
        ],
    ];
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{

    public $ns;
    public $controllerNs;
    public $db = 'db';
    public $baseClass = Element::class;
    public $entityContext;
    public $entityName;
    public $generateAll;
    public $class;
    public $action;

    private $allowedContexts = [
        'catalog',
        'crm',
        'tcm',
    ];

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Api Generator';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator generates an Api';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['entityContext', 'string'],
            ['entityName', 'string'],
            ['generateAll', 'boolean'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];

        if ($this->generateAll) {
            $elementClasses = ElementClassRecord::find()->all();

            foreach (($elementClasses) as $elementClass) {
                list($context, $entity) = @explode('\\', $elementClass->name);
                if ($context && $entity && in_array($context, $this->allowedContexts)) {
                    $this->entityContext = $context;
                    $this->entityName = $entity;
                    $files = array_merge($files, $this->renderEntity());
                }
            }

        } else {
            if ($this->entityContext && $this->entityName) {
                $files = $this->renderEntity();
            }
        }

        return $files;
    }

    private function renderEntity()
    {
        $files = [];
        $this->ns = sprintf('%s\components\%s\entities\%s',
            Yii::getAlias('@commonprj'),
            lcfirst($this->entityContext),
            lcfirst($this->entityName)
        );

        $this->class = sprintf('commonprj\components\%s\entities\%s\%s',
            lcfirst($this->entityContext),
            lcfirst($this->entityName),
            ucfirst($this->entityName)
        );

        $this->controllerNs = 'api\controllers';

        $controllerFile = sprintf('%s/controllers/%sController.php',
            Yii::getAlias('@api'),
            ucfirst($this->entityName)
        );

        $files[] = new CodeFile($controllerFile, $this->render('controller.php'));

        foreach ($this->getClassMethods($this->class) as $this->action) {
            $actionFile = sprintf('%s/extendedStdComponents/%s/%s/%s.php',
                Yii::getAlias('@common'),
                lcfirst($this->entityContext),
                lcfirst($this->entityName),
                ucfirst($this->action).'Action'
            );
            $files[] = new CodeFile($actionFile, $this->render('action.php'));
        }


        return $files;
    }

    public function getClassMethods($class)
    {
        $className = (new \ReflectionClass($class))->getShortName();
        $array1 = get_class_methods($class);
        if ($parent_class = get_parent_class($class)) {
            $array2 = get_class_methods($parent_class);
            $array3 = array_diff($array1, $array2);
        } else {
            $array3 = $array1;
        }
        $patterns = [
            '`^((?!get|bind|unbind).)*$`',
            '`^get(.*?)`',
            '`^bind(.*?)`',
            '`^unbind(.*?)`',

        ];
        $replacements = [
            "",
            "view$1",
            "createRelation{$className}2$1",
            "deleteRelation{$className}2$1",

        ];
        $methods = preg_replace($patterns, $replacements, $array3);
        return array_filter($methods);
    }

    public function getTypeOfClassMethod($method)
    {
        $patterns = [
            '`^view(.*?)`',
            '`^createRelation(.*?)2(.*?)`',
            '`^deleteRelation(.*?)2(.*?)`',
        ];
        $replacements = [
            "get$1",
            "bind$2",
            "unbind$2",
        ];

        return preg_replace($patterns, $replacements, $method);
    }

}
